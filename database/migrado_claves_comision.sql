use seguridadhigiene;
set sql_safe_updates = 0;
set foreign_key_checks = 0;

-- banco de claves de comision y sus centros de trabajo
drop temporary table if exists tmp_registro_cct;
create temporary table tmp_registro_cct
SELECT a.id_registro, e.id as centro_trabajo, a.cct, a.id_turno, e.nombre as nombrect, e.d_municipio as municipio, e.d_localidad as localidad, 
e.nivel, t.d_turno as turno, a.clave_comision
FROM sicse.registro_cct a
inner join rh.rh_cat_escuelas_lite e on e.cct = a.cct and e.id_turno = a.id_turno
left join rh.rh_cat_turnos t on t.id_turno = e.id_turno
where a.cct <> '' and a.id_turno <> 0 and clave_comision <>'' and length(clave_comision) = 16
group by a.clave_comision, a.cct, a.id_turno;

-- depurado de claves de comision
update tmp_registro_cct set clave_comision = replace(clave_comision, ' ','');
update tmp_registro_cct set clave_comision = replace(clave_comision, '-','');
delete from tmp_registro_cct where length(clave_comision) <> 16;

-- select count(*) from tmp_registro_cct;
-- select max(periodo) from sicse.registro_cct_responsables;
-- select max(trimestre) from sicse.registro_cct_responsables;

-- no de oficios y fechas oficiales y sindicales
drop temporary table if exists tmp_registro_cct_responsables;
create temporary table tmp_registro_cct_responsables
select a.id_registro, no_oficio_sindicato, fecha_oficio_sindicato, no_oficio_oficial, fecha_oficio_oficial 
from sicse.registro_cct_responsables a
inner join tmp_registro_cct t on t.id_registro = a.id_registro
where periodo = 2015 and trimestre = 4
group by t.clave_comision;

drop temporary table if exists tmp_t_comisiones; 
create temporary table tmp_t_comisiones (`id` int(10) unsigned AUTO_INCREMENT, PRIMARY KEY (`id`))
select a.clave_comision, 
no_oficio_sindicato, fecha_oficio_sindicato, no_oficio_oficial, fecha_oficio_oficial,
1 as sindicato, 1 as user, now() as created_at, null as updated_at 
from tmp_registro_cct a
left join tmp_registro_cct_responsables b on a.id_registro = b.id_registro
group by a.clave_comision;

## Inserciones hacia seguridadhigiene
truncate t_comisiones;
truncate t_comision_centros_de_trabajo;

insert into t_comisiones
select * from tmp_t_comisiones;

insert into t_comision_centros_de_trabajo
select '' as id, a.centro_trabajo, 
-- cct, nombrect, municipio, localidad, turno, nivel,
tt.id as comision_id, now() as created_at, null as updated_at 
from tmp_t_comisiones tt
inner join tmp_registro_cct a on a.clave_comision = tt.clave_comision
group by a.centro_trabajo;


-- 11/11/2015
-- Ligar usuarios con t_comision
  update seguridadhigiene.t_comisiones c
  inner join seguridadhigiene.t_comision_centros_de_trabajo ce on ce.comision_id = c.id
  inner join seguridadhigiene.users u on u.centro_trabajo_id = ce.centro_de_trabajo_id
  set c.user_id = u.id
  where role = 'usuario';



