<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalTrabajadoresToTComisioneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_comisiones', function (Blueprint $table) {
			$table->integer('total_trabajadores')->unsigned()->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_comisiones', function (Blueprint $table) {
            $table->dropColumn('total_trabajadores');
        });
    }
}
