<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTComisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_comisiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clave_comision', 25);
			$table->string('num_oficio_sindical', 140);
			$table->date('fecha_oficio_sindical');
			$table->string('num_oficio_oficial', 140)->nullable();
			$table->date('fecha_oficio_oficial')->nullable();
            $table->integer('sindicato_id')->unsigned();
            $table->foreign('sindicato_id')->references('id')->on('cat_sindicatos');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_comisiones');
    }
}
