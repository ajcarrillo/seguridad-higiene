<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCalendarioActividadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_calendario_actividades', function (Blueprint $table) {
            $table->increments('id');
			$table->string('descripcion', 1024);
			$table->string('unidad_de_medida', 140);
			$table->enum('trimestre', [1,2,3,4]);
			$table->integer('calendario_id')->unsigned();
			$table->foreign('calendario_id')->references('id')->on('t_calendarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_calendario_actividades');
    }
}
