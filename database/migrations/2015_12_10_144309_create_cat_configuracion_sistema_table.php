<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatConfiguracionSistemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cat_configuracion_sistema', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('valor');
            $table->boolean('es_obligatorio')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cat_configuracion_sistema');
    }
}
