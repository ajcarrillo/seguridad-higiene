<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTActualizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_actualizaciones', function (Blueprint $table) {
            $table->increments('id');
			$table->string('num_oficio_sindical', 140);
			$table->date('fecha_oficio_sindical');
			$table->string('num_oficio_oficial', 140)->nullable();
			$table->date('fecha_oficio_oficial')->nullable();
			$table->integer('reporte_id')->unsigned();
			$table->foreign('reporte_id')->references('id')->on('t_reportes');
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_actualizaciones');
    }
}
