<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTReportesTipoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_reportes_tipo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reporte_id')->unsigned();
			$table->foreign('reporte_id')->references('id')->on('t_reportes');
			$table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('cat_reportes_tipo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_reportes_tipo');
    }
}
