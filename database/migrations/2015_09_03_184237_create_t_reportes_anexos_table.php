<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTReportesAnexosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_reportes_anexos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_oficio', 50);
            $table->date('fecha');
            $table->integer('reporte_id')->unsigned();
            $table->foreign('reporte_id')->references('id')->on('t_reportes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_reportes_anexos');
    }
}
