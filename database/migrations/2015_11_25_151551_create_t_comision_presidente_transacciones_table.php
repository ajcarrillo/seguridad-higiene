<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTComisionPresidenteTransaccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_comision_presidente_transacciones', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('row_id')->unsigned();
			$table->string('nombre', 140);
			$table->string('primer_apellido', 140);
			$table->string('segundo_apellido', 140);
			$table->string('cargo', 140);
			$table->integer('comision_id')->unsigned();
			$table->enum('actions', ['Insert', 'Update', 'Delete']);
			$table->boolean('isApplied')->default(false);
			$table->integer('actualizacion_id')->unsigned();
			$table->foreign('actualizacion_id')->references('id')->on('t_actualizaciones');
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_comision_presidente_transacciones');
    }
}
