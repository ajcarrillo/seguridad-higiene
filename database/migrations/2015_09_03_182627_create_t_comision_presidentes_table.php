<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTComisionPresidentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_comision_presidentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 140);
            $table->string('primer_apellido', 140);
            $table->string('segundo_apellido', 140);
            $table->string('cargo', 140);
            $table->integer('comision_id')->unsigned();
            $table->foreign('comision_id')->references('id')->on('t_comisiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_comision_presidentes');
    }
}
