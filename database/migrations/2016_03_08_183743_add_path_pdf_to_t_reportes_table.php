<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPathPdfToTReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_reportes', function (Blueprint $table) {
            $table->string('path_pdf', 45)->after('status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_reportes', function (Blueprint $table) {
            $table->dropColumn('path_pdf');
        });
    }
}
