<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtapaProcesoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etapa_proceso', function (Blueprint $table) {
            $table->increments('id');
			$table->string('nombre', 50);
			$table->string('descripcion', 140);
			$table->date('apertura');
			$table->date('cierre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('etapa_proceso');
    }
}
