<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTReportesObservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_reportes_observaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('observacion', 1024);
            $table->integer('reporte_id')->unsigned();
            $table->foreign('reporte_id')->references('id')->on('t_reportes');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_reportes_observaciones');
    }
}
