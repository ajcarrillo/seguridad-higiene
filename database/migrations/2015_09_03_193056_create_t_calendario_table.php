<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCalendarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_calendarios', function (Blueprint $table) {
            $table->increments('id');
			$table->date('de');
			$table->date('a');
            $table->integer('reporte_id')->unsigned();
            $table->foreign('reporte_id')->references('id')->on('t_reportes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_calendarios');
    }
}
