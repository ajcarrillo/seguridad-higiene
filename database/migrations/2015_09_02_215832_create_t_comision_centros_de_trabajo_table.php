<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTComisionCentrosDeTrabajoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_comision_centros_de_trabajo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('centro_de_trabajo_id');
            $table->integer('comision_id')->unsigned();
            $table->foreign('comision_id')->references('id')->on('t_comisiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_comision_centros_de_trabajo');
    }
}
