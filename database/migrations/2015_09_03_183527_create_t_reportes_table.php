<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	// TODO: Corregir la fecha del reporte, quitar default value
    public function up()
    {
        Schema::create('t_reportes', function (Blueprint $table) {
            $table->increments('id');
			$table->date('fecha')->default(date("Y-m-d H:i:s"));
			$table->enum('trimestre', ['1', '2', '3', '4']);
			$table->integer('anio')->default(date("Y"));
			$table->integer('comision_id')->unsigned();
			$table->foreign('comision_id')->references('id')->on('t_comisiones');
            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('cat_reporte_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_reportes');
    }
}
