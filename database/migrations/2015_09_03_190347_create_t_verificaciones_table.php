<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTVerificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_verificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->enum('tipo', ['1', '2']);
            $table->integer('numero_de_riesgos_de_trabajo')->nullable();
            $table->integer('accidentes_de_trabajo')->nullable();
            $table->integer('enfermedad_profesional')->nullable();
            $table->string('observacion', 1024)->nullable();
            $table->string('propuesta_csst', 1024)->nullable();
            $table->integer('reporte_id')->unsigned();
            $table->foreign('reporte_id')->references('id')->on('t_reportes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_verificaciones');
    }
}
