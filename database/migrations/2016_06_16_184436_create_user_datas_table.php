<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_datas', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name');
	        $table->integer('centro_trabajo_id')->unsigned();

	        $table->integer('user_id')->unsigned();
	        $table->foreign('user_id')->references('id')->on('users');

	        $table->unique(['id', 'user_id']);
	        
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_datas');
    }
}
