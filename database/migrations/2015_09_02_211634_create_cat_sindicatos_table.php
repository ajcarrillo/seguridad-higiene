<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatSindicatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_sindicatos', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre', 140);
			$table->string('direccion', 255);
			$table->string('ciudad', 140);
			$table->string('colonia', 140);
			$table->integer('codigo_postal');
			$table->string('telefono', 10)->nullable();
			$table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cat_sindicatos');
    }
}
