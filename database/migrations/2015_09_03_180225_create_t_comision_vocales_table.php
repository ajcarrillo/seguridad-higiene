<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTComisionVocalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     * Sector: 1=Oficial, 2=Sindical
     * Tipo: 1=Propietario, 2=Suplente
     *
     */
    public function up()
    {
        Schema::create('t_comision_vocales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 140);
            $table->string('primer_apellido', 140);
            $table->string('segundo_apellido', 140);
            $table->string('cargo', 140);
            $table->enum('sector', ['1', '2']);
            $table->enum('tipo', ['1', '2']);
			$table->string('rfc', 13);
            $table->integer('comision_id')->unsigned();
            $table->foreign('comision_id')->references('id')->on('t_comisiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_comision_vocales');
    }
}
