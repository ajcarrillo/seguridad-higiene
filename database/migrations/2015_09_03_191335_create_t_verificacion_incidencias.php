<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTVerificacionIncidencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_verificacion_incidencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('centro_de_trabajo_id');
            $table->enum('status', ['1', '2', '3']);
			$table->integer('trimestre_subsanada')->default(0);
			$table->integer('anio_subsanada')->default(0);
            $table->integer('incidencia_id')->unsigned();
            $table->foreign('incidencia_id')->references('id')->on('cat_incidencias');
            $table->integer('verificacion_id')->unsigned();
            $table->foreign('verificacion_id')->references('id')->on('t_verificaciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_verificacion_incidencias');
    }
}
