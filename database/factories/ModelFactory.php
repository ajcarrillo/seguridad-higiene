<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(SeguridadHigiene\Models\User::class, function (Faker\Generator $faker) {
	return [
		'email'          => $faker->unique()->email,
		'username'       => $faker->unique()->userName,
		'password'       => bcrypt("admin"),
		'remember_token' => str_random(10),
		'role'           => 'operador',
	];
});

$factory->define(SeguridadHigiene\Models\UserData::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->name,
		'centro_trabajo_id' => 0,
	];
});

$factory->define(SeguridadHigiene\Models\Sindicato::class, function (Faker\Generator $faker) {
	return [
		'nombre'        => 'SINDICATO NACIONAL DE TRABAJADORES DE LA EDUCACIÓN (SNTE)',
		'direccion'     => 'AV. INSURGENTES',
		'ciudad'        => 'CHETUMAL, QUINTANA ROO',
		'colonia'       => 'CENTRO',
		'codigo_postal' => '77000',
		'telefono'      => '9833948473',
	];
});

$factory->define(SeguridadHigiene\Models\CatReporteTipo::class, function (Faker\Generator $faker) {
	return [
		'nombre' => $faker->sentence(),
	];
});

$factory->define(SeguridadHigiene\Models\CatIncidencia::class, function (Faker\Generator $faker) {
	return [
		'nombre' => $faker->sentence(),
	];
});

$factory->define(SeguridadHigiene\Models\CatIncidenciaStatus::class, function (Faker\Generator $faker) {
	return [
		'nombre' => $faker->sentence(),
	];
});

$factory->define(SeguridadHigiene\Models\CatReporteStatus::class, function (Faker\Generator $faker) {
	return [
		'nombre' => $faker->sentence(),
	];
});
$factory->define(SeguridadHigiene\Models\CatConfiguracionSistema::class, function (Faker\Generator $faker) {
	return [
		'descripcion' => $faker->sentence(),
	];
});
