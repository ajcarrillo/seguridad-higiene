<?php

use Illuminate\Database\Seeder;

class CatReportesTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SeguridadHigiene\Models\CatReporteTipo::class)->create([
			'nombre' => "Registro"
		]);
		factory(SeguridadHigiene\Models\CatReporteTipo::class)->create([
			'nombre' => "Actualización"
		]);
		factory(SeguridadHigiene\Models\CatReporteTipo::class)->create([
			'nombre' => "Calendario"
		]);
		factory(SeguridadHigiene\Models\CatReporteTipo::class)->create([
			'nombre' => "Verificación"
		]);
    }
}
