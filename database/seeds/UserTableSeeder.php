<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(\SeguridadHigiene\Models\User::class, 2)->create()->each(function($ru){
			$ru->data()->save(factory(\SeguridadHigiene\Models\UserData::class)->make());
		});
		/*factory(\SeguridadHigiene\Models\User::class, 1)->create([
			'username'          => 'admin',
			'email'             => 'alfa.sistemas.seq@gmail.com',
			'password'          => bcrypt('admin'),
			'role'              => "operador",
		])->each(function ($u) {
			$u->data()->save(factory(\SeguridadHigiene\Models\UserData::class)->make([
				'name' => 'Andres Carrillo',
				'centro_trabajo_id' => 0,
			]));
		});*/


		/*factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Saul',
			'primer_apellido' => 'Hernández',
			'segundo_apellido' => 'Dominguez',
			'username' => 'hernandez',
			'centro_trabajo_id' => 3975,
			'email' => 's_hernandez@fakeemail.com',
			'password' => bcrypt('hernandez'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Victor Josué',
			'primer_apellido' => 'López',
			'segundo_apellido' => 'Cetina',
			'username' => 'lopez',
			'centro_trabajo_id' => 3975,
			'email' => 'v_lopez@fakeemail.com',
			'password' => bcrypt('lopez'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Roger Alberto',
			'primer_apellido' => 'Villanueva',
			'segundo_apellido' => 'Ortíz',
			'username' => 'villanueva',
			'centro_trabajo_id' => 3975,
			'email' => 'roger070894@gmail.com',
			'password' => bcrypt('villanueva'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Julia Evelyn',
			'primer_apellido' => 'Silveira',
			'segundo_apellido' => 'Lara',
			'username' => 'jsilveira',
			'centro_trabajo_id' => 3975,
			'email' => 'aliju3381@hotmail.com',
			'password' => bcrypt('jsilveira'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Vanessa',
			'primer_apellido' => 'Sánchez',
			'segundo_apellido' => 'Fabro',
			'username' => 'vsanchez',
			'centro_trabajo_id' => 3975,
			'email' => 'vsanchezfabro@hotmail.com',
			'password' => bcrypt('vsanchez'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Jorge Alberto',
			'primer_apellido' => 'Eb',
			'segundo_apellido' => 'Espens',
			'username' => 'ebespensj',
			'centro_trabajo_id' => 3975,
			'email' => 'jorge.espens02@gmail.com',
			'password' => bcrypt('ebespensj'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Elsy Guadalupe',
			'primer_apellido' => 'Ávila',
			'segundo_apellido' => 'Pech',
			'username' => 'eavila',
			'centro_trabajo_id' => 3975,
			'email' => 'elsygpe69@hotmail.com',
			'password' => bcrypt('eavila'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Liliana Astrid',
			'primer_apellido' => 'Crespo',
			'segundo_apellido' => 'Jiménez',
			'username' => 'liliana',
			'centro_trabajo_id' => 3975,
			'email' => 'dalycrespo@hotmail.com',
			'password' => bcrypt('liliana'),
			'role' => "operador"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'José Emilio',
			'primer_apellido' => 'Vargas',
			'segundo_apellido' => 'Padilla',
			'username' => 'joseemilio',
			'centro_trabajo_id' => 3975,
			'email' => 'vape721022@hotmail.com',
			'password' => bcrypt('joseemilio'),
			'role' => "operador"
		]);

		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Nora Hugett',
			'primer_apellido' => 'Retana Villanueva',
			'segundo_apellido' => 'Pech',
			'username' => 'nora',
			'centro_trabajo_id' => 3975,
			'email' => 'n_retana@fakeemail.com',
			'password' => bcrypt('nora'),
			'role' => "operador"
		]);*/
		// Sin plantilla
		/*factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'persona1',
			'primer_apellido' => 'apellido1',
			'segundo_apellido' => 'apellido1',
			'username' => 'itch',
			'centro_trabajo_id' => 3040,
			'email' => 'itch@fakeemail.com',
			'password' => bcrypt('itch'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'persona2',
			'primer_apellido' => 'apellido2',
			'segundo_apellido' => 'apellido2',
			'username' => 'uqroo',
			'centro_trabajo_id' => 3044,
			'email' => 'uqroo@fakeemail.com',
			'password' => bcrypt('uqroo'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'persona3',
			'primer_apellido' => 'apellido3',
			'segundo_apellido' => 'apellido3',
			'username' => 'cobach',
			'centro_trabajo_id' => 214,
			'email' => 'cobach@fakeemail.com',
			'password' => bcrypt('cobach'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'persona4',
			'primer_apellido' => 'apellido4',
			'segundo_apellido' => 'apellido4',
			'username' => 'conalep',
			'centro_trabajo_id' => 1876,
			'email' => 'conalep@fakeemail.com',
			'password' => bcrypt('conalep'),
			'role' => "usuario"
		]);*/
		//unidades admvas
		/*factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Victor Alejandro',
			'primer_apellido' => 'Yah',
			'segundo_apellido' => 'Gonzalez',
			'username' => 'sistemas',
			'centro_trabajo_id' => 4012,
			'email' => 'victor.seq.sistemas@gmail.com',
			'password' => bcrypt('sistemas'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Liliana Astrid',
			'primer_apellido' => 'Crespo',
			'segundo_apellido' => 'Jiménez',
			'username' => 'registro',
			'centro_trabajo_id' => 3975,
			'email' => 'dalycrespo1@hotmail.com',
			'password' => bcrypt('registro'),
			'role' => "usuario"
		]);
		//Con plantilla, rescatados de la BD antigua
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Socorro del Rosario',
			'primer_apellido' => 'Trujeque',
			'segundo_apellido' => 'Xix',
			'username' => 'strujeque',
			'centro_trabajo_id' => 2480,
			'email' => 'socotrujeque@hotmail.com',
			'password' => bcrypt('strujeque'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Yadira Georgina',
			'primer_apellido' => 'Gonzalez',
			'segundo_apellido' => 'Pelayo',
			'username' => 'ygonzalez',
			'centro_trabajo_id' => 2494,
			'email' => 'geobaby66@hotmail.com',
			'password' => bcrypt('ygonzalez'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Suemy',
			'primer_apellido' => 'Orozco',
			'segundo_apellido' => 'Barcenas',
			'username' => 'sorozco',
			'centro_trabajo_id' => 2495,
			'email' => 'soboro31@hotmail.com',
			'password' => bcrypt('sorozco'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Gaspar Alberto',
			'primer_apellido' => 'Ceballos',
			'segundo_apellido' => 'Arce',
			'username' => 'gceballos',
			'centro_trabajo_id' => 2421,
			'email' => 'alberto--arce@hotmail.com',
			'password' => bcrypt('gceballos'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Manuel Salvador',
			'primer_apellido' => 'Pérez',
			'segundo_apellido' => 'Manrique',
			'username' => 'mperez',
			'centro_trabajo_id' => 553,
			'email' => 'manoloperez17@live.com',
			'password' => bcrypt('mperez'),
			'role' => "usuario"
		]);
		factory(SeguridadHigiene\Models\User::class)->create([
			'name'  => 'Deneb Patricia',
			'primer_apellido' => 'López',
			'segundo_apellido' => 'Cabrera',
			'username' => 'dlopez',
			'centro_trabajo_id' => 1775,
			'email' => 'denebpatita@hotmail.com',
			'password' => bcrypt('dlopez'),
			'role' => "usuario"
		]);*/
		//factory(SeguridadHigiene\Models\User::class, 9)->create();
	}
}
