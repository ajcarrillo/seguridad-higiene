<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->truncateTables(array(
            'users',
            'cat_sindicatos',
			'cat_reportes_tipo',
			'cat_incidencias',
			'cat_incidencia_status',
			'cat_reporte_status',
			't_calendarios',
			't_calendario_actividades',
			'etapa_proceso'
        ));


        $this->call(Cat_SindicatoTableSeeder::class);
        $this->call(CatReportesTipoSeeder::class);
        $this->call(CatIncidenciaSeeder::class);
        $this->call(CatIncidenciaStatusSeeder::class);
        $this->call(CatReporteStatus::class);
        $this->call(CatConfiguracionSistemaTableSeeder::class);
		$this->call(EtapaProcesoTableSeeder::class);

        Model::reguard();
    }

    private function truncateTables(array $tables)
    {
        $this->checkForeignKeys(false);

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        $this->checkForeignKeys(true);
    }

    private function checkForeignKeys($check)
    {
        $check = $check ? '1':'0';
        DB::statement('SET FOREIGN_KEY_CHECKS = '.$check);
    }
}
