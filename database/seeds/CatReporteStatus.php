<?php

use Illuminate\Database\Seeder;

class CatReporteStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(SeguridadHigiene\Models\CatReporteStatus::class)->create([
			'nombre' => "Aceptada"
		]);
		factory(SeguridadHigiene\Models\CatReporteStatus::class)->create([
			'nombre' => "Rechazada"
		]);
		factory(SeguridadHigiene\Models\CatReporteStatus::class)->create([
			'nombre' => "Enviada"
		]);
		factory(SeguridadHigiene\Models\CatReporteStatus::class)->create([
			'nombre' => "Pendiente"
		]);
		factory(SeguridadHigiene\Models\CatReporteStatus::class)->create([
			'nombre' => "Aprobado"
		]);
		factory(SeguridadHigiene\Models\CatReporteStatus::class)->create([
			'nombre' => "En Revision"
		]);
    }
}
