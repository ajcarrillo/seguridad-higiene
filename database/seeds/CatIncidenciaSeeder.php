<?php

use Illuminate\Database\Seeder;

class CatIncidenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// factory(SeguridadHigiene\Models\CatIncidencia::class, 29)->create();
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'INSTALACIÓN DE GAS EN MALAS CONDICIONES',
            'clave' => '01',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'ALMACENAMIENTO DE SUSTANCIAS O MATERIALES PELIGROSOS EN ÁREAS DE TRABAJO',
            'clave' => '02',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'EQUIPO CONTRA INCENDIO INEXISTENTE',
            'clave' => '03',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'EQUIPO CONTRA INCENDIO INAPROPIADO O CARENTE DE MANTENIMIENTO',
            'clave' => '04',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'PISOS EN MAL ESTADO',
            'clave' => '05',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'ESCALERAS SIN PASAMANOS O CON ESCALONES DETERIORADOS',
            'clave' => '06',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'ESCALERAS SON PROTECCIÓN ANTIDERRAPANTE',
            'clave' => '07',
        ]);
        factory(SeguridadHigiene\Models\CatIncidencia::class)->create([
            'nombre'  => 'FALTA DE EQUIPO DE PROTECCIÓN PERSONAL',
            'clave' => '08',
        ]);
    }
}
