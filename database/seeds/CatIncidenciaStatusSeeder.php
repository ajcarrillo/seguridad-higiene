<?php

use Illuminate\Database\Seeder;

class CatIncidenciaStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(SeguridadHigiene\Models\CatIncidenciaStatus::class)->create([
			'nombre' => "Detectadas"
		]);
		factory(SeguridadHigiene\Models\CatIncidenciaStatus::class)->create([
			'nombre' => "Subsistentes"
		]);
		factory(SeguridadHigiene\Models\CatIncidenciaStatus::class)->create([
			'nombre' => "Subsanadas"
		]);
    }
}
