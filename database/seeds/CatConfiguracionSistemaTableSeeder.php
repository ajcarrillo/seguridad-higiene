<?php

use Illuminate\Database\Seeder;

class CatConfiguracionSistemaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'NOMBRE DEL RESPONSABLE:',
        ]);
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'CARGO:',
        ]);
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'NOMBRE DEL DEPARTAMENTO:',
        ]);
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'NUMERO DE TELEFONO:',
        ]);
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'EXTENSION:',
        ]);
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'CORREO ELECTRONICO:',
        ]);
        factory(SeguridadHigiene\Models\CatConfiguracionSistema::class)->create([
            'descripcion'  => 'MENSAJE INICIAL:',
        ]);

    }
}
