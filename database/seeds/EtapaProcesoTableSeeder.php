<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EtapaProcesoTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('etapa_proceso')->insert([
			'nombre'      => "ALTA",
			'descripcion' => "Periodo para el registro del acta de verificación",
			'apertura'    => Carbon::createFromDate(2016, 01, 01),
			'cierre'      => Carbon::createFromDate(2016, 01, 31),
		]);

		DB::table('etapa_proceso')->insert([
			'nombre'      => "REVISION",
			'descripcion' => "Periodo para la revisión de las actas enviadas",
			'apertura'    => Carbon::createFromDate(2016, 02, 01),
			'cierre'      => Carbon::createFromDate(2016, 02, 28)
		]);

		/*factory(SeguridadHigiene\Models\EtapaProceso::class)->create([
            'nombre'=>"Alta",
            'descripcion'=>"Periodo para el registro del acta de verificación",
            'apertura'=>Carbon::createFromDate(2016,01,01),
            'cierre'=>Carbon::createFromDate(2016,31,01),
        ]);
        factory(SeguridadHigiene\Models\EtapaProceso::class)->create([
            'nombre'=>"Revisión",
            'descripcion'=>"Periodo para la revisión de las actas enviadas",
            'apertura'=>Carbon::createFromDate(2016,02,01),
            'cierre'=>Carbon::createFromDate(2016,02,28)
        ]);*/
	}
}
