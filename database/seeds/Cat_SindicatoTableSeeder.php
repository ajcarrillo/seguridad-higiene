<?php

use Illuminate\Database\Seeder;

class Cat_SindicatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(SeguridadHigiene\Models\Sindicato::class)->create([
			'nombre'         =>  'SINDICATO NACIONAL DE TRABAJADORES DE LA EDUCACIÓN (SNTE)',
			'direccion'      =>  'AV. INSURGENTES',
			'ciudad'         =>  'CHETUMAL, QUINTANA ROO',
			'colonia'        =>  'CENTRO',
			'codigo_postal'  =>  '77000',
			'telefono'       =>  '9833948473'
		]);
    }
}
