<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use SeguridadHigiene\Models\User;

class UserTest extends TestCase
{
	use DatabaseMigrations;

	public function test_check_user_has_role()
	{
		$role = "operador";
		$user = factory(User::class)->create([
			'role' => 'operador',
		]);
		$hasRole = $user->hasRole($role);

		$this->assertEquals(true, $hasRole);
	}
}
