<?php

namespace SeguridadHigiene\Providers;

use Illuminate\Support\ServiceProvider;
use SeguridadHigiene\Http\ViewComposers\CatRoles;
use SeguridadHigiene\Http\ViewComposers\CatSectores;
use SeguridadHigiene\Http\ViewComposers\CatSindicatos;
use SeguridadHigiene\Http\ViewComposers\CatVerificacionTipo;
use SeguridadHigiene\Http\ViewComposers\CatVocalTipo;
use SeguridadHigiene\Http\ViewComposers\ComisionCentros;
use SeguridadHigiene\Http\ViewComposers\ReturnUrl;
use SeguridadHigiene\Http\ViewComposers\Comision;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->app->make('view')->composer(
			['plantillas/modal_search_plantilla',
				'actualizaciones/vocalesTransacciones/create'],
			ComisionCentros::class
		);

		$this->app->make('view')->composer(
			['comisiones/secretarios/partials/form',
				'actualizaciones/secretarioTransacciones/partials/form',
				'actualizaciones/vocalesTransacciones/create_transaccion',
				'comisiones/vocales/create'],
			CatSectores::class
		);

		$this->app->make('view')->composer(
			['comisiones/create',
				'comisiones/oficios'],
			CatSindicatos::class
		);

		$this->app->make('view')->composer(
			['verificaciones/create'],
			CatVerificacionTipo::class
		);

		$this->app->make('view')->composer(
			['actualizaciones/vocalesTransacciones/create',
				'comisiones/centros/create',
				'comisiones/vocales/create_by_plantilla'
			],
			ReturnUrl::class
		);

		$this->app->make('view')->composer(
			['actualizaciones/vocalesTransacciones/create_transaccion',
				'comisiones/vocales/create'],
			CatVocalTipo::class
		);

		$this->app->make('view')->composer(
			['comisiones/centros/index',
				'comisiones/centros/create',
				'comisiones/total_trabajadores',
				'comisiones/presidentes/index',
				'comisiones/presidentes/create',
				'comisiones/presidentes/edit',
				'comisiones/secretarios/index',
				'comisiones/secretarios/create',
				'comisiones/secretarios/edit',
				'comisiones/vocales/index',
				'comisiones/vocales/create_by_plantilla',
				'comisiones/vocales/create',
				'comisiones/oficios',
				'comisiones/enviar'
			],
			Comision::class
		);

		$this->app->make('view')->composer(
			[
				'users/edit',
				'users/create',
				'users/change_rol',
			],
			CatRoles::class
		);

	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
