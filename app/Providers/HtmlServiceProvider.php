<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 11:01
 */

namespace SeguridadHigiene\Providers;
use Collective\Html\HtmlServiceProvider as CollectiveHtmlServiceProvider;
use SeguridadHigiene\Components\HtmlBuilder;

class HtmlServiceProvider extends CollectiveHtmlServiceProvider
{
	protected function registerHtmlBuilder()
	{
		$this->app->bindShared('html', function($app){
			return new HtmlBuilder($app['config'], $app['view'], $app['url']);
		});
	}
}
