<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ComisionCentro extends Model
{
	protected $fillable = ['centro_de_trabajo_id'];
    protected $table = "t_comision_centros_de_trabajo";

	// <editor-fold desc="Relationships">
	public function comision()
	{
		return $this->belongsTo(Comision::class, 'comision_id');
	}

	public function escuela_lite()
	{
		return $this->belongsTo(CentroLite::class, 'centro_de_trabajo_id');
	}
	// </editor-fold>

	public function hasComision($comision_id)
	{
		return $this->comision()->where('comision_id', $comision_id)->count();
	}

}
