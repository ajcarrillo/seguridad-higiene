<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class Verificacion extends Model
{
	protected $fillable = ['fecha', 'tipo', 'numero_de_riesgos_de_trabajo', 'accidentes_de_trabajo', 'enfermedad_profesional', 'observacion', 'propuesta_csst'];
    protected $table = "t_verificaciones";

	public function reporte()
	{
		return $this->belongsTo(Reporte::class);
	}

	public function incidencias()
	{
		return $this->hasMany(VerificacionIncidencia::class);
	}

	public function hasIncidencia($incidencia_id, $centro_de_trabajo_id)
	{
		return $this->incidencias()
			->where('centro_de_trabajo_id', $centro_de_trabajo_id)
			->where('incidencia_id', $incidencia_id)
			->whereIn('status',[statusIncidencia('Primera vez'),statusIncidencia('Subsistentes')])
			->count();
	}

}
