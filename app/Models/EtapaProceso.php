<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class EtapaProceso extends Model
{
    protected $table = "etapa_proceso";
	protected $fillable = ['nombre', 'descripcion', 'apertura', 'cierre'];




}
