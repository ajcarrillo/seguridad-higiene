<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class RHCatStatus extends Model
{
	protected $connection = 'rh';
	protected $table = 'rh_cat_status';
}
