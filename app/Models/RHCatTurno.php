<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class RHCatTurno extends Model
{
	protected $connection = 'rh';
	protected $table = 'rh_cat_turnos';
}
