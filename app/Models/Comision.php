<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 12:07
 */

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;


class Comision extends Model {

	protected $fillable = ['clave_comision', 'sindicato_id', 'num_oficio_sindical', 'fecha_oficio_sindical', 'num_oficio_oficial', 'fecha_oficio_oficial'];
	protected $table = "t_comisiones";

	/*
	 * Relationships
	 * */

	public function centros()
	{
		return $this->hasMany(ComisionCentro::class, 'comision_id', 'id');
	}

	public function reportes()
	{
		return $this->hasMany(Reporte::class, 'comision_id', 'id');
	}

	public function presidente()
	{
		return $this->hasOne(ComisionPresidente::class, 'comision_id', 'id');
	}

	public function secretario()
	{
		return $this->hasOne(ComisionSecretario::class, 'comision_id', 'id');
	}

	public function vocales()
	{
		return $this->hasMany(ComisionVocal::class, 'comision_id', 'id');
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function cat_sindicato()
	{
		return $this->belongsTo(Sindicato::class, 'sindicato_id');
	}

	/*
	 * Other things XD
	 * */

	public function hasCentro($centroId)
	{
		return $this->centros()->where('centro_de_trabajo_id', $centroId)->count();
	}

	public function getVocalesPaginatedAttribute()
	{
		return $this->vocales()->paginate(5);
	}

	public function getCentrosPaginatedAttribute()
	{
		return $this->centros()->paginate(5);
	}

	public function hasPresidente()
	{
		return $this->presidente()->count();
	}

	public function hasSecretario()
	{
		return $this->secretario()->count();
	}

	public function hasVocales()
	{
		return $this->vocales()->count();
	}

	public function hasCentros()
	{
		return $this->centros()->count();
	}

	public function isComplete()
	{
		if($this->hasPresidente() && $this->hasSecretario()){
			return true;
		}
		return false;
	}

	public function hasCentrosAndVocales()
	{
		if($this->hasCentros() && $this->hasVocales()){
			return true;
		}
		return false;
	}

}
