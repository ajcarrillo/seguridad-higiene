<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class InmuebleGeo extends Model
{
	/**
	 * The database connection used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'geo';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cct_inmueb';
    //

	public function centros()
	{
		try{
			return InmuebleGeo::select('CLAVECCT')
				->where('CVE_INMUEB', $this->CVE_INMUEB)->get();
		}catch (QueryException $e){
			return $e;
		}

	}
}
