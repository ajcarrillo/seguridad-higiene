<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
    protected $fillable = ['de', 'a'];
	protected $table = 't_calendarios';

	public function reporte()
	{
		return $this->belongsTo(Reporte::class);
	}

	public function actividades()
	{
		return $this->hasMany(CalendarioActividad::class);
	}

	public function getActividadesPaginatedAttribute()
	{
		return $this->actividades()->paginate(5);
	}
}
