<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 13:07
 */

namespace SeguridadHigiene\Models;


use Illuminate\Database\Eloquent\Model;

class CatReporteStatus extends Model
{
	protected $table = "cat_reporte_status";

}
