<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ComisionVocal extends Model
{
	protected $fillable = ['nombre', 'primer_apellido', 'segundo_apellido', 'cargo', 'sector', 'tipo'];
	protected $table = 't_comision_vocales';
	protected $appends = ['nombreCompleto', 'nombreSector', 'nombreTipo'];

	public function comision()
	{
		return $this->belongsTo(Comision::class, 'comision_id');
    }

	public function getNombreCompletoAttribute()
	{
		return $this->primer_apellido." ".$this->segundo_apellido." ".$this->nombre;
	}

	public function getNombreSectorAttribute()
	{
		return catSectoresById($this->sector);
	}

	public function getNombreTipoAttribute()
	{
		return catVocalesById($this->tipo);
	}
}
