<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class Sindicato extends BaseModel
{
    protected $fillable = ['nombre', 'direccion', 'colonia', 'codigo_postal', 'ciudad', 'telefono'];
    protected $table = 'cat_sindicatos';
}
