<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarioActividad extends Model
{
	protected $fillable = ['descripcion', 'unidad_de_medida', 'trimestre'];
	protected $table = 't_calendario_actividades';

	public function calendario()
	{
		return $this->belongsTo(Calendario::class);
	}

}
