<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class CatConfiguracionSistema extends Model
{
    protected $table = "cat_configuracion_sistema";
    protected $fillable = ['descripcion', 'valor'];
}
