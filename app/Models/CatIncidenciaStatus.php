<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 13:06
 */

namespace SeguridadHigiene\Models;


use Illuminate\Database\Eloquent\Model;

class CatIncidenciaStatus extends Model
{
	protected $table = "cat_incidencia_status";

}
