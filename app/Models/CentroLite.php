<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class CentroLite extends Model
{
	protected $connection = 'rh';
    protected $table = 'rh_cat_escuelas_lite';

	public function turno()
	{
		return $this->belongsTo(RHCatTurno::class, 'id_turno', 'id_turno');
	}

	public function status()
	{
		return $this->belongsTo(RHCatStatus::class, 'id_status', 'id_status');
	}

	public function getAllDescription()
	{
		$nombre = $this->nombre;
		$cct = $this->cct;
		$turno = $this->turno->d_turno;
		$mun = $this->abrev_municipio;
		$nivel = $this->nivel;

		return "{$cct} {$nombre} - {$turno} {$mun} {$nivel}";

	}
}
