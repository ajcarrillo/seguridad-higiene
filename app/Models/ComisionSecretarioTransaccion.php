<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ComisionSecretarioTransaccion extends BaseModel
{
    protected $fillable = ['row_id', 'nombre', 'primer_apellido', 'segundo_apellido', 'rfc', 'sector', 'comision_id', 'actions'];
	protected $table = "t_comision_secretario_transacciones";

	// <editor-fold desc="Relationships">
	public function actualizacion()
	{
		return $this->belongsTo(Actualizacion::class);
	}
	// </editor-fold>

	public function getNombreCompletoAttribute()
	{
		return $this->primer_apellido." ".$this->segundo_apellido." ".$this->nombre;
	}


}
