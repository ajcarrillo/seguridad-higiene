<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ComisionCentroTransaccion extends BaseModel
{
    protected $fillable = ['row_id', 'centro_de_trabajo_id', 'comision_id', 'actions'];
	protected $table = "t_comision_centro_transacciones";

	// <editor-fold desc="Relationships">
	public function actualizacion()
	{
		return $this->belongsTo(Actualizacion::class);
	}

	public function escuela_lite()
	{
		return $this->belongsTo(CentroLite::class, 'centro_de_trabajo_id');
	}
	// </editor-fold>

}
