<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 12:39
 */

namespace SeguridadHigiene\Models;


use Illuminate\Database\Eloquent\Model;

class CatReporteTipo extends Model
{
	protected $fillable = ['nombre'];
	protected $table = "cat_reportes_tipo";

}
