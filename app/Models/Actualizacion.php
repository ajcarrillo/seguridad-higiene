<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class Actualizacion extends BaseModel
{
	protected $fillable = ['num_oficio_sindical', 'fecha_oficio_sindical', 'num_oficio_oficial', 'fecha_oficio_oficial'];
	protected $table = "t_actualizaciones";

	// <editor-fold desc="Relationships">
	public function reporte()
	{
		return $this->belongsTo(Reporte::class, 'actualizacion_id', 'id');
	}

	public function centroTransacciones()
	{
		return $this->hasMany(ComisionCentroTransaccion::class, 'actualizacion_id', 'id');
	}

	public function presidenteTransacciones()
	{
		return $this->hasOne(ComisionPresidenteTransaccion::class, 'actualizacion_id', 'id');
	}

	public function secretarioTransacciones()
	{
		return $this->hasOne(ComisionSecretarioTransaccion::class, 'actualizacion_id', 'id');
	}

	public function vocalTransacciones()
	{
		return $this->hasMany(ComisionVocalTransaccion::class, 'actualizacion_id', 'id');
	}
	// </editor-fold>

	public function hasTransaccion($centro_id)
	{
		return $this->centroTransacciones()
			->where('centro_de_trabajo_id', $centro_id)
			->where('isApplied', false)
			->whereIn('actions', ['Insert','Delete'])
			->count();

	}

	public function hasTransaccionVocal($vocal_id)
	{
		return $this->vocalTransacciones()
			->where('row_id', $vocal_id)
			->where('isApplied', false)
			->whereIn('actions', ['Insert', 'Delete'])
			->count();
	}

}
