<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
	protected $table = 'user_datas';
	protected $fillable = ['name', 'centro_trabajo_id'];


	public function centro_trabajo()
	{
		return $this->hasOne(CentroLite::class, 'id', 'centro_trabajo_id');
	}

	public function user(){
		return $this->belongsTo(User::class);
	}
}
