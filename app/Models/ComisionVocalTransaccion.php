<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ComisionVocalTransaccion extends BaseModel
{
    protected $fillable = ['row_id', 'nombre', 'primer_apellido', 'segundo_apellido', 'cargo', 'sector', 'tipo', 'rfc', 'comision_id', 'actions'];
	protected $table = "t_comision_vocal_transacciones";
	protected $appends = ['nombreCompleto', 'nombreSector', 'nombreTipo'];

	// <editor-fold desc="Relationships">
	public function actualizacion()
	{
		return $this->belongsTo(Actualizacion::class);
	}
	// </editor-fold>


	public function getNombreCompletoAttribute()
	{
		return $this->primer_apellido." ".$this->segundo_apellido." ".$this->nombre;
	}

	public function getNombreSectorAttribute()
	{
		return catSectoresById($this->sector);
	}

	public function getNombreTipoAttribute()
	{
		return catVocalesById($this->tipo);
	}

}
