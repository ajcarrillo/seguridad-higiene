<?php

namespace SeguridadHigiene\models;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
    protected $table = 'cat_incidencias';
    protected $fillable = ['nombre', 'clave'];
}
