<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class VerificacionIncidencia extends Model
{
    protected $fillable = ['centro_de_trabajo_id', 'status', 'incidencia_id'];
	protected $table = 't_verificacion_incidencias';

	public function verificacion()
	{
		return $this->belongsTo(Verificacion::class);
	}

	public function incidencia()
	{
		return $this->belongsTo(CatIncidencia::class, 'incidencia_id');
	}

	public function centro()
	{
		return $this->belongsTo(ComisionCentro::class, 'centro_de_trabajo_id', 'centro_de_trabajo_id');
	}
}
