<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ReporteObservacion extends Model
{
    //
    protected $table = "t_reportes_observaciones";
    protected $fillable = ['observacion', 'usuario_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }
}
