<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ComisionPresidente extends Model
{
	protected $table = 't_comision_presidentes';
	protected $fillable = ['nombre', 'primer_apellido', 'segundo_apellido', 'cargo'];

	public function comision()
	{
		return $this->belongsTo(Comision::class, 'comision_id');
    }

	public function getNombreCompletoAttribute()
	{
		return $this->primer_apellido." ".$this->segundo_apellido." ".$this->nombre;
	}
}
