<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 12:09
 */

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model {
	protected $fillable = ['trimestre', 'status_id'];
	protected $table = "t_reportes";

	// return $this->belongsTo('App\User', 'foreign_key');
	public function comision()
	{
		return $this->belongsTo(Comision::class, 'comision_id');
	}

	public function tipos()
	{
		return $this->hasMany(ReporteTipo::class, 'reporte_id', 'id');
	}

	public function anexos()
	{
		return $this->hasMany(ReporteAnexo::class, 'reporte_id', 'id');
	}

	public function observaciones()
	{
		return $this->hasMany(ReporteObservacion::class, 'reporte_id', 'id');
	}

	public function status()
	{
		return $this->belongsTo(CatReporteStatus::class, 'status_id');
	}

	public function verificacion()
	{
		return $this->hasOne(Verificacion::class);
	}

	public function calendario()
	{
		return $this->hasOne(Calendario::class);
	}

	public function actualizacion()
	{
		return $this->hasOne(Actualizacion::class, 'reporte_id', 'id');
	}
}
