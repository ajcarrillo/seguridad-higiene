<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class CentroRH extends Model
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $connection = 'rh';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rh_cat_escuelas';
}
