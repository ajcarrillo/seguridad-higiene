<?php

namespace SeguridadHigiene\Models;

use Illuminate\Database\Eloquent\Model;

class ReporteTipo extends Model
{
	protected $fillable = ['tipo_id'];
    protected $table = "t_reportes_tipo";

	public function reporte()
	{
		return $this->belongsTo(Reporte::class, 'reporte_id');
	}

	public function tipo()
	{
		return $this->belongsTo(CatReporteTipo::class, 'tipo_id');
	}
}
