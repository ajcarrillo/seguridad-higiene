<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/10/15
 * Time: 9:06
 */

/*
|--------------------------------------------------------------------------
| ¿Qué es magic numbers?
|--------------------------------------------------------------------------
|
| 1.- http://stackoverflow.com/questions/47882/what-is-a-magic-number-and-why-is-it-bad
|
*/

/*
|--------------------------------------------------------------------------
| Función status
|--------------------------------------------------------------------------
|
| Esta función devuelve el id correspondiente a para cada estado
| en la tabla cat_reporte_status.
|
| Un reporte:
|
| Aceptado 		= El usuario puede imprimir su formato.
| Rechazado 	= El reporte ha sido rechazado por el área de RRRHH.
| Enviado 		= El reporte ha sido enviado para su revisión en RRHH.
| Pendiente 	= El reporte aún no ha sido enviado el usuario puede seguir capturando.
| Aprobado 		= El reporte ha sido aprobado, el usuario puede imprimir su reporte.
| En Revision 	= El área de RRHH está revisando el reporte, el usuario no puede editar.
|
*/
function statusReporte($name){
	$status = array(
		"Aceptado" 		=> 	1,
		"Rechazado" 	=>	2,
		"Enviado"		=> 	3,
		"Pendiente"		=>	4,
		"Aprobado"		=>	5,
		"En Revision"	=> 	6
	);

	return $status[$name];
}

/*
|--------------------------------------------------------------------------
| Función StatusIncidencia
|--------------------------------------------------------------------------
|
|
*/
function statusIncidencia($name){
	$status = array(
		"Primera vez"	=> 1,
		"Subsistentes"	=> 2,
		"Subsanadas"	=> 3
	);

	return $status[$name];
}

/*
|--------------------------------------------------------------------------
| Función Sectores
|--------------------------------------------------------------------------
|
|
*/
function catSectores($name){
	$sectores = array(
		"Oficial"	=> 1,
		"Sindical"	=> 2
	);
	return $sectores[$name];
}

function catSectoresById($id){
	$sectores = array(
		1 => "Oficial",
		2 => "Sindical"
	);
	return $sectores[$id];
}

/*
|--------------------------------------------------------------------------
| Función Vocales
|--------------------------------------------------------------------------
|
|
*/
function catVocales($name){
	$vocales = array(
		"Propietario"	=> 1,
		"Suplente"		=> 2
	);
	return $vocales[$name];
}

function catVocalesById($id){
	$vocales = array(
		1=>"Propietario",
		2=>"Suplente"
	);
	return $vocales[$id];
}

/*
|--------------------------------------------------------------------------
| Función TipoVerificacion
|--------------------------------------------------------------------------
|
|
*/
function catTipoVerificacion($name){
	$tipos = array(
		"Ordinaria"			=> 1,
		"Extraordinaria"	=> 2
	);

	return $tipos[$name];
}

/*
|--------------------------------------------------------------------------
| Función Trimestre by Id
|--------------------------------------------------------------------------
|
|
*/
function catTrimestreById($id){
	$trimestre = array(
		1	=>	"ENE-MAR",
		2	=>	"ABR-JUN",
		3	=>	"JUL-SEP",
		4	=>	"OCT-DIC"
	);
	return $trimestre[$id];
}

/*
|--------------------------------------------------------------------------
| Función Trimestre by Name
|--------------------------------------------------------------------------
|
|
*/
function catTrimestreByName($name){
	$trimestre = array(
		"ENE-MAR"	=>	1,
		"ABR-JUN"	=>	2,
		"JUL-SEP"	=>	3,
		"OCT-DIC"	=>	4
	);
	return $trimestre[$name];
}
