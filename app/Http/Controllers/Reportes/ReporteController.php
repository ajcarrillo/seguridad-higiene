<?php

namespace SeguridadHigiene\Http\Controllers\Reportes;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CatIncidencia;
use SeguridadHigiene\Models\CatReporteTipo;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\ReporteTipo;
use SeguridadHigiene\Repositories\ReportesRepository;
use SeguridadHigiene\Repositories\ReporteTipoRepository;
use SeguridadHigiene\Repositories\VerificacionRepository;

class ReporteController extends Controller
{
	/**
	 * @var ReportesRepository
	 */
	private $reportesRepository;
	/**
	 * @var ReporteTipoRepository
	 */
	private $reporteTipoRepository;
	/**
	 * @var VerificacionRepository
	 */
	private $verificacionRepository;

	public function __construct(
		ReportesRepository $reportesRepository,
		ReporteTipoRepository $reporteTipoRepository,
		VerificacionRepository $verificacionRepository)
	{

		$this->reportesRepository = $reportesRepository;
		$this->reporteTipoRepository = $reporteTipoRepository;
		$this->verificacionRepository = $verificacionRepository;
		$this->middleware('processIsOpen', ['except' => ['hasReporte', 'reject']]);
	}

	public function hasReporte($comision_id)
	{
		$hasReporte = false;
		if($this->reportesRepository->hasReporte($comision_id)){
			$hasReporte = true;
		}

		return response()->json(compact('hasReporte'));
	}

	public function getCreate()
	{

		if(trimestreActual() == 4){
			$tipos = CatReporteTipo::where('id', '!=', 1)->where('id', '!=', 4)
				->get();
		}else{
			$tipos = CatReporteTipo::where('id', '=', 2)
				->get();
		}
		return view('reportes.create', compact('tipos'));
	}

	public function postCreate(Request $request)
	{
		$comision = currentUser()->comision;
		$status = 4;


		DB::beginTransaction();
		try{
			$reporte = $this->reportesRepository->store($comision, trimestreActual(), $status);
			foreach($request->get('tipos') as $tipo){
				$this->reporteTipoRepository->store($reporte, $tipo);
			}
			DB::commit();
			//return $response = ['response' => true, 'reporteId' => $reporte->id];
			return redirect()->route('get.create.verificacion', $reporte->id);
		}catch (Exception $e){
			DB::rollBack();
			return redirect()->back();
		}
	}

	public function getReporte($reporteId, Request $request)
	{
		$reporte = Reporte::find($reporteId);
		switch ($request->input('tipo')){
			case 1:
				$comision = Reporte::find($reporteId)->comision;
				return redirect()->route('comision.centros.index', ['$comision' => $comision]);
				break;
			case 2:
				if($reporte->actualizacion){
					return redirect()->route('actualizacion.edit', [$reporte->id, $reporte->actualizacion->id]);
				}
				return redirect()->route('actualizacion.create', $reporte->id);
				break;
			case 3:
				if($reporte->calendario){
					return redirect()->route('actividad.index', $reporte->calendario->id);
				}
				return redirect()->route('get.create.calendario', $reporte->id);
				break;
			case 4:
				if ($reporte->verificacion){
					return redirect()->route('get.edit.verificacion', ['verificacionId' => $reporte->verificacion->id]);
				}
				return redirect()->route('get.create.verificacion', ['reporteId' => $reporte->id]);
				break;
			default:
				return response("hola mundo");
				break;
		}
	}

	public function send($reporteId)
	{
		return view('reportes.send_report', compact('reporteId'));
	}

	public function sendUpdate($reporteId)
	{
		try{
			$reporte = Reporte::find($reporteId);
			$this->reportesRepository->sent($reporte);
			//$response = ['response' => true];
		}catch (Exception $e){
			//$response = ['response' => false, 'msg' => $e->getMessage()];
		}
		return redirect()->back();

	}

	public function reject(Reporte $reporte)
	{
		return $reporte->observaciones->last();
	}

}
