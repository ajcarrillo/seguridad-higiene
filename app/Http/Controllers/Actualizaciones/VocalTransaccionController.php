<?php

namespace SeguridadHigiene\Http\Controllers\Actualizaciones;

use DB;
use Exception;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\ComisionVocal;
use SeguridadHigiene\Models\ComisionVocalTransaccion;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Repositories\VocalTransaccionRepository;

class VocalTransaccionController extends Controller
{
	/**
	 * set if the ajax response is valid or not
	 */
	private $isValid = true;

	/**
	 * @var VocalTransaccionRepository
	 */
	private $vocalTransaccionRepository;

	/**
	 * VocalTransaccionController constructor.
	 */
	public function __construct(VocalTransaccionRepository $vocalTransaccionRepository)
	{
		$this->vocalTransaccionRepository = $vocalTransaccionRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Reporte $reporte)
	{
		$transacciones = $this->getAll();

		return view('actualizaciones.vocalesTransacciones.index', compact('reporte', 'transacciones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Reporte $reporte)
	{
		$comision = $reporte->comision;
		return view('actualizaciones.vocalesTransacciones.create', compact('reporte', 'comision'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, Reporte $reporte)
	{
		$isValid = true;
		$actualizacion = $reporte->actualizacion;

		try {
			//Comprobar que la transaccion no exista para el mismo vocal
			if (!$actualizacion->hasTransaccionVocal($request->get('row_id'))) {
				if($request->get('actions') == "Delete"){
					$vocal = ComisionVocal::where('id', $request->get('row_id'))
						->select('id as row_id', 'nombre', 'primer_apellido', 'segundo_apellido', 'cargo', 'sector', 'tipo', 'rfc', 'comision_id')
						->get()->toArray();

					$vocal[0]['actions'] = $request->get('actions');
					$this->storeNewTransaccion($vocal[0],$actualizacion);
				}else{
					$this->storeNewTransaccion($request->all(),$actualizacion);
				}
			} else {
				Throw new Exception("La modificación ya existe");
			}

			$msg = "El registro se guardó correctamente";
		} catch (Exception $e) {
			$isValid = false;
			$msg = $e->getMessage();
		}

		$transacciones = $this->getAll();

		return compact('isValid', 'msg', 'transacciones');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Reporte $reporte, $transaccion)
	{

		try{
			ComisionVocalTransaccion::destroy($transaccion);
		}catch (Exception $e){
			$this->isValid = false;
			$msg = $e->getMessage();
		}

		$transacciones = $this->getAll();
		$isValid = $this->isValid;
		return compact('isValid', 'transacciones', 'msg');
	}

	/**
	 * @param $vocal
	 * @return ComisionVocalTransaccion
	 */
	private function storeNewTransaccion(array $vocal, Actualizacion $actualizacion)
	{
		$transaccion = new ComisionVocalTransaccion($vocal);
		$actualizacion->vocalTransacciones()->save($transaccion);
		return $transaccion;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	private function getAll()
	{
		$transacciones = ComisionVocalTransaccion::all();

		return $transacciones;
	}


	public function createVocalTransaccion(Request $request, Reporte $reporte)
	{
		$transaccion = new ComisionVocalTransaccion($request->all());
		return view('actualizaciones.vocalesTransacciones.create_transaccion', compact('transaccion', 'reporte'));
	}
}
