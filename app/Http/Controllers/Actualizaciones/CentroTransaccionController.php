<?php

namespace SeguridadHigiene\Http\Controllers\Actualizaciones;

use Exception;
use Illuminate\Http\Request;

use DB;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CentroLite;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Repositories\ComisionCentroTransaccionRepository;

class CentroTransaccionController extends Controller
{

	/**
	 * @var ComisionCentroTransaccionRepository
	 */
	private $comisionCentroTransaccionRepository;

	/**
	 * CentroTransaccionController constructor.
	 */
	public function __construct(ComisionCentroTransaccionRepository $comisionCentroTransaccionRepository)
	{
		$this->comisionCentroTransaccionRepository = $comisionCentroTransaccionRepository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Reporte $reporte)
    {
		$transacciones = $reporte->actualizacion->centroTransacciones;
		return view('actualizaciones.centros.index', compact('reporte', 'transacciones'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request, Reporte $reporte)
	{
		$returnURL = $request->has('returnURL') ? $request->get('returnURL') : '#!';

		return view('actualizaciones.centros.create', compact('returnURL', 'reporte'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Reporte $reporte)
    {
		if($request->ajax()){
			$centros = $request->get('centros');
			$array = array(
				"row_id" => 0,
				"centro_de_trabajo_id" => 0,
				"comision_id" => $reporte->comision->id,
				"actions" => "Insert"
			);

			DB::beginTransaction();
			try{
				foreach($centros as $centro){
					if( ! $reporte->actualizacion->hasTransaccion($centro)){
						if( ! $reporte->comision->hasCentro($centro)){
							$transaccion = array_set($array, "centro_de_trabajo_id", $centro);
							$this->comisionCentroTransaccionRepository->store($reporte->actualizacion, $transaccion);
						}else{
							$lite = CentroLite::find($centro);
							Throw new Exception("El centro ".$lite->cct." ya existe dentro de la comision");
						}
					}else{
						Throw new Exception("La modificación ya existe");
					}
				}
				DB::commit();
				$msg = "";
				$status = 200;
			}catch (Exception $e){
				DB::rollBack();
				$msg = $e->getMessage();
				$code = $e->getCode();
				$status = 500;
			}

			return response()->json(compact('msg', 'code'), $status);
		}

		if( ! $reporte->actualizacion->hasTransaccion($request->get('centro_de_trabajo_id'))){
			$this->comisionCentroTransaccionRepository->store($reporte->actualizacion, $request->all());
		}else{
			session()->flash('alert', 'La modificacion ya existe');
		}
		return redirect()->back();
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reporte $reporte, $id)
    {
        return $this->comisionCentroTransaccionRepository->destroy($id);
    }
}
