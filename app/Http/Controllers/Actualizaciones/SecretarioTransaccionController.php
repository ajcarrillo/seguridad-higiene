<?php

namespace SeguridadHigiene\Http\Controllers\Actualizaciones;

use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\ComisionSecretarioTransaccion;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Repositories\ComisionSecretarioRepository;
use SeguridadHigiene\Repositories\SecretarioTransaccionRepository;

class SecretarioTransaccionController extends Controller
{
	/**
	 * @var SecretarioTransaccionRepository
	 */
	private $secretarioTransaccionRepository;
	/**
	 * @var ComisionSecretarioRepository
	 */
	private $comisionSecretarioRepository;

	/**
	 * SecretarioTransaccionController constructor.
	 */
	public function __construct(SecretarioTransaccionRepository $secretarioTransaccionRepository,
								ComisionSecretarioRepository $comisionSecretarioRepository)
	{

		$this->secretarioTransaccionRepository = $secretarioTransaccionRepository;
		$this->comisionSecretarioRepository = $comisionSecretarioRepository;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($reporteId)
	{
		$reporte = $this->getReporte($reporteId);
		$secretario = $reporte->comision->secretario;
		$newSecretario = $reporte->actualizacion->secretarioTransacciones;

		return view('actualizaciones.secretarioTransacciones.index', compact('reporte', 'secretario', 'newSecretario'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, $reporteId)
	{
		$reporte = $this->getReporte($reporteId);
		$actualizacion = $reporte->actualizacion;
		$this->secretarioTransaccionRepository->store($actualizacion, $request->all());

		return back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $reporteId, $transaccionId)
	{
		$transaccion = ComisionSecretarioTransaccion::find($transaccionId);
		$this->secretarioTransaccionRepository->update($transaccion, $request->all());

		return back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * @param $reporteId
	 * @return mixed
	 */
	private function getReporte($reporteId)
	{
		$reporte = Reporte::find($reporteId);

		return $reporte;
	}
}
