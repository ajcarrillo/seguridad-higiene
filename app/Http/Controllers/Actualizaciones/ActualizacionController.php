<?php

namespace SeguridadHigiene\Http\Controllers\Actualizaciones;

use Exception;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionCentro;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\User;
use SeguridadHigiene\Repositories\ActualizacionRepository;
use SeguridadHigiene\Repositories\PlantillaRepository;
use SeguridadHigiene\Repositories\ReportesRepository;

class ActualizacionController extends Controller
{
	/**
	 * @var ActualizacionRepository
	 */
	private $actualizacionRepository;
	/**
	 * @var ReportesRepository
	 */
	private $reportesRepository;
	/**
	 * @var PlantillaRepository
	 */
	private $plantillaRepository;

	public function __construct(ActualizacionRepository $actualizacionRepository,
								ReportesRepository $reportesRepository,
								PlantillaRepository $plantillaRepository)
	{

		$this->actualizacionRepository = $actualizacionRepository;
		$this->reportesRepository = $reportesRepository;
		$this->plantillaRepository = $plantillaRepository;
	}

	public function sections($reporteId)
	{
		$reporte = $this->reportesRepository->findOrFail($reporteId);

		return view('actualizaciones.sections', compact('reporte'));
	}

	// <editor-fold desc="ACTUALIZACION CENTROS">

	public function centros($reporteId)
	{
	}

	// </editor-fold>

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	// <editor-fold desc="CREATE">

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create($reporteId)
	{
		$reporte = $this->reportesRepository->findOrFail($reporteId);

		return view('actualizaciones.actualizacion.create', compact('reporte'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	// TODO: Guardar el total de trabajadores de la comision en la tabla actualizacion
	public function store(Request $request, $reporteId)
	{
		try {
			$actualizacion = $this->actualizacionRepository->store($this->reportesRepository->findOrFail($reporteId), $request->all());
		} catch (Exception $e) {
			session()->flash('alert', 'Lo sentimos ha ocurrido un error');

			return redirect()->back()->withInput();
		}

		return redirect()->route('actualizacion.centros', $reporteId);
	}
	// </editor-fold>

	// <editor-fold desc="UPDATE">
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($reporteId, $actualizacionId)
	{
		$reporte = $this->reportesRepository->findOrFail($reporteId);
		$actualizacion = Actualizacion::find($actualizacionId);

		return view('actualizaciones.actualizacion.edit', compact('reporte', 'actualizacion'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $reporteId, $actualizacionId)
	{
		try {
			$actualizacion = Actualizacion::find($actualizacionId);
			$actualizacion->update($request->all());
		} catch (Exception $e) {
			session()->flash('alert', 'Lo sentimos ha ocurrido un error');

			return redirect()->back()->withInput();
		}


		return back();
	}
	// </editor-fold>

	// <editor-fold desc="DELETE">
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	// </editor-fold>

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($reporteId, $actualizacionId)
	{
		$reporte = $this->reportesRepository->findOrFail($reporteId);
		$actualizacion = Actualizacion::find($actualizacionId);

		return view('actualizaciones.actualizacion.show', compact('reporte', 'actualizacion'));
	}

	public function trabajadoresEdit(Reporte $reporte)
	{
		$comision = $reporte->comision;
		$centros = $comision->centros;
		$ccts = array();

		foreach($centros as $centro){
			array_push($ccts, $centro->escuela_lite->cct);
		}

		$total = $this->plantillaRepository->totalEmpleados($ccts);

		return view('actualizaciones.comision.total_trabajadores', compact('reporte', 'total'));
	}

	public function trabajadoresUpdate(Request $request, Reporte $reporte)
	{
		$actualizacion = $reporte->actualizacion;
		$actualizacion->total_trabajadores = $request->get('total_trabajadores');
		$actualizacion->save();

		return response()->json(['success'=>true]);
	}


}
