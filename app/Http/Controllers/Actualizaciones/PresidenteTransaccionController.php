<?php

namespace SeguridadHigiene\Http\Controllers\Actualizaciones;

use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\ComisionPresidenteTransaccion;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Repositories\ComisionPresidenteRepository;
use SeguridadHigiene\Repositories\PresidenteTransaccionRepository;

class PresidenteTransaccionController extends Controller
{
	/**
	 * @var ComisionPresidenteRepository
	 */
	private $comisionPresidenteRepository;
	/**
	 * @var PresidenteTransaccionRepository
	 */
	private $presidenteTransaccionRepository;

	/**
	 * PresidenteTransaccionController constructor.
	 */
	public function __construct(ComisionPresidenteRepository $comisionPresidenteRepository,
								PresidenteTransaccionRepository $presidenteTransaccionRepository)
	{
		$this->comisionPresidenteRepository = $comisionPresidenteRepository;
		$this->presidenteTransaccionRepository = $presidenteTransaccionRepository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($reporteId)
    {
		$reporte = $this->getReporte($reporteId);
		$presidente = $reporte->comision->presidente;
		$newPresidente = $reporte->actualizacion->presidenteTransacciones;
		return view('actualizaciones.presidenteTransacciones.index', compact('reporte', 'presidente', 'newPresidente'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $reporteId)
    {
		$reporte = $this->getReporte($reporteId);
		$actualizacion = $this->getActualizacion($reporte);
		$this->presidenteTransaccionRepository->store($actualizacion, $request->all());
		return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $reporteId, $transaccionId)
    {
        $transaccion = ComisionPresidenteTransaccion::find($transaccionId);
		$this->presidenteTransaccionRepository->update($transaccion, $request->all());
		return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	/**
	 * @param $reporteId
	 */
	private function getReporte($reporteId)
	{
		$reporte = Reporte::find($reporteId);

		return $reporte;
	}

	/**
	 * @param $reporte
	 * @return mixed
	 */
	private function getActualizacion($reporte)
	{
		$actualizacion = $reporte->actualizacion;

		return $actualizacion;
	}
}
