<?php

namespace SeguridadHigiene\Http\Controllers\Consultas;

use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use Zofe\Rapyd\Facades\DataGrid;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use SeguridadHigiene\Models\Comision;



class ClaveComisionController extends Controller
{
    //
    public function index(Request $request)
    {

        return view('admin.consultas.claveComision.index');
    }

    public function datos()
    {
        $comisiones = DB::table('t_comisiones as com')
            ->join('t_comision_centros_de_trabajo as cct', 'com.id', '=','cct.comision_id')
            ->join('rh.rh_cat_escuelas_lite as cel', 'cel.id', '=', 'cct.centro_de_trabajo_id')
            ->join('rh.rh_cat_turnos as turno', 'cel.id_turno','=','turno.id_turno')
            ->select('com.clave_comision', 'cel.cct', 'cel.nombre', 'turno.siglas');

        return Datatables::of($comisiones)->make(true);
        //return Datatables::of(Comision::select('*'))->make(true);
    }
}
