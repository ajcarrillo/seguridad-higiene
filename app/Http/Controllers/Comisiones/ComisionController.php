<?php

namespace SeguridadHigiene\Http\Controllers\Comisiones;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Http\Requests\CreateComision;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\Sindicato;
use SeguridadHigiene\Repositories\ComisionRepository;
use SeguridadHigiene\Repositories\ComisionVocalRepository;
use SeguridadHigiene\Repositories\PlantillaRepository;
use SeguridadHigiene\Repositories\ReportesRepository;
use SeguridadHigiene\Repositories\ReporteTipoRepository;

class ComisionController extends Controller
{
	/**
	 * @var ComisionRepository
	 */
	private $comisionRepository;
	/**
	 * @var ComisionVocalRepository
	 */
	private $comisionVocalRepository;
	/**
	 * @var ReportesRepository
	 */
	private $reportesRepository;
	/**
	 * @var ReporteTipoRepository
	 */
	private $reporteTipoRepository;
	/**
	 * @var PlantillaRepository
	 */
	private $plantillaRepository;

	public function __construct(ComisionRepository $comisionRepository,
								ComisionVocalRepository $comisionVocalRepository,
								ReportesRepository $reportesRepository,
								ReporteTipoRepository $reporteTipoRepository,
								PlantillaRepository $plantillaRepository)
	{

		$this->comisionRepository = $comisionRepository;
		$this->comisionVocalRepository = $comisionVocalRepository;
		$this->reportesRepository = $reportesRepository;
		$this->reporteTipoRepository = $reporteTipoRepository;
		$this->plantillaRepository = $plantillaRepository;
	}

	public function index()
	{

    }

	public function create()
	{
		return view('comisiones.create');
	}

	public function store(CreateComision $request)
	{
		$comision = $this->comisionRepository->store($request->all());
		return response()->redirectToRoute('comision.centros.index', compact('comision'));
	}

	public function sections($comision)
	{
		$comision = $this->comisionRepository->findOrFail($comision);
		$vocales = $this->comisionVocalRepository->index($comision);
		return view('comisiones.sections', compact('comision', 'vocales'));
	}

	public function enviar(Comision $comision)
	{
		if(!$comision->isComplete()){
			session()->flash('alert', 'Completa los datos de la comisión');
			return redirect()->back();
		}
		return view('comisiones.enviar');
	}

	public function sent(Comision $comision)
	{
		$hasReporteRegistro = $this->comisionRepository->hasReporteRegistro($comision->id);
		if($hasReporteRegistro){
			$reporte = Reporte::find($hasReporteRegistro[0]->reporte_id);
			$this->reportesRepository->sent($reporte);
			return redirect()->route('actas');
		}else{
			if($comision->hasCentrosAndVocales()){
				DB::beginTransaction();
				try{
					$reporte = $this->reportesRepository->store($comision, trimestreActual(), 3);
					$this->reporteTipoRepository->store($reporte,1);
					DB::commit();
					return redirect()->route('actas');
				}catch (Exception $e){
					DB::rollBack();
					return redirect()->back();
				}
			}
			session()->flash('alert', 'Completa los datos de la comisión');
		}
		return redirect()->back();

	}

	public function edit()
	{
		return view('comisiones.oficios');
	}

	public function update(Comision $comision, Request $request)
	{
		$comision->update($request->all());
		return redirect()->back();
	}

	public function editTotalTrabajadores(Comision $comision)
	{
		$ccts = array();

		foreach($comision->centros as $centro){
			array_push($ccts, $centro->escuela_lite->cct);
		}

		$total = $this->plantillaRepository->totalEmpleados($ccts);

		return view('comisiones.total_trabajadores', compact('total'));
	}

	public function updateTotalTrabajadores(Request $request, Comision $comision)
	{
		$comision->total_trabajadores = $request->get('total_trabajadores');
		$comision->update();

		return ['isValid'=>true, 'msg'=>"El número de trabajadores se actualizó correctamente"];
	}
}
