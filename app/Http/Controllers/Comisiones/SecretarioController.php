<?php

namespace SeguridadHigiene\Http\Controllers\Comisiones;

use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Repositories\SecretarioRepository;

class SecretarioController extends Controller
{

	/**
	 * @var SecretarioRepository
	 */
	private $secretarioRepository;

	public function __construct(SecretarioRepository $secretarioRepository)
	{

		$this->secretarioRepository = $secretarioRepository;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Comision $comision)
    {
		if(!$comision->hasSecretario()){
			return view('comisiones.secretarios.create');
		}
		$secretario = $comision->secretario;
		return view('comisiones.secretarios.edit', compact('secretario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($comision)
    {
		return view('comisiones.secretarios.create', compact('comision'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $comision)
    {
        $input = $request->all();
		$this->secretarioRepository->store(Comision::find($comision), $input);
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($comision, $secretario)
    {
		$secretario = $this->secretarioRepository->findOrFail($secretario);
		return view('comisiones.secretarios.edit', compact('secretario', 'comision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $comision, $secretario)
    {
        $this->secretarioRepository->update($request->all(), $secretario);
		return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
