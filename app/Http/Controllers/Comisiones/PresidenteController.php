<?php

namespace SeguridadHigiene\Http\Controllers\Comisiones;

use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Repositories\ComisionPresidenteRepository;

class PresidenteController extends Controller
{

	/**
	 * @var ComisionPresidenteRepository
	 */
	private $comisionPresidenteRepository;

	public function __construct(ComisionPresidenteRepository $comisionPresidenteRepository)
	{

		$this->comisionPresidenteRepository = $comisionPresidenteRepository;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Comision $comision)
    {
		if(!$comision->hasPresidente())
		{
			return view('comisiones.presidentes.create');
		}

		$presidente = $comision->presidente;
		return view('comisiones.presidentes.edit', compact('presidente'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($comision)
    {
        return view('comisiones.presidentes.create', compact('comision'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $comision)
    {
		$input = $request->all();
		$this->comisionPresidenteRepository->store(Comision::find($comision), $input);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($comision, $presidente)
    {
        $presidente = $this->comisionPresidenteRepository->findOrFail($presidente);
		return view('comisiones.presidentes.edit', compact('presidente', 'comision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $comision, $id)
    {
		$presidente = $this->comisionPresidenteRepository->findOrFail($id);
		$presidente->update($request->all());
		return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
