<?php

namespace SeguridadHigiene\Http\Controllers\Catalogos;

use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CatConfiguracionSistema;
use SeguridadHigiene\Repositories\CatConfiguracionSistemaRepository;

class CatConfiguracionSistemaController extends Controller
{
    //
    /**
     * @var CatConfiguracionSistemaRepository
     */
    private $catIncidenciaRepository;

    public function __construct(CatConfiguracionSistemaRepository $catConfiguracionSistemaRepository)
    {

        $this->catConfiguracionSistemaRepository = $catConfiguracionSistemaRepository;
    }

    //
    public function index(Request $request)
    {
        $configuraciones = $this->catConfiguracionSistemaRepository->lista();
        //dd($incidencias->currentPage());

        return view('admin.catalogos.configuracionSistema.index', compact('configuraciones'));
    }
    public function update(Request $request)
    {
        $configuraciones = $this->catConfiguracionSistemaRepository->lista();
        foreach($configuraciones as $configuracion) {

            $dato = 'dato'.$configuracion->id;
            $this->catConfiguracionSistemaRepository->update($configuracion->id, $request->input($dato));
            /*$configuracionUpdate = CatConfiguracionSistema::find($configuracion->id);
            $configuracionUpdate->valor = $request->input($dato);
            $configuracionUpdate->save();*/
        }
        return redirect()->back();

    }
}
