<?php

namespace SeguridadHigiene\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Repositories\CatSindicatoRepository;

class CatSindicatoController extends Controller
{
    //
    /**
     * @var CatSindicatoRepository
     */
    private $catSindicatoRepository;

    public function __construct(CatSindicatoRepository $catSindicatoRepository)
    {
        $this->catSindicatoRepository = $catSindicatoRepository;
    }

    public function index(Request $request)
    {
        $sindicatos = $this->catSindicatoRepository->lista();
        //dd($incidencias->currentPage());

        if($request->ajax()) {
            return response()->json($sindicatos);
        }

        return view('admin.catalogos.sindicatos.index', compact('sindicatos'));
    }

    public function agregar()
    {
        return view('admin.catalogos.sindicatos.create');
    }

    public function editar(Request $request, $id)
    {
        $sindicato = $this->catSindicatoRepository->find($id);

        return view('admin.catalogos.sindicatos.modify', compact('sindicato'));
    }

    public function eliminar(Request $request, $id)
    {
        try{
            $sindicato = $this->catSindicatoRepository->eliminar($id);

            $sindicatos = $this->lista();

            $ultima_pagina = $sindicatos->lastpage();
            $url = $sindicatos->url(1);

            $ok = true;
        }
        catch(Exception $e){
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg', 'ultima_pagina', 'url'));

    }

    public function postAgregar(Request $request)
    {
        $nombre = $request->input('nombre');
        $direccion = $request->input('direccion');
        $colonia = $request->input('colonia');
        $codigo_postal = $request->input('codigo_postal');
        $ciudad = $request->input('ciudad');
        $telefono = $request->input('telefono');
        try{
            $sindicato = $this->catSindicatoRepository->create($nombre, $direccion, $colonia, $codigo_postal, $ciudad, $telefono);

            $sindicatos = $this->lista();

            $ultima_pagina = $sindicatos->lastpage();
            $url = $sindicatos->url(1);

            $ok = true;
        }
        catch(Exception $e){
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg', 'sindicato', 'ultima_pagina', 'url'));

    }

    public function lista()
    {
        return $this->catSindicatoRepository->lista();
    }

    public function postModificar(Request $request)
    {
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $direccion = $request->input('direccion');
        $colonia = $request->input('colonia');
        $codigo_postal = $request->input('codigo_postal');
        $ciudad = $request->input('ciudad');
        $telefono = $request->input('telefono');

        try{
            $incidencia = $this->catSindicatoRepository->update($id, $nombre, $direccion, $colonia, $codigo_postal, $ciudad, $telefono);

            $ok = true;
        }
        catch(Exception $e){
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg', 'incidencia'));

    }

}
