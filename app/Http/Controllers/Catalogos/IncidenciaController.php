<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 15/02/2016
 * Time: 01:35 PM
 */
namespace SeguridadHigiene\Http\Controllers\Catalogos;

use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\models\Incidencia;
use Yajra\Datatables\Datatables;

class IncidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.catalogos.incidencia.index');
    }

    public function indexData()
    {
        $incidencias = Incidencia::all();
        return Datatables::of($incidencias)
            ->addColumn('acciones', function ($incidencias) {
                return '<div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                Opciones
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                               <li role="presentation"><a role="menuitem" tabindex="-1" href="'.route("incidencia.edit", $incidencias->id).'" data-button="edit">Modificar</a></li>
                               <li role="presentation"><a role="menuitem" tabindex="-1" href="'.route("incidencia.destroy", $incidencias->id).'" data-button="reset-password">Eliminar</a></li>
                            </ul>
                        </div>';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.catalogos.incidencia.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incidencia = new Incidencia();
        $incidencia->nombre = $request->nombre;
        $incidencia->clave = $request->clave;
        $incidencia->save();
        session()->flash('success', 'La incidencia se creó correctamente');
        return view('admin.catalogos.incidencia.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Incidencia $incidencia)
    {
        return view('admin.catalogos.incidencia.edit', compact('incidencia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incidencia $incidencia)
    {
        $incidenciaModificar = Incidencia::find($incidencia->id);
        $incidenciaModificar->nombre = $request->nombre;
        $incidenciaModificar->clave = $request->clave;
        $incidenciaModificar->save();
        session()->flash('success', 'La incidencia se modificó correctamente');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $incidencia = Incidencia::find($id);
        $incidencia->delete();
        session()->flash('success', 'La incidencia se modificó correctamente');
        return redirect()->back();
    }
}
