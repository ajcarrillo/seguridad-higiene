<?php

namespace SeguridadHigiene\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CatIncidencia;
use SeguridadHigiene\Repositories\CatIncidenciaRepository;

class CatIncidenciaController extends Controller
{
    //
    /**
     * @var CatIncidenciaRepository
     */
    private $catIncidenciaRepository;

    public function __construct(CatIncidenciaRepository $catIncidenciaRepository)
    {

        $this->catIncidenciaRepository = $catIncidenciaRepository;
    }

    public function index(Request $request)
    {
        $incidencias = $this->catIncidenciaRepository->lista();
        //dd($incidencias->currentPage());

        if($request->ajax()) {
            return response()->json($incidencias);
        }

        return view('admin.catalogos.incidencias.index', compact('incidencias'));
    }

    public function eliminar(Request $request, $id)
    {
        try{
            $incidencia = $this->catIncidenciaRepository->eliminar($id);

            $incidencias = $this->lista();

            $ultima_pagina = $incidencias->lastpage();
            $url = $incidencias->url(1);

            $ok = true;
        }
        catch(Exception $e){
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg', 'ultima_pagina', 'url'));

    }

    public function agregar()
    {
        return view('admin.catalogos.incidencias.create');
    }

    public function editar(Request $request, $id)
    {
        $incidencia = $this->catIncidenciaRepository->find($id);

        return view('admin.catalogos.incidencias.modify', compact('incidencia'));
    }

    public function lista()
    {
        return $this->catIncidenciaRepository->lista();
    }

    public function postAgregar(Request $request)
    {
        $nombre = $request->input('nombre');
        $clave = $request->input('clave');

        try{
            $incidencia = $this->catIncidenciaRepository->create($nombre, $clave);

            $incidencias = $this->lista();

            $ultima_pagina = $incidencias->lastpage();
            $url = $incidencias->url(1);

            $ok = true;
        }
        catch(Exception $e){
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg', 'incidencia', 'ultima_pagina', 'url'));

    }

    public function postModificar(Request $request)
    {
        $id = $request->input('id');
        $nombre = $request->input('nombre');
        $clave = $request->input('clave');

        try{
            $incidencia = $this->catIncidenciaRepository->update($id, $nombre, $clave);

            $ok = true;
        }
        catch(Exception $e){
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg', 'incidencia'));

    }

}
