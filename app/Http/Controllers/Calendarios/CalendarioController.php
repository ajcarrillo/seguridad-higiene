<?php

namespace SeguridadHigiene\Http\Controllers\Calendarios;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Calendario;
use SeguridadHigiene\Models\CalendarioActividad;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Repositories\CalendarioActividadRepository;
use SeguridadHigiene\Repositories\CalendarioRepository;

class CalendarioController extends Controller
{
	/**
	 * @var CalendarioRepository
	 */
	private $calendarioRepository;
	/**
	 * @var CalendarioActividadRepository
	 */
	private $actividadRepository;

	public function __construct(CalendarioRepository $calendarioRepository,
								CalendarioActividadRepository $actividadRepository)
	{

		$this->calendarioRepository = $calendarioRepository;
		$this->actividadRepository = $actividadRepository;
	}

	public function getCreate(Reporte $reporte)
	{
		return view('calendarios.create_calendario', compact('reporte'));
    }

	public function postCreate(Request $request, Reporte $reporte)
	{
		DB::beginTransaction();
		try{
			$calendario = $this->calendarioRepository->create($reporte, $request->all());
			$this->actividadRepository->create_default_activities($calendario);
			DB::commit();
		}catch (Exception $e){
			DB::rollBack();
			session()->flash('alert', 'Lo sentimos ha ocurrido un error');
			return redirect()->back();
		}
		return redirect()->route('actividad.index', ['calendarioId' => $calendario->id]);
	}

	public function index($calendario_id)
	{
		return $this->actividadRepository->index(Calendario::find($calendario_id));
	}
}
