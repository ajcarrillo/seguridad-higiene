<?php

namespace SeguridadHigiene\Http\Controllers\Calendarios;

use Exception;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Calendario;
use SeguridadHigiene\Models\CalendarioActividad;
use SeguridadHigiene\Models\Reporte;

class ActividadController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Calendario $calendario)
	{
		$actividades = $calendario->actividades()->paginate(10);

		return view('calendarios.actividad.index', compact('actividades', 'calendario'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Calendario $calendario)
	{
		return view('calendarios.actividad.create', compact('calendario'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request, Calendario $calendario)
	{

		try {
			$actividad = new CalendarioActividad($request->all());
			$calendario->actividades()->save($actividad);
			session()->flash('success', 'Los datos se guardaron correctamente');
		} catch (Exception $e) {
			session()->flash('alert', $e->getMessage());
		}

		if ($request->exists('_addanother')) {
			return redirect()->back();
		}

		return redirect()->route('actividad.index', $calendario->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(CalendarioActividad $actividad)
	{
		$calendario = $actividad->calendario;

		return view('calendarios.actividad.edit', compact('calendario', 'actividad'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CalendarioActividad $actividad)
	{
		try {
			$actividad->update($request->all());
			session()->flash('success', 'Los datos se guardaron correctamente');
		} catch (Exception $e) {
			session()->flash('alert', $e->getMessage());
		}
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param CalendarioActividad $actividad
	 * @return \Illuminate\Http\Response
	 * @internal param int $id
	 */
	public function destroy(CalendarioActividad $actividad)
	{
		$isValid = true;
		try{
			$actividad->delete();
			$msg = "La actividad se eliminó correctamente";
		}catch (Exception $e){
			$msg = $e->getMessage();
			$isValid = false;
		}

		return response()->json(compact('isValid', 'msg'));
	}
}
