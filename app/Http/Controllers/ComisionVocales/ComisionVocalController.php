<?php

namespace SeguridadHigiene\Http\Controllers\ComisionVocales;

use Exception;
use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionVocal;
use SeguridadHigiene\Repositories\ComisionVocalRepository;
use Yajra\Datatables\Datatables;

class ComisionVocalController extends Controller
{

	/**
	 * @var ComisionVocalRepository
	 */
	private $comisionVocalRepository;

	public function __construct(ComisionVocalRepository $comisionVocalRepository)
	{

		$this->comisionVocalRepository = $comisionVocalRepository;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Comision $comision)
    {
		return view('comisiones.vocales.index');
		//return $this->comisionVocalRepository->index(Comision::find($comision));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPlantilla(Comision $comision)
    {
        return view('comisiones.vocales.create_by_plantilla');
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Comision $comision, Request $request)
	{
		$vocal = new ComisionVocal($request->all());
		return view('comisiones.vocales.create', compact('vocal'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Comision $comision)
    {
		$isValid = true;
		$msg = "";

        $input = $request->all();
		try{
			$this->comisionVocalRepository->store($comision, $input);
		}catch(Exception $e){
			$ok = false;
			$msg = $e->getMessage();
		}

		return compact('isValid', 'msg');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($comision, $vocal)
    {
		$isValid = true;
		$msg = "";
		try{
			$this->comisionVocalRepository->destroy($vocal);
		}catch(Exception $e){
			$isValid = false;
			$msg = $e->getMessage();
		}
		return compact('isValid', 'msg');
	}

	public function vocalesDatatable($comision)
	{
		$vocales = ComisionVocal::where('comision_id',$comision)->select(['*']);

		return Datatables::of($vocales)
			->addColumn('action', function ($vocal) {
				return '<button
						class="btn btn-danger"
						data-button="delete-vocal"
						data-loading-text="Eliminando..."
						data-row_id="'.$vocal->id.'"
						data-nombre="'.$vocal->nombre.'"
						data-primer_apellido="'.$vocal->primer_apellido.'"
						data-segundo_apellido="'.$vocal->segundo_apellido.'"
						data-cargo="'.$vocal->cargo.'"
						data-sector="'.$vocal->sector.'"
						data-tipo="'.$vocal->tipo.'"
						data-rfc="'.$vocal->rfc.'"
						data-comision_id="'.$vocal->comision_id.'"
						>Quitar<button';
			})
			->make(true);

	}
}
