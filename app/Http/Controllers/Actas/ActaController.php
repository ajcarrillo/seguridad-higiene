<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 9:59
 */

namespace SeguridadHigiene\Http\Controllers\Actas;

use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\Reporte;

class ActaController extends Controller
{

	public function index()
	{

		/*
		 * Obtener la comision a la que pertenece el usuario
		 * 	1.- Si tiene comision y la clave de comision es 0 y no tiene reporte, quiere decir que no ha enviado
		 * 		el reporte de registro de comision y hay que mandarlo a las secciones para enviar el reporte de registro.
		 * 	2.- Si no tiene comision mandarlo directamente a hacer el registro de dicha comision.
		 * 	3.- Si tiene comision y la clave de comision es 0 y tiene al menos un reporte, mandarlo a la lista de actas
		 * 		sin mostrar el boton nueva acta, ya que el unico que reporte que puede hacer es el de registro.
		 * 	4.- Si tiene comision y tiene clave de comision mandarlo a la lista de actas con el boton para generar
		 * 		nuevas actas.
		 *
		 * */


		if (!currentUser()->comision) {
			return redirect()->route('comision.create');
		}

		$comision = $this->getComision();

		if ($comision->clave_comision == 0 && $comision->num_reportes == 0) {
			return redirect()->route('comision.centros.index', $comision->id);
		}

		return view('actas.list', compact('comision'));

	}

	public function downloadActa(Reporte $reporte)
	{
		$path = base_path() . DIRECTORY_SEPARATOR . $reporte->path_pdf;

		return response()->download($path);
	}

	/**
	 * @return mixed
	 */
	private function getComision()
	{
		$comision = Comision::selectRaw(
			't_comisiones.id, t_comisiones.clave_comision, '
			. '(SELECT COUNT(*) FROM t_reportes WHERE t_reportes.comision_id = t_comisiones.id) as num_reportes'
		)
			->with('reportes', 'reportes.tipos', 'reportes.tipos.tipo', 'reportes.status')
			->where('user_id', currentUser()->id)
			->first();

		return $comision;
	}

	//TODO: Implementar ver detalles del reporte
	//TODO: Implementar eliminar reporte
}
