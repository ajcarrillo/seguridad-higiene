<?php

namespace SeguridadHigiene\Http\Controllers\ComisionCentros;

use Exception;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CentroLite;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionCentro;
use SeguridadHigiene\Repositories\ComisionCentroRepository;
use Yajra\Datatables\Datatables;

class ComisionCentroController extends Controller
{

	/**
	 * @var ComisionCentroRepository
	 */
	private $comisionCentroRepository;

	public function __construct(ComisionCentroRepository $comisionCentroRepository)
	{

		$this->comisionCentroRepository = $comisionCentroRepository;
	}

	public function index()
	{
		/*$centros = ComisionCentro::with(['escuela_lite.turno', 'escuela_lite.status'])
			->where('comision_id', '=', $comision)
			->paginate(5);
		return $centros;*/
		return view('comisiones.centros.index');
	}

	public function create()
	{
		return view('comisiones.centros.create');
	}

	//TODO: Actualizar el total de trabajadores en la comision
	public function store(Request $request, Comision $comision)
	{
		$exists = false;
		$isValid = true;

		DB::beginTransaction();
		try{
			foreach ($request->get('centros') as $centro) {

				$comisionCentro = ComisionCentro::where('centro_de_trabajo_id', $centro)->first();

				if(!$comision->hasCentro($centro) && $comisionCentro == null){
					$this->comisionCentroRepository->store($comision, $centro);
				}else{
					$lite = CentroLite::find($centro);
					 Throw new Exception("El centro ".$lite->cct." ya existe en la comisión y/o está asignado a otra comisión");
				}
			}
			DB::commit();
		}catch (Exception $e){
			DB::rollback();
			$msg = $e->getMessage();
			$exists = true;
			$isValid = false;
		}

		return compact('msg', 'exists', 'isValid');

		/*$status = 200;

		$centroId = $request->get('centro_id');
		$comisionCentro = ComisionCentro::where('centro_de_trabajo_id', $centroId)->first();

		if($comisionCentro){
			$hasComision = true;
		}else{
			try{
				$Comision = Comision::find($comision);
				if(!$Comision->hasCentro($centroId)){
					$comisionCentro = $this->comisionCentroRepository->store($Comision, $centroId);
				}else{
					$exists = true;
				}
			}catch(Exception $e){
				$msg = $e->getMessage();
				$status = $e->getCode();
			}
		}

		return response()->json(compact('exists', 'hasComision', 'msg', 'comisionCentro'), $status);*/

	}

	public function destroy($comision, $centro)
	{
		ComisionCentro::destroy($centro);
		return redirect()->back();
	}

	public function dataTablesCentros($comision)
	{
		$centros = $this->comisionCentroRepository->centrosDataTable($comision);

		return Datatables::of($centros)
			->addColumn('action', function ($centro) {
				return '<button
						class="btn btn-danger"
						data-button="delete-centro"
						data-loading-text="Eliminando..."
						data-centro_de_trabajo_id="'.$centro->centro_de_trabajo_id.'"
						data-id="'.$centro->id.'">Quitar<button';
			})
			->make(true);
	}
}
