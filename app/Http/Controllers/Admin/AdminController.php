<?php

namespace SeguridadHigiene\Http\Controllers\Admin;

use SeguridadHigiene\Repositories\ReportesRepository;
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;
//use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\Calendario;
use SeguridadHigiene\Models\CatReporteTipo;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ReporteObservacion;
use SeguridadHigiene\Models\ReporteTipo;
use SeguridadHigiene\Models\Sindicato;
use SeguridadHigiene\Models\CentroRH;
use SeguridadHigiene\Models\ComisionCentro;
use SeguridadHigiene\Models\ComisionPresidente;
use SeguridadHigiene\Models\ComisionSecretario;
use SeguridadHigiene\Models\Verificacion;
use Zofe\Rapyd\Facades\DataGrid;
use Zofe\Rapyd\Facades\DataForm;
use Illuminate\Support\Facades\DB;
use SeguridadHigiene\Repositories\ReporteObservacionRepository;
use SeguridadHigiene\Repositories\VerificacionIncidenciaRepository;
use mPDF;
use SeguridadHigiene\Repositories\ComisionPresidenteRepository;
use SeguridadHigiene\Repositories\ComisionSecretarioRepository;
use SeguridadHigiene\Repositories\ComisionCentroRepository;

class AdminController extends Controller
{

    /**
     * @var reporteObservacion
     * @var VerificacionIncidenciaRepository
     */
    private $reporteObservacionRepository;
    private $verificacionIncidenciaRepository;
    private $comisionPresidenteRepository;
    private $comisionSecretarioRepository;
    private $comisionCentroRepository;
    private $reporteRepository;

    public function __construct(
        ReportesRepository $reportesRepository,
        ReporteObservacionRepository $reporteObservacionRepository,
        VerificacionIncidenciaRepository $verificacionIncidenciaRepository,
        ComisionPresidenteRepository $comisionPresidenteRepository,
        ComisionSecretarioRepository $comisionSecretarioRepository,
        ComisionCentroRepository $comisionCentroRepository
    ) {
        $this->reporteRepository = $reportesRepository;
        $this->reporteObservacionRepository = $reporteObservacionRepository;
        $this->verificacionIncidenciaRepository = $verificacionIncidenciaRepository;
        $this->comisionPresidenteRepository = $comisionPresidenteRepository;
        $this->comisionSecretarioRepository = $comisionSecretarioRepository;
        $this->comisionCentroRepository = $comisionCentroRepository;

    }

    public function index()
    {
        return view('admin.list');
    }
    public function enviadasJson()
    {
        $actas = $this->reporteRepository->findByStatus(statusReporte('Enviado'));

        return Datatables::of($actas)
            ->addColumn('acciones', function ($acta) {
                return '<div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Opciones <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                        <li><a href="'.route('admin.edit', $acta->id).'">Revisar</a></li>
                        </ul>
                        </div>';
            })
            ->make(true);
    }

    public function sinClaveComisionJson()
    {
        $actas = $this->reporteRepository->findByStatus(statusReporte('Aceptado'));

        return Datatables::of($actas)
            ->addColumn('acciones', function ($acta) {
                $opciones = '<div class="btn-group">
                             <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Opciones <span class="caret"></span>
                        </button>
                             <ul class="dropdown-menu">';

                foreach ($acta->tipos as $tipo) {
                    if ($tipo->tipo->nombre == 'Registro') {
                        $opciones .= '<li><a href="#!" data-button="asigna-clave-comision" data-id="' . $acta->id . '"> Asignar clave comisión </a></li>';
                    }
                }

                return $opciones . '</ul></div>';
            })
            ->make(true);
    }

    public function terminadasJson()
    {
        $actas = $this->reporteRepository->findByStatus(statusReporte('Aprobado'));

        return Datatables::of($actas)
            ->addColumn('acciones', function ($acta) {
                return '<div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Opciones <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                        <li ><a href="'.route('admin.reports',['reportes'=>$acta->id]).'" data-button="open-calendario" target="_blank">Impresión</a></li>
                        </ul></div>';
            })
            ->make(true);
    }

    public function conObservacionJson()
    {
        $actas = $this->reporteRepository->findByStatus(statusReporte('Rechazado'));

        return Datatables::of($actas)
            ->addColumn('acciones', function ($acta) {
                return '<div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        Opciones <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                        <li><a href="ver/'.$acta->id.'" data-button="open-calendario">Ver</a></li>
                        </ul></div>';
            })
            ->make(true);
    }

    public function reportes(Request $request, Reporte $reporte)
    {
        $a_tipoReporte = [
            'registro'      => false,
            'actualizacion' => false,
            'calendario'    => false,
            'verificacion'  => false
        ];
        $tipoReporte = implode(',', $reporte->tipos->load('tipo')->lists('tipo.nombre')->toArray());

        foreach ($reporte->tipos as $tipo) {
            switch ($tipo->tipo_id) {
                case 1:
                    $a_tipoReporte['registro'] = true;
                    break;
                case 2:
                    $a_tipoReporte['actualizacion'] = true;
                    break;
                case 3:
                    $a_tipoReporte['calendario'] = true;
                    break;
                case 4:
                    $a_tipoReporte['verificacion'] = true;
                    break;
            }
        }

        $verificacion = $reporte->verificacion;

        if ($verificacion) {
            $incidencias_detectadas = $this->verificacionIncidenciaRepository->detectadas($verificacion);
            $gridIncidenciasDetectadas = DataGrid::source($incidencias_detectadas);
            $gridIncidenciasDetectadas->paginate(5); //pagination
            $gridIncidenciasDetectadas->add('cct', 'Centro de Trabajo', false);
            $gridIncidenciasDetectadas->add('nombre', 'Incidencia', false);
            $gridIncidenciasDetectadas->add('trimestre', 'Trimestre', false);
            $gridIncidenciasDetectadas->add('anio', 'Año', false);//same source types of DataSet

            $incidencias_subsistentes = $this->verificacionIncidenciaRepository->subsistentes($verificacion);
            $gridIncidenciasSubsistentes = DataGrid::source($incidencias_subsistentes);  //same source types of DataSet
            $gridIncidenciasSubsistentes->paginate(5); //pagination
            $gridIncidenciasSubsistentes->add('cct', 'Centro de Trabajo', false);
            $gridIncidenciasSubsistentes->add('nombre', 'Incidencia', false);
            $gridIncidenciasSubsistentes->add('trimestre', 'Trimestre', false);
            $gridIncidenciasSubsistentes->add('anio', 'Año', false);

            $incidencias_subsanadas = $this->verificacionIncidenciaRepository->subsanadas($verificacion);
            $gridIncidenciasSubsanadas = DataGrid::source($incidencias_subsanadas);  //same source types of DataSet
            $gridIncidenciasSubsanadas->paginate(5); //pagination
            $gridIncidenciasSubsanadas->add('cct', 'Centro de Trabajo', false);
            $gridIncidenciasSubsanadas->add('nombre', 'Incidencia', false);
            $gridIncidenciasSubsanadas->add('trimestre', 'Trimestre', false);
            $gridIncidenciasSubsanadas->add('anio', 'Año', false);
        }

        $conBoton = true;
        return view('admin/consultarReporte', compact(
            'reporte',
            'tipoReporte',
            'a_tipoReporte',
            'gridIncidenciasDetectadas',
            'gridIncidenciasSubsistentes',
            'gridIncidenciasSubsanadas',
            'conBoton'
        ));
    }

    public function grabaComision(Request $request, Reporte $reporte)
    {
        $actualizar = false;
        foreach ($reporte->tipos as $tipo) {
            if ($tipo->tipo_id === 1) {
                $reporte->status_id = statusReporte('Aceptado');
            }

            if ($tipo->tipo_id === 4) {
                $actualizar = true;
                $reporte->status_id = statusReporte('Aprobado');
            }

            if ($tipo->tipo_id === 2) {
                $actualizar = $actualizar;
                $reporte->status_id = statusReporte('Aprobado');
            }
        }

        if ($actualizar) {
            $this->procesaActualizacion($reporte);
            $this->impresion($reporte);
        }
        $reporte->save();
        return response()->json(['success'=>true]);
    }

    public function procesaActualizacion($reporte)
    {
        if ($reporte->actualizacion) {
            if ($reporte->actualizacion->presidenteTransacciones->actions == 'Update') {
                $this->comisionPresidenteRepository->update($reporte->actualizacion->presidenteTransacciones);
            }

            if ($reporte->actualizacion->secretarioTransacciones->actions == 'Update') {
                $this->comisionSecretarioRepository->update($reporte->actualizacion->secretarioTransacciones);
            }

            foreach ($reporte->actualizacion->centroTransacciones as $centro) {
                if ($centro->actions == 'Insert') {
                    $this->comisionCentroRepository->store($reporte->comision, $centro->centro_de_trabajo_id);
                }
                if ($centro->actions == 'Delete') {
                    $this->comisionCentroRepository->destroy($centro->centro_de_trabajo_id);
                }
            }
        }
    }

    public function observacion(Request $request, Reporte $reporte)
    {
        return view('admin.modalObservacion', compact('reporte'));
    }

    public function grabaObservacion(Request $request, Reporte $reporte)
    {
        try {
            $reporteObservacion = new ReporteObservacion();
            $reporteObservacion->observacion = $request->observacion;
            $reporteObservacion->reporte_id = $reporte->id;
            $reporteObservacion->usuario_id = currentUser()->id;
            $reporteObservacion->save();

            $reporte->status_id = statusReporte('Rechazado');
            $reporte->save();

            $ok = true;
            $msg = 'La observación se almaceno exitosamente';
        } catch(Exception $e) {
            $ok = false;
            $msg = $e->getMessage();

        }
        return response()->json(compact('ok', 'msg'));
    }

    public function AsignaClaveComision(Request $request, Reporte $reporte)
    {
        $consecutivoComision = Comision::select(
            DB::raw('LPAD(max(substring(t_comisiones.clave_comision,8,5))+1,5,"0") as consecutivo'),
            DB::raw('year(curdate()) as anio'))
            ->where(DB::raw('year(curdate())'), DB::raw('substring(t_comisiones.clave_comision, 13,4)'))
            ->first();

        if (!$consecutivoComision->consecutivo)
            $consecutivoComision->consecutivo = '00001';

        return view('admin.modalClaveComision', compact('reporte', 'consecutivoComision'));
    }

    public function grabaClaveComision(Request $request, Reporte $reporte)
    {
        $estado = $request->input('estado');
        $ramo = $request->input('ramo');
        $consecutivo = $request->input('consecutivo');
        $anio = $request->input('anio');
        $claveComision=$estado.$ramo.$consecutivo.$anio;

        try {
            $comision = Comision::find($reporte->comision_id);
            $reporte->status_id = statusReporte('Aprobado');
            $reporte->save();

            $comision->clave_comision = $claveComision;
            $comision->save();

            $this->impresion($reporte);

            $ok = true;
            $msg = 'La clave de la comisión se almaceno exitosamente';
        } catch(Exception $e) {
            $ok = false;
            $msg = $e->getMessage();
        }
        return response()->json(compact('ok', 'msg'));
    }

    public function impresion(Reporte $reporte)
    {
        $a_tipoReporte['registro'] = false;
        $a_tipoReporte['actualizacion'] = false;
        $a_tipoReporte['calendario'] = false;
        $a_tipoReporte['verificacion'] = false;
        $reportePathAGuardar = DIRECTORY_SEPARATOR . $reporte->id . date('dmY') . '.pdf';
        $pathPDF = public_path('reportes') . $reportePathAGuardar;

        $this->reporteRepository->saveReport($reporte, 'public/reportes' . $reportePathAGuardar);

        foreach ($reporte->tipos as $tipo) {
            switch ($tipo->tipo_id) {
                case 1:
                    $a_tipoReporte['registro'] = true;
                    break;
                case 2:
                    $a_tipoReporte['actualizacion'] = true;
                    break;
                case 3:
                    $a_tipoReporte['calendario'] = true;
                    break;
                case 4:
                    $a_tipoReporte['verificacion'] = true;
                    break;
            }
        }

        $centros_trabajo['nombre'] = '';
        $centros_trabajo['domicilio'] = '';
        $centros_trabajo['ciudad'] = '';
        $centros_trabajo['codigo_postal'] = '';
        $datos = false;
        $iniciado = false;

        foreach ($reporte->comision->centros as $centro) {
            if ($iniciado) {
                $centros_trabajo['nombre'] .= ", ";
            }

            $centros_trabajo['nombre'] .= "[" . $centro->escuela_lite->cct . "] " . $centro->escuela_lite->nombre;
            $iniciado = true;

            if (!$datos) {
                $centros_trabajo['domicilio'] = $centro->escuela_lite->domicilio;
                $centros_trabajo['ciudad'] = $centro->escuela_lite->d_localidad . ", QUINTANA ROO";
                $centros_trabajo['codigo_postal'] = $centro->escuela_lite->codpost;
                $datos = true;
            }
        }

        $nommesFech = array("enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "sepiembre", "octubre", "noviembre", "diciembre");

        //definimos la fecha actual
        $diav = date("d"); //Dia del mes en numero
        $mesv = date("n"); //Mes actual en numero
        $aniov = date("Y");
        $fechvMes = ($diav . "-" . strtoupper($nommesFech[$mesv - 1]) . "-" . $aniov);
        $fecha = $diav . " DE " . strtoupper($nommesFech[$mesv - 1]) . " DE " . $aniov;

        $html = \View::make('admin.reporte', compact('reporte', 'a_tipoReporte', 'centros_trabajo', 'fecha'))->render();

        //$html = View::make('venta/venta_reporte/show',array('detalle'=>$Detalle, 'cliente'=>$Cliente, 'venta'=>$Venta));
        $mpdf = new mPDF('c', 'letter', '', '', 10, 10, 10, 10, 10, 10);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;    // 1 or 0 - whether to indent the first level of a list
        $mpdf->SetFooter('Seguridad y salud en el trabajo|- Pagina {PAGENO} de {nb} -|Fecha de impresión ' . $fechvMes);

        //si se quiere colocar un imagen de fondo en la hoja
        //$mpdf->showWatermarkImage = true;
        //$mpdf->SetWatermarkImage('public/packages/images/imagen_a_gusto.jpg', 0.4,'', array(0,1));

        // LOAD a stylesheet
        $stylesheet = file_get_contents('assets/css/grid_print.css');
        $mpdf->WriteHTML($stylesheet, 1);    // The parameter 1 tells that this is css/style only and no body/html/text

        $mpdf->WriteHTML($html, 2);
        $mpdf->Output($pathPDF, 'F');
    }

    public function consultar(Reporte $reporte)
    {
        $a_tipoReporte = [
            'registro'      => false,
            'actualizacion' => false,
            'calendario'    => false,
            'verificacion'  => false
        ];
        $tipoReporte = implode(',', $reporte->tipos->load('tipo')->lists('tipo.nombre')->toArray());

        foreach ($reporte->tipos as $tipo) {
            switch ($tipo->tipo_id) {
                case 1:
                    $a_tipoReporte['registro'] = true;
                    break;
                case 2:
                    $a_tipoReporte['actualizacion'] = true;
                    break;
                case 3:
                    $a_tipoReporte['calendario'] = true;
                    break;
                case 4:
                    $a_tipoReporte['verificacion'] = true;
                    break;
            }
        }

        foreach ($reporte->tipos as $tipo) {

            switch ($tipo->tipo_id) {
                case 1:
                    $a_tipoReporte['registro'] = true;
                    break;
                case 2:
                    $a_tipoReporte['actualizacion'] = true;
                    break;
                case 3:
                    $a_tipoReporte['calendario'] = true;
                    break;
                case 4:
                    $a_tipoReporte['verificacion'] = true;
                    break;
            }
        }

        $verificacion = $reporte->verificacion;

        if ($verificacion) {

            $incidencias_detectadas = $this->verificacionIncidenciaRepository->detectadas($verificacion);
            $gridIncidenciasDetectadas = DataGrid::source($incidencias_detectadas);
            $gridIncidenciasDetectadas->paginate(5); //pagination
            $gridIncidenciasDetectadas->add('cct', 'Centro de Trabajo', false);
            $gridIncidenciasDetectadas->add('nombre', 'Incidencia', false);
            $gridIncidenciasDetectadas->add('trimestre', 'Trimestre', false);
            $gridIncidenciasDetectadas->add('anio', 'Año', false);//same source types of DataSet

            $incidencias_subsistentes = $this->verificacionIncidenciaRepository->subsistentes($verificacion);
            $gridIncidenciasSubsistentes = DataGrid::source($incidencias_subsistentes);  //same source types of DataSet
            $gridIncidenciasSubsistentes->paginate(5); //pagination
            $gridIncidenciasSubsistentes->add('cct', 'Centro de Trabajo', false);
            $gridIncidenciasSubsistentes->add('nombre', 'Incidencia', false);
            $gridIncidenciasSubsistentes->add('trimestre', 'Trimestre', false);
            $gridIncidenciasSubsistentes->add('anio', 'Año', false);

            $incidencias_subsanadas = $this->verificacionIncidenciaRepository->subsanadas($verificacion);
            $gridIncidenciasSubsanadas = DataGrid::source($incidencias_subsanadas);  //same source types of DataSet
            $gridIncidenciasSubsanadas->paginate(5); //pagination
            $gridIncidenciasSubsanadas->add('cct', 'Centro de Trabajo', false);
            $gridIncidenciasSubsanadas->add('nombre', 'Incidencia', false);
            $gridIncidenciasSubsanadas->add('trimestre', 'Trimestre', false);
            $gridIncidenciasSubsanadas->add('anio', 'Año', false);
        }

        $conBoton = false;
        return view('admin/consultarReporte', compact(
            'reporte',
            'tipoReporte',
            'a_tipoReporte',
            'gridIncidenciasDetectadas',
            'gridIncidenciasSubsistentes',
            'gridIncidenciasSubsanadas',
            'conBoton'
        ));
    }

    public function reports(Request $request, Reporte $reporte)
    {
        $path = base_path() . DIRECTORY_SEPARATOR .$reporte->path_pdf;

        return response()->make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'application/pdf; filename='.$reporte->path_pdf,
        ]);
    }


}
