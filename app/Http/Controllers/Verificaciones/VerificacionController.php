<?php

namespace SeguridadHigiene\Http\Controllers\Verificaciones;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CatIncidencia;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\Verificacion;
use SeguridadHigiene\Repositories\ReportesRepository;
use SeguridadHigiene\Repositories\VerificacionIncidenciaRepository;
use SeguridadHigiene\Repositories\VerificacionRepository;

class VerificacionController extends Controller
{
	/**
	 * @var ReportesRepository
	 */
	private $reportesRepository;
	/**
	 * @var VerificacionRepository
	 */
	private $verificacionRepository;
	/**
	 * @var VerificacionIncidenciaRepository
	 */
	private $verificacionIncidenciaRepository;

	public function __construct(
		ReportesRepository $reportesRepository,
		VerificacionRepository $verificacionRepository,
		VerificacionIncidenciaRepository $verificacionIncidenciaRepository
	)
	{

		$this->reportesRepository = $reportesRepository;
		$this->verificacionRepository = $verificacionRepository;
		$this->verificacionIncidenciaRepository = $verificacionIncidenciaRepository;
	}
	public function getCreate(Reporte $reporte)
	{
		$catIncidencias = CatIncidencia::all();
		return view('verificaciones.create', compact('reporte', 'catIncidencias'));
    }

	public function postCreate(Request $request, Reporte $reporte)
	{
		DB::beginTransaction();
		try{
			$verificacion = $this->verificacionRepository
				->create($reporte, $request->all());
				$this->verificacionIncidenciaRepository->update_to_subsistentes($verificacion->reporte->comision->id);
			DB::commit();
			return redirect()
				->route('verificacion.incidencia.create', $verificacion->id);

		}catch (Exception $e){
			DB::rollback();
			return redirect()->route('get.create.verificacion', ['reporteId' => $reporte->id])
				->withInput()
				->withErrors($e->getMessage());
		}

	}

	public function getEdit(Verificacion $verificacion)
	{
		return view('verificaciones.edit', compact('verificacion'));
	}

	public function postEdit(Request $request, $verificacionId)
	{
		$response = true;
		$msg = "";

		$fecha = dateCreate($request->get('fecha'));
		$tipo = $request->get('tipo');
		$numero_de_riesgos_de_trabajo = $request->get('numero_de_riesgos_de_trabajo');
		$accidentes_de_trabajo = $request->get('accidentes_de_trabajo');
		$enfermedad_profesional = $request->get('enfermedad_profesional');
		$observacion = $request->get('observacion');
		$propuesta_csst = $request->get('propuesta_csst');

		$verificacion = $this->verificacionRepository->find($verificacionId);

		try{
			$this->verificacionRepository->update($verificacion, $fecha, $tipo, $numero_de_riesgos_de_trabajo,
				$accidentes_de_trabajo, $enfermedad_profesional, $observacion, $propuesta_csst);
		}catch (Exception $e){
			$response = false;
			$msg = $e->getMessage();
		}

		return redirect()->back();

	}
}
