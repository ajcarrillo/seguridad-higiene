<?php

namespace SeguridadHigiene\Http\Controllers\Verificaciones;

use Exception;
use Illuminate\Http\Request;
use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CatIncidencia;
use SeguridadHigiene\Models\Verificacion;
use SeguridadHigiene\Models\VerificacionIncidencia;
use SeguridadHigiene\Repositories\VerificacionIncidenciaRepository;

class VerificacionIncidenciaController extends Controller
{
	/**
	 * @var VerificacionIncidenciaRepository
	 */
	private $verificacionIncidenciaRepository;

	public function __construct(
		VerificacionIncidenciaRepository $verificacionIncidenciaRepository)
	{

		$this->verificacionIncidenciaRepository = $verificacionIncidenciaRepository;
	}

	public function create(Verificacion $verificacion)
	{
		$catIncidencias = CatIncidencia::all();
		return view('incidencias.create', compact('verificacion','catIncidencias'));
	}

	public function store(Request $request, $verficacionId)
	{
		$incidencia = 0;
		$existe = false;
		try{

			$verificacion = Verificacion::find($verficacionId);
			if($verificacion->hasIncidencia($incidencia_id = $request->get('incidencia_id'), $centro_de_trabajo_id = $request->get('centro_de_trabajo_id'))){
				$existe = true;
			}else{
				$incidencia = $this->verificacionIncidenciaRepository->create($verificacion, $request->all());
			}
			$response = ['ok' => true, 'incidencia' => $incidencia, 'exists'=> $existe, 'msg' => 'La incidencia se guardó correctamente'];
		}catch (Exception $e){
			$response = ['ok' => false, 'msg' => $e->getMessage()];
		}

		return response()->json($response);
	}

	public function delete($incidenciaId)
	{
		$response = true;
		$msg = "";
		try{
			$incidencia = VerificacionIncidencia::destroy($incidenciaId);
		}catch (Exception $e){
			$response = false;
			$msg = $e->getMessage();
		}
		return response()->json(compact('response', 'msg'));
	}

	public function patchSubsanar($incidenciaId)
	{
		$ok = true;
		$msg = "";

		try{
			$this->verificacionIncidenciaRepository->subsanar_incidencia($incidenciaId);
		}catch (Exception $e){
			$response = false;
			$msg = $e->getMessage();
		}

		return response()->json(compact('ok', 'msg'));
	}

	public function getSubsistentes(Request $request)
	{

		$verificacion = Verificacion::find($request->get('verificacion'));
		$incidencias_subsistentes = $this->verificacionIncidenciaRepository->subsistentes($verificacion);
		return view('incidencias.partials.modal_subsanar', compact('incidencias_subsistentes'));
	}

	public function getSubsanadas(Request $request)
	{
		$verificacion = Verificacion::find($request->get('verificacion'));
		$incidencias_subsanadas = $this->verificacionIncidenciaRepository->subsanadas($verificacion);
		return view('incidencias.partials.modal_subsanadas', compact('incidencias_subsanadas'));

	}
}
