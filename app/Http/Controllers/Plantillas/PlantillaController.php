<?php

namespace SeguridadHigiene\Http\Controllers\Plantillas;

use DB;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CentroLite;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Repositories\PlantillaRepository;
use Yajra\Datatables\Datatables;

class PlantillaController extends Controller
{

	/**
	 * @var PlantillaRepository
	 */
	private $plantillaRepository;

	public function __construct(PlantillaRepository $plantillaRepository)
	{

		$this->plantillaRepository = $plantillaRepository;
	}

	public function getSearchModule()
	{
		return view('plantillas/modalSearchPlantilla');
    }

	public function getJsonPlantilla($clavecct)
	{
		$result = DB::connection('rh')
			->select('SELECT
                    e.rfc, e.nombre_emp, e.ap_paterno_emp, e.ap_materno_emp,
                    if(epd.cve_area = "0", concat_ws(space(1), ct.clavecct, turno.siglas, ct.nombrect), d_unidad) as adscripcion
                    FROM rh_t_empleado e
                    inner JOIN rh_t_empl_plaza ep on ep.id_empleado = e.id_empleado
                    inner JOIN rh_t_emp_plaza_detalle epd ON epd.id_empleado = ep.id_empleado and epd.id_plaza = ep.id_plaza
                    left join rh_cat_unidades_administrativas ua on ua.id_unidad_admva = epd.cve_area
                    left join rh_cat_escuelas ct on ct.clavecct = epd.cct and ct.turno = epd.turno
                    LEFT JOIN rh_cat_turnos turno ON ct.turno = turno.id_turno
                    WHERE (ua.cct like :clavecct1 or epd.cct like :clavecct2)
                    and epd.estado <>"B"
                    and ep.estado <> "B"
                    and e.estatus_laboral in ("1","A")
                    group by e.id_empleado union


					SELECT
					e.rfc, e.nombre_emp, e.ap_paterno_emp, e.ap_materno_emp,
					if(epd.cve_area = "0", concat_ws(space(1), ct.clavecct, turno.siglas, ct.nombrect), d_unidad) as adscripcion
					FROM rh_t_empleado_externo e
					inner JOIN rh_t_empleado_externo_detalle epd ON epd.id_empleado = e.id_empleado
					left join rh_cat_unidades_administrativas ua on ua.id_unidad_admva = epd.cve_area
					left join rh_cat_escuelas ct on ct.clavecct = epd.cct and ct.turno = epd.turno
					LEFT JOIN rh_cat_turnos turno ON ct.turno = turno.id_turno
					WHERE
					(ua.cct like :clavecct3 or epd.cct like :clavecct4)
					and epd.estado <>"B"
					and e.estatus_laboral in ("1","A")
					group by e.id_empleado',
				['clavecct1' => $clavecct, 'clavecct2' => $clavecct, 'clavecct3' => $clavecct, 'clavecct4' => $clavecct]);

		return response()->json($result);
	}

	public function empleados(Request $request)
	{
		$page = $request->get('page', 1);
		$cct = $request->get('cct');
		return $this->plantillaRepository->empleados($page, $cct);
	}

	public function searchPlantilla()
	{
		return view('plantillas.modal_search_plantilla');
	}

	public function personal(Comision $comision)
	{
		$centro_de_trabajo_id = $comision->centros()->select('centro_de_trabajo_id')->get()->toArray();
		$ccts = CentroLite::whereIn('id', $centro_de_trabajo_id)->select('cct')->get()->toArray();

		$personal = $this->plantillaRepository->personal($ccts);

		return Datatables::of($personal)
			->addColumn('action', function($vocal){
				return '<button
						class="btn btn-success btn-block"
						data-button="add-vocal"
						data-loading-text="Espere..."
						data-nombre="'.$vocal->nombre_emp.'"
						data-primer_apellido="'.$vocal->ap_paterno_emp.'"
						data-segundo_apellido="'.$vocal->ap_materno_emp.'"
						>Agregar</button>';
			})
			->make(true);
	}

}
