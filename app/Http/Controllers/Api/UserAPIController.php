<?php

namespace SeguridadHigiene\Http\Controllers\Api;

Use DB;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\User;
use SeguridadHigiene\Models\UserData;

class UserAPIController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		/**
		 * [
		 * "name" => "john doe"
		 * "username" => "johndoe"
		 * "email" => "johndoe@johndoe.com"
		 * "password" => "johndoe"
		 * ]
		 */

		DB::transaction(function () use ($request) {

			$isAdmin = $request->get('admin');

			$user = User::create([
				'email'    => $request->get('email'),
				'role'     => $isAdmin ? 'operador' : 'invitado',
				'password' => bcrypt($request->get('password')),
				'username' => $request->get('username'),
			]);

			$data = new UserData();
			$data->name = $request->get('name');
			$data->centro_trabajo_id = 0;

			$user->data()->save($data);

		});

		return response()->json(["content" => [], "confirm" => true, "statusCode" => 200]);


	}

	public function changePassword(Request $request, $username)
	{
		$rules = ['password' => 'required|min:6|max:60'];
		$msg = [
			'password.required' => 'El password es requerido',
			'password.min' => 'El password debe contener al menos 6 caracteres',
			'password.max' => 'El password no puede contener mas de 60 caracteres',
		];

		$this->validate($request, $rules, $msg);

		$user = $this->getUser($username);
		$password = $request->get('password');

		$user->update(['password' => bcrypt($password)]);

		return response()->json(["content" => [], "updated" => true, "statusCode" => 200], 200);
	}

	public function changeEmail(Request $request, $username)
	{
		$rules = ['email' => 'required|email'];
		$msg = ['email.required' => 'El email es requerido', 'email.email' => 'El email no es valido'];

		$this->validate($request, $rules, $msg);

		$user = $this->getUser($username);
		$email = $request->get('email');

		$user->update(['email' => $email]);

		return response()->json(["content" => [], "updated" => true, "statusCode" => 200], 200);

	}

	/**
	 * @param $username
	 * @return mixed
	 */
	private function getUser($username)
	{
		$user = User::where('username', $username)->first();

		return $user;
	}


}
