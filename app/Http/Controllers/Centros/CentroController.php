<?php

namespace SeguridadHigiene\Http\Controllers\Centros;

use Exception;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\CentroLite;
use SeguridadHigiene\Repositories\CentroLiteRepository;
use Yajra\Datatables\Datatables;

class CentroController extends Controller
{

	/**
	 * @var CentroLiteRepository
	 */
	private $centroLiteRepository;

	public function __construct(
		CentroLiteRepository $centroLiteRepository
	){
		$this->centroLiteRepository = $centroLiteRepository;
	}

/*	public function getSearchModule()
	{
		return view('centros/modalsearch');
    }

	public function getJsonCentro(Request $request)
	{
		$centros = [];

		switch($request['filter']){
			case 'cct':
				$centros = $this->getCentroByClavecct($request['value']);
				break;
			case 'name':
				$centros = $this->getCentroByName($request['value']);
				break;
			default:
				dd("default");
				break;
		}

		return response()->json($centros);
	}

	public function getCentroByClavecct($clavecct)
	{
		//return $this->centroGEORepository->getCentroByClavecct($clavecct);
	}

	public function getCentroByName($name)
	{
		//return $this->centroGEORepository->getCentroByName($name);
	}*/

	/*
	 * Acciones nuevas
	 *
	 * */

	public function index()
	{
		$centros = CentroLite::paginate(20);
		return $centros;
	}

	public function show($centro)
	{
		$c = $this->centroLiteRepository->findOrFail($centro);
		return $c;
	}

	public function search(Request $request)
	{
		try{
			if($request->query->count()){
				$value = $request->query('filter');
				$by = $request->query('by');
				$centros = $this->centroLiteRepository->search($value, $by);
			}else{
				$centros = $this->centroLiteRepository->all();
			}
			return $centros;
		}catch (Exception $e){

			$getCode = $e->getCode();
			$getFile = $e->getFile();
			$getLine = $e->getLine();
			$getMessage = $e->getMessage();

			return response()->json(compact('getCode', 'getFile', 'getLine', 'getMessage'), 500);
		}

	}

	public function modalIndex()
	{
		return view('centros.modal_index');
	}

	public function centros()
	{
		return view('escuela_lite.index');
	}

	public function buscando()
	{
		$centros = CentroLite::with(['turno'])
			->select('*');

		return Datatables::of($centros)
			->setRowId('id')
			->make(true);
	}

}
