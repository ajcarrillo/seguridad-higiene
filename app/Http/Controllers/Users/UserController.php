<?php

namespace SeguridadHigiene\Http\Controllers\Users;

use DB;
use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\User;
use SeguridadHigiene\Models\UserData;
use SeguridadHigiene\Repositories\CentroLiteRepository;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{

	/**
	 * @var CentroLiteRepository
	 */
	private $centroLiteRepository;

	public function __construct(CentroLiteRepository $centroLiteRepository)
	{

		$this->centroLiteRepository = $centroLiteRepository;
	}

	public function index()
	{
		return view('users.index');
	}

	public function indexData()
	{
		$users = User::with('data')
			->whereNotIn('users.id', [\Auth::user()->id])
			->where('role', '!=', 'admin')
			->select('users.*');



		return Datatables::of($users)
			->addColumn('edit', function ($user) {
				return '<div class="dropdown">
			  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
			    Opciones
			    <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
			    <li role="presentation"><a role="menuitem" tabindex="-1" href="'.route('user.change.rol', $user).'" data-button="change-rol">Cambiar rol</a></li>
			  </ul>
			</div>';
			})
			->make(true);
	}

	public function changeRol(User $user)
	{
		$user = $user::with(['data', 'data.centro_trabajo', 'data.centro_trabajo.turno'])
		->where('id', $user->id)
		->first();

		return view('users.change_rol', compact('user'));
	}

	public function updateRol(Request $request, User $user)
	{
		$role = $request->only('role');
		$centroTrabajoId = $request->only('centro_trabajo_id');

		DB::transaction(function() use ($role, $centroTrabajoId, $user){
			$user->update($role);
			$user->data()->update($centroTrabajoId);
		});

		session()->flash('success', 'Los datos se guardaron correctamente');
		return back();
	}

	public function edit(User $user)
	{
		return view('users.edit', compact('user'));
	}

	public function update(Request $request, User $user)
	{
		$user->update($request->all());
		session()->flash('success', 'Los datos se guardaron correctamente');
		return back();
	}

	public function create()
	{
		return view('users.create');
	}

	public function store(Request $request)
	{
		User::create([
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'primer_apellido' => $request->get('primer_apellido'),
			'segundo_apellido' => $request->get('segundo_apellido'),
			'role' => $request->get('role'),
			'password' => bcrypt($request->get('password')),
			'username' => $request->get('username')
		]);

		return back();
	}

	public function profile(Request $request)
	{
		return view('users.profile');
	}

	public function autocompleteCentros(Request $request)
	{
		return $this->centroLiteRepository->autocomplete($request->get('term'));
	}

}
