<?php

namespace SeguridadHigiene\Http\Controllers\Users;

use Illuminate\Http\Request;

use SeguridadHigiene\Http\Requests;
use SeguridadHigiene\Http\Controllers\Controller;
use SeguridadHigiene\Models\User;

class PasswordController extends Controller
{
	public function changePassword(User $user)
	{
		return view('users.reset_password', compact('user'));
	}

	public function resetPassword(Request $request, User $user)
	{
		$credentials = $request->only('password');
		$user->password = bcrypt($credentials['password']);
		$user->save();

		session()->flash('success', 'La contraseña se cambió correctamente');

		return redirect()->back();
	}
}
