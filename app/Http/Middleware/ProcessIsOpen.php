<?php

namespace SeguridadHigiene\Http\Middleware;

use Carbon\Carbon;
use Closure;
use SeguridadHigiene\Models\EtapaProceso;

class ProcessIsOpen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$etapaProceso = EtapaProceso::find(1);
		$today = Carbon::now();

		if($today >= $etapaProceso->apertura && $today <= $etapaProceso->cierre){
			return $next($request);
		}else{
			if ($request->ajax()){
				return view('layout.processClosed');
			}
			return view('layout.process_closed_no_ajax');
		}
    }
}
