<?php

namespace SeguridadHigiene\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		\Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
		\Styde\Html\Alert\Middleware::class,
		\SeguridadHigiene\Http\Middleware\EncryptCookies::class,
		\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
		\Illuminate\Session\Middleware\StartSession::class,
		\Illuminate\View\Middleware\ShareErrorsFromSession::class,
		\SeguridadHigiene\Http\Middleware\VerifyCsrfToken::class,
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth'          => \SeguridadHigiene\Http\Middleware\Authenticate::class,
		'auth.basic'    => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
		'guest'         => \SeguridadHigiene\Http\Middleware\RedirectIfAuthenticated::class,
		'processIsOpen' => \SeguridadHigiene\Http\Middleware\ProcessIsOpen::class,
		'role'          => \SeguridadHigiene\Http\Middleware\Role::class,
	];
}
