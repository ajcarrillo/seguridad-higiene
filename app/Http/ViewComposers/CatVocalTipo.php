<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/12/15
 * Time: 13:17
 */

namespace SeguridadHigiene\Http\ViewComposers;


use Illuminate\Contracts\View\View;

class CatVocalTipo
{

	public function compose(View $view)
	{

		$vocalTipos = ['1'=>'Propietario', '2'=>'Suplente'];
		$view->with(compact('vocalTipos'));

	}

}
