<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/11/15
 * Time: 15:20
 */

namespace SeguridadHigiene\Http\ViewComposers;


use Illuminate\Contracts\View\View;
use SeguridadHigiene\Models\Sindicato;

class CatSindicatos
{

	public function compose(View $view)
	{
		$sindicatos = Sindicato::lists('nombre', 'id');
		$view->with(compact('sindicatos'));
	}

}
