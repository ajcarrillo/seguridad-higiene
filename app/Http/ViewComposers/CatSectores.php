<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/11/15
 * Time: 10:29
 */

namespace SeguridadHigiene\Http\ViewComposers;


use Illuminate\Contracts\View\View;

class CatSectores
{

	public function compose(View $view)
	{
		$sectores = ['1' => 'Oficial', '2' => 'Sindical'];
		$view->with(compact('sectores'));
	}

}
