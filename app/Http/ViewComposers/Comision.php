<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 11/12/15
 * Time: 11:41
 */

namespace SeguridadHigiene\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use SeguridadHigiene\Models\Comision as ComisionModel;

class Comision
{

	public function compose(View $view)
	{
		$comision = Route::current()->getParameter('comisiones');
		$view->with(compact('comision'));
	}

}
