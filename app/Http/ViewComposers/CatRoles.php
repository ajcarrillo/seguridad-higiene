<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 26/01/16
 * Time: 13:55
 */

namespace SeguridadHigiene\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class CatRoles
{

	public function compose(View $view)
	{

		$roles = [
			'usuario'  => 'Usuario',
			'operador' => 'Operador',
			'invitado' => 'Invitado',
		];
		$view->with(compact('roles'));

	}

}
