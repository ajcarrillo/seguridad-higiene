<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/11/15
 * Time: 11:38
 */

namespace SeguridadHigiene\Http\ViewComposers;


use Illuminate\Contracts\View\View;

class CatVerificacionTipo
{
	public function compose(View $view)
	{
		$verificacionTipos = ['1'=>'Ordinaria', '2'=>'Extraordinaria'];
		$view->with(compact('verificacionTipos'));
	}
}
