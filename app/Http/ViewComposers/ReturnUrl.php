<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/12/15
 * Time: 9:56
 */

namespace SeguridadHigiene\Http\ViewComposers;


use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;

class ReturnUrl
{

	public function compose(View $view)
	{
		$params = Request::only('returnURL');
		if($params['returnURL'] != null){
			$returnURL = $params['returnURL'];

			$view->with(compact('params', 'returnURL'));
		}

	}

}
