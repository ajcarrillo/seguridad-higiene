<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/11/15
 * Time: 22:21
 */

namespace SeguridadHigiene\Http\ViewComposers;


use DB;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Request;
use SeguridadHigiene\Models\ComisionCentro;

class ComisionCentros
{
	public function compose(View $view)
	{
		$params = Request::only('comision');

		if($params['comision'] != null){
			$centros = DB::connection('rh')
				->table('rh_cat_escuelas_lite')
				->join('seguridadhigiene.t_comision_centros_de_trabajo', 'seguridadhigiene.t_comision_centros_de_trabajo.centro_de_trabajo_id', '=', 'rh_cat_escuelas_lite.id')
				->where('seguridadhigiene.t_comision_centros_de_trabajo.comision_id', '=', $params['comision'])
				->lists('rh_cat_escuelas_lite.cct');
		}

		$view->with(compact('params', 'centros'));
	}

}
