<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/10/15
 * Time: 16:47
 */
Route::get('create', [
	'uses' => 'Comisiones\ComisionController@create',
	'as'   => 'comision.create'
]);

Route::post('', [
	'uses' => 'Comisiones\ComisionController@store',
	'as'   => 'comision.store'
]);

Route::get('{comisiones}/enviar', [
	'uses' => 'Comisiones\ComisionController@enviar',
	'as'   => 'comision.enviar'
]);

Route::post('{comisiones}/sent', [
	'uses' => 'Comisiones\ComisionController@sent',
	'as'   => 'comision.sent'
]);

Route::get('{comision}/apartados', [
	'uses' => 'Comisiones\ComisionController@sections',
	'as'   => 'comision.sections'
]);

Route::get('{comisiones}/oficios', [
	'uses' => 'Comisiones\ComisionController@edit',
	'as'   => 'comision.edit'
]);

Route::patch('{comisiones}/oficios', [
	'uses' => 'Comisiones\ComisionController@update',
	'as'   => 'comision.update'
]);

Route::get('{comisiones}/plantilla/trabajadores/edit', [
	'uses' => 'Comisiones\ComisionController@editTotalTrabajadores',
	'as'   => 'comision.edit.trabajadores'
]);

Route::patch('{comisiones}/plantilla/trabajadores/updateTotalTrabajadores', [
	'uses' => 'Comisiones\ComisionController@updateTotalTrabajadores',
	'as'   => 'comision.update.trabajadores'
]);
