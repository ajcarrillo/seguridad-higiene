<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/07/16
 * Time: 5:12 PM
 */
Route::group(['middleware' => ['auth', 'role:admin'], 'prefix'=>'developer'], function () {
	Route::get('api-urls/', ['as'=>'api.urls', function(){
		return view('developer_zone.api_urls');
	}]);
});
