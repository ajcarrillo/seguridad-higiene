<?php
/**
 * Created by PhpStorm.
 * User: wsanchez
 * Date: 10/12/2015
 * Time: 04:08 PM
 */

Route::get('', [
    'uses'=>'Catalogos\CatConfiguracionSistemaController@index',
    'as' => 'get.index.configuracion']);
Route::post('update', [
    'uses'=>'Catalogos\CatConfiguracionSistemaController@update',
    'as' => 'post.update.configuracion']);