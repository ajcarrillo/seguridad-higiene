<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 9:53
 *
 * URL: /actas/
 *
 */

Route::get('/', [
	'uses' => 'Actas\ActaController@index',
	'as'   => 'actas'
]);

Route::get('{reportes}/descargar', [
	'uses' => 'Actas\ActaController@downloadActa',
	'as'   => 'actas.download'
]);

/*Route::get('detalle/{id}/', [
    'uses'  =>  'Actas\ActaController@details',
    'as'    =>  'actas.details'
]);

Route::get('registro/', [
    'uses'  =>  'Actas\ActaController@getCreate',
    'as'    =>  'actas.get.create'
]);

Route::get('{comisionId}/registro', [
	'uses'	=>	'Actas\ActaController@getRegistroEdit',
	'as'	=>	'get.registro.edit'
]);

Route::put('{comisionId}/registro', [
	'uses'	=>	'Actas\ActaController@putRegistroEdit',
	'as'	=>	'put.registro.edit'
]);

Route::post('registro/', [
	'uses'  =>  'Actas\ActaController@postCreate',
	'as'    =>  'actas.post.create'
]);

Route::post('create/',[
	'uses'	=> 'Actas\ActaController@postCreate',
	'as'	=>	'actas.post.create'
]);

Route::post('{comisionId}/centro', [
	'uses'	=>	'Actas\ActaController@postComisionCentro',
	'as'	=>	'post.comision.centro'
]);

Route::delete('{comisionId}/centro', [
	'uses'	=>	'Actas\ActaController@deleteComisionCentro',
	'as'	=>	'delete.comision.centro'
]);

Route::put('{comisionId}/presidente', [
	'uses'	=>	'Actas\ActaController@putComisionPresidente',
	'as'	=>	'put.comision.presidente'
]);

Route::put('{comisionId}/secretario',[
	'uses'	=>	'Actas\ActaController@putComisionSecretario',
	'as'	=>	'put.comision.secretario'
]);


Route::delete('{comisionId}/vocal',[
	'uses'	=>	'Actas\ActaController@deleteComisionVocal',
	'as'	=>	'delete.comision.vocal'
]);
Route::put('{comisionId}/vocal',[
	'uses'	=>	'Actas\ActaController@putComisionVocal',
	'as'	=>	'put.comision.vocal'
]);
Route::post('{comisionId}/vocal',[
	'uses'	=>	'Actas\ActaController@postComisionVocal',
	'as'	=>	'post.comision.vocal'
]);*/
