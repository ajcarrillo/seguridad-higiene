<?php

Route::get('index', [
    'uses'  =>  'Admin\AdminController@index',
    'as'    =>  'admin'
]);
Route::get('enviadasJson', [
    'uses'  =>  'Admin\AdminController@enviadasJson',
    'as'    =>  'get.admin.enviadasJson'
]);
Route::get('sinClaveComisionJson', [
    'uses'  =>  'Admin\AdminController@sinClaveComisionJson',
    'as'    =>  'get.admin.sinClaveComisionJson'
]);
Route::get('terminadasJson', [
    'uses'  =>  'Admin\AdminController@terminadasJson',
    'as'    =>  'get.admin.terminadasJson'
]);
Route::get('conObservacionJson', [
    'uses'  =>  'Admin\AdminController@conObservacionJson',
    'as'    =>  'get.admin.conObservacionJson'
]);
Route::get('edit/{reportes}', [
    'uses' => 'Admin\AdminController@reportes',
    'as' => 'admin.edit'
]);
Route::post('grabaComision/{reportes}', [
    'uses' => 'Admin\AdminController@grabaComision',
    'as' => 'admin.grabaComision'
]);
Route::get('/observacion/{reportes}/', [
    'uses' => 'Admin\AdminController@Observacion',
    'as' => 'get.observacion.acta'
]);
Route::post('grabaObservacion/{reportes}', [
    'uses' => 'Admin\AdminController@grabaObservacion',
    'as' => 'post.guardar-observacion'
]);
Route::get('/AsignaClaveComision/{reportes}/', [
    'uses' => 'Admin\AdminController@AsignaClaveComision',
    'as' => 'get.asignaClaveComison.acta'
]);
Route::post('grabaClaveComision/{reportes}', [
    'uses' => 'Admin\AdminController@grabaClaveComision',
    'as' => 'post.guardar-clave-comision'
]);
Route::get('/impresion/{reportes}', [
    'uses' => 'Admin\AdminController@impresion',
    'as' => 'get.impresion.acta'
]);
Route::get('/ver/{reportes}', [
    'uses' => 'Admin\AdminController@consultar',
    'as' => 'get.ver.acta'
]);

Route::get('/reports/{reportes}', [
    'uses' => 'Admin\AdminController@reports',
    'as' => 'admin.reports'
]);
