<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 03/10/15
 * Time: 0:50
 *
 * URL: verificaciones/
 *
 */
Route::get('reporte/{reportes}/', [
	'uses'	=>	'Verificaciones\VerificacionController@getCreate',
	'as'	=>	'get.create.verificacion'
]);

Route::post('reporte/{reportes}/', [
	'uses'	=>	'Verificaciones\VerificacionController@postCreate',
	'as'	=>	'post.create.verificacion'
]);

Route::get('{verificacion}', [
	'uses'	=>	'Verificaciones\VerificacionController@getEdit',
	'as'	=>	'get.edit.verificacion'
]);

Route::patch('{verificacionId}', [
	'uses'	=>	'Verificaciones\VerificacionController@postEdit',
	'as'	=>	'post.edit.verificacion'
]);

Route::post('{verificacionId}/incidencias', [
	'uses'	=>	'Verificaciones\VerificacionIncidenciaController@store',
	'as'	=>	'post.create.incidencia'
]);

Route::delete('incidencia/{incidenciaId}', [
	'uses'	=>	'Verificaciones\VerificacionIncidenciaController@delete',
	'as'	=>  'delete.delete.incidencia'
]);
