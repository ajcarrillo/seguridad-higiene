<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 11/03/16
 * Time: 12:12 PM
 *
 * URL: /calendarios/
 *
 */
Route::get('{calendario}/actividades', [
	'uses' 	=> 	'Calendarios\ActividadController@index',
	'as'	=>	'actividad.index'
]);


Route::get('{calendario}/actividades/create', [
	'uses' 	=> 	'Calendarios\ActividadController@create',
	'as'	=>	'actividad.create'
]);

Route::post('{calendario}/actividades', [
	'uses'	=>	'Calendarios\ActividadController@store',
	'as'	=>	'actividad.store'
]);

Route::delete('actividad/{actividad}', [
	'uses'	=>	'Calendarios\ActividadController@destroy',
	'as'	=>	'actividad.delete'
]);

Route::get('actividades/{actividad}/edit', [
	'uses'	=>	'Calendarios\ActividadController@edit',
	'as'	=>	'actividad.edit'
]);

Route::patch('actividades/{actividad}', [
	'uses'	=>	'Calendarios\ActividadController@update',
	'as'	=>	'actividad.update'
]);
