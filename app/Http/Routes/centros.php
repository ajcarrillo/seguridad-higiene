<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 15:46
 */
Route::get('buscar/',[
	'uses'	=> 'Centros\CentroController@getSearchModule',
	'as'	=> 'centros.search.module'
]);

Route::get('json/{cct?}/',[
	'uses'	=> 'Centros\CentroController@getJsonCentro',
	'as'	=>	'centros.get.json'
]);

/*
 * Nuevas rutas
 *
 * */

Route::get('', [
	'uses' 	=> 	'Centros\CentroController@index',
	'as'	=>	'centro.index'
]);

Route::get('modal', [
	'uses'	=>	'Centros\CentroController@modalIndex',
	'as'	=>	'centro.modal.index'
]);

Route::get('search/', [
	'uses'	=> 	'Centros\CentroController@search',
	'as'	=>	'centro.search'
]);

Route::get('buscando/',[
	'uses'	=> 	'Centros\CentroController@buscando',
	'as'	=>	'centro.buscando'
]);

Route::get('{centro}', [
	'uses'	=>	'Centros\CentroController@show',
	'as'	=>	'centro.show'
]);

Route::get('buscar/',[
	'uses'	=> 	'Centros\CentroController@centros',
	'as'	=>	'centro.centros'
]);
