<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/10/15
 * Time: 13:08
 *
 * URL: /calendarios/
 *
 */
Route::get('reporte/{reportes}', [
	'uses'	=> 	'Calendarios\CalendarioController@getCreate',
	'as'	=>	'get.create.calendario'
]);

Route::post('reporte/{reportes}', [
	'uses'	=>	'Calendarios\CalendarioController@postCreate',
	'as'	=>	'post.create.calendario'
]);
