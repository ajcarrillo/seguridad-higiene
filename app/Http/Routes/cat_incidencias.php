<?php
/**
 * Created by PhpStorm.
 * User: lewis_rm
 * Date: 12/10/2015
 * Time: 11:02 AM
 */

Route::group(['middleware' => ['auth', 'role:operador'], 'prefix' => '/admin/catalogos/incidencia'], function () {
	Route::get('/', [
		'uses' => 'Catalogos\IncidenciaController@index',
		'as'   => 'incidencia.index',
	]);
	Route::get('indexData', [
		'uses' => 'Catalogos\IncidenciaController@indexData',
		'as'   => 'incidencia.index.data',
	]);
	Route::get('edit/{incidencia}', [
		'uses' => 'Catalogos\IncidenciaController@edit',
		'as'   => 'incidencia.edit',
	]);
	Route::post('update/{incidencia}', [
		'uses' => 'Catalogos\IncidenciaController@update',
		'as'   => 'incidencia.update',
	]);
	Route::get('create', [
		'uses' => 'Catalogos\IncidenciaController@create',
		'as'   => 'incidencia.create',
	]);
	Route::post('store', [
		'uses' => 'Catalogos\IncidenciaController@store',
		'as'   => 'incidencia.store',
	]);
	Route::get('destroy/{id}', [
		'uses' => 'Catalogos\IncidenciaController@destroy',
		'as'   => 'incidencia.destroy',
	]);
});
