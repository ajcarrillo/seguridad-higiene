<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 06/11/15
 * Time: 11:38
 */

Route::get('{comisiones}/presidentes', [
	'uses'	=>	'Comisiones\PresidenteController@index',
	'as'	=>	'presidente.index'
]);

Route::get('{comision}/presidentes/create', [
	'uses'	=>	'Comisiones\PresidenteController@create',
	'as'	=>	'presidente.create'
]);

Route::post('{comision}/presidentes/', [
	'uses'	=>	'Comisiones\PresidenteController@store',
	'as'	=>	'presidente.store'
]);

Route::get('{comision}/presidentes/{presidente}/edit', [
	'uses'	=>	'Comisiones\PresidenteController@edit',
	'as'	=>	'presidente.edit'
]);

Route::patch('{comision}/presidentes/{presidente}', [
	'uses'	=>	'Comisiones\PresidenteController@update',
	'as'	=>	'presidente.update'
]);
