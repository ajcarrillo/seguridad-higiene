<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 02/11/15
 * Time: 15:50
 */
Route::get('{comisiones}/vocales', [
	'uses'	=>	'ComisionVocales\ComisionVocalController@index',
	'as'	=>	'vocales.index'
]);

Route::get('{comisiones}/vocales/create', [
	'uses'	=>	'ComisionVocales\ComisionVocalController@create',
	'as'	=>	'vocales.create'
]);

Route::get('{comisiones}/vocales/create/plantilla', [
	'uses'	=>	'ComisionVocales\ComisionVocalController@createPlantilla',
	'as'	=>	'vocales.create.plantilla'
]);


Route::post('{comisiones}/vocales', [
	'uses'	=>	'ComisionVocales\ComisionVocalController@store',
	'as'	=>	'vocales.store'
]);

Route::delete('{comisiones}/vocales/{vocal}',[
	'uses'	=>	'ComisionVocales\ComisionVocalController@destroy',
	'as'	=>	'vocales.destroy'
]);

Route::get('{comision}/vocales/datatables', [
	'uses'=>'ComisionVocales\ComisionVocalController@vocalesDatatable',
	'as'=>'vocales.datatables'
]);
