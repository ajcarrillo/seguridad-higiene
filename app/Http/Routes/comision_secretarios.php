<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/11/15
 * Time: 10:39
 */

Route::get('{comisiones}/secretarios', [
	'uses'	=>	'Comisiones\SecretarioController@index',
	'as'	=>	'secretario.index'
]);

Route::get('{comision}/secretarios/create', [
	'uses'	=>	'Comisiones\SecretarioController@create',
	'as'	=>	'secretario.create'
]);

Route::post('{comision}/secretarios/', [
	'uses'	=>	'Comisiones\SecretarioController@store',
	'as'	=>	'secretario.store'
]);

Route::get('{comision}/secretarios/{secretario}/edit', [
	'uses'	=>	'Comisiones\SecretarioController@edit',
	'as'	=>	'secretario.edit'
]);

Route::patch('{comision}/secretarios/{secretario}', [
	'uses'	=>	'Comisiones\SecretarioController@update',
	'as'	=>	'secretario.update'
]);
