<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 16:06
 */
Route::get('buscar/',[
	'uses'	=> 'Plantillas\PlantillaController@getSearchModule',
	'as'	=> 'plantillas.search.module'
]);

Route::get('json/{clavecct}/',[
	'uses'	=> 'Plantillas\PlantillaController@getJsonPlantilla',
	'as'	=> 'plantillas.get.json'
]);

Route::get('empleados',[
	'uses'	=>	'Plantillas\PlantillaController@empleados',
	'as'	=>	'plantillas.empleados'
]);

Route::get('/', [
	'uses'	=>	'Plantillas\PlantillaController@searchPlantilla',
	'as'	=>	'plantillas.searchPlantilla'
]);

Route::get('total', [
	'uses'=>'Plantillas\PlantillaController@total',
	'as'=>'plantilla.total'
]);

Route::get('personal/{comisiones}', [
	'uses'	=> 'Plantillas\PlantillaController@personal',
	'as'	=> 'plantillas.personal'
]);
