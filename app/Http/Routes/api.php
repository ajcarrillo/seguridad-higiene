<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 30/05/16
 * Time: 4:11 PM
 */


Route::group(['prefix' => 'api/'], function () {

	Route::resource('user', 'Api\UserAPIController',
		['only' => ['store']]);

	Route::group(['prefix' => 'user'], function () {
		Route::post('{username}/change-password', [
			'uses' => 'Api\UserAPIController@changePassword',
			'as'   => 'user.reset.password',
		]);

		Route::post('{username}/change-email', [
			'uses' => 'Api\UserAPIController@changeEmail',
			'as'   => 'user.reset.email',
		]);
	});


});
