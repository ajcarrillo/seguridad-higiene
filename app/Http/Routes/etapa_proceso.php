<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/01/16
 * Time: 13:32
 */
Route::get('etapas', [
	'uses' => 'Admin\EtapaProcesoController@index',
	'as'   => 'etapa.index'
]);

Route::get('etapas/{etapa}/edit', [
	'uses'=>'Admin\EtapaProcesoController@edit',
	'as'=>'etapa.edit'
]);

Route::patch('etapas/{etapa}', [
	'uses'=>'Admin\EtapaProcesoController@update',
	'as'=>'etapa.update'
]);
