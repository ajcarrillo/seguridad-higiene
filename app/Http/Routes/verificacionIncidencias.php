<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 21/10/15
 * Time: 15:38
 *
 * URL: incidencias/
 *
 */
Route::get('verificacion/{verificacion}', [
	'uses' => 'Verificaciones\VerificacionIncidenciaController@create',
	'as'   => 'verificacion.incidencia.create'
]);


Route::patch('{incidenciaId}/subsanar', [
	'uses' => 'Verificaciones\VerificacionIncidenciaController@patchSubsanar',
	'as'   => 'patch.subsanar.incidencia'
]);

Route::get('subsistentes/', [
	'uses' => 'Verificaciones\VerificacionIncidenciaController@getSubsistentes',
	'as'   => 'get.incidencias.subsistentes'
]);

Route::get('subsanadas/', [
	'uses' => 'Verificaciones\VerificacionIncidenciaController@getSubsanadas',
	'as'   => 'get.incidencias.subsanadas'
]);
