<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 25/11/15
 * Time: 10:52
 */

Route::group(['prefix' => '{reporteId}/actualizacion'], function () {
	Route::get('create', [
		'uses' => 'Actualizaciones\ActualizacionController@create',
		'as'   => 'actualizacion.create'
	]);

	Route::post('', [
		'uses' => 'Actualizaciones\ActualizacionController@store',
		'as'   => 'actualizacion.store'
	]);

	Route::get('{actualizacion}', [
		'uses' => 'Actualizaciones\ActualizacionController@show',
		'as'   => 'actualizacion.show'
	]);

	Route::get('{actualizacion}/edit', [
		'uses' => 'Actualizaciones\ActualizacionController@edit',
		'as'   => 'actualizacion.edit'
	]);

	Route::patch('{actualizacion}', [
		'uses' => 'Actualizaciones\ActualizacionController@update',
		'as'   => 'actualizacion.update'
	]);

	Route::get('comision', [
		'uses' => 'Actualizaciones\ActualizacionController@sections',
		'as'   => 'actualizacion.sections'
	]);
});

/*Route::group(['prefix' => '{reporteId}/actualizacion/comision'], function () {
	Route::get('centros', [
		'uses' => 'Actualizaciones\ActualizacionController@centros',
		'as'   => 'actualizacion.centros'
	]);
});*/

/*Route::get('{reporteId}/actualizacion/comision/centros', function(){
	return view('actualizaciones.centros.index');
});*/
