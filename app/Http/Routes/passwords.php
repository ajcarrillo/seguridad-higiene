<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 1:08
 */
// Password reset link request routes...
Route::get('email', 'Auth\PasswordController@getEmail');
Route::post('email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('reset/{token}', 'Auth\PasswordController@getReset');
Route::post('reset', 'Auth\PasswordController@postReset');
