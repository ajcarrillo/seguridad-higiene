<?php
Route::group(['prefix' => '{reporteId}/actualizacion/comision'], function () {
	Route::get('presidentes', [
		'uses' => 'Actualizaciones\PresidenteTransaccionController@index',
		'as'   => 'actualizacion.presidente.index'
	]);
	Route::post('presidentes', [
		'uses' => 'Actualizaciones\PresidenteTransaccionController@store',
		'as'   => 'actualizacion.presidente.store'
	]);
	Route::patch('presidentes/{transaccionId}', [
		'uses' => 'Actualizaciones\PresidenteTransaccionController@update',
		'as'   => 'actualizacion.presidente.update'
	]);
});
