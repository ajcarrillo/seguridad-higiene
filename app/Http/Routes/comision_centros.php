<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/10/15
 * Time: 13:07
 */
Route::get('{comisiones}/centros', [
	'uses'	=>	'ComisionCentros\ComisionCentroController@index',
	'as'	=>	'comision.centros.index'
]);

Route::get('{comisiones}/centros/create',[
	'uses'	=>	'ComisionCentros\ComisionCentroController@create',
	'as'	=>	'comision.centros.create'
]);

Route::post('{comisiones}/centros/',[
	'uses'	=>	'ComisionCentros\ComisionCentroController@store',
	'as'	=>	'comision.centros.store'
]);

Route::delete('{comision}/centros/{centro}', [
	'uses'	=>	'ComisionCentros\ComisionCentroController@destroy',
	'as'	=>	'comision.centros.destroy'
]);

Route::get('{comision}/datatables/centros/', [
	'uses'	=>	'ComisionCentros\ComisionCentroController@dataTablesCentros',
	'as'	=>	'comision.centros.data'
]);

/*Route::get('{comision}/centros/create', [
	'uses'	=>	'',
	'as'	=>	'comision.centros.create'
]);



Route::get('{comision}/centros/{centro}', [
	'uses'	=>	'',
	'as'	=>	'comision.centros.show'
]);

Route::get('{comision}/centros/{centro}/edit', [
	'uses'	=>	'',
	'as'	=>	'comision.centros.edit'
]);

Route::patch('{comision}/centros/{centro}', [
	'uses'	=>	'',
	'as'	=>	'comision.centros.update'
]);*/
