<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 08/09/15
 * Time: 20:45
 */
Route::get('/', [
	'uses'	=> 	'Comisiones\ComisionController@index',
	'as'	=>	'comisiones'
]);

Route::get('create/', [
	'uses'	=>	'Comisiones\ComisionController@getCreate',
	'as'	=>	'comisiones.get.create'
]);

Route::post('create/', [
	'uses'	=>	'Comisiones\ComisionController@postCreate',
	'as'	=>	'comisiones.post.create'
]);
