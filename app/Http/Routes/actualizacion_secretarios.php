<?php
Route::group(['prefix' => '{reporteId}/actualizacion/comision'], function () {
	Route::get('secretario', [
		'uses' => 'Actualizaciones\SecretarioTransaccionController@index',
		'as'   => 'actualizacion.secretario.index'
	]);
	Route::post('secretario', [
		'uses' => 'Actualizaciones\SecretarioTransaccionController@store',
		'as'   => 'actualizacion.secretario.store'
	]);
	Route::patch('secretario/{transaccionId}', [
		'uses' => 'Actualizaciones\SecretarioTransaccionController@update',
		'as'   => 'actualizacion.secretario.update'
	]);
});
