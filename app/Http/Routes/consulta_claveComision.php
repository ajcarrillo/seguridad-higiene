<?php
/**
 * Created by PhpStorm.
 * User: wsanchez
 * Date: 26/11/2015
 * Time: 05:14 PM
 */

Route::get('index', [
    'uses'=>'Consultas\ClaveComisionController@index',
    'as' => 'get.index.claveComision']);

Route::get('datos', [
    'uses'=>'Consultas\ClaveComisionController@datos',
    'as' => 'claveComision.datos']);