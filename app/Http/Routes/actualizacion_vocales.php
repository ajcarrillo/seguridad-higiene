<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 03/12/15
 * Time: 14:37
 */
Route::group(['prefix' => '{reportes}/actualizacion/comision'], function () {
	Route::get('vocales', [
		'uses' => 'Actualizaciones\VocalTransaccionController@index',
		'as'   => 'actualizacion.vocales.index'
	]);

	Route::get('vocales/create/transaccion',[
		'uses' => 'Actualizaciones\VocalTransaccionController@createVocalTransaccion',
		'as'   => 'actualizacion.vocales.createTransaccion'
	]);

	Route::get('vocales/create',[
		'uses' => 'Actualizaciones\VocalTransaccionController@create',
		'as'   => 'actualizacion.vocales.create'
	]);

	Route::post('vocales', [
		'uses' => 'Actualizaciones\VocalTransaccionController@store',
		'as'   => 'actualizacion.vocales.store'
	]);

	Route::delete('vocales/{transaccion}', [
		'uses' => 'Actualizaciones\VocalTransaccionController@destroy',
		'as'   => 'actualizacion.vocales.destroy'
	]);
});
