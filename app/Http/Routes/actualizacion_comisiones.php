<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 03/12/15
 * Time: 11:33
 */
Route::group(['prefix' => '{reportes}/actualizacion/comision'], function () {
	Route::get('trabajadores', [
		'uses'=>'Actualizaciones\ActualizacionController@trabajadoresEdit',
		'as'=>'comision.trabajadores.edit'
	]);

	Route::post('trabajadores', [
		'uses'=>'Actualizaciones\ActualizacionController@trabajadoresUpdate',
		'as'=>'comision.trabajadores.update'
	]);
});
