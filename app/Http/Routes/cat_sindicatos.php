<?php
/**
 * Created by PhpStorm.
 * User: wsanchez
 * Date: 12/10/2015
 * Time: 11:02 AM
 */

require_once(__DIR__ . '/helpers.php');

Route::get('', [
    'uses'=>'Catalogos\CatSindicatoController@index',
    'as' => 'get.index.sindicato']);


Route::delete('/{sindicatoId}', [
    'uses' => 'Catalogos\CatSindicatoController@eliminar',
    'as' => 'delete.sindicato'
]);
Route::get('/nuevo', [
    'uses' => 'Catalogos\CatSindicatoController@agregar',
    'as' => 'get.agregar.sindicato'
]);
Route::get('/edita/{id}/', [
    'uses' => 'Catalogos\CatSindicatoController@editar',
    'as' => 'get.editar.sindicato'
]);
Route::get('/lista', [
    'uses' => 'Catalogos\CatSindicatoController@lista',
    'as' => 'get.lista.sindicatos'
]);
Route::post('/nuevo', [
    'uses' => 'Catalogos\CatSindicatoController@postAgregar',
    'as' => 'post.agregar.sindicato'
]);
Route::post('/edita', [
    'uses' => 'Catalogos\CatSindicatoController@postModificar',
    'as' => 'post.modificar.sindicato'
]);