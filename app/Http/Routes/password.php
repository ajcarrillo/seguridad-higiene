<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/01/16
 * Time: 10:56
 */
Route::get('{user}/reset-password', [
	'uses' => 'Users\PasswordController@changePassword',
	'as'   => 'change.password'
]);

Route::post('{user}/reset-password', [
	'uses' => 'Users\PasswordController@resetPassword',
	'as'   => 'reset.password'
]);
