<?php

/*
 *
 * URL: /usuarios/
 *
 * */

Route::get('/', [
	'uses' => 'Users\UserController@index',
	'as'   => 'user.index',
]);

Route::get('index-data', [
	'uses' => 'Users\UserController@indexData',
	'as'   => 'user.index.data',
]);

Route::get('change-rol/{user}', [
	'uses' => 'Users\UserController@changeRol',
	'as'   => 'user.change.rol',
]);

Route::post('change-rol/{user}', [
	'uses' => 'Users\UserController@updateRol',
	'as'   => 'user.update.rol',
]);

Route::get('create', [
	'uses' => 'Users\UserController@create',
	'as'   => 'user.create',
]);

Route::post('/', [
	'uses' => 'Users\UserController@store',
	'as'   => 'user.store',
]);

Route::get('{user}/edit', [
	'uses' => 'Users\UserController@edit',
	'as'   => 'user.edit',
]);

Route::patch('{user}', [
	'uses' => 'Users\UserController@update',
	'as'   => 'user.update',
]);

Route::get('perfil', [
	'uses' => 'Users\UserController@profile',
	'as'   => 'user.profile',
]);
