<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 29/11/15
 * Time: 18:12
 */


Route::group(['prefix' => '{reportes}/actualizacion/comision'], function () {
	Route::get('centros', [
		'uses' => 'Actualizaciones\CentroTransaccionController@index',
		'as'   => 'actualizacion.centros'
	]);

	Route::get('centros/create', [
		'uses' => 'Actualizaciones\CentroTransaccionController@create',
		'as'   => 'actualizacion.centros.create'
	]);

	Route::post('centros', [
		'uses' => 'Actualizaciones\CentroTransaccionController@store',
		'as'   => 'actualizacion.centros.store'
	]);

	Route::delete('centros/{id}', [
		'uses' => 'Actualizaciones\CentroTransaccionController@destroy',
		'as'   => 'actualizacion.centros.destroy'
	]);
});
