<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 17/06/16
 * Time: 12:12 PM
 *
 * URL: /autocomplete/
 *
 */

Route::group(['prefix' => 'autocomplete'], function () {
	Route::get('centros', [
		'uses' => 'Users\UserController@autocompleteCentros',
		'as'   => 'autocomplete.centros',
	]);
});


