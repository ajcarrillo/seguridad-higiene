<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 22/09/15
 * Time: 13:04
 */

Route::get('redirect/{reporteId}/',[
	'uses'	=> 	'Reportes\ReporteController@getReporte',
	'as'	=>	'get.reporte'
]);

Route::get('{reporteId}/enviar', [
	'uses'	=> 	'Reportes\ReporteController@send',
	'as'	=>	'reporte.send'
]);

Route::post('{reporteId}/enviar', [
	'uses'	=> 	'Reportes\ReporteController@sendUpdate',
	'as'	=>	'reporte.sendUpdate'
]);

//Obtener form para crear nuevo reporte
Route::get('/nuevo', [
	'uses'	=>	'Reportes\ReporteController@getCreate',
	'as'	=>	'get.reporte.create'
]);

Route::post('/nuevo', [
	'uses'	=>	'Reportes\ReporteController@postCreate',
	'as'	=>	'post.reporte.create'
]);

Route::get('check/{comision_id}', [
	'uses'	=>	'Reportes\ReporteController@hasReporte',
	'as'	=>	'hasReporte'
]);

Route::get('{reportes}/observaciones', [
	'uses'	=>	'Reportes\ReporteController@reject',
	'as'	=>	'reporte.reject'
]);
