<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 1:05
 */
// Authentication routes...
Route::get('login', [
	'uses' 	=> 'Auth\AuthController@getLogin',
	'as'	=> 'login'
]);

Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', [
	'uses'	=> 'Auth\AuthController@getLogout',
	'as'	=> 'logout'
]);

// Registration routes...
Route::get('register', [
	'uses'	=> 'Auth\AuthController@getRegister',
	'as'	=> 'register'
]);
Route::post('register', 'Auth\AuthController@postRegister');
