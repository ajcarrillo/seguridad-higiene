<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


$proxy_url    = env('PROXY_URL');
$proxy_schema = env('PROXY_SCHEMA');

if (!empty($proxy_url)) {
	URL::forceRootUrl($proxy_url);
}

if (!empty($proxy_schema)) {
	URL::forceSchema($proxy_schema);
}


include __DIR__ . '/Routes/api.php';
include __DIR__ . '/Routes/autocomplete.php';
include __DIR__ . '/Routes/developer_zone.php';

Route::get('/', function () {
	return response()->redirectToRoute('login');
});

Route::get('login', function () {
	return response()->redirectToRoute('login');
});

Route::get('logout', function () {
	return response()->redirectToRoute('logout');
});

Route::group(['middleware' => ['auth', 'role:usuario'], 'prefix' => 'actas/'], function () {
	require(__DIR__ . '/Routes/actas.php');
});

Route::group(['middleware' => 'auth', 'prefix' => 'centros/'], function () {
	require(__DIR__ . '/Routes/centros.php');
});

Route::group(['middleware' => 'auth', 'prefix' => 'comisiones/'], function () {
	require(__DIR__ . '/Routes/comisiones.php');
});

Route::group(['middleware' => 'auth', 'prefix' => 'plantillas/'], function () {
	require(__DIR__ . '/Routes/plantillas.php');
});

Route::group(['prefix' => 'auth/'], function () {
	require(__DIR__ . '/Routes/auth.php');
});

Route::group(['prefix' => 'password/'], function () {
	require(__DIR__ . '/Routes/passwords.php');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'reportes/'], function () {
	require(__DIR__ . '/Routes/reportes.php');
});

Route::group(['middleware' => ['auth', 'role:usuario', 'processIsOpen'], 'prefix' => 'reportes/'], function () {
	require(__DIR__ . '/Routes/actualizaciones.php');
	require(__DIR__ . '/Routes/actualizacion_centros.php');
	require(__DIR__ . '/Routes/actualizacion_comisiones.php');
	require(__DIR__ . '/Routes/actualizacion_presidentes.php');
	require(__DIR__ . '/Routes/actualizacion_secretarios.php');
	require(__DIR__ . '/Routes/actualizacion_vocales.php');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'verificaciones/'], function () {
	require(__DIR__ . '/Routes/verificaciones.php');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'incidencias/'], function () {
	require(__DIR__ . '/Routes/verificacionIncidencias.php');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'calendarios/'], function () {
	require(__DIR__ . '/Routes/calendarios.php');
	require(__DIR__ . '/Routes/calendario_actividad.php');
});

Route::group(['middleware' => ['auth', 'role:operador'], 'prefix' => 'admin/'], function () {
	require(__DIR__ . '/Routes/admin.php');
});

include __DIR__ . '/Routes/cat_incidencias.php';

Route::group(['prefix' => 'admin/catalogos/configuracion'], function () {
	require(__DIR__ . '/Routes/cat_configuracion_sistema.php');
});

Route::group(['prefix' => 'admin/catalogos/sindicatos'], function () {
	require(__DIR__ . '/Routes/cat_sindicatos.php');
});

Route::group(['prefix' => 'admin/consulta/claveComision'], function () {
	require(__DIR__ . '/Routes/consulta_claveComision.php');
});
Route::group(['middleware' => ['auth'], 'prefix' => 'centro/'], function () {
	require(__DIR__ . '/Routes/centros.php');
});

Route::group(['middleware' => ['auth', 'role:usuario'], 'prefix' => 'comision/'], function () {
	require(__DIR__ . '/Routes/comision.php');
	require(__DIR__ . '/Routes/comision_centros.php');
	require(__DIR__ . '/Routes/comision_vocales.php');
	require(__DIR__ . '/Routes/comision_presidentes.php');
	require(__DIR__ . '/Routes/comision_secretarios.php');
});

Route::group(['middleware' => ['auth', 'role:operador'], 'prefix' => 'usuarios'], function () {
	require(__DIR__ . '/Routes/users.php');
	require(__DIR__ . '/Routes/password.php');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'configuracion'], function () {
	require(__DIR__ . '/Routes/etapa_proceso.php');
});

Route::get('libre/', ['as'=>'libre', function () {
	dd('libre');
}]);

Route::get('acceso-denegado', ['as'=>'invitado', 'middleware'=>['auth'], function(){
	return view('plantillas.invitado');
}]);
