<?php

namespace SeguridadHigiene\Http\Requests;

use SeguridadHigiene\Http\Requests\Request;

class CreateComision extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'fecha_oficio_oficial' => 'required',
			'fecha_oficio_sindical' => 'required',
			'sindicato_id' => 'required',
			'num_oficio_sindical' => 'required',
			'num_oficio_oficial' => 'required'
        ];
    }
}
