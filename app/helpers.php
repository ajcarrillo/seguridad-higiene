<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 12:49
 */

function currentUser()
{
	return auth()->user();
}

function hasComision(){
	$centroTrabajo = currentUser()->centro_trabajo;

	if($centroTrabajo){
		$comision = $centroTrabajo->comision;
		if($comision){
			return true;
		}
	}
	return false;
}

function dateCreate($date){
	return date_create_from_format('d/m/Y', $date);
}

function trimestreActual(){
	$today = new DateTime();
	$month = $today->format('m');
	$trimestre = 0;

	switch(true){
		case ($month >= 1 && $month <= 3):
			$trimestre = 1;
			break;
		case ($month >= 4 && $month <= 6):
			$trimestre = 2;
			break;
		case ($month >= 7 && $month <= 9):
			$trimestre = 3;
			break;
		case ($month >= 10 && $month <= 12):
			$trimestre = 4;
			break;
	}

	return $trimestre;
}

function getYear(){
	$today = new DateTime();
	return $today->format('Y');
}
