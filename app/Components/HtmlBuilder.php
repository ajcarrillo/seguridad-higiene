<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 10:52
 */

namespace SeguridadHigiene\Components;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\View\Factory as View;
use Illuminate\Routing\UrlGenerator;
use Collective\Html\HtmlBuilder as CollectiveHtmlBuilder;

class HtmlBuilder extends CollectiveHtmlBuilder {

	/**
	 * @var Config
	 */
	private $config;
	/**
	 * @var View
	 */
	private $view;

	public function __construct(Config $config, View $view, UrlGenerator $url)
	{
		$this->config = $config;
		$this->view = $view;
		$this->url = $url;
	}

	public function menu(array $items)
	{
		return $this->view->make('layout/partials/menu', compact('items'));
	}
	/**
	 * Builds an HTML class attribute dynamically
	 * Usage:
	 * {!! Html::classes(['home' => true, 'main', 'dont-use-this' => false]) !!}
	 * Returns:
	 *  class="home main".
	 *
	 * @param array $classes
	 *
	 * @return string
	 */
	public function classes(array $classes)
	{
		$html = '';
		foreach ($classes as $name => $bool) {
			if (is_int($name)) {
				$name = $bool;
				$bool = true;
			}
			if ($bool) {
				$html .= $name.' ';
			}
		}
		if (! empty($html)) {
			return ' class="'.trim($html).'"';
		}
		return '';
	}
}
