<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 03/10/15
 * Time: 14:54
 */

namespace SeguridadHigiene\Repositories;

use DB;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\Verificacion;
use SeguridadHigiene\Models\VerificacionIncidencia;

class VerificacionIncidenciaRepository extends BaseRepository
{
	public function getModel()
	{
		return new VerificacionIncidencia();
	}

	public function create(Verificacion $verificacion, $input)
	{
		$incidencia = new VerificacionIncidencia($input);
		$verificacion->incidencias()->save($incidencia);
		return $incidencia;
	}

	public function detectadas(Verificacion $verificacion)
	{
		return $this->get_incidencias_by_status($verificacion->reporte->comision, statusIncidencia('Primera vez'));

	}

	public function subsistentes(Verificacion $verificacion)
	{
		return $this->get_incidencias_by_status($verificacion->reporte->comision, statusIncidencia('Subsistentes'));

	}

	public function subsanadas(Verificacion $verificacion)
	{
		return $this->get_incidencias_by_status($verificacion->reporte->comision, statusIncidencia('Subsanadas'));
	}

	public function update_to_subsistentes($comisionId)
	{
		DB::table('t_verificacion_incidencias')
			->join('t_verificaciones', 't_verificaciones.id', '=', 't_verificacion_incidencias.verificacion_id')
			->join('t_reportes', 't_reportes.id', '=', 't_verificaciones.reporte_id')
			->join('t_comisiones', 't_comisiones.id', '=', 't_reportes.comision_id')
			->where('t_comisiones.id', '=', $comisionId)
			->where('t_verificacion_incidencias.status', '=', statusIncidencia("Primera vez"))
			->update(['status' => statusIncidencia("Subsistentes")]);
	}

	public function subsanar_incidencia($incidenciaId)
	{
		$incidencia = $this->newQuery()->find($incidenciaId);
		$incidencia->status = statusIncidencia("Subsanadas");
		$incidencia->trimestre_subsanada = trimestreActual();
		$incidencia->anio_subsanada = getYear();
		$incidencia->save();
	}

	protected function get_incidencias_by_status(Comision $comision, $statusIncidencia){
		/*$incidencias = DB::table('t_verificacion_incidencias')
			->join('cat_incidencias', 'cat_incidencias.id',  '=', 't_verificacion_incidencias.incidencia_id')
			->join('t_verificaciones', 't_verificaciones.id', '=', 't_verificacion_incidencias.verificacion_id')
			->join('t_reportes', 't_reportes.id', '=', 't_verificaciones.reporte_id')
			->join('t_comisiones', 't_comisiones.id', '=', 't_reportes.comision_id')
			->join('t_comision_centros_de_trabajo', 't_comision_centros_de_trabajo.id', '=', 't_verificacion_incidencias.centro_de_trabajo_id')
			->select('t_comision_centros_de_trabajo.clavecct',
				't_verificacion_incidencias.*',
				'cat_incidencias.nombre',
				't_reportes.trimestre',
				't_reportes.anio')
			->where('t_comisiones.id', '=', $comision->id)
			->where('t_verificacion_incidencias.status', '=', $statusIncidencia)
			->orderBy('t_reportes.trimestre', 'asc')
			->orderBy('t_reportes.anio', 'asc')
			->get();*/
		$incidencias = DB::table('t_verificacion_incidencias')
			->join('rh.rh_cat_escuelas_lite', 'rh.rh_cat_escuelas_lite.id', '=', 't_verificacion_incidencias.centro_de_trabajo_id')
			->join('cat_incidencias', 'cat_incidencias.id', '=', 't_verificacion_incidencias.incidencia_id')
			->join('t_verificaciones', 't_verificaciones.id', '=', 't_verificacion_incidencias.verificacion_id')
			->join('t_reportes', 't_reportes.id', '=', 't_verificaciones.reporte_id')
			->select('rh.rh_cat_escuelas_lite.cct',
				't_verificacion_incidencias.*',
				'cat_incidencias.nombre',
				't_reportes.trimestre',
				't_reportes.anio')
			->where('t_reportes.comision_id', '=', $comision->id)
			->where('t_verificacion_incidencias.status', '=', $statusIncidencia)
			->orderBy('t_reportes.trimestre', 'asc')
			->orderBy('t_reportes.anio', 'asc')
			->get();
		//dd($incidencias);
		return $incidencias;
	}

}
