<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/09/15
 * Time: 16:01
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionSecretario;

class ComisionSecretarioRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionSecretario();
	}

	public function create(Comision $comision, $nombre, $primer_apellido, $segundo_apellido, $rfc, $sector)
	{
		$secretario = new ComisionSecretario(compact('nombre', 'primer_apellido', 'segundo_apellido', 'rfc', 'sector'));
		$comision->secretario()->save($secretario);
	}

	public function update($secretarioActualizacion)
	{
		$secretario = ComisionSecretario::find($secretarioActualizacion->row_id);
		$secretario->nombre = $secretarioActualizacion->nombre;
		$secretario->primer_apellido = $secretarioActualizacion->primer_apellido;
		$secretario->segundo_apellido = $secretarioActualizacion->segundo_apellido;
		$secretario->rfc = $secretarioActualizacion->rfc;
		$secretario->sector = $secretarioActualizacion->sector;
		$secretario->save();
	}
}
