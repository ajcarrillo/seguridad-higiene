<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 01/10/15
 * Time: 11:06
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\ReporteTipo;

class ReporteTipoRepository extends BaseRepository
{
	public function getModel()
	{
		return new ReporteTipo();
	}

	public function store(Reporte $reporte, $tipo_id)
	{
		$tipo = new ReporteTipo(compact('tipo_id'));
		$reporte->tipos()->save($tipo);
		return $tipo;
	}

}
