<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 13:02
 */

namespace SeguridadHigiene\Repositories;


use DB;
use phpDocumentor\Reflection\DocBlock\Tag\ReturnTag;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\ReporteTipo;

class ReportesRepository extends BaseRepository {

	/**
	 * @var ReporteTipoRepository
	 */
	private $reporteTipoRepository;

	public function __construct(ReporteTipoRepository $reporteTipoRepository)
	{

		$this->reporteTipoRepository = $reporteTipoRepository;
	}
	public function getModel()
	{
		return new Reporte();
	}

	public function find($id)
	{
		return Reporte::where('id', '=', $id)
			->with('comision')
			->with('tipos')
			->first();
	}

	public function findOrFail($id)
	{
		return $this->newQuery()->findOrFail($id);
	}

	public function create(Comision $comision, $action)
	{
		$status_id = ($action == 'save' ? 4 : 3);
		$reporte = $this->store($comision, trimestreActual(), $status_id);
		$this->reporteTipoRepository->store($reporte, 1);

		return $reporte;
	}

	public function store(Comision $comision, $trimestre, $status_id)
	{
		$reporte = new Reporte(compact('trimestre', 'status_id'));
		$comision->reportes()->save($reporte);
		return $reporte;
	}

	public function getReportes($comisionId)
	{
		return $this->newQuery()
			->where('comision_id', $comisionId)
			->orderBy('created_at', 'DESC')
			->get();
	}


	public function sent(Reporte $reporte)
	{
		$reporte->status_id = 3;
		$reporte->save();
	}

	public function hasReporte($comision_id)
	{
		$reporte = DB::table('t_reportes_tipo')
			->join('t_reportes', 't_reportes.id', '=', 't_reportes_tipo.reporte_id')
			->join('t_comisiones', 't_comisiones.id', '=', 't_reportes.comision_id')
			->where('t_reportes_tipo.tipo_id', '!=', 1)
			->where('t_reportes.anio', intval(getYear()))
			->where('t_reportes.trimestre', trimestreActual())
			->where('t_comisiones.id', $comision_id)
			->count();

		return $reporte >= 1 ? true:false;
	}

	public function isRegistro($comision_id)
	{
		$reporte = DB::table('t_reportes_tipo')
			->join('t_reportes', 't_reportes.id', '=', 't_reportes_tipo.reporte_id')
			->join('t_comisiones', 't_comisiones.id', '=', 't_reportes.comision_id')
			->where('t_reportes.anio', intval(getYear()))
			->where('t_reportes.trimestre', trimestreActual())
			->where('t_comisiones.id', $comision_id)
			->count();

		return $reporte;
	}

    public function findByStatus($status)
    {
        return Reporte::where('t_reportes.status_id', $status)
            ->with('comision.centros.escuela_lite.turno', 'tipos.tipo', 'status')
            ->orderBy('t_reportes.id', 'ASC')
            ->get();
    }

    public function saveReport(Reporte $reporte, $path)
    {
        Reporte::where('id', $reporte->id)
        ->update([
            'path_pdf' => $path
        ]);

    }

}
