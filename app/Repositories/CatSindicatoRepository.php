<?php
/**
 * Created by PhpStorm.
 * User: wsanchez
 * Date: 12/10/2015
 * Time: 10:46 AM
 */

namespace SeguridadHigiene\Repositories;


use Exception;
use SeguridadHigiene\Models\Sindicato;

class CatSindicatoRepository extends BaseRepository
{
    public function getModel(){
        return new Sindicato();
    }

    public function lista()
    {
        return Sindicato::paginate(15);
    }

    public function find($id)
    {
        $sindicato = Sindicato::find($id);

        return $sindicato;
    }

    public function eliminar($id)
    {
        Sindicato::destroy($id);
    }

    public function create($nombre, $direccion, $colonia, $codigo_postal, $ciudad, $telefono)
    {
        $sindicato = new Sindicato(compact ('nombre', 'direccion', 'colonia', 'codigo_postal', 'ciudad', 'telefono'));
        $sindicato->save();

        return $sindicato;
    }
    public function update($id, $nombre, $direccion, $colonia, $codigo_postal, $ciudad, $telefono)
    {
        $sindicato = Sindicato::find($id);
        $sindicato->nombre = $nombre;
        $sindicato->direccion = $direccion;
        $sindicato->colonia = $colonia;
        $sindicato->codigo_postal = $codigo_postal;
        $sindicato->ciudad = $ciudad;
        $sindicato->telefono = $telefono;

        $sindicato->save();

        return $sindicato;
    }
}