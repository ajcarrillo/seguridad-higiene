<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/10/15
 * Time: 13:14
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Calendario;
use SeguridadHigiene\Models\Reporte;

class CalendarioRepository extends BaseRepository
{
	public function getModel()
	{
		return new Calendario();
	}

	public function find($id)
	{
		return Calendario::find($id);
	}

	public function create(Reporte $reporte, array $input)
	{
		$calendario = new Calendario($input);
		$reporte->calendario()->save($calendario);
		return $calendario;
	}

	public function update(Calendario $calendario, $de, $a, $descripcion, $unidad_de_medida)
	{
		$calendario->de = $de;
		$calendario->a = $a;
		$calendario->descripcion = $descripcion;
		$calendario->unidad_de_medida = $unidad_de_medida;

		$calendario->save();
	}

}
