<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/11/15
 * Time: 23:18
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\ComisionSecretarioTransaccion;

class SecretarioTransaccionRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionSecretarioTransaccion();
	}

	public function store(Actualizacion $actualizacion, $inputs)
	{
		$transaccion = new ComisionSecretarioTransaccion($inputs);
		$actualizacion->secretarioTransacciones()->save($transaccion);

		return $transaccion;
	}

	public function update(ComisionSecretarioTransaccion $transaccion, $inputs)
	{
		$transaccion->update($inputs);

		return $transaccion;
	}

}
