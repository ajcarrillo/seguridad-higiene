<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 29/09/15
 * Time: 10:05
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionVocal;

class ComisionVocalRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionVocal();
	}

	public function update($id, $cargo, $sector, $tipo)
	{
		$vocal = $this->newQuery()->find($id);
		$vocal->cargo = $cargo;
		$vocal->sector = $sector;
		$vocal->tipo = $tipo;
		$vocal->save();
	}

	public function create(Comision $comision, $nombre, $primer_apellido, $segundo_apellido, $cargo, $sector, $tipo, $rfc)
	{
		$vocal = new ComisionVocal(compact('nombre', 'primer_apellido', 'segundo_apellido', 'cargo', 'sector', 'tipo', 'rfc'));
		$vocal->comision = $comision->id;
		$comision->vocales()->save($vocal);
		return $vocal;
	}

	/*
	 * Metodos nuevos
	 *
	 * */

	public function index(Comision $comision)
	{
		return $comision->vocales_paginated;
	}

	public function store(Comision $comision, $input)
	{
		$vocal = new ComisionVocal($input);
		$comision->vocales()->save($vocal);
		return $vocal;
	}

	public function destroy($vocal)
	{
		$this->newQuery()->find($vocal)->delete();
	}
}
