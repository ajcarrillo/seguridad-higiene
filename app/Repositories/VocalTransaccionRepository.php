<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 03/12/15
 * Time: 14:38
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\ComisionVocalTransaccion;

class VocalTransaccionRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionVocalTransaccion();
	}

	public function store(Actualizacion $actualizacion, array $values)
	{
		$transaccion = new ComisionVocalTransaccion($values);
		$actualizacion->vocalTransacciones()->save($transaccion);

		return $transaccion;
	}

	public function destroy($id)
	{
		$transaccion = $this->newQuery()->find($id);
		$transaccion->delete();

		return $this->newQuery()->get();
	}

}
