<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 08/09/15
 * Time: 20:52
 */

namespace SeguridadHigiene\Repositories;

use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionCentro;

class ComisionCentroRepository extends BaseRepository {

	public function getModel()
	{
		return new ComisionCentro();
	}

	public function getComisionId($user)
	{
		$comisionId = $this->newQuery()
			->select('comision')
			->where('centro_de_trabajo', $user->centro_trabajo)
			->first();

		return $comisionId;
	}

	public function destroy($id)
	{
		$this->newQuery()->where('centro_de_trabajo_id', $id)->delete();
	}

	public function create(Comision $comision, $centro_de_trabajo_id, $clavecct, $nombre, $municipio, $localidad, $nivel)
	{
		$centro = new ComisionCentro(compact('centro_de_trabajo_id', 'clavecct', 'nombre', 'municipio', 'localidad', 'nivel'));
		$comision->centros()->save($centro);
	}

	/*
	 * Metodos nuevos
	 *
	 * */
	public function store(Comision $comision, $centro_de_trabajo_id)
	{
		$centro = new ComisionCentro(compact('centro_de_trabajo_id'));
		$comision->centros()->save($centro);
		return $centro;
	}

	public function centrosDataTable($comision)
	{
		return ComisionCentro::with(['escuela_lite','escuela_lite.turno', 'escuela_lite.status'])
			->where('comision_id', $comision)
			->select('*');
	}


}
