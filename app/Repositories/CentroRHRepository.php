<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 08/09/15
 * Time: 10:51
 */

namespace SeguridadHigiene\Repositories;


use Illuminate\Database\QueryException;
use SeguridadHigiene\Models\CentroRH;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CentroRHRepository extends BaseRepository {

	public function __construct()
	{

	}
	public function getModel(){
		return new CentroRH();
	}

	public function getCentroByClavecct($clavecct)
	{
		return $this->newQuery()->where('clavecct', $clavecct)->first();
	}

	public function find($id)
	{
		return $this->newQuery()->find($id);
	}

}
