<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 12:29
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Http\Requests\Request;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionCentro;
use SeguridadHigiene\Models\User;

class UserRepository extends BaseRepository {

	public function getModel()
	{
		return new User();
	}
}
