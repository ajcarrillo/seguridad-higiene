<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/11/15
 * Time: 10:07
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionSecretario;

class SecretarioRepository extends BaseRepository
{

	public function getModel()
	{
		return new ComisionSecretario();
	}

	public function findOrFail($id)
	{
		return $this->newQuery()->findOrFail($id);
	}

	public function store(Comision $comision, $input)
	{
		$secretario = new ComisionSecretario($input);
		$comision->secretario()->save($secretario);
		return $secretario;
	}

	public function update($input, $secretario_id)
	{
		$secretario = $this->findOrFail($secretario_id);
		$secretario->update($input);
		return $secretario;
	}

}
