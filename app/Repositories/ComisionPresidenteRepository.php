<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/09/15
 * Time: 12:32
 */

namespace SeguridadHigiene\Repositories;

use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionPresidente;

class ComisionPresidenteRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionPresidente();
	}

	public function update($presidenteActualizacion)
	{
		$presidente = ComisionPresidente::find($presidenteActualizacion->row_id);
		$presidente->nombre = $presidenteActualizacion->nombre;
		$presidente->primer_apellido = $presidenteActualizacion->primer_apellido;
		$presidente->segundo_apellido = $presidenteActualizacion->segundo_apellido;
		$presidente->cargo = $presidenteActualizacion->cargo;
		$presidente->save();
	}

	public function create(Comision $comision, $nombre, $primer_apellido, $segundo_apellido, $cargo)
	{
		$presidente = new ComisionPresidente(compact('nombre', 'primer_apellido', 'segundo_apellido', 'cargo'));
		$comision->presidente()->save($presidente);
	}

	/*
	 * Nuevos Metodos
	 *
	 * */

	public function findOrFail($id)
	{
		return $this->newQuery()->findOrFail($id);
	}

	public function store(Comision $comision, $input)
	{
		$presidente = new ComisionPresidente($input);
		$comision->presidente()->save($presidente);
		return $presidente;
	}
}
