<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/09/15
 * Time: 21:16
 */

namespace SeguridadHigiene\Repositories;


abstract class BaseRepository {

	abstract public function getModel();
	/**
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function newQuery()
	{
		return $this->getModel()->newQuery();
	}

	public function findOrFail($id)
	{
		return $this->newQuery()->findOrFail($id);
	}
}
