<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 30/11/15
 * Time: 8:45
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\ComisionCentroTransaccion;

class ComisionCentroTransaccionRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionCentroTransaccion();
	}

	public function store(Actualizacion $actualizacion, $inputs)
	{
		$transaccion = new ComisionCentroTransaccion($inputs);
		$actualizacion->centroTransacciones()->save($transaccion);

		return $transaccion;
	}

	public function destroy($id)
	{
		$transaccion = $this->newQuery()->find($id);
		$transaccion->delete();
		return $this->newQuery()->with(['escuela_lite', 'escuela_lite.turno'])->get();
	}

}
