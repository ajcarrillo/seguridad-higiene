<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 08/09/15
 * Time: 20:51
 */

namespace SeguridadHigiene\Repositories;

use DB;
use SeguridadHigiene\Models\Comision;
use SeguridadHigiene\Models\ComisionCentro;
use SeguridadHigiene\Models\ComisionPresidente;
use SeguridadHigiene\Models\ComisionSecretario;
use SeguridadHigiene\Models\ComisionVocal;
use SeguridadHigiene\Models\User;

class ComisionRepository extends BaseRepository {

	/**
	 * @var ComisionCentroRepository
	 */
	private $comisionCentroRepository;

	public function __construct(ComisionCentroRepository $comisionCentroRepository)
	{

		$this->comisionCentroRepository = $comisionCentroRepository;
	}
	public function getModel()
	{
		return new Comision();
	}

	public function getUserComision($comisionId){

		return $this->newQuery()->find($comisionId);

	}

	public function create($clave_comision, $generales){

		$fechaOficioSindical = date_create_from_format('d/m/Y',$generales[0]['fecha_oficio_oficial']);
		$fechaOficioOficial = date_create_from_format('d/m/Y',$generales[0]['fecha_oficio_sindical']);

		$sindicato_id = $generales[0]['sindicato_id'];
		$num_oficio_sindical = $generales[0]['num_oficio_oficial'];
		$num_oficio_oficial = $generales[0]['num_oficio_sindical'];
		$fecha_oficio_sindical = $fechaOficioSindical;
		$fecha_oficio_oficial = $fechaOficioOficial;

		$comision = new Comision(compact('clave_comision', 'sindicato_id','num_oficio_sindical', 'fecha_oficio_sindical', 'num_oficio_oficial', 'fecha_oficio_oficial'));
		$comision->user_id = currentUser()->id;
		$comision->save();
		return $comision;
	}

	public function update($id, $sindicato_id,
						   $num_oficio_sindical, $fecha_oficio_sindical, $num_oficio_oficial,
						   $fecha_oficio_oficial)
	{
		$comision = $this->newQuery()->find($id);

		$fechaSindical = date_create_from_format('d/m/Y', $fecha_oficio_sindical);
		$fechaOficial = date_create_from_format('d/m/Y', $fecha_oficio_oficial);

		$comision->sindicato_id = $sindicato_id;
		$comision->num_oficio_sindical = $num_oficio_sindical;
		$comision->fecha_oficio_sindical = $fechaSindical;
		$comision->num_oficio_oficial = $num_oficio_oficial;
		$comision->fecha_oficio_oficial = $fechaOficial;
		$comision->save();
	}

	public function setPresidente(Comision $comision, $nombre, $primer_apellido, $segundo_apellido, $cargo)
	{
		$presidente = new ComisionPresidente(compact(
			'nombre',
			'primer_apellido',
			'segundo_apellido',
			'cargo'));
		$presidente->comision_id = $comision->id;
		$comision->presidente()->save($presidente);
	}

	public function setSecretario(Comision $comision, $nombre, $primer_apellido, $segundo_apellido, $rfc, $sector)
	{
		$secretario = new ComisionSecretario(compact(
			'nombre',
			'primer_apellido',
			'segundo_apellido',
			'rfc',
			'sector'));
		$secretario->comision_id = $comision->id;
		$comision->secretario()->save($secretario);
	}

	public function setCentros(Comision $comision, $centros)
	{
		foreach($centros as $c){
			$centro_de_trabajo_id = $c['id'];
			$clavecct = $c['clavecct'];
			$nombre = $c['NOMBRECT'];
			$municipio = $c['NOMBREMUN'];
			$localidad = $c['NOMBRELOC'];
			$nivel = $c['NIVEL'];

			$this->comisionCentroRepository->create($comision, $centro_de_trabajo_id, $clavecct, $nombre, $municipio, $localidad, $nivel);

		}

	}

	public function setVocales(Comision $comision, $vocales)
	{
		foreach ($vocales as $v) {
			$nombre = $v['nombre'];
			$primer_apellido = $v['primer_apellido'];
			$segundo_apellido = $v['segundo_apellido'];
			$cargo = $v['cargo'];
			$sector = $v['sector'];
			$tipo = $v['tipo'];
			$rfc = $v['rfc'];

			$vocal = new ComisionVocal(compact('nombre', 'primer_apellido', 'segundo_apellido', 'cargo', 'sector', 'tipo', 'rfc'));
			$vocal->comision_id = $comision->id;

			$comision->vocales()->save($vocal);
		}

	}

	/*
	 * Nuevos metodos
	 *
	 * */
	public function store($data)
	{
		$comision = new Comision($data);
		$comision->user_id = currentUser()->id;
		$comision->save();

		return $comision;
	}

	public function hasReporteRegistro($comision_id)
	{
		return DB::table('t_reportes_tipo')
			->join('t_reportes', 't_reportes.id', '=', 't_reportes_tipo.reporte_id')
			->join('t_comisiones', 't_comisiones.id', '=', 't_reportes.comision_id')
			->where('t_comisiones.id', '=', $comision_id)
			->where('t_reportes_tipo.tipo_id', '=', 1)
			->select('t_reportes_tipo.reporte_id')
			->get();
	}

}
