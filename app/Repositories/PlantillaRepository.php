<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/09/15
 * Time: 16:06
 */

namespace SeguridadHigiene\Repositories;


use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;


class PlantillaRepository {

	public function empleados($page, $cct)
	{
		$empleadoInterno = DB::connection('rh')
			->table('rh_t_empleado')
			->join('rh_t_empl_plaza', 'rh_t_empl_plaza.id_empleado', '=', 'rh_t_empleado.id_empleado')
			->join('rh_t_emp_plaza_detalle', function($join){
				$join->on('rh_t_emp_plaza_detalle.id_empleado', '=', 'rh_t_empleado.id_empleado')
					->on('rh_t_emp_plaza_detalle.id_plaza', '=', 'rh_t_empl_plaza.id_plaza');
			})
			->leftJoin('rh_cat_unidades_administrativas', 'rh_cat_unidades_administrativas.id_unidad_admva', '=', 'rh_t_emp_plaza_detalle.cve_area')
			->where('rh_t_emp_plaza_detalle.cct', 'like', $cct)
			->orWhere('rh_cat_unidades_administrativas.cct', 'like', $cct)
			->where('rh_t_emp_plaza_detalle.estado', '<>', 'B')
			->where('rh_t_empl_plaza.estado', '<>', 'B')
			->whereIn('rh_t_empleado.estatus_laboral', ['1', 'A'])
			->select('rh_t_empleado.rfc', 'rh_t_empleado.nombre_emp', 'rh_t_empleado.ap_paterno_emp', 'rh_t_empleado.ap_materno_emp')
			->groupBy('rh_t_empleado.id_empleado');

		$empleadoExterno = DB::connection('rh')
			->table('rh_t_empleado_externo')
			->join('rh_t_empleado_externo_detalle', 'rh_t_empleado_externo_detalle.id_empleado', '=', 'rh_t_empleado_externo.id_empleado')
			->leftJoin('rh_cat_unidades_administrativas', 'rh_cat_unidades_administrativas.id_unidad_admva', '=', 'rh_t_empleado_externo_detalle.cve_area')
			->where('rh_t_empleado_externo_detalle.cct', 'like', $cct)
			->orWhere('rh_cat_unidades_administrativas.cct', 'like', $cct)
			->where('rh_t_empleado_externo_detalle.estado', '<>', 'B')
			->whereIn('rh_t_empleado_externo.estatus_laboral', ['1', 'A'])
			->select('rh_t_empleado_externo.rfc', 'rh_t_empleado_externo.nombre_emp', 'rh_t_empleado_externo.ap_paterno_emp', 'rh_t_empleado_externo.ap_materno_emp')
			->groupBy('rh_t_empleado_externo.id_empleado')
			->union($empleadoInterno)
			->get();


		$paginate = 5;
		$slice = array_slice($empleadoExterno, $paginate * ($page - 1), $paginate);
		$result = new LengthAwarePaginator($slice, count($empleadoExterno), $paginate, $page, [
			'path' => Paginator::resolveCurrentPath(),
			'pageName' => 'page',
		]);
		return $result;
	}

	public function totalEmpleados(array $ccts)
	{
		$totalEmpleadoInterno = DB::connection('rh')
			->table('rh_t_empleado')
			->join('rh_t_empl_plaza', 'rh_t_empl_plaza.id_empleado', '=', 'rh_t_empleado.id_empleado')
			->join('rh_t_emp_plaza_detalle', function($join){
				$join->on('rh_t_emp_plaza_detalle.id_empleado', '=', 'rh_t_empleado.id_empleado')
					->on('rh_t_emp_plaza_detalle.id_plaza', '=', 'rh_t_empl_plaza.id_plaza');
			})
			->leftJoin('rh_cat_unidades_administrativas', 'rh_cat_unidades_administrativas.id_unidad_admva', '=', 'rh_t_emp_plaza_detalle.cve_area')
			->whereIn('rh_t_emp_plaza_detalle.cct', $ccts)
			->orWhereIn('rh_cat_unidades_administrativas.cct', $ccts)
			->where('rh_t_emp_plaza_detalle.estado', '<>', 'B')
			->where('rh_t_empl_plaza.estado', '<>', 'B')
			->whereIn('rh_t_empleado.estatus_laboral', ['1', 'A'])
			->groupBy('rh_t_empleado.id_empleado')
			->get();

		$totalEmpleadoExterno = DB::connection('rh')
			->table('rh_t_empleado_externo')
			->join('rh_t_empleado_externo_detalle', 'rh_t_empleado_externo_detalle.id_empleado', '=', 'rh_t_empleado_externo.id_empleado')
			->leftJoin('rh_cat_unidades_administrativas', 'rh_cat_unidades_administrativas.id_unidad_admva', '=', 'rh_t_empleado_externo_detalle.cve_area')
			->whereIn('rh_t_empleado_externo_detalle.cct', $ccts)
			->orWhereIn('rh_cat_unidades_administrativas.cct', $ccts)
			->where('rh_t_empleado_externo_detalle.estado', '<>', 'B')
			->whereIn('rh_t_empleado_externo.estatus_laboral', ['1', 'A'])
			->groupBy('rh_t_empleado_externo.id_empleado')
			->get();

		return count($totalEmpleadoInterno)+count($totalEmpleadoExterno);
	}

	public function personal(array $ccts)
	{
		$empleadoInterno = DB::connection('rh')
			->table('rh_t_empleado')
			->join('rh_t_empl_plaza', 'rh_t_empl_plaza.id_empleado', '=', 'rh_t_empleado.id_empleado')
			->join('rh_t_emp_plaza_detalle', function($join){
				$join->on('rh_t_emp_plaza_detalle.id_empleado', '=', 'rh_t_empleado.id_empleado')
					->on('rh_t_emp_plaza_detalle.id_plaza', '=', 'rh_t_empl_plaza.id_plaza');
			})
			->leftJoin('rh_cat_unidades_administrativas', 'rh_cat_unidades_administrativas.id_unidad_admva', '=', 'rh_t_emp_plaza_detalle.cve_area')
			->whereIn('rh_t_emp_plaza_detalle.cct', $ccts)
			->orWhereIn('rh_cat_unidades_administrativas.cct', $ccts)
			->where('rh_t_emp_plaza_detalle.estado', '<>', 'B')
			->where('rh_t_empl_plaza.estado', '<>', 'B')
			->whereIn('rh_t_empleado.estatus_laboral', ['1', 'A'])
			->select(['rh_t_empleado.rfc', 'rh_t_empleado.nombre_emp', 'rh_t_empleado.ap_paterno_emp', 'rh_t_empleado.ap_materno_emp'])
			->groupBy('rh_t_empleado.id_empleado');

		$empleadoExterno = DB::connection('rh')
			->table('rh_t_empleado_externo')
			->join('rh_t_empleado_externo_detalle', 'rh_t_empleado_externo_detalle.id_empleado', '=', 'rh_t_empleado_externo.id_empleado')
			->leftJoin('rh_cat_unidades_administrativas', 'rh_cat_unidades_administrativas.id_unidad_admva', '=', 'rh_t_empleado_externo_detalle.cve_area')
			->whereIn('rh_t_empleado_externo_detalle.cct', $ccts)
			->orWhereIn('rh_cat_unidades_administrativas.cct', $ccts)
			->where('rh_t_empleado_externo_detalle.estado', '<>', 'B')
			->whereIn('rh_t_empleado_externo.estatus_laboral', ['1', 'A'])
			->select(['rh_t_empleado_externo.rfc', 'rh_t_empleado_externo.nombre_emp', 'rh_t_empleado_externo.ap_paterno_emp', 'rh_t_empleado_externo.ap_materno_emp'])
			->groupBy('rh_t_empleado_externo.id_empleado')
			->union($empleadoInterno);

		return $empleadoExterno;
	}

}
