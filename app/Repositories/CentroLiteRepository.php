<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/10/15
 * Time: 12:00
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\CentroLite;

class CentroLiteRepository extends BaseRepository
{
	private $paginationSize = 5;

	public function getModel()
	{
		return new CentroLite();
	}

	public function findOrFail($id)
	{
		return $this->newQuery()->with(['turno', 'status'])->findOrFail($id);
	}

	public function search($value, $by)
	{
		switch($by){
			case "cct":
				return $this->searchByCCT($value);
				break;
			case "nombre":
				return $this->searchByNombre($value);
			default:
				return "";
				break;
		}
	}

	protected function doSearch($column, $operator, $value)
	{
		return $this->newQuery()->with(['turno', 'status'])->where($column, $operator, $value)->paginate($this->paginationSize);
	}

	protected function searchByNombre($nombre)
	{
		return $this->doSearch('nombre', 'LIKE', '%'.$nombre.'%');
	}

	protected function searchByCCT($cct)
	{
		return $this->doSearch('cct', 'LIKE', '%'.$cct.'%');
	}

	public function all(){
		return $this->newQuery()->with(['turno', 'status'])->paginate($this->paginationSize);
	}

	public function autocomplete($term){
		return $this->newQuery()
			->with(['turno'])
			->where('nombre', 'like', "%$term%")
			->orWhere('cct', 'like', "%$term%")
			->get();
	}

}
