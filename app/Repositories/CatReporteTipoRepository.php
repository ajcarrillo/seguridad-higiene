<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 01/10/15
 * Time: 11:04
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\CatReporteTipo;

class CatReporteTipoRepository extends BaseRepository
{
	public function getModel()
	{
		return new CatReporteTipo();
	}

	public function create()
	{

	}
}
