<?php
/**
 * Created by PhpStorm.
 * User: wsanchez
 * Date: 12/10/2015
 * Time: 10:46 AM
 */

namespace SeguridadHigiene\Repositories;


use Exception;
use SeguridadHigiene\Models\CatIncidencia;

class CatIncidenciaRepository extends BaseRepository
{
    public function getModel(){
        return new CatIncidencia();
    }

    public function create($nombre, $clave)
    {
        $incidencia = new CatIncidencia();
        $incidencia->nombre = $nombre;
        $incidencia->clave = $clave;
        $incidencia->save();

        return $incidencia;
    }

    public function lista()
    {
        return CatIncidencia::paginate(15);
    }

    public function eliminar($id)
    {
        CatIncidencia::destroy($id);
    }

    public function find($id)
    {
        $incidencia = CatIncidencia::find($id);

        return $incidencia;
    }

    public function update($id, $nombre, $clave)
    {
        $incidencia = CatIncidencia::find($id);
        $incidencia->nombre = $nombre;
        $incidencia->clave = $clave;
        $incidencia->save();

        return $incidencia;
    }
}