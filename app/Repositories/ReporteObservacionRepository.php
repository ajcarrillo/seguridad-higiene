<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 01/10/15
 * Time: 11:06
 */

namespace SeguridadHigiene\Repositories;

use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\ReporteObservacion;

class ReporteObservacionRepository extends BaseRepository
{
	public function getModel()
	{
		return new ReporteObservacion();
	}

	public function create(Reporte $reporte, $observacion, $usuario)
	{
		$reporteObservacion = new ReporteObservacion(compact('observacion', $reporte->id, 'usuario_id'));
		$reporte->observaciones()->save($reporteObservacion);
	}

}
