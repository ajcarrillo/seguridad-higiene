<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 09/10/15
 * Time: 11:56
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Calendario;
use SeguridadHigiene\Models\CalendarioActividad;

class CalendarioActividadRepository extends BaseRepository
{
	public function getModel()
	{
		return new CalendarioActividad();
	}

	public function find($actividadId)
	{
		return $this->newQuery()->findOrFail($actividadId);
	}

	public function create(Calendario $calendario, $descripcion, $unidad_de_medida, $trimestre)
	{
		$actividad = new CalendarioActividad(compact('descripcion', 'unidad_de_medida', 'trimestre'));
		$calendario->actividades()->save($actividad);
		return $actividad;
	}

	public function update(CalendarioActividad $actividad, $descripcion, $unidad_de_medida, $trimestre)
	{
		$actividad->descripcion = $descripcion;
		$actividad->unidad_de_medida = $unidad_de_medida;
		$actividad->trimestre = $trimestre;
		$actividad->save();
	}

	public function create_default_activities(Calendario $calendario)
	{
		$actividades = [
			[
				'descripcion' => "Llevar a cabo recorridos de verificación al centro de trabajo (solo comisiones auxiliares)",
				'unidad_de_medida' => "Verificación",
				'trimestre' => 1
			],
			[
				'descripcion' => "Capacitación en materia de seguridad, higiene y medio ambiente en el trabajo",
				'unidad_de_medida' => "Cursos",
				'trimestre' => 1
			],
			[
				'descripcion' => "Difusión de material",
				'unidad_de_medida' => "Carteles, trípticos o folletos",
				'trimestre' => 1
			],
			[
				'descripcion' => "Promoción de actividades de seguridad y salud",
				'unidad_de_medida' => "Campañas",
				'trimestre' => 1
			]
		];

		foreach($actividades as $actividad){
			$this->create($calendario, $actividad["descripcion"], $actividad["unidad_de_medida"], $actividad["trimestre"]);
		}

	}

	public function index(Calendario $calendario)
	{
		return $calendario->actividades_paginated;
	}
}
