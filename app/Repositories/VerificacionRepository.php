<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 01/10/15
 * Time: 17:09
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Reporte;
use SeguridadHigiene\Models\Verificacion;

class VerificacionRepository extends BaseRepository
{
	public function getModel()
	{
		return new Verificacion();
	}

	public function find($verificacionId)
	{
		return Verificacion::find($verificacionId);
	}

	public function create(Reporte $reporte, $inputs)
	{
		$verificacion = new Verificacion($inputs);
		$reporte->verificacion()->save($verificacion);
		return $verificacion;
	}

	public function update(Verificacion $verificacion, $fecha, $tipo, $numero_de_riesgos_de_trabajo,
						   $accidentes_de_trabajo, $enfermedad_profesional, $observacion, $propuesta_csst)
	{
		$verificacion->fecha = $fecha;
		$verificacion->tipo = $tipo;
		$verificacion->numero_de_riesgos_de_trabajo = $numero_de_riesgos_de_trabajo;
		$verificacion->accidentes_de_trabajo = $accidentes_de_trabajo;
		$verificacion->enfermedad_profesional = $enfermedad_profesional;
		$verificacion->observacion = $observacion;
		$verificacion->propuesta_csst = $propuesta_csst;

		$verificacion->save();
	}

}
