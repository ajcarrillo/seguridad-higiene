<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/11/15
 * Time: 11:08
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\ComisionPresidenteTransaccion;

class PresidenteTransaccionRepository extends BaseRepository
{
	public function getModel()
	{
		return new ComisionPresidenteTransaccion();
	}

	public function store(Actualizacion $actualizacion, $inputs)
	{
		$transaccion = new ComisionPresidenteTransaccion($inputs);
		$actualizacion->presidenteTransacciones()->save($transaccion);

		return $transaccion;
	}

	public function update(ComisionPresidenteTransaccion $transaccion, $inputs)
	{
		$transaccion->update($inputs);

		return $transaccion;
	}

	public function applyTransaction(ComisionPresidenteTransaccion $transaccion)
	{

	}

}
