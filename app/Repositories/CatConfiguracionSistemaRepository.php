<?php
/**
 * Created by PhpStorm.
 * User: wsanchez
 * Date: 12/10/2015
 * Time: 10:46 AM
 */

namespace SeguridadHigiene\Repositories;


use Exception;
use SeguridadHigiene\Models\CatConfiguracionSistema;

class CatConfiguracionSistemaRepository extends BaseRepository
{

    public function getModel(){
        return new CatConfiguracionSistema();
    }


    public function lista()
    {
        return CatConfiguracionSistema::all();
    }


    public function find($id)
    {

        $configuracion = CatConfiguracionSistema::find($id);

        return $configuracion;
    }

    public function update($id, $valor)
    {
        $configuracion = CatConfiguracionSistema::find($id);
        $configuracion->valor = $valor;
        $configuracion->save();

        return $configuracion;
    }
}