<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 25/11/15
 * Time: 10:24
 */

namespace SeguridadHigiene\Repositories;


use SeguridadHigiene\Models\Actualizacion;
use SeguridadHigiene\Models\Reporte;

class ActualizacionRepository extends BaseRepository
{

	public function getModel()
	{
		return new Actualizacion();
	}

	public function store(Reporte $reporte, $inputs)
	{
		$actualizacion = new Actualizacion($inputs);
		$reporte->actualizacion()->save($actualizacion);
		return $actualizacion;
	}

	public function update(Actualizacion $actualizacion, $inputs)
	{

	}

	public function destroy(Actualizacion $actualizacion)
	{

	}

}
