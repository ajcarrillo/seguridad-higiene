<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 07/09/15
 * Time: 11:24
 */

return [
	'menu' => [
		'admin'    => [
			'Developer' => ['route' => 'api.urls'],
		],
		'operador' => [
			'Actas'         => ['route' => 'admin'],
			'Reportes'      => [],
			'Configuración' => ['submenu' => [
				'Claves de comisión'      => ['route' => 'get.index.claveComision'],
				'Periodos de captura'     => ['route' => 'etapa.index'],
				'Carga de documentación'  => [],
				'Catalogo de Sindicatos'  => ['route' => 'get.index.sindicato'],
				'Catálogo de Incidencias' => ['route' => 'incidencia.index'],
				'Usuarios'                => ['route' => 'user.index'],
				'Sistema'                 => ['route' => 'get.index.configuracion'],
			]],
		],
		'usuario'  => [
			'Actas' => ['route' => 'actas'],
		],
		'invitado' => [
			'' => [],
		],
	],
];
/*return array(
	'menu' => array(
		'actas.get.create'	=> 'Nuevo',
		'actas'				=> 'Actas',
		'admin'				=> 'Reportes enviados',
		'get.admin.claveComision' => 'Asignar clave comision',
	)
);*/
