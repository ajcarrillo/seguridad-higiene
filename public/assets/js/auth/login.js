"use strict";
/**
 * Created by andres on 01/06/16.
 */
(function () {
	var app = $('#app').val(),
		url = $('#login-url-users').val(),
		$form = $('#login-form'),
		$unauthorizedAlert = $('#unauthorized-alert');

	$form.on("submit", onSubmitForm);
	$unauthorizedAlert.hide();

	function onSubmitForm(e) {
		e.preventDefault();

		$unauthorizedAlert.hide();

		var username = $('#username').val(),
			data = {'username': username, 'app_key': app};

		$.post(url, data, function(response){
			$('#login-form')[0].submit();
		}).fail(function() {
			$unauthorizedAlert.show();
		});
	}
}());

