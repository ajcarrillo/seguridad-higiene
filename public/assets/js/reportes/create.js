"use strict";
/**
 * Created by andres on 01/10/15.
 */

(function () {
	var $btnSaveReporte = $('#btn-save-reporte');

	$btnSaveReporte.on("click", onClickSaveReporte);

	function onClickSaveReporte(){
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
	}
})();
