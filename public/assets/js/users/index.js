"use strict";
/**
 * Created by andres on 26/01/16.
 */

/*
*
* Datatables: users and user_datas
*
* */

(function () {
	var $usersTable = $('#users-table'),
		url = $usersTable.data('url');

	$usersTable.DataTable({
		processing: true,
		serverSide: true,
		ajax: url,
		columns: [
			{data: 'id', name: 'users.id'},
			{data: 'data.name', name: 'data.name'},
			{data: 'role', name: 'users.role'},
			{data: 'edit', name: 'edit', orderable: false, searchable: false}
		]
	});
}());
