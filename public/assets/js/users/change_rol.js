/**
 * Created by andres on 17/06/16.
 */
"use strict";

(function () {
	var input = $('#centro_trabajo'),
		url = input.data('url');

	var options = {
		url: function (term) {
			return url.replace('TERM', term);
		},

		getValue: function (element) {
			return element.cct + ' - ' +
				element.nombre + ' - ' +
				element.turno.d_turno + ' - ' +
				element.abrev_municipio + ' - ' +
				element.nivel
		},
		highlightPhrase: false,
		requestDelay: 600,
		list:{
			onClickEvent: function(){
				var centroTrabajoId = $('#centro_trabajo_id');
				centroTrabajoId.val(input.getSelectedItemData().id);
			}
		}
	};
	input.easyAutocomplete(options);
}());
