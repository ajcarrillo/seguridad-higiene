"use strict";
/**
 * Created by andres on 30/11/15.
 */
var selected = [];

(function () {
	var $esculaLite = $('#escuela-lite');

	$esculaLite.DataTable({
		processing: true,
		serverSide: true,
		ajax: $esculaLite.data('url'),
		rowCallback: function( row, data ) {
			if ( $.inArray(data.DT_RowId.toString(), selected) !== -1 ) {
				$(row).addClass('active');
			}
		},
		columns: [
			{data: 'cct', name:'cct'},
			{data: 'nombre', name:'nombre'},
			{data: 'abrev_municipio', name:'abrev_municipio'},
			{data: 'd_localidad', name:'d_localidad'},
			{data: 'nivel', name:'nivel'},
			{data: 'turno.d_turno', name:'turno.d_turno', orderable: false, searchable: false}
		]
	});

	$esculaLite.find('tbody').on("click", "tr", function () {
		var id = this.id;
		var index = $.inArray(id, selected);

		if ( index === -1 ) {
			selected.push( id );
		} else {
			selected.splice( index, 1 );
		}
		$(this).toggleClass('active');
	});

}());
