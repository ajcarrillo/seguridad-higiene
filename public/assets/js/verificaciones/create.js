"use strict";
/**
 * Created by andres on 01/10/15.
 */
(function () {

	var $fecha = $('#txt-fecha');

	$fecha.datetimepicker({timepicker: false, format: 'd/m/Y', mask: true});
	$fecha.val(Seq.date.now());

})();
