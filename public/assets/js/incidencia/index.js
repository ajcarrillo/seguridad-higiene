/**
 * Created by lewis on 16/02/2016.
 */
(function () {
    "use strict";
    var $usersTable = $('#users-table'),
        url = $usersTable.data('url');

    $usersTable.DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        columns: [
            {data: 'nombre', name: 'nombre'},
            {data: 'clave', name: 'clave'},
            {data: 'acciones', name: 'acciones', orderable: false, searchable: false}
        ]
    });
}());

