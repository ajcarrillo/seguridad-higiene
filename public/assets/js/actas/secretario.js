"use strict";
/**
 * Created by andres on 11/09/15.
 */
(function () {
	var $btnBuscarSecretario = $('#btn-buscar-secretario');
	$btnBuscarSecretario.on('click', fnBuscarSecretario);

	function fnBuscarSecretario(){
		App.Config.modalPlantillaOpen = "secretario";
		route.urlName = "plantillasSearchModule";
		Seq.buttons.$button = $btnBuscarSecretario;
		Seq.buttons.loading();
		Seq.get.html(route.get.url(), fnShowModalSecretario);
	}

	function fnShowModalSecretario(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal);
		Seq.buttons.reset();
	}
})();
