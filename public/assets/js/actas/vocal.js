"use strict";
/**
 * Created by andres on 11/09/15.
 */
(function () {
	var $btnBuscarVocal = $('#btn-buscar-vocal');
	$btnBuscarVocal.on('click', fnBuscarVocal);

	function fnBuscarVocal(){
		App.Config.modalPlantillaOpen = "vocal";
		route.urlName = "plantillasSearchModule";
		Seq.buttons.$button = $btnBuscarVocal;
		Seq.buttons.loading();
		Seq.get.html(route.get.url(), fnShowModalVocal);
	}

	function fnShowModalVocal(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal);
		Seq.buttons.reset();
	}
})();
