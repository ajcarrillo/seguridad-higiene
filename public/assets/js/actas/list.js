"use strict";
/**
 * Created by andres on 28/09/15.
 */
(function () {
	App.Config.updateCentroTrabajo = false;
	var $btnNuevoReporte = $('[data-button="nuevo-reporte"]'),
		$btnOpenCalendario = $('[data-button="open-calendario"]'),
		$btnSendReporte = $('[data-button="send-reporte"]'),
		$btnGetReject = $('[data-button="get-reject"]');

	$btnNuevoReporte.on("click", nuevoReporte);
	$btnOpenCalendario.on("click", onClickOpenCalendario);
	$btnGetReject.on("click", onClickGetReject);

	$btnSendReporte.ChanModal({
		$modal: $('#modal'),
		size: "small",
		htmlAjax: true,
		ajax:{
			url: "data"
		}
	});

	function onClickGetReject(){
		var url = $(this).data('url');
		$.get(url, function (response) {
			bootbox.dialog({
				message: response.observacion,
				title: "Motivo de rechazo"
			});
		});
	}

	function onClickOpenCalendario(e){
		e.preventDefault();
		var url = $(this).data('url');
		Seq.get.html(url, showModalCalendario);
	}

	function nuevoReporte(){
		var that = this;
		$.getJSON($(that).data('check-reporte'),"", function (response) {
			if(response.hasReporte){
				Seq.alert.info("No puede realizar otro reporte para el mismo trimestre");
			}else{
				var url = $(that).data('action');
				Seq.buttons.$button = $btnNuevoReporte;
				Seq.buttons.loading();
				Seq.get.html(url, showModal);
			}
		});


	}

	function showModal(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal, "small");
		Seq.buttons.reset();
	}

	function showModalCalendario(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal, "small");
	}
})();
