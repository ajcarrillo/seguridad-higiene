"use strict";
/**
 * Created by andres on 09/09/15.
 */
(function(){
	var $body = $('body');
	var $btnSubmitRegistro = $('#btn-submit-registro');
	var $btnSaveRegistro = $('#btn-save-registro');

	$body.on("click", "[data-button='remove-centro']", fnRemoveCentro);
	$body.on("click", "[data-button='remove-vocal']", fnRemoveVocal);

	$body.on("change", "[data-button='cbx-add-tipo-vocal']", fnAddTipoVocal);
	$body.on("change", "[data-button='cbx-add-sector-vocal']", fnAddSectorVocal);


	$('#btn-submit-registro, #btn-save-registro').on('click', fnSubmitRegistro);

	$('#txt-fecha-oficio-oficial, #txt-fecha-oficio-sindical').datetimepicker({timepicker: false, format:'d/m/Y'});

	function fnRemoveCentro(){
		var row = $(this).parents('tr');
		var id = row.data('id');
		comision.removeCentro(id);
		removeRow(row);
	}

	function fnRemoveVocal(){
		var row = $(this).parents('tr');
		var id = row.data('id');
		comision.removeVocal(id);
		removeRow(row);
	}

	function removeRow(row){
		row.fadeOut(function () {
			this.remove();
		});
	}

	function fnAddTipoVocal(){
		var tipo = $(this).val();
		var row = $(this).parents('tr');
		var id = row.data('id');
		comision.vocales[findWithAttr(comision.vocales, 'id', id)].tipo = tipo;
	}

	function fnAddSectorVocal(){
		var sector = $(this).val();
		var row = $(this).parents('tr');
		var id = row.data('id');
		comision.vocales[findWithAttr(comision.vocales, 'id', id)].sector = sector;
	}

	function findWithAttr(array, attr, value) {
		for(var i = 0; i < array.length; i += 1) {
			if(array[i][attr] === value) {
				return i;
			}
		}
	}

	function fnSubmitRegistro(){
		route.urlName = "actasPostCreate";

		comision.addPresidente($('#form-presidente').serializeJSON());
		comision.addSecretario($('#form-secretario').serializeJSON());
		comision.addGenerales($('#form-general').serializeJSON());

		var postComision = {
			'_token': $('input[name=_token]').val(),
			'action': $(this).data('name')
		};

		postComision.presidente = comision.presidente;
		postComision.secretario = comision.secretario;
		postComision.centros = comision.centros;
		postComision.vocales = comision.vocales;
		postComision.generales = comision.generales;

		/*
		*
		* Obtener de la tabla vocales, el cargo de cada vocal
		* */
		var t = $('#table-vocales');
		var tbody = t.find('tbody');
		var filas = tbody.children('tr');

		for(var index=0; index < filas.length; ++index){
			var fila = $(filas[index]);
			var id = fila.data('id');
			var txt = fila.find('#txt-cargo-vocal');
			comision.vocales[findWithAttr(comision.vocales, 'id', id)].cargo = txt.val();
		}
		//

		comision.validate();
		if (comision.isValid){
			$.post(route.get.url(), postComision, function (result) {
				if(result.response){
					route.urlName = 'actas';
					window.location.replace(route.get.url());
				}
			}, "json");
		}else{
			var $errorListTemplate = $('#error-list-template').html();
			var $errorList = $('#error-list');

			$errorList.empty();
			$errorList.append(_.template($errorListTemplate)({'json':comision.validaciones}));
			$('#container-error-list').removeClass('hidden');

		}
	}

})();
