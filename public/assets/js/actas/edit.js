"use strict";
/**
 * Created by andres on 22/09/15.
 */
(function () {
	init();
	App.Config.updateCentroTrabajo = true;
	var $body = $('body');

	$body.on("click", "[data-button='remove-centro']", fnRemoveCentro);
	$body.on("click","[data-button='update-vocal']", fnUpdateVocal);
	$body.on("click","[data-button='save-vocal']", fnSaveVocal);
	$body.on("click","[data-button='remove-vocal']", fnRemoveVocal);

	var $btnSentRegistro = $('#btn-sent-registro');

	$btnSentRegistro.on("click", fnSentRegistro);

	var dataComision = JSON.parse(comisionEdit);

	function init(){
		var editComision = JSON.parse(comisionEdit);
		var editCentros = JSON.parse(centrosEdit);

		var oldElementsOnComision = (JSON.parse(JSON.stringify(comision)));

		oldElementsOnComision.presidente.push(editComision.presidente);
		oldElementsOnComision.secretario.push(editComision.secretario);

		editComision.vocales.forEach(function (element, index, array) {
			oldElementsOnComision.vocales.push(element);
			comision.vocales.push(element);
		});

		editCentros.forEach(function (element) {
			element.forEach(function (element) {
				oldElementsOnComision.centros.push(element);
				comision.centros.push(element);
			});
		});

		$('#txt-nombre-presidente').val(oldElementsOnComision.presidente[0].nombre);
		$('#txt-primer-apellido-presidente').val(oldElementsOnComision.presidente[0].primer_apellido);
		$('#txt-segundo-apellido-presidente').val(oldElementsOnComision.presidente[0].segundo_apellido);
		$('#txt-cargo-presidente').val(oldElementsOnComision.presidente[0].cargo);

		$('#txt-nombre-secretario').val(oldElementsOnComision.secretario[0].nombre);
		$('#txt-primer-apellido-secretario').val(oldElementsOnComision.secretario[0].primer_apellido);
		$('#txt-segundo-apellido-secretario').val(oldElementsOnComision.secretario[0].segundo_apellido);
		$('#txt-rfc-secretario').val(oldElementsOnComision.secretario[0].rfc);
		$('#cbx-sector-secretario').val(oldElementsOnComision.secretario[0].sector);

		var templateCentros = $('#template-centros').html();
		var tableCentros = $('#table-centros');

		var templateVocales = $('#template-vocales').html();
		var tableVocales = $('#table-vocales');

		renderTable(oldElementsOnComision.vocales, tableVocales, templateVocales);
		renderTable(oldElementsOnComision.centros, tableCentros, templateCentros);

		var templateUpdateButton = $('#template-update-button').html();
		var formPresidente = $('#form-presidente');
		var formSecretario = $('#form-secretario');

		formPresidente.append(_.template(templateUpdateButton)({'button': 'update-presidente'}));
		formSecretario.append(_.template(templateUpdateButton)({'button': 'update-secretario'}));

		var btnUpdatePresidente = $("[data-button='update-presidente']");
		var btnUpdateSecretario = $("[data-button='update-secretario']");
		var btnUpdateComision = $("[data-button='update-comision']");

		btnUpdatePresidente.on("click", fnUpdatePresidente);
		btnUpdateSecretario.on("click", fnUpdateSecretario);
		btnUpdateComision.on("click", fnUpdateComision);

		$('#txt-fecha-oficio-oficial, #txt-fecha-oficio-sindical').datetimepicker({timepicker: false, format:'d/m/Y'});

	}

	function fnRemoveCentro(){
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		var form = $('#form-edit-centro');
		var url = form.attr('action');
		var row = $(this).parents('tr');
		var id = row.data('id');
		var button = $(this).data('button');

		$.ajax({
			url: url,
			type: 'DELETE',
			data: form.serialize()+'&centro=' + id,
			success: function (response) {
				if(response.response){
					fnRemoveRow(row, button);
					Seq.buttons.reset();
				}
			}
		});
	}

	function fnRemoveRow(row, button){
		var id = row.data('id');

		switch (button){
			case 'remove-centro':
				comision.removeCentro(id);
				break;
			case 'remove-vocal':
				comision.removeVocal(row.data('rfc'));
				break;
		}

		row.fadeOut(function () {
			this.remove();
		});
	}

	function fnUpdatePresidente(){
		var txts = ['#txt-nombre-presidente', '#txt-primer-apellido-presidente', '#txt-segundo-apellido-presidente', '#txt-cargo-presidente'];

		if(isValid(null, txts)){
			Seq.errors.hide();
			Seq.buttons.$button = $(this);
			Seq.buttons.loading();
			var $form = $('#form-put-presidente');
			var url = $form.attr('action');
			var $formData = $('#form-presidente');
			var data = $form.serialize() +'&' + $formData.serialize() + '&id=' + dataComision.presidente.id;

			$.ajax({
				url: url,
				type: 'PUT',
				data: data,
				success: function (response) {
					console.log(response);
					Seq.buttons.reset();
				}
			});
		}else{
			Seq.errors.errorMessages.push('Los campos marcados en rojo son obligatorios');
			Seq.errors.show();
		}



	}

	function fnUpdateSecretario(){
		var txts = ['#txt-nombre-secretario', '#txt-primer-apellido-secretario', '#txt-segundo-apellido-secretario', '#txt-rfc-secretario'];
		var cbxs = ['#cbx-sector-secretario'];

		if(isValid(cbxs, txts)){
			Seq.errors.hide();
			Seq.buttons.$button = $(this);
			Seq.buttons.loading();
			var $form = $('#form-put-secretario');
			var $formData = $('#form-secretario');
			var url = $form.attr('action');
			var data = $form.serialize() + '&' + $formData.serialize() + '&id=' + dataComision.secretario.id;

			$.ajax({
				url: url,
				type: 'PUT',
				data: data,
				success: function (response) {
					console.log(response);
					Seq.buttons.reset();
				}
			});
		}else{
			Seq.errors.errorMessages.push('Los campos marcados en rojo son obligatorios');
			Seq.errors.show();
		}

	}

	function fnUpdateComision(){
		var cbxs = ['#sindicato'];
		var txts = ['#txt-oficio-oficial', '#txt-fecha-oficio-oficial', '#txt-oficio-sindical', '#txt-fecha-oficio-sindical'];

		if(isValid(cbxs, txts)){
			Seq.errors.hide();
			Seq.buttons.$button = $(this);
			Seq.buttons.loading();
			var $formData = $('#form-general');
			var $form = $('#form-put-registro');
			var url = $form.attr('action');
			var data = $form.serialize() + '&' + $formData.serialize();

			$.ajax({
				url: url,
				type: 'PUT',
				data: data,
				success: function (response) {
					console.log(response);
					Seq.buttons.reset();
				}
			});
		}else{
			Seq.errors.errorMessages.push('Los campos marcados en rojo son obligatorios');
			Seq.errors.show();
		}
	}

	function fnRemoveVocal(){
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		var $form = $('#form-vocales');
		var url = $form.attr('action');

		var row = $(this).parents('tr');
		var id = row.data('id');
		var button = $(this).data('button');

		$.ajax({
			url: url,
			type: 'DELETE',
			data: $form.serialize()+'&id=' + id,
			success: function (response) {
				if(response.response){
					fnRemoveRow(row, button);
					Seq.buttons.reset();
				}
			}
		});


	}

	function fnProcessDataVocal(t){
		var $form = $('#form-vocales');
		var url = $form.attr('action');
		var row = t.parents('tr');
		var id = row.data('id');

		var nombre = row.data('nombre');
		var primerApellido = row.data('primer_apellido');
		var segundoApellido = row.data('segundo_apellido');
		var txtCargo = row.find('#txt-cargo-vocal');
		var cbxSector = row.find("[data-button='cbx-add-sector-vocal']");
		var cbxTipo = row.find("[data-button='cbx-add-tipo-vocal']");
		var rfc = row.data('rfc');

		var data = "";

		data += $form.serialize();
		data += '&';
		data += 'id=' + id;
		data += '&';
		data += 'nombre=' + nombre;
		data += '&';
		data += 'primer_apellido=' + primerApellido;
		data += '&';
		data += 'segundo_apellido=' + segundoApellido;
		data += '&';
		data += 'cargo=' + txtCargo.val();
		data += '&';
		data += 'sector=' + cbxSector.val();
		data += '&';
		data += 'tipo=' + cbxTipo.val();
		data += '&';
		data += 'rfc=' + rfc;

		return {'data': data, 'url': url};
	}

	function fnUpdateVocal(){
		var row = $(this).parents('tr');

		var errors = validateVocal(row);

		if (errors <= 0){
			var data = fnProcessDataVocal($(this));

			Seq.errors.hide();
			Seq.buttons.$button = $(this);
			Seq.buttons.loading();

			$.ajax({
				url: data.url,
				type: 'PUT',
				data: data.data,
				success: function (response) {
					console.log(response);
					Seq.buttons.reset();
				}
			});
		}else{
			Seq.errors.errorMessages.push('Verifique que los datos esten completos');
			Seq.errors.show();
		}


	}

	function fnSaveVocal(){
		var row = $(this).parents('tr');

		var errors = validateVocal(row);

		if (errors <= 0){
			Seq.errors.hide();
			Seq.buttons.$button = $(this);
			Seq.buttons.loading();

			var data = fnProcessDataVocal($(this));
			var btnSaveVocal = $('[data-button="save-vocal"]');
			var btnUpdateVocal = $('[data-button="update-vocal"]');
			var btnRemoveVocal = $('[data-button="remove-vocal"]');

			$.post(data.url, data.data, function (result) {
				row.data('id', result.id);
				btnSaveVocal.addClass('hidden');
				btnUpdateVocal.removeClass('hidden');
				btnRemoveVocal.removeClass('hidden');
				Seq.buttons.reset();
			}, 'json');
		}else{
			Seq.errors.errorMessages.push('Verifique que los datos esten completos');
			Seq.errors.show();
		}


	}

	function validateVocal(row){
		var validate = ['#txt-cargo-vocal', "[data-button='cbx-add-sector-vocal']", "[data-button='cbx-add-tipo-vocal']"];
		var errors = 0;

		Seq.errors.errorMessages = [];
		validate.forEach(function (element) {
			var $element = row.find(element);
			if($element.val() === ''){
				errors += 1;
			}
		});

		return errors;
	}

	function renderTable(data, table, template){
		Seq.undescoreTableRender.render(template, table, data);
	}

	function isValid(cbxs, txts){
		var errors = 0;
		var c = cbxs || [];
		var t = txts || [];

		c.forEach(function (element, index, array) {
			var $element = $(element);
			var parent = $element.parent('div');
			parent.removeClass('has-error');
			if ($element.val() === ''){
				parent.addClass('has-error');
				errors += 1;
			}
		});

		t.forEach(function (element, index, array) {
			var $element = $(element);
			var parent = $element.parent('div');
			parent.removeClass('has-error');
			if ($element.val() === ''){
				parent.addClass('has-error');
				errors += 1;
			}
		});

		return errors <= 0;
	}

	function fnSentRegistro(){
		route.urlName = 'actas';
		var $form = $('#form-enviar-reporte');
		var url = $form.attr('action');

		$.post(url, $form.serialize(), function (result) {
			if (result.response){
				window.location.href = route.get.url();
			}
		});
	}

})();
