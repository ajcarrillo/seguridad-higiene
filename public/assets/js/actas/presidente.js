"use strict";
/**
 * Created by andres on 11/09/15.
 */
(function () {
	var $btnBuscarPresidente = $('#btn-buscar-presidente');
	$btnBuscarPresidente.on('click', fnBuscarPresidente);

	function fnBuscarPresidente(){
		App.Config.modalPlantillaOpen = "presidente";
		route.urlName = "plantillasSearchModule";
		Seq.buttons.$button = $btnBuscarPresidente;
		Seq.buttons.loading();
		Seq.get.html(route.get.url(), fnShowModalPresidente);
	}

	function fnShowModalPresidente(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal);
		Seq.buttons.reset();
	}
})();
