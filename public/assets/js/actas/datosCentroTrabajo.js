"use strict";
/**
 * Created by andres on 23/09/15.
 */
(function () {
	var $btnAddCentro = $('#add-centro');
	$btnAddCentro.on('click', fnGetCentroTrabajoTemplate);

	function fnGetCentroTrabajoTemplate() {
		route.urlName = "centrosSearchModule";
		Seq.buttons.$button = $btnAddCentro;
		Seq.buttons.loading();
		Seq.get.html(route.get.url(), showModal);
	}

	function showModal(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal);
		Seq.buttons.reset();
	}
})();
