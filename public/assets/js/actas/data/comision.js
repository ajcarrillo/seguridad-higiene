"use strict";
/**
 * Created by andres on 14/09/15.
 */
var comision = {
	isValid: false,
	presidente: [],
	secretario: [],
	vocales: [],
	centros: [],
	generales: [],
	validaciones: [],

	hasCentro: function (centroId) {
		return this.find(this.centros, centroId);
	},
	hasVocal: function (vocalId) {
		return this.find(this.vocales, vocalId);
	},
	find: function(obj, centroId){
		var array = Seq.json.getKeys(obj, centroId);
		return array.length > 0;
	},
	add: function(array, data){
		array.push(data);
	},
	remove: function(element, array, vocal){
		return array.filter(function (obj) {
			if(vocal){
				return obj.rfc !== element;
			}
			return obj.id !== element;
		});
	},
	addPresidente: function (data) {
		this.presidente = [];
		this.presidente.push(data);
	},
	addSecretario: function (data) {
		this.secretario = [];
		this.secretario.push(data)
	},
	addVocal: function (data) {
		if(!this.hasVocal(data.rfc)){
			this.add(this.vocales, data);
			return true;
		}
		return false;
	},
	addCentro: function (data) {
		if(!this.hasCentro(data.id)){
			this.add(this.centros, data);
			return true;
		}
		return false;
	},
	addGenerales: function (data) {
		this.generales = [];
		this.generales.push(data);
	},
	removeVocal: function (vocalId) {
		this.vocales = this.remove(vocalId, this.vocales, true);
	},
	removeCentro: function (centroId) {
		this.centros = this.remove(centroId, this.centros);
	},
	validate: function(){
		comision.validar.vacios();
	}
};

comision.validar = {
	vacios: function () {
		comision.validaciones = [];
		var selectIds = ['#sindicato', '#cbx-sector-secretario'];
		var textBoxIds = ['#txt-oficio-oficial',
			'#txt-fecha-oficio-oficial',
			'#txt-oficio-sindical',
			'#txt-fecha-oficio-sindical',
			'#txt-nombre-presidente',
			'#txt-primer-apellido-presidente',
			'#txt-segundo-apellido-presidente',
			'#txt-cargo-presidente',
			'#txt-nombre-secretario',
			'#txt-primer-apellido-secretario',
			'#txt-segundo-apellido-secretario',
			'#txt-rfc-secretario'
		];
		var strings = ['vocales', 'centros'];
		var secciones = [comision.vocales, comision.centros];

		secciones.forEach(function (element, index, array) {
			if (element.length === 0){
				comision.validaciones.push('La sección ' + strings[index] + ' es requerida, por favor verifique que los datos estén completos');
			}
		});

		selectIds.forEach(function (element, index, array) {
			var $element = $(element);
			var parent = $element.parent('div');
			parent.removeClass('has-error');
			if ($element.val() === ''){
				parent.addClass('has-error');
			}
		});

		textBoxIds.forEach(function (element, index, array) {
			var $element = $(element);
			var parent = $element.parent('div');
			parent.removeClass('has-error');
			if ($element.val() === ''){
				parent.addClass('has-error');
			}
		});

		if(comision.validaciones.length === 0)
			comision.isValid = true;
	}
};
