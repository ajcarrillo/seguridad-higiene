"use strict";
/**
 * Created by andres on 27/11/15.
 */

(function () {
	var $btnUpdatePresidente = $('[data-button="update-presidente-actual"]'),
		$btnUpdatePresidentePropuesto = $('[data-button="update-presidente-propuesto"]'),
		$container = $('#update-presidente-container');

	$btnUpdatePresidente.on("click", onClickUpdatePresidente);
	$btnUpdatePresidentePropuesto.on("click", onClickUpdatePresidentePropuesto);

	function onClickUpdatePresidente(){
		$container.find('#create-transaction').removeClass('hidden').addClass('animated fadeIn');

	}

	function onClickUpdatePresidentePropuesto(){
		$container.find('#update-transaction').removeClass('hidden').addClass('animated fadeIn');
	}
}());
