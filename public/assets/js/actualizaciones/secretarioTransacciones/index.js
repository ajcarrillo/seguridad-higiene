"use strict";
/**
 * Created by andres on 27/11/15.
 */
(function () {
	var $btnUpdateSecretarioActual = $('[data-button="update-secretatio-actual"]'),
		$btnUpdateSecretarioPropuesto = $('[data-button="update-secretatio-propuesto"]'),
		$container = $('#update-secretario-container');

	$btnUpdateSecretarioActual.on("click", onClickSecretarioActual);
	$btnUpdateSecretarioPropuesto.on("click", onClickSecretarioPropuesto);

	function onClickSecretarioActual(){
		$container.find('#create-transaction').removeClass('hidden').addClass('animated fadeIn');
	}

	function onClickSecretarioPropuesto(){
		$container.find('#update-transaction').removeClass('hidden').addClass('animated fadeIn');
	}

}());
