"use strict";
/**
 * Created by andres on 03/12/15.
 */
(function () {
	var $vocalesTable = $('#vocales-table'),
		$transaccionesTable = $('#transaccion-table');

	$vocalesTable.on("click", '[data-button="delete-vocal"]', onClickDeleteVocal);
	$transaccionesTable.on("click", '[data-button="delete-transaccion"]', onclickDeleteTransaccion);

	function onclickDeleteTransaccion(){
		var $form = $('#delete-transaccion-form'),
			url = $form.attr('action').replace(':ID:', $(this).data('id'));

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		var post = $.post(url, $form.serialize());

		post.done(function (response) {
			fillTable(response);
		});

		post.fail(function (response) {
			console.log(response);
		});

		post.always(function () {
			Seq.buttons.reset();
		});

	}

	function onClickDeleteVocal() {
		$('#actions').val("Delete");
		var $form = $('#create-transaccion-form'),
			url = $form.attr('action'),
			$that = $(this),
			data = $.param($.extend({}, $form.serializeJSON(), $that.data()));

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		var request = $.post(url, data, function (response) {
			request.then(Seq.response.success(response, fillTable), Seq.response.fail);
			Seq.buttons.reset();
		});

	}

	function fillTable(response){
		if (response.isValid) {
			var template = $('#transaccion-table-template').html(),
				table = $('#transaccion-table');
			Seq.undescoreTableRender.render(template, table, response.transacciones, 5);
		}else{
			Seq.alert.danger(response.msg);
		}
	}

	$vocalesTable.DataTable({
		processing: true,
		serverSide: true,
		searching: false,
		ordering: false,
		ajax: $vocalesTable.data('url'),
		columns: [
			{data: 'id', name: 'id', 'visible': false},
			{data: 'nombreCompleto', name: 'nombreCompleto'},
			{data: 'cargo', name: 'cargo'},
			{data: 'nombreSector', name: 'nombreSector'},
			{data: 'nombreTipo', name: 'nombreTipo'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});

}());
