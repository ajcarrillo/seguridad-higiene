"use strict";
/**
 * Created by andres on 09/12/15.
 */

(function () {
	var $form = $('#create-vocal-transaccion');

	$form.on("submit", onSubmitForm);

	function onSubmitForm(e){
		e.preventDefault();
		Seq.buttons.$button = $('#btn-create-transaccion');
		Seq.buttons.loading();
		var post = $.post($(this).attr('action'), $(this).serialize());

		post.done(function (response) {
			if(response.isValid){
				Seq.alert.success(response.msg);
			}else{
				Seq.alert.danger(response.msg);
			}
			$('#modal').modal('hide')
		});

		post.fail(function (response) {
			Seq.alert.standarError();
			console.log(response);
		});

		post.always(function () {
			Seq.buttons.reset();
		});
	}

}());
