"use strict";
/**
 * Created by andres on 01/12/15.
 */

(function () {
	var $addCentros = $('[data-button="add-centros"]');

	$addCentros.on("click", onClickAddCentros);

	function onClickAddCentros() {
		var $form = $('#add-centros-form'),
			$centros = $('#centros'),
			url = $form.attr('action'),
			data;

		/*$centros.val(selected);*/
		/*data.centros = JSON.stringify(selected);*/
		data = $form.serializeJSON();
		data.centros = selected;

		if (selected.length == 0) {
			Seq.alert.danger("Por favor seleccione un centro de trabajo");
		} else {
			$.post(url, data)
				.done(function () {
					Seq.alert.success("Agregado correctamente");
				})
				.fail(function (response) {
					Seq.alert.danger(response.responseJSON.msg);
				});
		}


	}
}());
