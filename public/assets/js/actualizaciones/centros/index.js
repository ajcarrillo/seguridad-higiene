"use strict";
/**
 * Created by andres on 26/11/15.
 */

(function () {

	var $body = $('body'),
		$centrosTable = $('#centros-table'),
		$form = $('#centro-transaccion-form'),
		$deleteTransaccion = $('[data-button="delete-transaccion"]');


	$body.on("click", "[data-button='delete-centro']", onClickDeleteCentro);
	$body.on("click", "[data-button='delete-transaccion']", onClickDeleteTransaccion);


	//$deleteTransaccion.on("click", onClickDeleteTransaccion);

	function onClickDeleteTransaccion(){
		var $form = $('#delete-transaccion-form'),
			url = $form.attr('action').replace(':ID:', $(this).data("id")),
			data = $form.serialize(),
			template = $('#transaccion-table-template').html(),
			table = $('#transaccion-table');

		bootbox.confirm({
			message: "¿Desea quitar el elemento seleccionado?",
			size: 'small',
			title: "Eliminar",
			callback: function (ok) {
				if(ok){
					$.post(url, data).done(function (result) {
						Seq.alert.success("Se eliminó correctamente");
						Seq.undescoreTableRender.render(template, table, result, 7);
					}).fail(function (result) {
						Seq.alert.standarError();
					});
				}
			}
		});
	}

	function onClickDeleteCentro() {
		$('#row_id').val($(this).data('id'));
		$('#centro_de_trabajo_id').val($(this).data('centro_de_trabajo_id'));
		$('#actions').val('Delete');

		$form.submit();

	}

	$centrosTable.DataTable({
		processing: true,
		serverSide: true,
		searching: false,
		ordering: false,
		ajax: $centrosTable.data('url'),
		columns: [
			{data: 'id', name: 'id', 'visible': false},
			{data: 'centro_de_trabajo_id', name: 'centro_de_trabajo_id', visible: false},
			{data: 'escuela_lite.cct', orderable: false, searchable: false},
			{data: 'escuela_lite.nombre', orderable: false, searchable: false},
			{data: 'escuela_lite.abrev_municipio', orderable: false, searchable: false},
			{data: 'escuela_lite.d_localidad', orderable: false, searchable: false},
			{data: 'escuela_lite.nivel', orderable: false, searchable: false},
			{data: 'escuela_lite.turno.d_turno', orderable: false, searchable: false},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});
}());
