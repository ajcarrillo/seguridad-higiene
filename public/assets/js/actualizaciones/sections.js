"use strict";
/**
 * Created by andres on 03/12/15.
 */

(function () {
	var $btnTrabajadores = $('[data-button="trabajadores"]');

	$btnTrabajadores.on("click", onClickTrabajadores);

	function onClickTrabajadores(){
		var $form = $('#trabajadores-form'),
			url = $(this).data("url");

		Seq.get.html(url, showForm);


		function showForm(html){
			var $modal = $('#modal');
			Seq.modal.showModal(html, $modal, "small");
		}


	}
}());
