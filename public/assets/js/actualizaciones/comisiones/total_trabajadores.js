"use strict";
/**
 * Created by andres on 03/12/15.
 */
(function () {
	var $updateTrabajadores = $('[data-button="update-trabajadores"]'),
		$form = $('#trabajadores-form');

	/*$updateTrabajadores.on("click", onClickUpdateTrabajadores);*/
	$form.on("submit", onClickUpdateTrabajadores);

	function onClickUpdateTrabajadores(e) {
		e.preventDefault();
		var data = $form.serialize(),
			url = $form.attr('action');

		Seq.buttons.$button = $updateTrabajadores;
		Seq.buttons.loading();
		$.post(url, data)
			.done(function (result) {
				Seq.alert.success("El número de trabajadores se actualizó correctamente");
			})
			.fail(function (result) {
				Seq.alert.standarError();
			})
			.always(function () {
				Seq.buttons.reset();
			});
	}

}());
