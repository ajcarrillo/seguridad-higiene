"use strict";
/**
 * Created by andres on 06/11/15.
 */

(function ($) {
	$.fn.ChanModal = function (options) {
		var opts = $.extend(true, {}, {
			$modal: "",
			size: "",
			htmlAjax: false,
			ajax:{
				url: ""
			}
		}, options);

		var that = this;
		$(this).on("click", onClick);

		function onClick(){
			var url;
			if(opts.htmlAjax){
				if(opts.ajax.url == "data"){
					url = $(that).data('route');
				}else{
					url = opts.ajax.url;
				}
				$.get(url, function (html) {
					render(html);
				});
			}else{
				opts.$modal.modal('show');
			}
		}

		function render(html){
			var $modalDialog = opts.$modal.find('.modal-dialog');
			$modalDialog.removeClass("modal-sm modal-lg");
			switch (opts.size){
				case "small":
					$modalDialog.addClass("modal-sm");
					break;
				case "large":
					$modalDialog.addClass("modal-lg");
					break;
				default :
					$modalDialog.addClass("modal-lg");
					break;
			}

			opts.$modal.children().children().html(html);
			opts.$modal.modal('show');
		}

	}
}(jQuery));
