"use strict";
/**
 * Created by andres on 09/09/15.
 */
(function () {

	$('body').on("click", "[data-button='add-centro']", fnAddCentro);

	var $btnBuscar = $('#btn-search');
	var $btnBuscarByName = $('#btn-search-by-name');
	var $searchTextbox = $('#txt-cct, #txt-name');

	$btnBuscar.on("click", fnSearchCentro);
	$btnBuscarByName.on("click", fnSearchCentroByName);

	$searchTextbox.on("keypress", dispatch);

	function dispatch(e){
		if(e.which == 13){
			switch ($(this).attr('id')){
				case "txt-cct":
					fnSearchCentro();
					break;
				case "txt-name":
					fnSearchCentroByName();
					break;
			}
		}
	}

	function fnSearchCentro(){
		Seq.buttons.$button = $btnBuscar;
		route.urlName = "centrosByClavecct";
		var url = route.get.url().replace("{cct}", $('#txt-cct').val());
		Seq.buttons.loading();
		Seq.get.json(url, renderTable);
	}

	function fnSearchCentroByName(){
		Seq.buttons.$button = $btnBuscarByName;
		route.urlName = "centrosByName";
		var url = route.get.url().replace("{name}", $('#txt-name').val());
		Seq.buttons.loading();
		Seq.get.json(url, renderTable);
	}

	function renderTable(data){
		var template = $('#template-centro').html();
		var table = $('#centros');
		Seq.undescoreTableRender.render(template, table, data);
		Seq.buttons.reset();
	}

	function fnAddCentro(){
		var data = {
			'id': $(this).data("id"),
			'clavecct': $(this).data("cct"),
			'NOMBRECT': $(this).data("name"),
			'NOMBREMUN': $(this).data("municipio"),
			'NOMBRELOC': $(this).data("localidad"),
			'NIVEL': $(this).data("nivel")
		};

		if (App.Config.updateCentroTrabajo){
			fnPostAddCentro(data);
		}else{
			fnAddToTable(data);
		}

	}

	function fnPostAddCentro(data){
		var $formCentro = $('#form-post-centro');
		var url =  $formCentro.attr('action');
		var dataSerialize = $formCentro.serialize()+'&' + $.param(data);

		if(comision.addCentro(data)){
			$.post(url, dataSerialize, function (result) {
				console.log(result);
				var $tabelCentro = $('#table-centros');
				$tabelCentro.find('tbody').append(_.template("<tr data-id='<%= id %>'><td><%= clavecct %></td><td><%= NOMBRECT %></td><td><%= NOMBREMUN %></td><td><%= NOMBRELOC %></td><td><%= NIVEL %></td><td><button type='button' class='btn btn-danger' data-button='remove-centro' data-loading-text='Cargando...' >Quitar</button></td></tr>")(data));
			}, 'json');
		}
	}

	function fnAddToTable(data){
		if(comision.addCentro(data)){
			var $tabelCentro = $('#table-centros');
			$tabelCentro.find('tbody').append(_.template("<tr data-id='<%= id %>'><td><%= clavecct %></td><td><%= NOMBRECT %></td><td><%= NOMBREMUN %></td><td><%= NOMBRELOC %></td><td><%= NIVEL %></td><td><button type='button' class='btn btn-danger' data-button='remove-centro' data-loading-text='Cargando...' >Quitar</button></td></tr>")(data));
		}
	}
})();
