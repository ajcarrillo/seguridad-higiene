"use strict";
/**
 * Created by andres on 29/10/15.
 */
(function () {

	var $modal = $('#modal');
	$modal.unbind();
	$modal.on("click", "[data-button='add-centro']", onClickAddCentro);
	$modal.on("click", "[data-button='paginate']", onClickPaginate);

	var $btnSearchByCCT = $('#btn-search-by-cct');
	var $btnSearchByNombre = $('#btn-search-by-nombre');
	var $searchTextbox = $('#txt-cct, #txt-nombre');
	var $formSearchCentro = $('#form-search-centro');
	var template = $('#template-add-centro').html();
	var table = $('#centros');
	var that;

	$btnSearchByCCT.on("click", fnSearchByCCT);
	$btnSearchByNombre.on("click", fnSearchByNombre);

	$searchTextbox.on("keypress", dispatch);

	function dispatch(e){
		if(e.which == 13){
			switch ($(this).attr('id')){
				case "txt-cct":
					fnSearchByCCT();
					break;
				case "txt-nombre":
					fnSearchByNombre();
					break;
			}
		}
	}

	function fnSearchByCCT(){
		var url = $formSearchCentro.attr('action').replace('PFILTER', $('#txt-cct').val()).replace('PBY', 'cct');
		fnDoSearch(url, $btnSearchByCCT);
	}

	function fnSearchByNombre(){
		var url = $formSearchCentro.attr('action').replace('PFILTER', $('#txt-nombre').val()).replace('PBY', 'nombre');
		fnDoSearch(url, $btnSearchByNombre);
	}

	function fnDoSearch(url, $btn){
		Seq.buttons.$button = $btn;
		Seq.buttons.loading();
		Seq.get.json(url, fnRender);
	}

	function fnRender(){
		Seq.undescoreTableRender.render(template, table, arguments[0].data, 6);
		Seq.undescoreTableRender.pager(arguments[1], arguments[0].last_page);
		$('#pager').find('nav .pagination li').first().addClass("active");
		Seq.buttons.reset();
	}

	function onClickPaginate(){
		that = this;
		var url = $(this).data('action');
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		Seq.get.json(url, fnPaginate);

	}

	function fnPaginate(){
		Seq.undescoreTableRender.render(template, table, arguments[0].data, 6);
		$(that).parents("ul").find("li").removeClass("active");
		$(that).parent().addClass("active");
		Seq.buttons.reset();
	}

	function onClickAddCentro(){
		var $form = $('#form-store-comision-centro');
		var url = $form.attr('action').replace(':comision_id:', $('#hf-comision-id').val());
		var data = $form.serializeJSON();
		var that = this;

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		data.centro_id = $(this).data('id');

		$.post(url, $.param(data), function (result) {
			var comisionCentro = {
				'id': $(that).data('id'),
				'cct': $(that).data('cct'),
				'nombre': $(that).data('nombre'),
				'abrev_municipio': $(that).data('abrev_municipio'),
				'd_localidad': $(that).data('d_localidad'),
				'nivel': $(that).data('nivel'),
				'd_turno': $(that).data('d_turno')
			};
			var template = $('#row-centro-template').html();
			var table = $('#table-centros-de-trabajo');

			if(result.exists || result.hasComision){
				Seq.alert.warning("El centro ya existe en la comisión o ya está asignado a otra comisión");
			}else{
				table.ChanTable({
					ajax:{
						url: route.get.url('comision.centros.index').replace(":comision_id:", $('#hf-comision-id').val()),
						data: "data"
					},
					template: template
				});
				Seq.alert.success("El centro de agregó correctamente");
			}
		}).fail(function(result){
			Seq.alert.standarError();
			console.log(result);
		}).always(function () {
			Seq.buttons.reset();
		});
	}



})();
