"use strict";
/**
 * Created by andres on 11/12/15.
 */

(function () {
	var $addCentros = $('[data-button="add-centros"]');

	$addCentros.on("click", onClickAddCentros);

	function onClickAddCentros(){
		var $form = $('#add-centros-form'),
			url = $form.attr('action'),
			data;

		data = $form.serializeJSON();
		data.centros = selected;

		if (selected.length == 0) {
			Seq.alert.danger("Por favor seleccione un centro de trabajo");
		} else {
			var post = $.post(url, data);

			post.done(function (response) {
				if(response.isValid){
					Seq.alert.success("Registro guardado correctamente");
				}else{
					Seq.alert.danger(response.msg);
				}
			});

			post.fail(function (response) {
				Seq.alert.standarError();
				console.log(response)
			});
		}


	}

}());
