"use strict";
/**
 * Created by andres on 29/10/15.
 */
(function () {
	var $body = $('body'),
		$tableCentros = $('#table-centros-de-trabajo');

	$body.on("click", '[data-button="delete-centro"]', onClickDeleteCentro);

	function onClickDeleteCentro(){
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		var $form = $('#form-delete-centro-comision');
		var url = $form.attr('action').replace(':id:', $(this).data('id'));
		$form.attr('action', url);
		$form.submit();
	}

	$tableCentros.DataTable({
		processing: true,
		serverSide: true,
		searching: false,
		ordering: false,
		ajax: $tableCentros.data('url'),
		columns: [
			{data: 'id', name: 'id', 'visible': false},
			{data: 'centro_de_trabajo_id', name: 'centro_de_trabajo_id', visible: false},
			{data: 'escuela_lite.cct', orderable: false, searchable: false},
			{data: 'escuela_lite.nombre', orderable: false, searchable: false},
			{data: 'escuela_lite.abrev_municipio', orderable: false, searchable: false},
			{data: 'escuela_lite.d_localidad', orderable: false, searchable: false},
			{data: 'escuela_lite.nivel', orderable: false, searchable: false},
			{data: 'escuela_lite.turno.d_turno', orderable: false, searchable: false},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});

})();
