"use strict";
/**
 * Created by andres on 02/11/15.
 */
(function () {

	var $vocalesTable = $('#vocales-table'),
		$btnAddVocal = $("[data-button='add-vocal']");

	$btnAddVocal.on("click", onClickAddVocal);
	$vocalesTable.on("click", "[data-button='delete-vocal']", onClickDeleteVocal);

	var table = $vocalesTable.DataTable({
		processing: true,
		serverSide: true,
		searching: false,
		ordering: false,
		ajax: $vocalesTable.data('url'),
		columns: [
			{data: 'id', name: 'id', 'visible': false},
			{data: 'nombreCompleto', name: 'nombreCompleto'},
			{data: 'cargo', name: 'cargo'},
			{data: 'nombreSector', name: 'nombreSector'},
			{data: 'nombreTipo', name: 'nombreTipo'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});

	function onClickDeleteVocal(){

		var id = $(this).data('row_id'),
			$form = $('#delete-vocal'),
			url = $form.attr('action').replace(':VOCAL_ID:', id),
			data = $form.serialize();

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();

		bootbox.confirm({
			message: "¿Desea quitar el elemento seleccionado?",
			size: 'small',
			title: "Eliminar",
			callback: function (ok) {
				if(ok){
					$.post(url, data).done(function (result) {

						if(result.isValid){
							Seq.alert.success("Se eliminó correctamente");
							table.ajax.reload();
						}else{
							Seq.alert.standarError();
						}
					}).fail(function (result) {
						Seq.alert.standarError();
						console.log(result);
					}).always(function () {
						Seq.buttons.reset();
					});
				}else{
					Seq.buttons.reset();
				}
			}
		});

	}

	function onClickAddVocal(){
		var url = $(this).data('url');

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		Seq.get.html(url, showForm);

	}

	function showForm(html){
		var $modal = $('#modal');
		Seq.modal.showModal(html, $modal, "small");
		Seq.buttons.reset();
	}

	$('#modal').on('hidden.bs.modal', function (e) {
		table.ajax.reload();
	})

})();
