"use strict";
/**
 * Created by andres on 07/01/16.
 */

(function () {
	var $personalTable = $('#personal-table');

	$personalTable.on("click", "[data-button='add-vocal']", onClickAddVocal);

	function onClickAddVocal(){
		var $hidden = $('#form-url'),
			url = $hidden.data('url')
				.replace('pNOMBREp',$(this).data('nombre'))
				.replace('pPRIMER_APELLIDOp',$(this).data('primer_apellido'))
				.replace('pSEGUNDO_APELLIDOp',$(this).data('segundo_apellido'));

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		Seq.get.html(url, showForm);
	}

	function showForm(html){
		var $modal = $('#modal');
		Seq.modal.showModal(html, $modal, "small");
		Seq.buttons.reset();
	}
}());
