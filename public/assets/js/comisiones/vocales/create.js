"use strict";
/**
 * Created by andres on 03/11/15.
 */
(function () {
	var $newVocalForm = $('#new-vocal-form');
	var $btnSaveVocal = $('[data-button="save-vocal"]');

	$newVocalForm.on("submit", onSubmitNewVocal);

	function onSubmitNewVocal(e){
		e.preventDefault();
		var url = $(this).attr('action');
		var data = $(this).serialize();
		var that = this;

		Seq.buttons.$button = $btnSaveVocal;
		Seq.buttons.loading();
		$.post(url, data, function (result) {
			if(result.isValid){
				Seq.alert.success("Se agregó el vocal correctamente");
			}else{
				Seq.alert.standarError();
			}

		}).fail(function () {
			Seq.alert.standarError();
		}).always(function () {
			Seq.buttons.reset();
			$(that)[0].reset();
		});
	}
})();
