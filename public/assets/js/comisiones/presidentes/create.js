"use strict";
/**
 * Created by andres on 06/11/15.
 */

(function(){
	var $createNewPresidenteForm = $('#create-new-presidente-form'),
		$createNewPresidenteBtn = $('[data-button="create-new-presidente"]');

	$createNewPresidenteForm.on("submit", onSubmitCreateNewPresidente);


	function onSubmitCreateNewPresidente() {
		$createNewPresidenteBtn.button('loading');
	}


}());
