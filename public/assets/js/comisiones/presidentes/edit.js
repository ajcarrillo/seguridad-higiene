"use strict";
/**
 * Created by andres on 06/01/16.
 */

(function () {
	var $btnEditPresidente = $('[data-button="show-presidente-form"]');

	$btnEditPresidente.on("click", onClickShowPresidenteForm);

	function onClickShowPresidenteForm(){
		var form = $('#presidente-form');
		$(this).parent().hide();
		form.removeClass('hidden');
	}
}());
