"use strict";
/**
 * Created by andres on 12/11/15.
 */
(function(){
	var $createNewSecretarioForm = $('#create-new-secretario-form'),
		$createNewSecretarioBtn = $('[data-button="create-new-secretario"]');

	$createNewSecretarioForm.on("submit", onSubmitCreateNewSecretario);


	function onSubmitCreateNewSecretario() {
		$createNewSecretarioBtn.button('loading');
	}


}());
