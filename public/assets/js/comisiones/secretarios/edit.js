"use strict";
/**
 * Created by andres on 06/01/16.
 */

(function () {
	var $btnEditSecretario = $('[data-button="show-secretario-form"]');

	$btnEditSecretario.on("click", onClickEditSecretario);

	function onClickEditSecretario(){
		var form = $('#secretario-form');
		$(this).parent().hide();
		form.removeClass('hidden');
	}

}());
