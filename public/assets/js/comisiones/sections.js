"use strict";
/**
 * Created by andres on 06/11/15.
 */
(function () {
	var $btnCreateNewPresidente = $('[data-button="create-new-presidente"]'),
		$btnEditPresidente = $('[data-button="edit-presidente"]'),
		$btnCreateNewSecretario = $('[data-button="create-new-secretario"]'),
		$btnEditSecretario = $('[data-button="edit-secretario"]');

	$btnCreateNewPresidente.on("click", onClickCallModal);
	$btnEditPresidente.on("click", onClickCallModal);
	$btnCreateNewSecretario.on("click", onClickCallModal);
	$btnEditSecretario.on("click", onClickCallModal);

	function onClickCallModal() {
		modal(this);
	}

	function modal(that){
		var url = $(that).data('route');
		Seq.buttons.$button = $(that);
		Seq.buttons.loading();
		Seq.get.html(url, showModal);
	}

	function showModal(html){
		var $modal = $('#modal');
		Seq.modal.showModal(html, $modal, "small");
		Seq.buttons.reset();
	}

	/*$btnCreateNewPresidenteFromPlantilla.on("click", onClickCreateNewPresidenteFromPlantilla);
	$btnCreateNewSecretario.on("click", onClickCreateNewSecretario);
	$btnCreateNewSecretarioFromPlantilla.on("click", onClickCreateNewSecretarioFromPlantilla);



	function onClickCreateNewPresidenteFromPlantilla() {
		alert("js");
	}
	function onClickCreateNewSecretario() {
		alert("js");
	}
	function onClickCreateNewSecretarioFromPlantilla() {
		alert("js");
	}*/



}());
