"use strict";
/**
 * Created by andres on 15/12/15.
 */

(function () {
	var $form = $('#trabajadores-form'),
		$updateTrabajadores = $('[data-button="update-trabajadores"]');

	$form.on("submit", onSubmitForm);


	function onSubmitForm(e) {
		e.preventDefault();

		Seq.buttons.$button = $updateTrabajadores;
		Seq.buttons.loading();

		var url = $(this).attr('action'),
			data = $(this).serialize(),
			$labelTotal = $('#label-total'),
			post;

		post = $.post(url, data);

		post.done(function (response) {
			if(response.isValid){
				Seq.alert.success(response.msg);
				$labelTotal.text($('#total_trabajadores').val());
			}
		});

		post.fail(function (response) {
			Seq.alert.standarError();
			console.log(response);
		});

		post.always(function () {
			Seq.buttons.reset();
		});

	}



}());
