"use strict";
/**
 * Created by andres on 28/01/16.
 */

(function () {
	var apertura = document.getElementById('apertura'),
		cierre = document.getElementById('cierre'),
		$form = $('#reset-dates-form');

	$form.on("submit", onSubmitForm);

	function onSubmitForm(){
		Seq.buttons.$button = $('#btn-submit-form');
		Seq.buttons.loading();
	}

	function validateDates(){
		if(cierre.value <= apertura.value){
			cierre.setCustomValidity("El cierre del proceso no debe ser menor o igual a la apertura");
		}else{
			cierre.setCustomValidity('');
		}
	}

	apertura.onblur = validateDates;
	cierre.onblur = validateDates;

}());
