"use strict";
/**
 * Created by andres on 27/01/16.
 */

(function () {

	var $editEtapa = $("[data-button='edit-etapa']");

	$editEtapa.on("click", onClickEditEtapa);

	function onClickEditEtapa(){
		var url = $(this).data('url');

		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		Seq.get.html(url, showForm);
	}

	function showForm(html){
		var $modal = $('#modal');
		Seq.modal.showModal(html, $modal);
		Seq.buttons.reset();
	}

}());
