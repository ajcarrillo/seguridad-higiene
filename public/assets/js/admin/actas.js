/**
 * Created by wsanchez on 06/10/2015.
 * Updated by lewis on 23/02/2016
 */
$(document).ready(function() {
    var $body = $('body');

    $('#tabs_actas a').on("click", function (e) {
        var url = this.getAttribute("data-url"),
            grid = $('[data-id="gridActasRecorrido"]').DataTable();

        grid.ajax.url(url).load();
        e.preventDefault();
    });
    /*$(document).on('click', '#panel-busqueda', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.removeClass('glyphicon glyphicon-menu-up').addClass('glyphicon glyphicon-menu-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.removeClass('glyphicon glyphicon-menu-down').addClass('glyphicon glyphicon-menu-up');
        }
    });*/

    $body.on("click", '[data-button="aceptar-acta"]', onAceptaActa);
    $body.on("click", '[data-button="observacion-acta"]', onObservacionActa);
    $body.on("click", '[data-button="asigna-clave-comision"]', onAsignaClaveComision);



    function onAceptaActa(e){
        e.preventDefault();
        var idReporte = $(this).data('id');
        $.ajax({
            url: 'grabaComision',
            type: "post",
            data: {'idReporte' : idReporte, '_token': $('input[name=_token]').val()},
            success: function(data){
                var url = 'enviadas';
                Seq.alert.success("El acta fue aceptada");

                $('[data-id="gridActasRecorrido"]').DataTable().ajax.url('enviadasJson').load();
            }
        });
    }

    function fnShowModalObservacionActa(html){
        var $modal = $('#modal');
        Seq.modal.render(html, $modal, "large");
        Seq.buttons.reset();
    }

    function onObservacionActa(){
        Seq.buttons.$button = $(this);
        route.urlName = "getObservacionActa";
        Seq.buttons.loading();
        Seq.get.html(route.get.url().replace("{id}", $(this).data('id')), fnShowModalObservacionActa);
    }

    function fnShowModalAsignaClaveComision(html){
        var $modal = $('#modal');
        Seq.modal.render(html, $modal, "large");
        Seq.buttons.reset();
    }

    function onAsignaClaveComision(){
        Seq.buttons.$button = $(this);
        console.log($(this).data('id'));
        route.urlName = "getAsignaClaveComision";
        Seq.buttons.loading();
        Seq.get.html(route.get.url().replace("{id}", $(this).data('id')), fnShowModalAsignaClaveComision);
    }


});
