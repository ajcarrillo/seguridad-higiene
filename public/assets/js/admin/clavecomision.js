"use strict";
/**
 * Created by wsanchez on 12/10/2015.
 */
(function (){
    var $btnModificar = $('#btn-guardar-clavecomision');
    $btnModificar.on('click', onModificaIncidencia);
    function onModificaIncidencia(){
        var $form = $('#form-clavecomision');
        var url = $form.attr('action');
        var data = $form.serialize();

        $.post(url, data, function(result){
            if (result.ok){
                var url = 'claveComision';
                $('#modal').modal('hide');

                Seq.alert.success(result.msg);

                $('[data-id="gridActasRecorrido"]').DataTable().ajax.url("sinClaveComisionJson").load();
            }
            else {
                console.log(result.msg);
            }
        });
    }

})();