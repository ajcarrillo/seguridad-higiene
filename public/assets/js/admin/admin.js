/**
 * Created by wsanchez on 06/10/2015.
 * Updated by levi on 04/03/2016
 */
$(document).ready(function() {

    $(document).on('click', '#panel-comision', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.removeClass('glyphicon glyphicon-menu-up').addClass('glyphicon glyphicon-menu-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.removeClass('glyphicon glyphicon-menu-down').addClass('glyphicon glyphicon-menu-up');
        }
    });

    $('#btnAceptar').on("click", function (e) {
        var url = $("[data-id='formRevision']").attr("action"),
            idReporte = $("#idReporte").val(),
            message = "";

        $.ajax({
            url: url,
            type: "post",
            dataType: "json",
            data: {'_token': $('input[name=_token]').val()}
        })
            .done(function (response) {
                if (response.success) {
                    message = "Reporte fue aceptado";
                } else {
                    message = "Reporte no se pudo aceptar";
                }

                bootbox.alert({
                    message: message,
                    callback: function () {
                        window.location.href = '../index';
                    },
                    buttons: {
                        ok: {
                            label: "Aceptar"
                        }
                    }
                });
            })
            .fail(function (xhr) {
                console.log(xhr);
            });
    });

    $('#btnRechazar').on("click", function(e) {
        Seq.buttons.$button = $(this);
        route.urlName = "getObservacionActa";
        Seq.buttons.loading();
        Seq.get.html(route.get.url().replace("{id}", $('#idReporte').val()), fnShowModalObservacionActa);




      /*/  bootbox.dialog({
            message: '<div class="row">  ' +
            '<div class="col-md-12"> ' +
            '<form class="form-horizontal"> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="observacion">Observación</label> ' +
            '<div class="col-md-6"> ' +
                '<textarea rows="2" cols="50" name="observacion" id="observacion"></textarea>'+
            //'<input id="observacion" name="observacion" type="text" placeholder="Observación" class="form-control input-md"> ' +
            '</div> ' +
            '</div> ' +
            '</form> </div>  </div>',
            title: "Ingrese la observación",
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-success",
                    callback: function() {
                        var observacion = $('#observacion').val();
                        var idReporte = $('#idReporte').val();
                        $.ajax({
                            url: 'grabaObservacion',
                            type: "post",
                            data: {'observacion': observacion, 'idReporte' : idReporte, '_token': $('input[name=_token]').val()},
                            success: function(data){
                                bootbox.alert({
                                    message: "La observación fue Grabada",
                                    callback: function(){
                                        window.location.href = 'index';
                                    },
                                    buttons: {
                                        ok: {
                                            label: "Aceptar"
                                        }
                                    }
                                });
                            }
                        });
                    }
                },
                danger: {
                    label: "Cancelar",
                    className: "btn-danger",

                },
            }
        });*/
    });

    function fnShowModalObservacionActa(html){
        var $modal = $('#modal');
        Seq.modal.render(html, $modal, "large");
        Seq.buttons.reset();
    }


});
