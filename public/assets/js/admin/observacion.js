/**
 * Created by wsanchez on 12/10/2015.
 * Updated by levi on 03/03/2016
 */
(function ($) {
    "use strict";
    var $btnModificar = $('#btn-guardar-observacion'),
        $form = $('#form-observacion-acta');

    $btnModificar.on('click', function () {
        $("[data-id='enviarObservacion']").trigger("click");
    });

    $form.on("submit", onFormulario);

    function onFormulario(event) {
        var url = $form.attr('action'),
         data = $form.serialize();

        event.preventDefault();

        $.post(url, data, function (result) {
            if (result.ok) {
                $('#modal').modal('hide');
                bootbox.alert({
                    message: result.msg,
                    callback: function () {
                        window.location.href = '../index';
                    },
                    buttons: {
                        ok: {
                            label: "Aceptar"
                        }
                    }
                });
            }
            else {
                console.log(result.msg);
            }
        });
    }
}(jQuery));