/**
 * Created by lewis on 18/02/2016.
 */
(function () {
    "use strict";
    var $usersTable = $("[data-id='gridActasRecorrido']"),
        url = $usersTable.data("url");

    $usersTable.DataTable({
        processing: true,
        serverSide: true,
        ajax: url,
        columns: [
            {data: "comision.clave_comision", name: "comision.clave_comision"},
            {
                render: function (data, type, row) {
                    return row.comision.centros[0].escuela_lite.cct +
                        " " +
                        row.comision.centros[0].escuela_lite.nombre +
                        " " +
                        row.comision.centros[0].escuela_lite.turno.siglas;
                }
            },
            {
                data: "tipos.tipo.nombre",
                name: "tipos.tipo.nombre",
                render: function (data, type, row) {
                    var nombre = [];
                    row.tipos.forEach(function (item) {
                        nombre.push(item.tipo.nombre);
                    });

                    return nombre.join();
                }
            },
            {data: 'acciones', name: 'acciones', orderable: false, searchable: false}
        ],
        "columnDefs": [
            { "searchable": true, "targets": [0,1,2,3] }
        ]
    });
}());

