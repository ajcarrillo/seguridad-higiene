"use strict";
/**
 * Created by andres on 11/09/15.
 */
var App = App || {
		trimestreActual: 0,
		dateNow: new Date(),
		_setTrimestre: function () {
			var mes = this.dateNow.getMonth();
			switch (true){
				case (mes >= 0 && mes <= 2):
					this.trimestreActual = 1;
					break;
				case (mes >= 3 && mes <= 5):
					this.trimestreActual = 2;
					break;
				case (mes >= 6 && mes <=8):
					this.trimestreActual = 3;
					break;
				case (mes >= 9 && mes <=11):
					this.trimestreActual = 4;
					break;
			}
		}
	};

App.Config = {
	modalPlantillaOpen: "",
	updateCentroTrabajo: false
};

App._setTrimestre();
