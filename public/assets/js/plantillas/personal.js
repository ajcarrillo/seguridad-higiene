"use strict";
/**
 * Created by andres on 09/12/15.
 */

(function () {
	var $personalTable = $('#personal-table'),
		url = $personalTable.data('url');

	$personalTable.DataTable({
		processing: true,
		serverSide: true,
		ajax: url,
		columns: [
			{data: 'ap_paterno_emp', name:'ap_paterno_emp'},
			{data: 'ap_materno_emp', name:'ap_materno_emp'},
			{data: 'nombre_emp', name:'nombre_emp'},
			{data: 'rfc', name:'rfc'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
	});
}());
