"use strict";
/**
 * Created by andres on 05/11/15.
 */

(function () {
	var $filterByCtt = $('#filter-by-cct'),
		ChanTable,
		$tablePlantilla = $('#table-plantilla');

	$filterByCtt.unbind().on("click", '[data-button="get-empleados"]', onClickGetEmpleados);
	$tablePlantilla.unbind().on("click", '[data-button="add-vocal-from-plantilla"]', onClickAddVocalFromPlantilla);

	function onClickGetEmpleados(){
		var url = $(this).data('action');
		ChanTable = $tablePlantilla.ChanTable({
			ajax:{
				url: url,
				data: "data"
			},
			template: $('#plantilla-table-template').html()
		});
	}

	function onClickAddVocalFromPlantilla(){
		var data = {},
			sent,
			url = $(this).data('action').replace(':comision:', $('#hf-comision-id').val()),
			row = $(this).parents('tr'),
			$tableVocales = $('#vocales-table');

		data.nombre = $(this).data('nombre');
		data.primer_apellido = $(this).data('primer_apellido');
		data.segundo_apellido = $(this).data('segundo_apellido');
		data.cargo = row.find('[data-id="cargo"]').val();
		data.tipo = row.find('[data-id="tipo"]').val();
		data.sector = row.find('[data-id="sector"]').val();

		function errors(data){
			var errors = 0;
			$.each(data, function (key, value) {
				if(value==""){
					errors += 1;
				}
			});
			return errors;
		}

		if(errors(data) <= 0){
			sent = $('#add-vocal-from-plantilla-form').serialize()+'&'+$.param(data);
			Seq.buttons.$button = $(this);
			Seq.buttons.loading();
			$.post(url, sent, function (result) {
				var url = $('#hf-reload-table-vocales').val().replace(':comision:', $('#hf-comision-id').val());
				Seq.alert.success("Se agregó correctamente");
				$tableVocales.ChanTable({
					ajax:{
						url: url,
						data: "data"
					},
					template: $('#vocales-template-rows').html()
				});
			}).always(function () {
				Seq.buttons.reset();
			});
		}else{
			Seq.alert.danger("Verifique que los datos estén completos")
		}
	}

}());
