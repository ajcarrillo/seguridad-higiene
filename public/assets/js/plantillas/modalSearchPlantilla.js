"use strict";
/**
 * Created by andres on 11/09/15.
 */
(function () {
	$('body').on("click", "[data-button='add-persona']", fnDispatch);

	var $btnModalSearchPlantilla = $('#btn-search-plantilla');
	var $cbxSearchPlantillaByCct = $('#cbx-search-plantilla-by-cct');
	var $filterPlantillaByCct = $('#filter-by-cct');

	comision.centros.forEach(function (element) {
		$filterPlantillaByCct.append("<li><a href='#!' data-value='"+element.clavecct+"' data-button='filter-plantilla-by-cct'>"+element.clavecct+"</a></li>");
	});

	var $btnFilterPlantillaByCct = $("[data-button='filter-plantilla-by-cct']");

	$btnModalSearchPlantilla.on("click", fnSearchPlantilla);
	$btnFilterPlantillaByCct.on("click", fnGetPlantillaByCct);

	function fnSearchPlantilla(cct){
		Seq.buttons.$button = $cbxSearchPlantillaByCct;
		route.urlName = "plantillasGetJson";
		var url = route.get.url().replace("{cct}", cct);
		Seq.buttons.loading();
		Seq.get.json(url, renderTable);
	}

	function renderTable(data){
		var template = $('#template-plantilla').html();
		var table = $('#table-plantilla');
		Seq.undescoreTableRender.render(template, table, data);
		Seq.buttons.reset();
	}

	function fnDispatch(){
		var data = {
			'id': $(this).data("rfc"),
			'rfc': $(this).data("rfc"),
			'nombre': $(this).data("name"),
			'primer_apellido': $(this).data("first-name"),
			'segundo_apellido': $(this).data("last-name")
		};

		switch(App.Config.modalPlantillaOpen){
			case "presidente":
				fnAddPresidente(data);
				break;
			case "secretario":
				fnAddSecretario(data);
				break;
			case "vocal":
				fnAddVocal(data);
				break;
		}
	}

	function fnAddPresidente(data){
		$('#txt-nombre-presidente').val(data.nombre);
		$('#txt-primer-apellido-presidente').val(data.primer_apellido);
		$('#txt-segundo-apellido-presidente').val(data.segundo_apellido);
	}

	function fnAddSecretario(data){
		$('#txt-nombre-secretario').val(data.nombre);
		$('#txt-primer-apellido-secretario').val(data.primer_apellido);
		$('#txt-segundo-apellido-secretario').val(data.segundo_apellido);
		$('#txt-rfc-secretario').val(data.rfc);
	}

	function fnAddVocal(data){
		if(comision.addVocal(data)){
			var $tableVocales = $('#table-vocales');
			var template =
				"<tr data-id='<%= id %>' data-nombre='<%= nombre %>' data-primer_apellido='<%= primer_apellido %>' data-segundo_apellido='<%= segundo_apellido %>' data-rfc='<%= rfc %>' >" +
					"<td><%= nombre %> <%= primer_apellido %> <%= segundo_apellido %></td>" +
					"<td><input type='text' class='form-control' id='txt-cargo-vocal'></td>" +
					"<td>" +
						"<select data-button='cbx-add-tipo-vocal' class='form-control'>" +
							"<option value=''>TIPO</option>" +
							"<option value='1'>Propietario</option>" +
							"<option value='2'>Suplente</option>" +
						"</select>" +
					"</td>" +
					"<td>" +
						"<select data-button='cbx-add-sector-vocal' class='form-control'>" +
							"<option value=''>SECTOR</option>" +
							"<option value='1'>Oficial</option>" +
							"<option value='2'>Sindical</option>" +
						"</select>" +
					"</td>" +
					buttons() +
				"</tr>";
			$tableVocales.find('tbody').append(_.template(template)(data));
		}

		function buttons(){
			var buttons = "";
			if(App.Config.updateCentroTrabajo){
				buttons += "<td class='text-right'>";
				buttons += "<button class='btn btn-primary hidden' data-button='update-vocal' data-loading-text='Cargando...' >Actualizar</button> ";
				buttons += "<button class='btn btn-primary ' data-button='save-vocal' data-loading-text='Cargando...' >Guardar</button> ";
				buttons += "<button class='btn btn-danger hidden' data-button='remove-vocal' data-loading-text='Cargando...' >Quitar</button> ";
				buttons += "</td>";
			}else{
				buttons += "<td class='text-right'><button class='btn btn-danger' data-button='remove-vocal' data-loading-text='Cargando...' >Quitar</button></td>"
			}
			return buttons;
		}
	}

	function fnGetPlantillaByCct(){
		fnSearchPlantilla($(this).data("value"));
	}
})();
