"use strict";
/**
 * Created by andres on 09/10/15.
 */
(function () {
	var $btnSiguiente = $('#btn-siguiente');

	$('#txt-de, #txt-a').datetimepicker({timepicker: false, format:'d/m/Y'});
	$btnSiguiente.on("click", onClickSiguiente);

	function onClickSiguiente(){
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
	}

})();
