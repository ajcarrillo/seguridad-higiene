"use strict";
/**
 * Created by andres on 14/03/16.
 */

(function () {

	var $deleteActivity = $('[data-button="delete-activity"]');

	$deleteActivity.on("click", onClickDeleteActivity);

	function onClickDeleteActivity(e) {
		e.preventDefault();

		var token = $(this).data('token'),
			url = $(this).attr('href'),
			method = $(this).data('method').toUpperCase();

		$.ajax({
			url: url,
			data: '_token=' + token,
			method: method,
			dataType: 'json'
		}).always(function (response) {
			bootbox.alert(response.msg, function(){
				if(response.isValid){
					location.reload();
				}
			});
		});

	}

})();
