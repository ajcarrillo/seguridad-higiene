"use strict";
/**
 * Created by andres on 07/10/15.
 */
(function () {
	var $body = $('body'),
		$btnSaveCalendario = $('[data-button="save-calendario"]'),
		$form = $('#form-actividad'),
		$table = $('#table-actividades'),
		template = $('#template-row-actividad').html(),
		$chanTable;

	$chanTable = $table.ChanTable({
		ajax:{
			url:$('#hf-url-index-actividades').val(),
			data: "data"
		},
		template: template
	});

	$body.on("click", '[data-button="delete-actividad"]', onClickDeleteActividad);
	$body.on("click", '[data-button="update-actividad"]', onClickUpdateActividad);

	$form.on("submit", onSubmitForm);

	function onSubmitForm(e){
		e.preventDefault();
		var dataTable = $form.serializeJSON();
		var url = $form.attr('action');
		Seq.buttons.$button = $btnSaveCalendario;
		Seq.buttons.loading();
		$.post(url, dataTable, function (result){}).then(successHandler, failHandler).always(alwaysHandler);
	}

	function successHandler(){
		Seq.forms.reset($form);
		Seq.alert.success("La actividad se guardó correctamente");
		$chanTable.reload();
	}

	function failHandler(){
		Seq.alert.standarError();
	}

	function alwaysHandler(){
		Seq.buttons.reset();
	}

	function onClickDeleteActividad(){
		var id = $(this).data('id');
		var $form = $('#form-delete-actividad');
		var url = $form.attr('action').replace(':ACTIVIDAD_ID', $(this).data('id'));
		var data = $form.serialize() +'&id='+ id;
		var row = $(this).parents('tr');
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();

		$.ajax({
			method: 'DELETE',
			url: url,
			data: data
		}).then(successHandler, failHandler).always(alwaysHandler);

	}

	function onClickUpdateActividad(){
		var url = $(this).data('route').replace(':actividad_id:', $(this).data('id'));
		Seq.get.html(url, showModal);
	}

	function showModal(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal, "small");
	}
})();
