"use strict";
/**
 * Created by andres on 02/12/15.
 */
bootbox.setDefaults({
	locale: "es",
	show: true,
	backdrop: true,
	closeButton: true,
	animate: true,
	className: "my-modal"
});
