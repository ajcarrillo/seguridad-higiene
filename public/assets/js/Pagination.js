"use strict";
/**
 * Created by andres on 04/11/15.
 */

(function ($) {

	$.fn.ChanTable = function(options){
		var opts = $.extend(true, {},{
			ajax:{
				url: "",
				data: ""
			},
			lastPage: "last_page",
			paginate: true,
			params: "",
			search: false,
			template: ""
		}, options);

		var that = this;
		var table = $(this);

		//$.getJSON(opts.ajax.url, opts.params, init);
		getJson("", init);

		function init(result){
			renderTable(result[opts.ajax.data]);
			if(opts.paginate){
				paginate(result);
			}
		}

		function renderTable(data){
			if(data.length > 0){
				table.find('tbody').html(_.template(opts.template)({'json': data}));
			}else{
				table.find('tbody').html("<tr><td colspan='"+table.data('colspan')+"' class='text-center'>SIN RESULTADOS</td></tr>");
			}
		}

		function paginate(result){
			var $container = table.parent("div");
			var pagination = "<div id='chan-pagination' class='text-right'><nav><ul class='pagination'>:PAGES:</ul></nav></div>";
			var pager = "";

			for(var i = 1; i <= result[opts.lastPage]; i++){
				pager += "<li><a href='#!' data-loading-text='...' data-button='paginate' data-page='"+i+"'>"+i+"</a></li>";
			}
			$container.find('#chan-pagination').remove();

			$(that).after(pagination.replace(":PAGES:", pager));
			var $paginate = $container.find('[data-button="paginate"]');
			$paginate.on("click", onClickPaginate);
			$container.find('#chan-pagination nav ul li').first().addClass('active');
		}

		function onClickPaginate(){
			var that = this;
			var params = {'page': $(this).data('page')};
			getJson($.param(params), paginate);
			function paginate(result){
				renderTable(result[opts.ajax.data]);
				$(that).parents("ul").find("li").removeClass("active");
				$(that).parent().addClass("active");
			}
		}

		function getJson(params, callback){
			$.getJSON(opts.ajax.url, params, callback);
		}

		this.reload = function () {
			getJson("", init);
		};

		return this;
	}
}(jQuery));
