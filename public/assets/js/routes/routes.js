"use strict";
/**
 * Created by andres on 09/09/15.
 */

var route = route || {};
var baseUrl= window.location.origin;

route = {
	urlName: ""
};

route.get = {
	url: function (urlName) {
		var relativeUrl;

		console.log(urlName);

		if(urlName == undefined){
			console.log(urlName);
			console.log(route.urlName);
			relativeUrl = this.getValue(routes, route.urlName);
			console.log(relativeUrl);
		}else{
			relativeUrl = this.getValue(routes, urlName);
		}
		if(relativeUrl.length>0)
			return baseUrl+relativeUrl[0];
		return "NaN"
	},
	getValue: function(obj, key) {
		var objects = [];
		for (var i in obj) {
			if (!obj.hasOwnProperty(i)) continue;
			if (typeof obj[i] == 'object') {
				objects = objects.concat(this.getValue(obj[i], key));
			} else if (i == key) {
				objects.push(obj[i]);
			}
		}
		return objects;
	}
};

var routes = {
	'centrosSearchModule' 		: '/centros/buscar',
	'centrosGetJson'			: '/centros/json/{cct}',
	'centrosByClavecct'			: '/centros/json/?value={cct}&filter=cct',
	'centrosByName'				: '/centros/json/?value={name}&filter=name',
	'actas'						: '/comision/',
	'actasPostCreate'			: '/comision/registro',
	'plantillasSearchModule'	: '/plantillas/buscar/',
	'plantillasGetJson'			: '/plantillas/json/{cct}/',
	'getAgregarIncidencia'		: '/admin/catalogos/incidencias/nueva',
	'getEditarIncidencia'		: '/admin/catalogos/incidencias/edita/{id}/',
	'obtenListaIncidencias'		: '/admin/catalogos/incidencias/lista',
	'getAgregarSindicato'		: '/admin/catalogos/sindicatos/nuevo',
	'getEditarSindicato'		: '/admin/catalogos/sindicatos/edita/{id}/',
	'vocales.index'				: '/comision/:comision_id:/vocales/',
	'comision.centros.index'	: '/comision/:comision_id:/centros/',
	'getObservacionActa'		: '/admin/observacion/{id}/',
	'getAsignaClaveComision'	: '/admin/AsignaClaveComision/{id}/'
};
