"use strict";
/**
 * Created by andres on 21/10/15.
 */

(function () {
	var $btnSubsanarIncidencia = $('[data-button="subsanar-incidencia"]');
	$btnSubsanarIncidencia.unbind().on("click", onClickSubsanarIncidencia);

	function onClickSubsanarIncidencia(){
		var $form = $('#form-subsanar-incidencia');
		var url = $(this).data('action');
		var data  = $form.serialize();
		var row = $(this).parents('tr');
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();

		$.ajax({
			method: "PATCH",
			url: url,
			data: data,
			success: function (result) {
				if(result.ok){
					Seq.alert.success("Se eliminó correctamente la actividad");
					Seq.table.removeRow(row);
				}
				else{
					Seq.alert.standarError();
				}
				Seq.buttons.reset();
			}
		}).fail(function () {
			Seq.alert.standarError();
			Seq.buttons.reset();
		});
	}
})();
