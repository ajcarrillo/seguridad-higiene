"use strict";
/**
 * Created by andres on 01/10/15.
 */
(function () {
	var $body = $('body'),
		$btnAddIncidencia = $('#btn-add-incidencia'),
		$cbxIncidencia = $('#cbx-incidencia'),
		$cbxCentroTrabajo = $('#cbx-centro-trabajo'),
		$hdTrimestre = $('#hd-trimestre'),
		$hdAnio = $('#hd-anio'),
		$btnGoToSubsanar = 	 $('[data-button="go-to-subsanar"]'),
		$btnGoToSubsanadas = $('[data-button="go-to-subsanadas"]');

	$btnAddIncidencia.on("click", onClickAddIncidencias);
	$btnGoToSubsanar.on("click", onClickGoToSubsanar);
	$btnGoToSubsanadas.on("click", onClickGoToSubsanadas);

	$body.on('click', '[data-button="delete-incidencia"]', onClickDeleteIncidencia);

	function onClickGoToSubsanar(){
		var url = $(this).data('href');
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		Seq.get.html(url, showModal);
	}

	function onClickGoToSubsanadas(){
		var url = $(this).data('href');
		Seq.buttons.$button = $(this);
		Seq.buttons.loading();
		Seq.get.html(url, showModal);
	}

	function showModal(html){
		var $modal = $('#modal');
		Seq.modal.render(html, $modal);
		Seq.buttons.reset();
	}

	function onClickAddIncidencias(){
		if($cbxCentroTrabajo.val() == "" || $cbxIncidencia.val() == ""){
			Seq.alert.danger("Verifique que los datos estén completos");
		}else{
			var $formIncidencia = $('#form-incidencias');
			var data = $formIncidencia.serialize();
			var url = $(this).data('url');
			var template = $('#template-row-incidencia').html();
			var table = $('#table-incidencia');

			$.post(url, data, function (result) {
				if (result.ok){
					if(!result.exists){
						table.find('tbody').append(_.template(template)({
							'incidencia': $cbxIncidencia.find("option:selected" ).text(),
							'centro': $cbxCentroTrabajo.find("option:selected").data('clavecct'),
							'anio': $hdAnio.val(),
							'trimestre': $hdTrimestre.val(),
							'id': result.incidencia.id
						}));
						Seq.alert.success(result.msg);
					}else{
						Seq.alert.danger("La incidencia ya existe");
					}

				}
			}).fail(function (result) {
				Seq.post.standarError();
				console.log(result.msg)
			});
		}
	}

	function onClickDeleteIncidencia(){
		var id = $(this).data('id');
		var row = $(this).parents('tr');
		var $form = $('#form-delete-incidencia');
		var data = $form.serialize();
		var url = $form.attr('action').replace(':INCIDENCIA_ID', id);

		$.ajax({
			method: 'DELETE',
			url: url,
			data: data,
			success: function(result){
				if(result.response){
					Seq.alert.success("Registro eliminado correctamente");
					Seq.table.removeRow(row);
				}
			}
		}).fail(function () {
			Seq.alert.standarError();
		});
	}
})();
