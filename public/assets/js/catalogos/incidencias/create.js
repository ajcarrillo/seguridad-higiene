"use strict";
/**
 * Created by wsanchez on 12/10/2015.
 */
(function (){
    var $btnAgregar = $('#btn-guardar-incidencia');
    $btnAgregar.on('click', onAgregaIncidencia);
    function onAgregaIncidencia(){
        var $form = $('#form-nueva-incidencia');
        var url = $form.attr('action');
        var data = $form.serialize();

        $.post(url, data, function(result){
            if (result.ok){
                var urlBase = result.url.split("/nueva/?page=")[0];
                $('#modal').modal('hide');
                window.location = urlBase+"/?page="+result.ultima_pagina;
            }
            else {
                console.log(result.msg);
            }
        });
    }
})();