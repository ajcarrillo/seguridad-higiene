"use strict";
/**
 * Created by wsanchez on 12/10/2015.
 */
(function (){
    var $btnModificar = $('#btn-modificar-incidencia');
    $btnModificar.on('click', onModificaIncidencia);
    function onModificaIncidencia(){
        var $form = $('#form-modifica-incidencia');
        var url = $form.attr('action');
        var data = $form.serialize();

        $.post(url, data, function(result){
            if (result.ok){
                var $pagina_actual = $('#pagina_actual').val();
                var urlBase = url.split("/edita")[0];
                $('#modal').modal('hide');
               window.location = urlBase+"/?page="+$pagina_actual;
            }
            else {
                console.log(result.msg);
            }
        });
    }

})();