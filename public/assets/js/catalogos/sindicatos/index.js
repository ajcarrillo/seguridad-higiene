"use strict";
/**
 * Created by wsanchez on 12/10/2015.
 */
(function () {
    var urlBase = url.split("/?page=")[0];
    paginar(ultima_pagina, urlBase);

    var $btnBorrarSindicato = $('[data-button="borrar-sindicato"]');
    var $btnNuevoSindicato = $('#btn-nuevo');
    var $btnEditaSindicato = $('[data-button="edita-sindicato"]');
    var $body = $('body');
    var $pagina = $('.pagination').find('li').find('a');

    $pagina.on('click', onClickPaginado);
    $btnNuevoSindicato.on('click', onNuevoSindicato);
    $body.on('click', '[data-button="borrar-sindicato"]', onBorrarSindicato);
    $body.on('click', '[data-button="edita-sindicato"]', onEditaSindicato);

    function paginar(ultima_pagina, urlBase){
        var paginador = "<nav><ul class='pagination'>";
        for (var i=1; i<=ultima_pagina; i++){
            paginador += "<li><a href='"+urlBase+"/?page="+i+"'>"+i+"</a></li>";
        }
        paginador += "</ul></nav>";
        $('#paginacion').append(paginador);

    }

    function onBorrarSindicato (){
        var $form = $('#form-borrar-sindicato');
        var url = $form.attr('action').replace(':SINDICATO_ID', $(this).data('id'));
        var row = $(this).parents('tr');
        var id = $(this).data('id');
        Seq.buttons.$button=$(this);
        Seq.buttons.loading();

        //bootbox.alert('Esta seguro de eliminar la inciendencia');

        bootbox.dialog({
            message: 'Esta seguro de eliminar el sindicato?',
            title: "Confirmar",
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            method : 'DELETE',
                            data: $form.serialize(),
                            url: url,
                            success: function (result) {
                                if (result.ok){
                                    var $pagina_actual = $('#pagina_actual').val();
                                    var urlBase = url.split("/"+id)[0];
                                    if (result.ultima_pagina < $pagina_actual)
                                        $pagina_actual = result.ultima_pagina;
                                    window.location = urlBase+"/?page="+$pagina_actual;

                                    //console.log(urlBase);
                                }
                            }
                        });
                    }
                },
                danger: {
                    label: "Cancelar",
                    className: "btn-danger",


                },
            }
        });
        Seq.buttons.reset();
    }

    function onClickPaginado(e){
        e.preventDefault();
        var url = $(this).attr('href');
        var that = this;
        var pagina_actual = url.split('page=')[1];
        $('#pagina_actual').val(pagina_actual);
        $.get(url, function(result){
               renderTable(result.data);
            $('.pagination').find('li').removeClass('active');
            $(that).parent().addClass('active');
        });

    }

    function renderTable(data) {
        var template = $('#template-table-sindicato').html();
        var table = $('#tabla-sindicatos');
        Seq.undescoreTableRender.render(template, table, data);
    }

    function fnShowModalNuevoSindicato(html){
        var $modal = $('#modal');
        Seq.modal.render(html, $modal);
        Seq.buttons.reset();
    }

    function onNuevoSindicato(){
        Seq.buttons.$button = $(this);
        Seq.buttons.loading();
        route.urlName= 'getAgregarSindicato';
        Seq.get.html(route.get.url(), fnShowModalNuevoSindicato);
    }

    function fnShowModalEditaSindicato(html){
        var $modal = $('#modal');
        Seq.modal.render(html, $modal);
        Seq.buttons.reset();
    }

    function onEditaSindicato(){
        Seq.buttons.$button = $(this);
        route.urlName = "getEditarSindicato";
        Seq.buttons.loading();
        Seq.get.html(route.get.url().replace("{id}", $(this).data('id')), fnShowModalEditaSindicato);
    }
})();

