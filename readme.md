# Seguridad y Salud en el trabajo

## Dependencias
```
node
bower
stylus
nib
```

- Instalar node desde su sitio [Node website](https://nodejs.org)
- Crear base de datos para el proyecto (el nombre puede ser el de tu preferencia).

### Instalar dependencias
```
$ sudo npm install -g bower
$ sudo npm install -g stylus
$ sudo npm install -g nib
```

## :baby_bottle: Baby Steps
- Renombrar los archivos `/.env.example` a `.env`.
- Ejecutar en la linea de comandos `$ bower install`.

### Archivo .env
Aquí se encuentra contenidas algunas variables y configuraciones de vital importancia para que el proyecto funcione.

Existen tres variables que debemos inicializar `APP_KEY`, `KEY_LOGIN_APP_KEY` y `LOGIN_USERS_URL`. 

La variable `APP_KEY` se rellena automáticamente ejecutando el comando:
```
$ php artisan key:generate
```

La variable `KEY_LOGIN_APP_KEY` deberá ser proporcionada por la API de usuarios cuando el sistema sea dado de alta en la API.

La variable `LOGIN_USERS_URL` es la url de verificación de usuario de la API de usuarios

Configuración de tu base de datos.
```
DB_HOST=SomeIPAdress
DB_PORT=ThePort
DB_DATABASE=YourDatabaseName
DB_USERNAME=YourUsername
DB_PASSWORD=YourPassword
```

Ejecutar los siguientes comandos para aplicar las migraciones e inicializar la base datos.
```
$ php artisan migrate
$ php artisan db:seed
```

## :baby_bottle: Baby steps II
Aplicar las dependencias de laravel vía composer:

```
$ composer install
$ composer dump-autoload
```
### Cada que se integre código y algo falle :dizzy_face:, ejecutar los comandos anteriores :wink: 

  
  
## Instalación de rapyd-laravel

Seguir los siguiente pasos de instalación: `https://github.com/zofe/rapyd-laravel`


## Compilar archivos stylus
```
$ stylus -c resources/assets/stylus/main.styl -o public/assets/css
$ stylus -c resources/assets/stylus/grid_print.styl -o public/assets/css
```
