@extends('base')

@section('css')
	{!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@stop

@section('content')
	<div class="container top">
		@yield('centrosHeader')
		<div class="row">
			<div class="col-md-12">
				<h2>Buscar centros de trabajo</h2>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered" id="escuela-lite" data-url="{{ route('centro.buscando') }}">
					<thead>
					<tr>
						<th width="10%">CCT</th>
						<th width="30%">Nombre</th>
						<th width="10%">Municipio</th>
						<th width="10%">Localidad</th>
						<th width="10%">Nivel</th>
						<th width="10%">Turno</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	@yield('centrosFooter')

@stop

@section('scripts')
	{!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	{!! Html::script('assets/js/spanish.datatables.js') !!}
	{!! Html::script('assets/js/escuela_lite/index.js') !!}
@stop
