@extends('base')

@section('content')
	<div class="seyc-intro">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="intro-message">
						<h1>Seguridad y Salud en el Trabajo</h1>
						<h3>Coordinación de Recursos Humanos</h3>
						<hr class="intro-divider">
					</div>
				</div>
				<div class="col-md-4">
					<div class="login panel-body">
						<div class="alert alert-danger" id="unauthorized-alert">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							Verifique su usuario y contraseña
						</div>
						@include('layout.partials.unauthorized')
						<input type="hidden" id="app"
							   value="{{ env('LOGIN_APP_KEY') }}">
						<input type="hidden" id="login-url-users" value="{{ env('LOGIN_USERS_URL') }}">
						<form id="login-form" class="form-horizontal" role="form" method="POST"
							  action="{{ url('/auth/login') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<input type="text" id="username" name="username" class="form-control top"
								   value="{{ old('username') }}" placeholder="Usuario" required/>
							<input type="password" class="form-control bottom" name="password" placeholder="Contraseña"
								   required>

							<div class="remember-me checkbox">
								<label for="user_remember_me">
									<input type="checkbox" name="remember">
									<span>Recordar</span>
								</label>
							</div>

							<div class="form-group">
								<div class="col-md-2">
									<button id="btn-login" type="submit" class="btn btn-success">Iniciar Sesión</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{!! Html::script('assets/js/auth/login.js') !!}
@stop
