@extends('layout.baseModal')

@section('modalTitle', "Buscar plantilla")

@section('modalBody')
	<input type="hidden" id="hf-reload-table-vocales" value="{{ route('vocales.index', ':comision:') }}">
	<div class="container-fluid">
		<div class="row">
			<div class="form-horizontal">
				<div class="form-group">
					{{--
					<div class="col-md-4">
						<div class="input-group">
							<input type="text" class="form-control" id="txt-cct" placeholder="Clave de centro de trabajo">
							<div class="input-group-addon" id="btn-search-plantilla" data-loading-text="Buscando..."><span class="glyphicon glyphicon-search"></span></div>
						</div>
					</div>--}}
					<div class="col-md-4">
						<!-- Single button -->
						<div class="btn-group">
							<button type="button" id="cbx-search-plantilla-by-cct" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-loading-text="Buscando..." aria-haspopup="true" aria-expanded="false">
								Centro de Trabajo <span class="caret"></span>
							</button>
							<ul id="filter-by-cct" class="dropdown-menu">
								@foreach($centros as $centro)
									<li>
										<a href="#!" data-button="get-empleados" data-action="{{ route('plantillas.empleados', ['cct'=>$centro]) }}">{{ $centro }}</a>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table" id="table-plantilla" data-colspan="5">
					<thead>
					<tr>
						<th>Nombre</th>
						<th>Cargo</th>
						<th>Sector</th>
						<th>Tipo</th>
						<th>Opciones</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
	{!! Form::open(['id'=>'add-vocal-from-plantilla-form']) !!}
	{!! Form::close() !!}
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
	<script type="text/html" id="plantilla-table-template">
		<% _.each(json, function(empleado){%>
		<tr>
			<td><%= empleado.ap_paterno_emp %> <%= empleado.ap_materno_emp %> <%= empleado.nombre_emp %></td>
			<td>
				<input type="text" class="form-control" data-id="cargo" data-name="cargo">
			</td>
			<td>
				<select data-name="sector" data-id="sector" class="form-control">
					<option value="">Seleccione...</option>
					<option value="1">Oficial</option>
					<option value="2">Sindical</option>
				</select>
			</td>
			<td>
				<select data-name="tipo" data-id="tipo" class="form-control">
					<option value="">Seleccione...</option>
					<option value="1">Propietario</option>
					<option value="2">Suplente</option>
				</select>
			</td>
			<td>
				<button type="button"
						data-nombre="<%= empleado.nombre_emp %>"
						data-primer_apellido="<%= empleado.ap_paterno_emp %>"
						data-segundo_apellido="<%= empleado.ap_materno_emp %>"
						data-button="add-vocal-from-plantilla"
						data-action="{{ route('vocales.store', ':comision:') }}"
						class="btn btn-success">Agregar</button>
			</td>
		</tr>
		<%}); %>
	</script>
	{!! Html::script('assets/js/plantillas/modal_search_plantilla.js') !!}
@stop
