@extends('layout/baseModal')

@section('modalTitle', "Buscar Persona")

@section('modalBody')
	<div class="container-fluid">
		<div class="row">
			<div class="form-horizontal">
				<div class="form-group">
					{{--
					<div class="col-md-4">
						<div class="input-group">
							<input type="text" class="form-control" id="txt-cct" placeholder="Clave de centro de trabajo">
							<div class="input-group-addon" id="btn-search-plantilla" data-loading-text="Buscando..."><span class="glyphicon glyphicon-search"></span></div>
						</div>
					</div>--}}
					<div class="col-md-4">
						<!-- Single button -->
						<div class="btn-group">
							<button type="button" id="cbx-search-plantilla-by-cct" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-loading-text="Buscando..." aria-haspopup="true" aria-expanded="false">
								Centro de Trabajo <span class="caret"></span>
							</button>
							<ul id="filter-by-cct" class="dropdown-menu">

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table" id="table-plantilla">
					<thead>
					<tr>
						<th>Nombre</th>
						<th>Primer Apellido</th>
						<th>Segundo Apellido</th>
					</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/bower_components/underscore/underscore-min.js') !!}

	<script id="template-plantilla" type="text/html">
		<% _.each(json, function(persona) { %>
			<tr>
				<td>
					<button type="button" class="btn btn-link"
							data-button="add-persona"
							data-name="<%= persona.nombre_emp %>"
							data-first-name="<%= persona.ap_paterno_emp %>"
							data-last-name="<%= persona.ap_materno_emp %>"
							data-rfc="<%= persona.rfc %>">
						<%= persona.nombre_emp %>
					</button>
				</td>
				<td>
					<%= persona.ap_paterno_emp %>
				</td>
				<td>
					<%= persona.ap_materno_emp %>
				</td>
			</tr>
		<% }) %>
	</script>

	{!! Html::script('assets/js/plantillas/modalSearchPlantilla.js') !!}
@stop()
