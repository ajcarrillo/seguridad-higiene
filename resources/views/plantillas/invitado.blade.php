<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Seguridad e Higiene - Invitado</title>

	{!! Html::style('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
	{!! Html::style('/assets/css/main.css') !!}

	<style>
		body{
			display: flex;
			flex-direction: column;
			min-height: 100vh;
		}

		main{
			padding: 1em;
		}

		main h1{
			font-size: 2.5em;
		}

		main p {
			font-size: 1.3em;
		}

		footer{
			margin-top: auto;
		}
	</style>
</head>
<body>

<main>
	<section class="info">
		<h1>Información</h1>
		<p>
			El usuario <strong>{{ Auth::user()->username }}</strong> no tiene los suficientes privilegios para acceder al sistema,
			favor de comunicarse con el administrador del sistema
		</p>
		<br>
		<a href="{{ route('logout') }}" class="btn btn-success">Cerrar sesión</a>
	</section>
</main>


<footer>
	<div class="footer">

	</div>
</footer>
</body>
</html>
