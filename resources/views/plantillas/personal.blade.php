@extends('base')

@section('css')
	{!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@stop

@section('content')
	<div class="container top">
		@yield('personalHeader')
		<div class="row">
			<div class="col-md-12">
				<h2>Buscar personal</h2>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered" id="personal-table" data-url="{{ route('plantillas.personal', $comision) }}">
					<thead>
					<tr>
						<th width="20%">Primer Apellido</th>
						<th width="20%">Segundo Apellido</th>
						<th width="20%">Nombre</th>
						<th width="10%">RFC</th>
						<th width="10%">Opciones</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	@yield('personalFooter')

@stop

@section('scripts')
	{!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	{!! Html::script('assets/js/spanish.datatables.js') !!}
	{!! Html::script('assets/js/plantillas/personal.js') !!}
@stop
