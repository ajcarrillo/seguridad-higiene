@extends('base')

@section('css')
	{!! Html::style('/assets/bower_components/datetimepicker/jquery.datetimepicker.css') !!}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Registro</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-horizontal">
					{!! Form::open() !!}
					{!! Form::close() !!}
					<form id="form-general">
						<div class="form-group">
							<label for="sindicato" class="col-sm-3 control-label" style="text-align: left!important">Organización Sindical:</label>
							<div class="col-sm-9">
								<select name="sindicato_id" id="sindicato" name="sindicato" class="form-control">
									<option value="">Seleccione...</option>
									@foreach($sindicatos as $sindicato)
										<option value="{{$sindicato->id}}">{{$sindicato->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="sindicato" class="col-sm-3 control-label" style="text-align: left!important">Oficio Designación Oficial:</label>
							<div class="col-sm-5">
								<input type="text" id="txt-oficio-oficial" name="num_oficio_oficial" class="form-control" placeholder="Número de Oficio de designación de representación oficial"/>
							</div>
							<div class="col-sm-4">
								<input type="text" id="txt-fecha-oficio-oficial" name="fecha_oficio_oficial" class="form-control" placeholder="Fecha oficio" readonly/>
							</div>
						</div>

						<div class="form-group">
							<label for="sindicato" class="col-sm-3 control-label" style="text-align: left!important">Oficio Designación Sindical:</label>
							<div class="col-sm-5">
								<input type="text" id="txt-oficio-sindical" name="num_oficio_sindical" class="form-control" placeholder="Número de Oficio de designación de representación sindical"/>
							</div>
							<div class="col-sm-4">
								<input type="text" id="txt-fecha-oficio-sindical" name="fecha_oficio_sindical" class="form-control" placeholder="Fecha oficio" readonly/>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>

		@include('actas/partials/datosCentroTrabajo')

		@include('actas/partials/presidente')

		@include('actas/partials/secretario')

		@include('actas/partials/vocales')

		<div class="row">
			<div class="col-md-2 pull-right">
				<button class="btn btn-primary btn-block" id="btn-submit-registro" type="button" data-name="saveAndSend">Guardar y enviar</button>
			</div>
			<div class="col-md-2 pull-right">
				<button class="btn btn-success btn-block" id="btn-save-registro" type="button" data-name="save">Guardar</button>
			</div>
		</div>
	</div>
	<br>
@endsection

@section('scripts')
	{!! Html::script('assets/bower_components/datetimepicker/jquery.datetimepicker.js') !!}
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/actas/data/comision.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/routes/routes.js') !!}
	{!! Html::script('assets/js/actas/create.js') !!}
@endsection
