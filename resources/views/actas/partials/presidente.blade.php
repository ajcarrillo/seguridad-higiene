<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Presidente</h3>
			</div>
			<div class="panel-body">
				<div class="form-horizontal" role="form">
					@include('actas/partials/buscarPlantilla', ['data'=>'presidente'])
					<form id="form-presidente">
						<div class="form-group">
							<div class="col-sm-3">
								<label for="nombre">Nombre</label>
								<input type="text" class="form-control" name="nombre" id="txt-nombre-presidente" placeholder="Nombre">
							</div>
							<div class="col-sm-3">
								<label for="primer-apellido">Primer Apellido</label>
								<input type="text" class="form-control" name="primer_apellido" id="txt-primer-apellido-presidente" placeholder="Primer Apellido">
							</div>
							<div class="col-sm-3">
								<label for="segundo-apellido">Segundo Apellido</label>
								<input type="text" class="form-control" name="segundo_apellido" id="txt-segundo-apellido-presidente" placeholder="Segundo Apellido">
							</div>
							<div class="col-sm-3">
								<label for="cargo">Cargo</label>
								<input type="text" name="cargo" id="txt-cargo-presidente" class="form-control" placeholder="Cargo" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	{!! Html::script('assets/js/actas/presidente.js') !!}
@endsection
