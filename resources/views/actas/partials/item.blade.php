<tr>
	<td class="text-center">{{ date('d/m/Y',strtotime($reporte->created_at)) }}</td>
	<td class="text-center">{{ $reporte->trimestre }}</td>
	<td>
		{{--@foreach($reporte->tipos as $reporteTipo)
			{{ $reporteTipo->tipo->nombre }}
		@endforeach--}}
	</td>
	<td>
		{{ $reporte->status->nombre }}
	</td>
	<td class="text-center">
		<!-- Single button -->
		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Opciones <span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				@if($reporte->status->id == statusReporte('Pendiente'))
					@foreach($reporte->tipos as $reporteTipo)
						<li><a href="{{ route('get.reporte', ['reporteId'=>$reporte->id, 'tipo'=>$reporteTipo->tipo->id]) }}">{{ $reporteTipo->tipo->nombre }}</a></li>
					@endforeach
						<li role="separator" class="divider"></li>
						<li><a href="#!" data-button="send-reporte" data-route="{{ route('reporte.send', $reporte->id)  }}">Enviar</a></li>
						<li role="separator" class="divider"></li>
						{{--<li><a href="#!" data-button="send-reporte" style="color: #D9534F!important;">Eliminar</a></li>--}}
				@elseif($reporte->status->id == statusReporte('Rechazado'))
					@foreach($reporte->tipos as $reporteTipo)
						<li><a href="{{ route('get.reporte', ['reporteId'=>$reporte->id, 'tipo'=>$reporteTipo->tipo->id]) }}">{{ $reporteTipo->tipo->nombre }}</a></li>
					@endforeach
					<li role="separator" class="divider"></li>
					<li><a href="#!" data-button="send-reporte" data-route="{{ route('reporte.send', $reporte->id)  }}">Enviar</a></li>
					<li role="separator" class="divider"></li>
					<li><a href="#!" data-url="{{ route('reporte.reject', $reporte->id) }}" data-button="get-reject">Ver motivo de rechazo</a></li>
				@elseif($reporte->status->id == statusReporte('Aprobado'))
					<li><a href="{{ route('actas.download', $reporte->id) }}">Descargar acta</a></li>
				@elseif($reporte->status->id == statusReporte('Enviado'))
					<li class="disabled"><a href="#!">Sin opciones</a></li>
				@endif

			</ul>

		</div>
	</td>
</tr>
