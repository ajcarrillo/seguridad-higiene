<div class="row">
	<div class="col-md-12">
		<div id="panel-centros" class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Datos del centro de trabajo</h3>
			</div>
			<div class="panel-body">
				<div class="form-horizontal" role="form">
					<div class="form-group">
						<button type="button" data-loading-text="Cargando..." class="btn btn-link" id="add-centro">Agregar Centro</button>
					</div>
				</div>
			</div>
			<table class="table" id="table-centros">
				<thead>
				<tr>
					<th>Clave Centro</th>
					<th>Nombre</th>
					<th>Municipio</th>
					<th>Localidad</th>
					<th>Nivel</th>
					<th>Acciones</th>
				</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	{!! Html::script('assets/js/actas/datosCentroTrabajo.js') !!}
@endsection
