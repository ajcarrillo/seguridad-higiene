<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Vocales</h3>
			</div>
			<div class="panel-body">
				<div class="form-horizontal">
					@include('actas/partials/buscarPlantilla', ['data'=>'vocal'])
					{{--
					<div class="form-group">
						<div class="col-sm-3">
							<label for="nombre">Nombre</label>
							<input type="text" class="form-control" id="nombre" placeholder="Nombre">
						</div>
						<div class="col-sm-3">
							<label for="primer-apellido">Primer Apellido</label>
							<input type="text" class="form-control" id="primer-apellido" placeholder="Primer Apellido">
						</div>
						<div class="col-sm-3">
							<label for="segundo-apellido">Segundo Apellido</label>
							<input type="text" class="form-control" id="segundo-apellido" placeholder="Segundo Apellido">
						</div>
						<div class="col-sm-3">
							<label for="segundo-apellido">Cargo</label>
							<input type="text" class="form-control" id="segundo-apellido" placeholder="Cargo">
						</div>
					</div>--}}
				</div>
			</div>
			<table class="table" id="table-vocales">
				<thead>
				<tr>
					<th>Nombre completo</th>
					<th>Cargo</th>
					<th>Tipo</th>
					<th>Sector</th>
					<th>Acciones</th>
				</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	{!! Html::script('assets/js/actas/vocal.js') !!}
@endsection
