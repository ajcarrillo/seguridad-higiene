<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Secretario</h3>
			</div>
			<div class="panel-body">
				<div class="form-horizontal" role="form">
					@include('actas/partials/buscarPlantilla', ['data'=>'secretario'])
					<form id="form-secretario">
						<div class="form-group">
							<div class="col-sm-4">
								<label for="nombre">Nombre</label>
								<input type="text" class="form-control" name="nombre" id="txt-nombre-secretario" placeholder="Nombre">
							</div>
							<div class="col-sm-4">
								<label for="primer-apellido">Primer Apellido</label>
								<input type="text" class="form-control" name="primer_apellido" id="txt-primer-apellido-secretario" placeholder="Primer Apellido">
							</div>
							<div class="col-sm-4">
								<label for="segundo-apellido">Segundo Apellido</label>
								<input type="text" class="form-control" name="segundo_apellido" id="txt-segundo-apellido-secretario" placeholder="Segundo Apellido">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="txt-rfc">RFC</label>
								<input type="text" class="form-control" name="rfc" id="txt-rfc-secretario" placeholder="RFC"/>
							</div>
							<div class="col-sm-4">
								<label for="sector">Sector</label>
								<select name="sector" id="cbx-sector-secretario" class="form-control">
									<option value="">Seleccione..</option>
									<option value="1">Oficial</option>
									<option value="2">Sindical</option>
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@section('scripts')
	@parent
	{!! Html::script('assets/js/actas/secretario.js') !!}
@endsection
