@extends('base')

@section('css')
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<h2>Actas de Recorrido</h2>
		</div>
	</div>
	@if($comision->clave_comision != '0')
		<div class="row">
			<div class="col-md-12">
				<div class="form-group gray-content-block">
					<button type="button" class="btn btn-success" data-check-reporte="{{ route('hasReporte', $comision->id) }}" data-action="{{ route('get.reporte.create') }}" data-loading-text="Cargando..." data-button="nuevo-reporte">Nueva Acta</button>
				</div>
			</div>
		</div>
	@endif
	<br>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover">
				<thead>
				<tr>
					<th width="10%" class="text-center">Fecha</th>
					<th width="10%" class="text-center">Trimestre</th>
					<th width="50%" class="text-center">Tipo</th>
					<th width="20%" class="text-center">Estatus</th>
					<th width="10%" class="text-center">Acciones</th>
				</tr>
				</thead>
				<tbody>
					@forelse($comision->reportes as $reporte)
						@include('actas.partials.item', compact('reporte'))
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/Modal.js') !!}
	{!! Html::script('assets/bower_components/bootbox.js/bootbox.js') !!}
	{!! Html::script('assets/js/actas/list.js') !!}
@endsection
