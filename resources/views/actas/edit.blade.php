@extends('base')

@section('css')
	{!! Html::style('/assets/bower_components/datetimepicker/jquery.datetimepicker.css') !!}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Registro</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-horizontal">
					{!! Form::open(['id' => 'form-edit-comision']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['delete.comision.centro', $comision->id], 'id' => 'form-edit-centro']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['post.comision.centro', $comision->id], 'id' => 'form-post-centro']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['put.comision.presidente', $comision->id], 'id' => 'form-put-presidente']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['put.comision.secretario', $comision->id], 'id' => 'form-put-secretario']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['put.registro.edit', $comision->id], 'id' => 'form-put-registro']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['delete.comision.vocal', $comision->id], 'id' => 'form-vocales']) !!}
					{!! Form::close() !!}

					{!! Form::open(['route' => ['post.enviar.reporte', $reporte], 'id' => 'form-enviar-reporte']) !!}
					{!! Form::close() !!}

					<form id="form-general">
						<div class="form-group">
							<label for="sindicato" class="col-sm-3 control-label" style="text-align: left!important">Organización Sindical:</label>
							<div class="col-sm-9">
								<select name="sindicato" id="sindicato" name="sindicato" class="form-control">
									<option value="">Seleccione...</option>
									@foreach($sindicatos as $sindicato)
										<option value="{{$sindicato->id}}"
												@if($comision->cat_sindicato == $sindicato)
												selected
											@endif>{{$sindicato->nombre}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="sindicato" class="col-sm-3 control-label" style="text-align: left!important">Oficio Designación Oficial:</label>
							<div class="col-sm-5">
								<input type="text" id="txt-oficio-oficial" name="num_oficio_oficial" class="form-control" placeholder="Número de Oficio de designación de representación oficial" value="{{ $comision->num_oficio_oficial }}"/>
							</div>
							<div class="col-sm-4">
								<input type="text" id="txt-fecha-oficio-oficial" name="fecha_oficio_oficial" class="form-control" placeholder="Fecha oficio" readonly value="{{ date('d/m/Y',strtotime($comision->fecha_oficio_oficial)) }}"/>
							</div>
						</div>

						<div class="form-group">
							<label for="sindicato" class="col-sm-3 control-label" style="text-align: left!important">Oficio Designación Sindical:</label>
							<div class="col-sm-5">
								<input type="text" id="txt-oficio-sindical" name="num_oficio_sindical" class="form-control" placeholder="Número de Oficio de designación de representación sindical" value="{{ $comision->num_oficio_sindical }}"/>
							</div>
							<div class="col-sm-4">
								<input type="text" id="txt-fecha-oficio-sindical" name="fecha_oficio_sindical" class="form-control" placeholder="Fecha oficio" readonly value="{{ date('d/m/Y',strtotime($comision->fecha_oficio_sindical)) }}"/>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-12">
				<button type="button" class="btn btn-primary pull-right" data-button="update-comision"  data-loading-text="Cargando..." >Actualizar</button>
			</div>
		</div>
		<br>
		@include('actas/partials/datosCentroTrabajo')

		@include('actas/partials/presidente')

		@include('actas/partials/secretario')

		@include('actas/partials/vocales')

		<div class="row">
			<div class="col-md-2 pull-right">
				<button class="btn btn-success btn-block" id="btn-sent-registro" type="button" data-name="sent">Enviar</button>
			</div>
		</div>
	</div>
	<br>
@endsection

@section('scripts')
	<script type="text/html" id="template-centros">
		<% _.each(json, function(centro){ %>
			<tr data-id='<%= centro.id %>'>
				<td><%= centro.clavecct %></td>
				<td><%= centro.NOMBRECT %></td>
				<td><%= centro.NOMBREMUN %></td>
				<td><%= centro.NOMBRELOC %></td>
				<td><%= centro.NIVEL %></td>
				<td><button type='button' class='btn btn-danger' data-button='remove-centro' data-loading-text="Cargando..." >Quitar</button></td>
			</tr>
		<% }); %>
	</script>

	<script type="text/html" id="template-vocales">
		<% _.each(json, function(vocal){ %>
			<tr data-id='<%= vocal.id %>' data-nombre="<%= vocal.nombre %>" data-primer_apellido="<%= vocal.primer_apellido %>" data-segundo_apellido="<%= vocal.segundo_apellido %>" data-rfc="<%= vocal.rfc %>">
				<td><%= vocal.nombre %> <%= vocal.primer_apellido %> <%= vocal.segundo_apellido %></td>
				<td><input type='text' class='form-control' id='txt-cargo-vocal' value="<%= vocal.cargo %>"></td>
				<td>
					<select data-button='cbx-add-tipo-vocal' class='form-control'>
						<option value=''>TIPO</option>
						<option value='1' <% if(vocal.tipo == 1){%>selected<%} %>>Propietario</option>
						<option value='2' <% if(vocal.tipo == 2){%>selected<%} %>>Suplente</option>
					</select>
				</td>
				<td>
					<select data-button='cbx-add-sector-vocal' class='form-control'>
						<option value=''>SECTOR</option>
						<option value='1' <% if(vocal.sector == 1){%>selected<%} %> >Oficial</option>
						<option value='2' <% if(vocal.sector == 2){%>selected<%} %> >Sindical</option>
					</select>
				</td>
				<td class='text-right'>
					<button class="btn btn-primary" data-button='update-vocal' data-loading-text="Cargando..." >Actualizar</button>
					<button class='btn btn-danger' data-button='remove-vocal' data-loading-text="Cargando..." >Quitar</button>
				</td>
			</tr>
		<% });%>

	</script>

	<script type="text/html" id="template-table-foot">
		<tfoot>
			<tr>
				<td colspan="6" class="text-right">
					<button class="btn btn-primary" data-button="<%= button %>" data-loading-text="Cargando..." >Actualizar</button>
				</td>
			</tr>
		</tfoot>
	</script>

	<script type="text/html" id="template-update-button">
		<div>
			<button type="button" class="btn btn-primary pull-right" data-button="<%= button %>" data-loading-text="Cargando..." >Actualizar</button>
		</div>
	</script>

	<script type="text/javascript">
		var comisionEdit = '{!! $comision !!}';
		var centrosEdit = '{!! $centros !!}'
	</script>
	{!! Html::script('assets/bower_components/datetimepicker/jquery.datetimepicker.js') !!}
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/actas/data/comision.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/routes/routes.js') !!}
	{!! Html::script('assets/js/actas/edit.js') !!}
@endsection
