@extends('layout/baseModal')

@section('modalTitle', "Buscar centro de trabajo")

@section('modalBody')
	<div class="container-fluid">
		<div class="row">
			<div class="form-horizontal">
				<form action="{{ route('centro.search', ['filter'=>'PFILTER', 'by'=>'PBY']) }}" id="form-search-centro"></form>
				<div class="col-md-4">
					<div class="input-group">
						<div class="input-group-addon" id="btn-search-by-cct" data-loading-text="Buscando..."><span class="glyphicon glyphicon-search"></span></div>
						<input id="txt-cct" type="text" class="form-control" placeholder="Clave centro de trabajo"/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<div class="input-group-addon" id="btn-search-by-nombre" data-loading-text="Buscando..."><span class="glyphicon glyphicon-search"></span></div>
						<input type="text" id="txt-nombre" class="form-control" placeholder="Nombre centro de trabajo">
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-12">
				<table class="table" id="centros">
					<thead>
					<tr>
						<th>CCT</th>
						<th>Nombre</th>
						<th>Municipio</th>
						<th>Localidad</th>
						<th>Turno</th>
						<th>Nivel</th>
					</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div id="pager" class="col-md-12 text-right">

			</div>
		</div>
		<input type="hidden" id="hf-comision-centro">
		{!! Form::open(['route' => ['comision.centros.store', ':comision_id:'], 'id'=>'form-store-comision-centro']) !!}
		{!! Form::close() !!}
	</div>
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/bower_components/underscore/underscore-min.js') !!}
	{!! Html::script('assets/js/centros/modal_index.js') !!}
@stop
