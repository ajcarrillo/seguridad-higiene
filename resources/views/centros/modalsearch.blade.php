@extends('layout/baseModal')

@section('modalTitle', "Buscar centro de trabajo")

@section('modalBody')
	<div class="container-fluid">
		<div class="row">
			<div class="form-horizontal">
				<div class="col-md-4">
					<div class="input-group">
						<div class="input-group-addon" id="btn-search" data-loading-text="Buscando..."><span class="glyphicon glyphicon-search"></span></div>
						<input id="txt-cct" type="text" class="form-control" placeholder="Clave centro de trabajo"/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<div class="input-group-addon" id="btn-search-by-name" data-loading-text="Buscando..."><span class="glyphicon glyphicon-search"></span></div>
						<input type="text" id="txt-name" class="form-control" placeholder="Nombre centro de trabajo">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table" id="centros">
					<thead>
					<tr>
						<th>CCT</th>
						<th>Nombre</th>
						<th>Municipio</th>
						<th>Localidad</th>
						<th>Turno</th>
						<th>Nivel</th>
					</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/bower_components/underscore/underscore-min.js') !!}

	<script id="template-centro" type="text/html">
		<% _.each(json, function(centro) { %>
			<tr>
				<td>
					<button type="button" class="btn btn-link" data-button="add-centro"
							data-id="<%= centro.id %>"
							data-cct="<%= centro.clavecct %>"
							data-name="<%= centro.NOMBRECT %>"
							data-municipio="<%= centro.NOMBREMUN %>"
							data-localidad="<%= centro.NOMBRELOC %>"
							data-nivel="<%= centro.NIVEL %>">
						<%= centro.clavecct %>
					</button>
				</td>
				<td><%= centro.NOMBRECT %></td>
				<td><%= centro.NOMBREMUN %></td>
				<td><%= centro.NOMBRELOC %></td>
				<td><%= centro.d_turno %></td>
				<td><%= centro.NIVEL %></td>
			</tr>
		<% }); %>
	</script>

	{!! Html::script('assets/js/centros/modalsearch.js') !!}
@stop
