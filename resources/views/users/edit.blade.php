@extends('base')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@if (Session::has('success'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('success') }}
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Editar usuario</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! Form::model($user,['route' => ['user.update', $user->id], 'id'=>'user-edit-form', 'class'=>'form-horizontal', 'method'=>'PATCH']) !!}
				@include('users.partials.form')
				<div class="form-group gray-content-block">
					<div class="">
						<button type="submit" class="btn btn-success">Guardar</button>
						<a href="{{ route('user.index') }}" class="btn btn-default pull-right">Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop

@section('scripts')
@stop
