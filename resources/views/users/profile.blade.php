@extends('base')

@section('pageTitle', 'Perfil de usuario')

@section('appTitle', 'Perfil de usuario')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				@include('layout.partials.messages')
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Perfil de usuario</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="well form-section">Datos generales</div>
			</div>
		</div>
	</div>

@stop
