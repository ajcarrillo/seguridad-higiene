<div class="well form-section">Datos generales</div>
<div class="form-group">
	{!! Form::label('name', 'Nombre:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('primer_apellido', 'Primer Apellido:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('primer_apellido', null, ['class'=>'form-control', 'required']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('segundo_apellido', 'Segundo Apellido:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('segundo_apellido', null, ['class'=>'form-control']) !!}
	</div>
</div>
<div class="well form-section">Establecer rol</div>
<div class="form-group">
	{!! Form::label('role', 'Rol:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::select('role', $roles, null, ['class'=>'form-control', 'required','placeholder'=>'Seleccione...']) !!}
	</div>
</div>
<div class="well form-section">Datos de inicio de sesión</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Nombre de usuario:</label>
	<div class="col-sm-10">
		<p class="form-control-static">{{ $user->username }}</p>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label">Contraseña:</label>
	<div class="col-sm-10">
		<p class="form-control-static"><a href="{{ route("change.password", $user->id) }}">Cambiar contraseña</a></p>
	</div>
</div>

<div class="form-group">
	{!! Form::label('email', 'Correo electrónico:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('email', null, ['class'=>'form-control', 'required']) !!}
	</div>
</div>
