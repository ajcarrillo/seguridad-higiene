@extends('base')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@if (Session::has('success'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('success') }}
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Cambiar contraseña</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! Form::open(['route' => ['reset.password', $user->id], 'id'=>'reset-password-form', 'class'=>'form-horizontal']) !!}
				<div class="well form-section">Reestablecer contraseña</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Nombre:</label>
					<div class="col-sm-10">
						<p class="form-control-static">{{ $user->nombre_completo }}</p>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Nombre de usuario:</label>
					<div class="col-sm-10">
						<p class="form-control-static">{{ $user->username }}</p>
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('password', 'Contraseña:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::password('password', ['id'=>'password', 'class'=>'form-control', 'placeholder'=>'Contraseña','required']) !!}
					</div>
				</div>

				<div class="form-group">
					<label for="confirm_password" class="col-sm-2 control-label">Confirmar:</label>
					<div class="col-md-10">
						<input type="password" id="confirm_password" class="form-control" placeholder="Confirmar contraseña" name="confirm_password">
					</div>
				</div>

				<div class="form-group gray-content-block">
					<div class="">
						<button type="submit" class="btn btn-success">Guardar</button>
						<a href="{{ route('user.index') }}" class="btn btn-default pull-right">Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/js/users/create.js') !!}
@stop
