@extends('base')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@if (Session::has('success'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('success') }}
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Crear usuario</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! Form::open(['route' => ['user.store', $user->id], 'id'=>'user-edit-form', 'class'=>'form-horizontal']) !!}
				<div class="well form-section">Datos generales</div>

				<div class="form-group">
					{!! Form::label('name', 'Nombre:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Nombre','required', 'autofocus']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('primer_apellido', 'Primer apellido:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('primer_apellido', null, ['class'=>'form-control', 'placeholder'=>'Primer apellido','required']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('segundo_apellido', 'Segundo apellido:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('segundo_apellido', null, ['class'=>'form-control', 'placeholder'=>'Segundo apellido']) !!}
					</div>
				</div>
				<div class="well form-section">Establecer rol del usuario</div>
				<div class="form-group">
					{!! Form::label('role', 'Rol:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::select('role', $roles, null, ['class'=>'form-control', 'required','placeholder'=>'Seleccione...']) !!}
					</div>
				</div>
				<div class="well form-section">Correo y usuario</div>
				<div class="form-group">
					{!! Form::label('username', 'Nombre de usuario:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Nombre de usuario','required']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('email', 'Email:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email','required']) !!}
					</div>
				</div>
				<div class="well form-section">Establecer contraseña</div>
				<div class="form-group">
					{!! Form::label('password', 'Contraseña:', ['class'=>'col-sm-2 control-label']) !!}
					<div class="col-sm-10">
						{!! Form::password('password', ['id'=>'password', 'class'=>'form-control', 'placeholder'=>'Contraseña','required']) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="confirm_password" class="col-sm-2 control-label">Confirmar:</label>
					<div class="col-md-10">
						<input type="password" id="confirm_password" class="form-control" placeholder="Confirmar contraseña" name="confirm_password">
					</div>
				</div>

				<div class="form-group gray-content-block">
					<div class="">
						<button type="submit" class="btn btn-success">Guardar</button>
						<a href="{{ route('user.index') }}" class="btn btn-default pull-right">Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/js/users/create.js') !!}
@stop
