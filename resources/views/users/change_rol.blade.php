@extends('base')

@section('css')
	{!! Html::style('assets/bower_components/EasyAutocomplete/dist/easy-autocomplete.min.css') !!}
@stop

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@if (Session::has('success'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('success') }}
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h2>Asignar Rol</h2>
				<form action="{{ route('user.update.rol', $user) }}" method="post" class="form-horizontal" role="form">
					{{ csrf_field() }}
					<div class="well form-section">Datos generales</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nombre</label>
						<div class="col-sm-10">
							<p class="form-control-static">{{ $user->data->name }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Usuario</label>
						<div class="col-sm-10">
							<p class="form-control-static">{{ $user->username }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static">{{ $user->email }}</p>
						</div>
					</div>
					<div class="well form-section">Centro de trabajo</div>

					<div class="form-group">
						<label for="centro_trabajo_id" class="col-sm-2 control-label">CCT</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="centro_trabajo"
								   placeholder="Escriba nombre o clave centro de trabajo"
								   data-url="{{ route('autocomplete.centros', ['term'=>'TERM']) }}"
									value="{{ is_null($user->data->centro_trabajo) ? "" : $user->data->centro_trabajo->getAllDescription() }}">
							<input type="hidden" name="centro_trabajo_id" id="centro_trabajo_id" value="{{ $user->data->centro_trabajo_id }}">

						</div>
					</div>

					<div class="well form-section">Establecer rol</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Rol</label>
						<div class="col-sm-10">
							{!! Form::select('role', $roles, $user->role, ['class'=>'form-control', 'required','placeholder'=>'Seleccione...']) !!}
						</div>
					</div>
					<div class="form-group gray-content-block">
						<div class="">
							<button type="submit" class="btn btn-success">Guardar</button>
							<a href="{{ route('user.index') }}" class="btn btn-default pull-right">Regresar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/bower_components/EasyAutocomplete/dist/jquery.easy-autocomplete.min.js') !!}
	{!! Html::script('assets/js/users/change_rol.js') !!}
@stop
