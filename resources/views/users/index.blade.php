@extends('base')

@section('css')
	{!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>Usuarios</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table id="users-table" class="table table-striped table-hover" data-url="{{ route('user.index.data') }}">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombre</th>
						<th>Rol</th>
						<th width="50px">Opciones</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>

		</div>
	</div>
</div>
@stop

@section('scripts')
	{!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	{!! Html::script('assets/js/spanish.datatables.js') !!}
	{!! Html::script('assets/js/users/index.js') !!}
@stop
