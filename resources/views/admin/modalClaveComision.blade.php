

@extends('layout.baseModal')

@section('modalTitle')
    Ingrese la clave de la comisión
@endsection

@section('modalBody')
    <div class="container">
        <div class="row">
            <div class="col-xs-10">
                {!! Form::open(['route'=>['post.guardar-clave-comision', $reporte->id], 'id'=>'form-clavecomision']) !!}
                {!! Form::hidden('id', $id, array('id' => 'idReporte')) !!}
                <div class="row">
                    <div class="col-sm-2 control-label">
                        <label for="estado">Estado</label>
                        <input class="form-control" id="estado" name="estado" value = "23" type="text" readonly="1" >
                    </div>
                    <div class="col-sm-2 control-label">
                        <label for="ramo">Ramo</label>
                        <input class="form-control" id="ramo" name="ramo" value = "11923" type="text" readonly="1" >
                    </div>
                    <div class="col-sm-2 control-label">
                        <label for="consecutivo">Consecutivo</label>
                        <input class="form-control" id="consecutivo" name="consecutivo" value = {{$consecutivoComision->consecutivo}} type="text">
                    </div>
                    <div class="col-sm-2 control-label">
                        <label for="anio">Año</label>
                        <input class="form-control" id="anio" name="anio" value = {{$consecutivoComision->anio}} type="text" readonly="1">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('modalFooter')
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <!--<button type="submit"
                    class="btn btn-success"
                    form="form-observacion-acta"
                    data-loading-text="Enviando...">Guardar</button>-->
            <button type="button" class="btn btn-success" id="btn-guardar-clavecomision">Guardar</button>
        </div>
        </div>
    </div>
@endsection

@section('modalScripts')
    @parent
    {!! Html::script('assets/js/admin/clavecomision.js') !!}
@endsection



