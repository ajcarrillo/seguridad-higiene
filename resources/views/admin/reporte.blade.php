@include('admin.partials-reporte.reporte')
@include('admin.partials-reporte.centro_trabajo')
@include('admin.partials-reporte.sindicato')
@include('admin.partials-reporte.presidente')

    <pagebreak />
@include('admin.partials-reporte.secretario')
@include('admin.partials-reporte.vocales')
@include('admin.partials-reporte.documentos')
@if ($a_tipoReporte['calendario'])
    <pagebreak />
    @include('admin.partials-reporte.calendario')
@endif
@if ($a_tipoReporte['verificacion'])
    @if(!$a_tipoReporte['calendario'])
        <pagebreak />
    @endif
    @include('admin.partials-reporte.verificacion')
    <br>
@endif
<table width="100%">
    <tr>
        <td colspan = "3" style="text-align: center; font-size: 7pt; ">
            {{ isset($centros_trabajo['ciudad']) ? $centros_trabajo['ciudad'] : "________________" }} A {{isset($fecha) ? $fecha : "____  DE ______________________ DE 20___"}}
        </td>
    </tr>
    <tr>
        <td colspan = "3" style="text-align: center; font-size: 5pt; ">
            (40)
        </td>
    </tr>
</table>