<table class="table table-condensed" data-id="gridActasRecorrido" data-url="{{ route('get.admin.enviadasJson') }}">
    <thead>
    <tr>
        <th width="15%" >Clave comisión</th>
        <th width="30%" >Centros de trabajo</th>
        <th width="20%" >Tipo de reporte</th>
        <th width="10%" class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>