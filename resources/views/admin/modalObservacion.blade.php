

@extends('layout.baseModal')

@section('modalTitle')
    Ingrese la observación
@endsection

@section('modalBody')
    <div class="container">
        <div class="row">
            <div class="col-xs-9">
                {!! Form::open(['route'=>['post.guardar-observacion', $reporte->id], 'id'=>'form-observacion-acta']) !!}
                {!! Form::hidden('id', $id, array('id' => 'idReporte')) !!}
                <div class="form-group">
                    <div class="col-md-12">
                        <!--<textarea rows="2" cols="130" name="observacion" id="observacion" class = "form-control"></textarea>-->
                        {!! Form::textarea('observacion', null, ['class'=>'form-control', 'placeholder'=>'Observación', 'rows'=>'2','cols'=>'130','required']) !!}
                    </div>
                </div>
                {!! Form::button('env',['type'=>'submit','style'=>'visibility:hidden', 'data-id'=>'enviarObservacion']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('modalFooter')
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <!--<button type="submit"
                    class="btn btn-success"
                    form="form-observacion-acta"
                    data-loading-text="Enviando...">Guardar</button>-->
            <button type="button" class="btn btn-success" id="btn-guardar-observacion">Guardar</button>
        </div>
    </div>
@endsection

@section('modalScripts')
    @parent
    {!! Html::script('assets/js/admin/observacion.js') !!}
@endsection



