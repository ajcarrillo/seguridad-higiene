@extends('layout/baseModal')

@section('modalTitle', "Cambiar fechas")

@section('modalBody')

	{!! Form::model($etapaProceso, ['route'=>['etapa.update', $etapaProceso->id], 'class'=>'form-horizontal', 'id'=>'reset-dates-form', 'method'=>'PATCH']) !!}
	<div class="form-group">
		{!! Form::label('apertura', 'Apertura:', ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::date('apertura', null, ['class'=>'form-control', 'placeholder'=>'Apertura','required', 'id'=>'apertura']) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('cierre', 'Cierre:', ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::date('cierre', null, ['class'=>'form-control', 'placeholder'=>'Cierre','required', 'id'=>'cierre']) !!}
		</div>
	</div>
	{!! Form::close() !!}

@stop
@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	<button type="submit"
			id="btn-submit-form"
			form="reset-dates-form"
			class="btn btn-success"
			data-button="save-vocal"
			data-loading-text="Guardando...">Guardar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/js/admin/etapa_proceso/edit.js') !!}
@stop
