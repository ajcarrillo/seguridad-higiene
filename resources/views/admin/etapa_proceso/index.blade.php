@extends('base')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@if (Session::has('success'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('success') }}
					</div>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Etapa del proceso</h2>
				<hr>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
					<tr>
						<th>Etapa</th>
						<th>Descripción</th>
						<th>Apertura</th>
						<th>Cierre</th>
						<th>Opciones</th>
					</tr>
					</thead>
					<tbody>
					@foreach($etapas as $etapa)
						<tr>
							<td>{{ $etapa->nombre }}</td>
							<td>{{ $etapa->descripcion }}</td>
							<td>{{ \Carbon\Carbon::parse($etapa->apertura)->format('d/m/Y') }}</td>
							<td>{{ \Carbon\Carbon::parse($etapa->cierre)->format('d/m/Y') }}</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
											data-toggle="dropdown">
										Opciones
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										<li role="presentation">
											<a role="menuitem"
											   tabindex="-1"
											   href="#!"
											   data-url="{{ route('etapa.edit', $etapa->id) }}"
											   data-button="edit-etapa">Cambiar fechas</a></li>
										<li role="presentation" class="divider"></li>
										<li role="presentation">
											<a role="menuitem" tabindex="-1" href="#">Abrir proceso</a></li>
										<li role="presentation">
											<a role="menuitem" tabindex="-1" href="#">Cerrar proceso</a></li>
									</ul>
								</div>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/admin/etapa_proceso/index.js') !!}
@stop
