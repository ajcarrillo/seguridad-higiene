@extends('base')
@section('css')
    {!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
	<div class="container top">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Actas de recorrido</div>
					<div class="panel-body">
						<div id="content">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<ul id="tabs_actas" class="nav nav-tabs" data-tabs="tabs" >
								<li class="active"><a href="#" data-toggle="tab" data-url="enviadasJson">Enviadas</a></li>
								<li><a href="#" data-toggle="tab" data-url="sinClaveComisionJson">Sin clave comisión</a></li>
								<li><a href="#" data-toggle="tab" data-url="terminadasJson">Terminadas</a></li>
								<li><a href="#" data-toggle="tab" data-url="conObservacionJson">Con observaciones</a></li>
							</ul>
							<div id="tabs_verificacion_content" class="tab-content">
								<div class="tab-pane active" id="enviadas" style="padding: 3%">
									@include('admin.listaActas')
								</div>
								<div class="tab-pane" id="sinClaveComision">
								</div>
								<div class="tab-pane" id="terminadas">
								</div>
								<div class="tab-pane" id="conObservacion">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/Modal.js') !!}
	{!! Html::script('assets/js/admin/actas.js') !!}
	{!! Html::script('assets/js/routes/routes.js') !!}
    {!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/js/spanish.datatables.js') !!}
    {!! Html::script('assets/js/admin/index.js') !!}
@endsection