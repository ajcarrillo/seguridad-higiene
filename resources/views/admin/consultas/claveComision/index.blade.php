@extends('base')
@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

@endsection
@section('content')
    <div class="container top">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Claves de comisión</div>
                    <div class="panel-body">
                        <div id="content">
                            <table class="table table-bordered" id="comision-table">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Clave de comisión</th>
                                        <th colspan="3" style="text-align: center">Centro de trabajo</th>
                                    </tr>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Nombre</th>
                                        <th>Turno</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script>
    $(function() {
        $('#comision-table').DataTable({
            processing: true,
            serverSide: true,
            sort: false,
            //headerSort: false,
            ajax: '{!! route('claveComision.datos') !!}',
            columns: [
                { data: 'clave_comision', name: 'clave_comision'},
                { data: 'cct', name: 'cct'},
                { data: 'nombre', name: 'nombre'},
                { data: 'siglas', name: 'siglas'},

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontro información",
                "info": "Pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "search":"Buscar:",
                "previous":"Anterior",
                "next":"Siguiente"

            }
        });
    });
</script>
@endsection