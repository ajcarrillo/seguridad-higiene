<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Datos de la actualización
                <span class="glyphicon glyphicon-menu-up pull-right" id="panel-comision"></span>
            </div>
            <div class="panel-body">
                <div id="content">
                    <ul id="tabs_comision" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#centro_trabajo_actualizacion" data-toggle="tab">Centro de trabajo</a>
                        </li>
                        <li><a href="#presidente_actualizacion" data-toggle="tab">Presidente</a></li>
                        <li><a href="#secretario_actualizacion" data-toggle="tab">Secretario</a></li>
                        <li><a href="#vocales_actualizacion" data-toggle="tab">Vocales</a></li>
                        <li><a href="#documentacion_actualizacion" data-toggle="tab">Documentación anexa</a></li>
                    </ul>
                    <div id="tabs_verificacion_content" class="tab-content">
                        <div class="tab-pane active" id="centro_trabajo_actualizacion">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Clave CT
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Turno
                                        </th>
                                        <th>
                                            Municipio
                                        </th>
                                        <th>
                                            Localidad
                                        </th>
                                        <th>
                                            Nivel
                                        </th>
                                        <th>
                                            Modalidad
                                        </th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody id="myTable">
                                    @if(!$reporte->actualizacion->centroTransacciones)
                                        <tr>
                                            <td colspan="8"> <p>Sin actualizacion de centros de trabajo</p> </td>
                                        </tr>
                                        @else
                                        @foreach ($reporte->actualizacion->centroTransacciones as $centro)
                                            <tr class="{{ $centro->actions == "Delete" ? "danger":"success" }}">
                                                <td>{{$centro->escuela_lite->cct}}</td>
                                                <td>{{$centro->escuela_lite->nombre}}</td>
                                                <td>{{$centro->escuela_lite->turno->d_turno}}</td>
                                                <td>{{$centro->escuela_lite->d_municipio}}</td>
                                                <td>{{$centro->escuela_lite->d_localidad}}</td>
                                                <td>{{$centro->escuela_lite->nivel}}</td>
                                                <td>{{$centro->escuela_lite->modalidad}}</td>
                                                <td>@if ($centro->actions == 'Delete')
                                                        Eliminar
                                                    @elseif ($centro->actions == 'Insert')
                                                        Agregar
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="presidente_actualizacion">
                            <br>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        {!! Form::label ('presidente', 'Nombre:') !!}
                                        {!! Form::text('presidente', isset($reporte->actualizacion->presidenteTransacciones->nombre_completo) ? $reporte->actualizacion->presidenteTransacciones->nombre_completo : null , array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        {!! Form::label ('cargo', 'Cargo:') !!}
                                        {!! Form::text('cargo', isset($reporte->actualizacion->presidenteTransacciones->cargo) ? $reporte->actualizacion->presidenteTransacciones->cargo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane" id="secretario_actualizacion">
                            <br>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        {!! Form::label ('secretario', 'Nombre:') !!}
                                        {!! Form::text('secretario', isset($reporte->actualizacion->secretarioTransacciones->nombre_completo) ? $reporte->actualizacion->secretarioTransacciones->nombre_completo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <?php
                                            if (!isset($reporte->actualizacion->secretarioTransacciones->sector))
                                                $sector = null;
                                            else
                                                if ($reporte->actualizacion->secretarioTransacciones->sector == 1)
                                                    $sector = 'OFICIAL';
                                                else if ($reporte->actualizacion->secretarioTransacciones->sector = 2)
                                                    $sector = 'SINDICAL';
                                        ?>
                                        {!! Form::label ('sector', 'Sector:') !!}
                                        {!! Form::text('sector', $sector , array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        {!! Form::label ('rfc', 'RFC:') !!}
                                        {!! Form::text('rfc', isset($reporte->actualizacion->secretarioTransacciones->rfc) ? $reporte->actualizacion->secretarioTransacciones->rfc : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane" id="vocales_actualizacion">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Cargo
                                    </th>
                                    <th>
                                        Sector
                                    </th>
                                    <th>
                                        Tipo de representación
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="myTable">
                                @if(!$reporte->actualizacion->vocalTransacciones)
                                    <tr>
                                        <td colspan="8"> <p>Sin actualización de vocales</p> </td>
                                    </tr>
                                    @else
                            @foreach($reporte->actualizacion->vocalTransacciones as $vocal)
                                <tr class="{{ $centro->actions == "Delete" ? "danger":"success" }}">
                                    <td>{{$vocal->nombre_completo}}</td>
                                    <td>{{$vocal->cargo}}</td>
                                    <td>{{$vocal->nombreSector}}</td>
                                    <td>{{$vocal->nombreTipo}}</td>
                                    <td>@if ($vocal->actions == 'Delete')
                                            Eliminar
                                        @elseif ($vocal->actions == 'Insert')
                                            Agregar
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="documentacion_actualizacion">
                            <br>
                                <div class="row">
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('oficio_oficial', 'Oficio designación oficial:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('oficio_oficial', isset($reporte->actualizacion->num_oficio_oficial) ? $reporte->actualizacion->num_oficio_oficial : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('fecha_oficio_oficial', 'Fecha:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('fecha_oficio_oficial', isset($reporte->actualizacion->fecha_oficio_oficial) ? $reporte->actualizacion->fecha_oficio_oficial : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('oficio_sindical', 'Oficio designación sindical:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('oficio_sindical', isset($reporte->actualizacion->num_oficio_sindical) ? $reporte->actualizacion->num_oficio_sindical : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('fecha_oficio_sindical', 'Fecha:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('fecha_oficio_sindical', isset($reporte->actualizacion->fecha_oficio_sindical) ? $reporte->actualizacion->fecha_oficio_sindical : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>