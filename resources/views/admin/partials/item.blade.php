<tr>

    <td>{{ $acta->comision->clave_comision }}</td>
    <td>@foreach($acta->comision->centros as $centro)
            [{{$centro->escuela_lite->cct}}]
            {{$centro->escuela_lite->nombre}}
            {{$centro->escuela_lite->turno->siglas}}
            <br>
        @endforeach
    </td>
    <td>
        @foreach($acta->tipos as $tipo)
            {{$tipo->tipo->nombre}}
            <br>
        @endforeach
    </td>
    <td class="text-center">
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Opciones <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                @if($acta->status->nombre == 'Enviada')
                    <li><a href="edit?modify={{$acta->id}}">Revisar</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#!"  data-button="aceptar-acta" data-id="{{$acta->id}}">Aceptar</a></li>
                    <li><a href="#!" data-button="observacion-acta" data-id="{{$acta->id}}">Capturar Observación</a></li>
                @endif
                @if($acta->status->nombre == 'Aceptada')
                        @foreach($acta->tipos as $tipo)
                            @if($tipo->tipo->nombre == 'Registro')
                                <li><a href="#!" data-button="asigna-clave-comision" data-id="{{$acta->id}}">Asignar clave comisión</a></li>
                                <li role="separator" class="divider"></li>
                            @endif
                        @endforeach
                        <li><a href="impresion/{{$acta->id}}" data-button="open-calendario">Impresión </a></li>
                @endif
                @if($acta->status->nombre == 'Aprobado')
                     <li><a href="impresion/{{$acta->id}}" data-button="open-calendario">Impresión </a></li>
                @endif
                @if($acta->status->nombre == 'Rechazada')
                        <li><a href="ver/{{$acta->id}}" data-button="open-calendario">Ver</a></li>
                @endif
            </ul>
        </div>

    </td>
</tr>
