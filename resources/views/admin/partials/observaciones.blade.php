<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Observaciones
                <span class="glyphicon glyphicon-menu-up pull-right" id="panel-comision"></span>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Observación
                            </th>
                        </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($reporte->observaciones as $observacion)
                            <tr>
                                <td>{{$observacion->user->primer_apellido.' '.
                                      $observacion->user->segundo_apellido.' '.
                                      $observacion->user->name}}</td>
                                <td>{{$observacion->observacion}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>