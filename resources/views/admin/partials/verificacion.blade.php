<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Acta de verficación
                <span class="glyphicon glyphicon-menu-up pull-right" id="panel-comision"></span>
            </div>
            <div class="panel-body">
                <div id="content">
                    <ul id="tabs_verificacion" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#generales" data-toggle="tab">Datos generales</a></li>
                        <li><a href="#detectadas" data-toggle="tab">Incidencias detectadas</a></li>
                        <li><a href="#subsistentes" data-toggle="tab">Incidencias subsistentes</a></li>
                        <li><a href="#subsanadas" data-toggle="tab">Incidencias subsanadas</a></li>
                    </ul>
                    <div id="tabs_verificacion_content" class="tab-content">
                        <div class="tab-pane active" id="generales">
                            <br>
                            <div class="row">
                                <div class="col-sm-2 control-label">
                                    {!! Form::label ('fecha_verificacion', 'Fecha verificación:') !!}
                                </div>
                                <div class="col-sm-2">
                                    {!! Form::text('fecha_verificacion',  isset($reporte->verificacion->fecha) ? $reporte->verificacion->fecha : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                </div>
                                <div class="col-sm-2 control-label">
                                    {!! Form::label ('tipo_verificacion', 'Tipo verificación:') !!}
                                </div>
                                <div class="col-sm-2">
                                    <?php
                                        if (!isset($reporte->verificacion->tipo))
                                            $tipo = null;
                                        else
                                            if ($reporte->verificacion->tipo == 1)
                                                $tipo = 'ORDINARIA';
                                            else if ($reporte->verificacion->tipo = 2)
                                                $tipo = 'EXTRAORDINARIA';
                                    ?>
                                    {!! Form::text('tipo_verificacion',  $tipo, array('class' => 'form-control', 'readonly' => true)) !!}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-2 control-label">
                                    {!! Form::label ('riesgos', 'No. riesgos de trabajo:') !!}
                                </div>
                                <div class="col-sm-2 ">
                                    {!! Form::text('riesgos',  isset($reporte->verificacion->numero_de_riesgos_de_trabajo) ? $reporte->verificacion->numero_de_riesgos_de_trabajo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                </div>
                                <div class="col-sm-2 control-label">
                                    {!! Form::label ('accidentes', 'Accidente de trabajo:') !!}
                                </div>
                                <div class="col-sm-2 ">
                                    {!! Form::text('accidentes', isset($reporte->verificacion->accidentes_de_trabajo) ? $reporte->verificacion->accidentes_de_trabajo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                </div>
                                <div class="col-sm-2 control-label">
                                    {!! Form::label ('enfermedades', 'Enfermedades profesionales:') !!}
                                </div>
                                <div class="col-sm-2 ">
                                    {!! Form::text('enfermedades', isset($reporte->verificacion->enfermedad_profesional) ? $reporte->verificacion->enfermedad_profesional : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label ('observaciones', 'Observaciones:') !!}
                                {!! Form::textarea('observaciones', isset($reporte->verificacion->observacion) ? $reporte->verificacion->observacion : null, array('class' => 'form-control', 'readonly' => true, 'size' => '30x2')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label ('observacionesCSST', 'Observaciones CSST:') !!}
                                {!! Form::textarea('observacionesCSST',  isset($reporte->verificacion->propuesta_csst) ? $reporte->verificacion->propuesta_csst : null, array('class' => 'form-control', 'readonly' => true, 'size' => '30x2')) !!}
                            </div>
                        </div>
                        <div class="tab-pane" id="detectadas">
                            {!! $gridIncidenciasDetectadas !!}
                        </div>
                        <div class="tab-pane" id="subsistentes">
                            {!! $gridIncidenciasSubsistentes !!}
                        </div>
                        <div class="tab-pane" id="subsanadas">
                            {!! $gridIncidenciasSubsanadas !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
