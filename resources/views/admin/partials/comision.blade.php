<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Datos de la comisión
                <span class="glyphicon glyphicon-menu-up pull-right" id="panel-comision"></span>
            </div>
            <div class="panel-body">
                <div id="content">
                    <ul id="tabs_comision" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#centro_trabajo" data-toggle="tab">Centro de trabajo</a>
                        </li>
                        <li><a href="#sindicato" data-toggle="tab">Organización sindical</a></li>
                        <li><a href="#presidente" data-toggle="tab">Presidente</a></li>
                        <li><a href="#secretario" data-toggle="tab">Secretario</a></li>
                        <li><a href="#vocales" data-toggle="tab">Vocales</a></li>
                        <li><a href="#documentacion" data-toggle="tab">Documentación anexa</a></li>
                    </ul>
                    <div id="tabs_verificacion_content" class="tab-content">
                        <div class="tab-pane active" id="centro_trabajo">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Clave CT
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Turno
                                        </th>
                                        <th>
                                            Municipio
                                        </th>
                                        <th>
                                            Localidad
                                        </th>
                                        <th>
                                            Nivel
                                        </th>
                                        <th>
                                            Modalidad
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="myTable">
                                    @forelse($reporte->comision->centros as $centro)
                                        <tr>
                                            <td>{{$centro->escuela_lite->cct}}</td>
                                            <td>{{$centro->escuela_lite->nombre}}</td>
                                            <td>{{$centro->escuela_lite->turno->d_turno}}</td>
                                            <td>{{$centro->escuela_lite->d_municipio}}</td>
                                            <td>{{$centro->escuela_lite->d_localidad}}</td>
                                            <td>{{$centro->escuela_lite->nivel}}</td>
                                            <td>{{$centro->escuela_lite->modalidad}}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7"> <p>No contiene centros de trabajo</p> </td>
                                        </tr>
                                    @endforelse
                                   </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="sindicato">
                            <br>
                            <div class="form-group">
                                 {!! Form::label ('sindicato', 'Sindicato:') !!}
                                 {!! Form::text('sindicato',   isset($reporte->comision->cat_sindicato->nombre) ? $reporte->comision->cat_sindicato->nombre : null , array('class' => 'form-control', 'readonly' => true)) !!}
                            </div>
                        </div>
                        <div class="tab-pane" id="presidente">
                            <br>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        {!! Form::label ('presidente', 'Nombre:') !!}
                                        {!! Form::text('presidente', isset($reporte->comision->presidente->nombre_completo) ? $reporte->comision->presidente->nombre_completo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        {!! Form::label ('cargo', 'Cargo:') !!}
                                        {!! Form::text('cargo',  isset($reporte->comision->presidente->cargo) ? $reporte->comision->presidente->cargo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane" id="secretario">
                            <br>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        {!! Form::label ('secretario', 'Nombre:') !!}
                                        {!! Form::text('secretario', isset($reporte->comision->secretario->nombre_completo) ? $reporte->comision->secretario->nombre_completo : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <?php
                                            if (!isset($reporte->comision->secretario->sector))
                                                $sector = null;
                                            else
                                                if ($reporte->comision->secretario->sector == 1)
                                                    $sector = 'OFICIAL';
                                                else if ($reporte->comision->secretario->sector = 2)
                                                    $sector = 'SINDICAL';
                                        ?>
                                        {!! Form::label ('sector', 'Sector:') !!}
                                        {!! Form::text('sector', $sector , array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        {!! Form::label ('rfc', 'RFC:') !!}
                                        {!! Form::text('rfc', isset($reporte->comision->secretario->rfc) ? $reporte->comision->secretario->rfc : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                        </div>
                        <div class="tab-pane" id="vocales">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Cargo
                                        </th>
                                        <th>
                                            Sector
                                        </th>
                                        <th>
                                            Tipo de representación
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        @forelse($reporte->comision->vocales as $vocal)
                                            <tr>
                                                <td>{{$vocal->nombre_completo}}</td>
                                                <td>{{$vocal->cargo}}</td>
                                                <td>{{$vocal->nombreSector}}</td>
                                                <td>{{$vocal->nombreTipo}}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4"><p>No contiene vocales</p></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="documentacion">
                            <br>
                                <div class="row">
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('oficio_oficial', 'Oficio designación oficial:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('oficio_oficial', isset($reporte->comision->num_oficio_oficial) ? $reporte->comision->num_oficio_oficial : null , array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('fecha_oficio_oficial', 'Fecha:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('fecha_oficio_oficial', isset($reporte->comision->fecha_oficio_oficial) ? $reporte->comision->fecha_oficio_oficial : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('oficio_sindical', 'Oficio designación sindical:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('oficio_sindical', isset($reporte->comision->num_oficio_sindical) ? $reporte->comision->num_oficio_sindical : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                    <div class="col-sm-3 control-label" style="text-align: left!important">
                                        {!! Form::label ('fecha_oficio_sindical', 'Fecha:') !!}
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('fecha_oficio_sindical', isset($reporte->comision->fecha_oficio_sindical) ? $reporte->comision->fecha_oficio_sindical : null, array('class' => 'form-control', 'readonly' => true)) !!}
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>