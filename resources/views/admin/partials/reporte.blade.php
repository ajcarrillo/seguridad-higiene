<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Datos del Reporte</div>
            <div class="panel-body">
                <div class="row">
                    {!! Form::hidden('idReporte', $reporte->id, array('id' => 'idReporte')) !!}
                    <div class="col-sm-3 form-group">
                        {!! Form::label ('fecha', 'Fecha del reporte:') !!}
                        {!! Form::date('fecha', $reporte->fecha, array('class' => 'form-control', 'readonly' => true))
                        !!}
                    </div>
                    <div class="col-sm-6 form-group">
                        {!! Form::label ('tipo', 'Tipo de reporte:') !!}
                        {!! Form::text('tipo', $tipoReporte, array('class' => 'form-control', 'readonly' => true)) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 control-label">
                        {!! Form::label ('comision', 'Clave comision:') !!}
                    </div>
                    <div class="col-sm-2 control-label">
                        {!! Form::label ('estado', 'Estado:') !!}
                        {!! Form::text('estado', substr($reporte->comision->clave_comision, 0, 2), array('class' => 'form-control','readonly' => true)) !!}
                    </div>
                    <div class="col-sm-2 control-label">
                       {!! Form::label ('rama', 'Rama:') !!}
                        {!! Form::text('rama', substr($reporte->comision->clave_comision, 2, 5), array('class' => 'form-control','readonly' => true)) !!}
                    </div>
                        <div class="col-sm-2 control-label">
                       {!! Form::label ('consecutivo', 'Consecutivo:') !!}
                        {!! Form::text('consecutivo', substr($reporte->comision->clave_comision, 7, 5), array('class' => 'form-control',
                        'readonly' => true)) !!}
                        </div>
                            <div class="col-sm-2 control-label">

                        {!! Form::label ('anio', 'Año:') !!}
                        {!! Form::text('anio', substr($reporte->comision->clave_comision, 12, 4), array('class' => 'form-control',
                        'readonly' => true)) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
