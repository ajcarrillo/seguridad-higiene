<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Calendario de Actividades
                <span class="glyphicon glyphicon-menu-up pull-right" id="panel-comision"></span>
            </div>
            <div class="panel-body">
                <div id="content">
                    <div class="row">
                        <div class="col-sm-3 control-label" style="text-align: left!important">
                            {!! Form::label ('de', 'Periodo:') !!}
                        </div>
                        <div class="col-sm-3">
                            {!! Form::text('de', isset($reporte->calendario->de) ? $reporte->calendario->de : null, array('class' => 'form-control', 'readonly' => true)) !!}
                        </div>
                        <div class="col-sm-3 control-label" style="text-align: left!important">
                            {!! Form::label ('a', 'a') !!}
                        </div>
                        <div class="col-sm-3">
                            {!! Form::text('de', isset($reporte->calendario->a) ? $reporte->calendario->a : null, array('class' => 'form-control', 'readonly' => true)) !!}
                        </div>
                    </div>
                    <br>
                    <h4>Actividades</h4>
                    <br>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>
                                    Descripción
                                </th>
                                <th>
                                    Unidad de medida
                                </th>
                                <th>
                                    ENE-MAR
                                </th>
                                <th>
                                    ABR-JUN
                                </th>
                                <th>
                                    JUL-SEP
                                </th>
                                <th>
                                    OCT-DIC
                                </th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                                @forelse($reporte->calendario->actividades as $actividad)
                                    <tr>
                                        <td>{{$actividad->descripcion}}</td>
                                        <td>{{$actividad->unidad_de_medida}}</td>
                                        <td style="text-align: center">@if ($actividad->trimestre == 1) X @else  &nbsp;&nbsp; @endif</td>
                                        <td style="text-align: center">@if ($actividad->trimestre == 2) X @else  &nbsp;&nbsp; @endif</td>
                                        <td style="text-align: center">@if ($actividad->trimestre == 3) X @else  &nbsp;&nbsp; @endif</td>
                                        <td style="text-align: center">@if ($actividad->trimestre == 4) X @else  &nbsp;&nbsp; @endif</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6"> <p>Sin actividades calendarizadas</p> </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>