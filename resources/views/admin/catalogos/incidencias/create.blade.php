@extends('layout.baseModal')

@section('modalTitle')
Crear incidencia
@endsection

@section('modalBody')
    <div class="container">
    	<div class="row">
    		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                {!! Form::open(['route'=>['post.agregar.incidencia'], 'id'=>'form-nueva-incidencia']) !!}
                    <div class="form-group">
                        <label for="nombre">Nombre </label>
                        <input name="nombre" type="text" class="form-control">
                    </div>
                <div class="form-group">
                    <label for="nombre">Clave </label>
                    <input name="clave" type="text" class="form-control">
                </div>
                {!! Form::close() !!}
    		</div>
    	</div>
    </div>
@endsection

@section('modalFooter')
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="button" class="btn btn-primary" id="btn-guardar-incidencia">Guardar</button>
        </div>
    </div>
@endsection

@section('modalScripts')
    @parent
    {!! Html::script('assets/js/catalogos/incidencias/create.js') !!}
@endsection

