@extends('layout.baseModal')

@section('modalTitle')
Modificar incidencia
@endsection

@section('modalBody')
    <div class="container">
    	<div class="row">
    		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                {!! Form::open(['route'=>['post.modificar.incidencia'], 'id'=>'form-modifica-incidencia']) !!}
                {!! Form::hidden('id', $incidencia->id, array('id' => 'idIncidencia')) !!}
                {!! Form::label ('nombre', 'Nombre:') !!}
                {!! Form::text('nombre',  $incidencia->nombre, array('class' => 'form-control')) !!}
                {!! Form::label ('nombre', 'Nombre:') !!}
                {!! Form::text('clave',  $incidencia->clave, array('class' => 'form-control')) !!}
                {!! Form::close() !!}
    		</div>
    	</div>
    </div>
@endsection

@section('modalFooter')
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="button" class="btn btn-primary" id="btn-modificar-incidencia">Guardar</button>
        </div>
    </div>
@endsection

@section('modalScripts')
    @parent
    {!! Html::script('assets/js/catalogos/incidencias/modify.js') !!}
@endsection

