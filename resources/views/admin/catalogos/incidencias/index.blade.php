@extends('base')

@section('content')
    {!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
    {!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
    {!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
    {!! Html::script('assets/js/bootbox/bootbox.min.js') !!}
    <div class="container">
        <h1>Catalogo de incidencias</h1>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" type="button" id="btn-nuevo">Agregar incidencia</button>
            </div>
        </div>
        <br>
        {!! Form::hidden('pagina_actual', $incidencias->currentPage(), array('id' => 'pagina_actual')) !!}
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="tabla-incidencias">
                	<thead>
                		<tr>
                			<th>Nombre</th>
                            <th>Clave</th>
                            <th>Opciones</th>
                		</tr>
                	</thead>
                	<tbody>
                        @forelse ($incidencias as $incidencia)
                		<tr >
                			<td>{{$incidencia->nombre}}</td>
                            <td>{{$incidencia->clave}}</td>
                            <td>
                                <button type="button" class="btn btn-success" data-button="edita-incidencia" data-id="{{$incidencia->id}}"><span class="glyphicon glyphicon-pencil"></span></button>
                                <button type="button" class="btn btn-danger" data-button="borrar-incidencia" data-id="{{$incidencia->id}}"><span class="glyphicon glyphicon-trash"></span></button>
                            </td>
                		</tr>
                        @empty
                            <tr>
                                <td colspan="2"> SIN RESULTADOS </td>
                            </tr>
                        @endforelse
                	</tbody>

                </table>

                <div id="paginacion">

                </div>

                {{-- {!! $incidencias->render() !!} --}}

            </div>
        </div>
    </div>

{{--    <link rel="stylesheet" href="{{ $cdn }}"> --}}

    {!! Form::open(['route'=>['delete.incidencia', ':INCIDENCIA_ID'], 'id'=>'form-borrar-incidencia']) !!}
    {!! Form::close() !!}

@endsection

@section('scripts')
    <script type = "text/javascript">
        var total = "{{ $incidencias->total() }}";
        var ultima_pagina = "{{$incidencias->lastpage()}}";
        var url = "{{$incidencias->url(1)}}";
        var pagina_actual = "{{$incidencias->currentPage()}}";
    </script>
    <script type="text/html" id="template-row-incidencia">
    <tr>
        <td><%= nombre %></td>
        <td><%= clave %></td>
        <td>
            <button type="button" class="btn btn-success" data-button="edita-incidencia" data-id="<%= id %>"><span class="glyphicon glyphicon-pencil"></span></button>
            <button type="button" class="btn btn-danger" data-button="borrar-incidencia" data-id="<%= id %>"><span class="glyphicon glyphicon-trash"></span></button>
        </td>
    </tr>
    </script>
    <script type="text/html" id="template-table-incidencia">
        <% _.each(json, function(incidencia) { %>
        <tr>
            <td><%= incidencia.nombre %></td>
            <td><%= incidencia.clave %></td>
            <td>
                <button type="button" class="btn btn-success" data-button="edita-incidencia" data-id="<%= incidencia.id %>"><span class="glyphicon glyphicon-pencil"></span></button>
                <button type="button" class="btn btn-danger" data-button="borrar-incidencia" data-id="<%= incidencia.id %>"><span class="glyphicon glyphicon-trash"></span></button>
            </td>
        </tr>
        <% }); %>
    </script>
    {!! Html::script('assets/js/routes/routes.js') !!}
    {!! Html::script('assets/js/SEQ.Utilities.js') !!}
    {!! Html::script('assets/js/catalogos/incidencias/index.js') !!}
@endsection