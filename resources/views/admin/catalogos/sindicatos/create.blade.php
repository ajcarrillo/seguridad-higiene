@extends('layout.baseModal')

@section('modalTitle')
Crear sindicato
@endsection

@section('modalBody')
    <div class="container">
    	<div class="row">
    		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                {!! Form::open(['route'=>['post.agregar.sindicato'], 'id'=>'form-nuevo-sindicato']) !!}
                {!! Form::label ('nombre', 'Nombre:') !!}
                {!! Form::text('nombre',  NULL, array('class' => 'form-control')) !!}
                {!! Form::label ('direccion', 'Dirección:') !!}
                {!! Form::text('direccion',  NULL, array('class' => 'form-control')) !!}
                {!! Form::label ('colonia', 'Colonia:') !!}
                {!! Form::text('colonia',  NULL, array('class' => 'form-control')) !!}
                {!! Form::label ('codigo_postal', 'Codigo Postal:') !!}
                {!! Form::text('codigo_postal',  NULL, array('class' => 'form-control')) !!}
                {!! Form::label ('ciudad', 'Ciudad:') !!}
                {!! Form::text('ciudad',  NULL, array('class' => 'form-control')) !!}
                {!! Form::label ('telefono', 'Telefono:') !!}
                {!! Form::text('telefono',  NULL, array('class' => 'form-control')) !!}
                {!! Form::close() !!}
    		</div>
    	</div>
    </div>
@endsection

@section('modalFooter')
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="button" class="btn btn-primary" id="btn-guardar-sindicato">Guardar</button>
        </div>
    </div>
@endsection

@section('modalScripts')
    @parent
    {!! Html::script('assets/js/catalogos/sindicatos/create.js') !!}
@endsection

