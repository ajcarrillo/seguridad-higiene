@extends('base')

@section('content')
    {!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
    {!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
    {!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
    {!! Html::script('assets/js/bootbox/bootbox.min.js') !!}
    <div class="container">
        <h1>Catalogo de sindicatos</h1>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary" type="button" id="btn-nuevo">Agregar sindicato</button>
            </div>
        </div>
        <br>
        {!! Form::hidden('pagina_actual', $sindicatos->currentPage(), array('id' => 'pagina_actual')) !!}
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover" id="tabla-incidencias">
                	<thead>
                		<tr>
                			<th>Nombre</th>
                            <th>Opciones</th>
                		</tr>
                	</thead>
                	<tbody>
                        @forelse ($sindicatos as $sindicato)
                		<tr >
                			<td>{{$sindicato->nombre}}</td>
                            <td>
                                <button type="button" class="btn btn-success" data-button="edita-sindicato" data-id="{{$sindicato->id}}"><span class="glyphicon glyphicon-pencil"></span></button>
                                <button type="button" class="btn btn-danger" data-button="borrar-sindicato" data-id="{{$sindicato->id}}"><span class="glyphicon glyphicon-trash"></span></button>
                            </td>
                		</tr>
                        @empty
                            <tr>
                                <td colspan="2"> SIN RESULTADOS </td>
                            </tr>
                        @endforelse
                	</tbody>

                </table>

                <div id="paginacion">

                </div>

                {{-- {!! $incidencias->render() !!} --}}

            </div>
        </div>
    </div>

{{--    <link rel="stylesheet" href="{{ $cdn }}"> --}}

    {!! Form::open(['route'=>['delete.sindicato', ':SINDICATO_ID'], 'id'=>'form-borrar-sindicato']) !!}
    {!! Form::close() !!}

@endsection

@section('scripts')
    <script type = "text/javascript">
        var total = "{{ $sindicatos->total() }}";
        var ultima_pagina = "{{$sindicatos->lastpage()}}";
        var url = "{{$sindicatos->url(1)}}";
        var pagina_actual = "{{$sindicatos->currentPage()}}";
    </script>
    <script type="text/html" id="template-row-incidencia">
    <tr>
        <td><%= nombre %></td>
        <td>
            <button type="button" class="btn btn-success" data-button="edita-sindicato" data-id="<%= id %>"><span class="glyphicon glyphicon-pencil"></span></button>
            <button type="button" class="btn btn-danger" data-button="borrar-sindicato" data-id="<%= id %>"><span class="glyphicon glyphicon-trash"></span></button>
        </td>
    </tr>
    </script>
    <script type="text/html" id="template-table-incidencia">
        <% _.each(json, function(sindicato) { %>
        <tr>
            <td><%= sindicato.nombre %></td>
            <td>
                <button type="button" class="btn btn-success" data-button="edita-sindicato" data-id="<%= sindicato.id %>"><span class="glyphicon glyphicon-pencil"></span></button>
                <button type="button" class="btn btn-danger" data-button="borrar-sindicato" data-id="<%= sindicato.id %>"><span class="glyphicon glyphicon-trash"></span></button>
            </td>
        </tr>
        <% }); %>
    </script>
    {!! Html::script('assets/js/routes/routes.js') !!}
    {!! Html::script('assets/js/SEQ.Utilities.js') !!}
    {!! Html::script('assets/js/catalogos/sindicatos/index.js') !!}
@endsection