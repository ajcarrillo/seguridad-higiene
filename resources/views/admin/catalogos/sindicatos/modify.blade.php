@extends('layout.baseModal')

@section('modalTitle')
Modificar sindicato
@endsection

@section('modalBody')
    <div class="container">
    	<div class="row">
    		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                {!! Form::open(['route'=>['post.modificar.sindicato'], 'id'=>'form-modifica-sindicato']) !!}
                {!! Form::hidden('id', $sindicato->id, array('id' => 'idSindicato')) !!}
                {!! Form::label ('nombre', 'Nombre:') !!}
                {!! Form::text('nombre',  $sindicato->nombre, array('class' => 'form-control')) !!}
                {!! Form::label ('direccion', 'Dirección:') !!}
                {!! Form::text('direccion',  $sindicato->direccion, array('class' => 'form-control')) !!}
                {!! Form::label ('colonia', 'Colonia:') !!}
                {!! Form::text('colonia',  $sindicato->colonia, array('class' => 'form-control')) !!}
                {!! Form::label ('codigo_postal', 'Codigo Postal:') !!}
                {!! Form::text('codigo_postal',  $sindicato->codigo_postal, array('class' => 'form-control')) !!}
                {!! Form::label ('ciudad', 'Ciudad:') !!}
                {!! Form::text('ciudad',  $sindicato->ciudad, array('class' => 'form-control')) !!}
                {!! Form::label ('telefono', 'Telefono:') !!}
                {!! Form::text('telefono',  $sindicato->telefono, array('class' => 'form-control')) !!}
                {!! Form::close() !!}
    		</div>
    	</div>
    </div>
@endsection

@section('modalFooter')
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="button" class="btn btn-primary" id="btn-modificar-sindicato">Guardar</button>
        </div>
    </div>
@endsection

@section('modalScripts')
    @parent
    {!! Html::script('assets/js/catalogos/sindicatos/modify.js') !!}
@endsection

