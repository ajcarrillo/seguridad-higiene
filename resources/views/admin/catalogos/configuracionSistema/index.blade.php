@extends('base')

@section('content')

   <div class="container top">
        <div class="row">
           <div class="col-md-12">
               <div class="panel panel-default">
                   <div class="panel-heading">Configuración del sistema</div>
                   <div class="panel-body">
                        {!! Form::open(['route'=>['post.update.configuracion'], 'id'=>'form-configuracion-sistema']) !!}
                        @foreach($configuraciones as $configuracion)
                        <div class="form-group">
                            <?php $dato='dato'.$configuracion->id; ?>
                            {!! Form::label ($dato, $configuracion->descripcion) !!}
                            {!! Form::text($dato,  $configuracion->valor, array('class' => 'form-control')) !!}
                        </div>
                        @endforeach
                       <div class="form-group">
                           <div class="col-md-6 col-md-offset-6">
                               <button type="submit" class="btn btn-primary">
                                   Grabar
                               </button>
                           </div>
                       </div>
                        {!!  Form::close() !!}
                    </div>
               </div>
           </div>
        </div>
   </div>
@endsection