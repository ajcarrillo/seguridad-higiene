<?php
/**
 * Created by PhpStorm.
 * User: lewis
 * Date: 15/02/2016
 * Time: 03:08 PM
 */
?>
@extends('base')
@section('css')
    {!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Incidencias</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group gray-content-block">
                <a href="{{ route('incidencia.create') }}" class="btn btn-success">Crear incidencia</a>
            </div>
            <table id="users-table" class="table table-striped table-hover" data-url="{{ route('incidencia.index.data') }}">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Clave</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
</div>
@endsection
@section('scripts')
    {!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('assets/js/spanish.datatables.js') !!}
    {!! Html::script('assets/js/incidencia/index.js') !!}
@endsection