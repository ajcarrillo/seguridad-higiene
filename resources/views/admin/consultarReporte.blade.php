@extends('base')

@section('content')
	{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
	{!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
	{!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}

	<div class="container top">
		{!! Form::open(['route'=>['admin.grabaComision','reporte'=>$reporte->id], 'data-id'=>'formRevision']) !!}
			@include('admin.partials.reporte')
			@if ($a_tipoReporte['registro'] or $a_tipoReporte['actualizacion'])
				@include('admin.partials.comision')
			@endif
			@if ($a_tipoReporte['actualizacion'])
				@include('admin.partials.actualizacion')
			@endif
			@if ($a_tipoReporte['calendario'])
				@include('admin.partials.calendario')
			@endif
			@if ($a_tipoReporte['verificacion'])
				@include('admin.partials.verificacion')
			@endif
			@if ($reporte->observaciones->count() > 0)
				@include('admin.partials.observaciones')
			@endif


			<div class="row">
				<div class="col-md-4 col-md-offset-8">
					@if ($conBoton)
						{!! Form::button('Aceptar', array('class'=>"btn btn-primary", 'id'=>"btnAceptar")) !!}
						{!! Form::button('Observación', array('class'=>"btn btn-warning", 'id'=>"btnRechazar")) !!}
					@endif
					{!! link_to(URL::previous(), 'Regresar', ['class' => 'btn btn-default']) !!}
				</div>
			</div>
		{!! Form::close() !!}
	</div>
@endsection

@section('scripts')

	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/Modal.js') !!}
	{!! Html::script('assets/js/routes/routes.js') !!}
	{!! Html::script('assets/js/admin/admin.js') !!}

@endsection



