<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    8. ACTA DE VERIFICACIÓN
</div>
<div class="section row">
    <table width="100%">
        <tr>
            <td width = '35%' style="text-align: center; font-size: 7pt;">
                FECHA DE VERIFICACIÓN
            </td>
            <td width = '30%' style="text-align: center; font-size: 7pt;">
                TIPO DE VERIFICACIÓN
            </td>
            <td width = '35%' style="text-align: center; font-size: 7pt;">
                TRIMESTRE QUE SE REPORTA
            </td>
        </tr>
        <tr>
            <td width = '35%' >
                <table width = "100%">
                    <tr>
                        <td width = "90%"  style="text-align: center; font-size: 7pt; border: solid 1px black;">{{ isset($reporte->verificacion->fecha) ? $reporte->verificacion->fecha : null }}</td>
                        <td width = "10%"  style="text-align: center; font-size: 7pt; ">(31)</td>
                    </tr>
                </table>

            </td>
            <td width = '30%' style="text-align: center; font-size: 7pt;">
                <table width = "100%">
                    <tr>
                        <td width = "30%"  style="text-align: center; font-size: 7pt;"> ORDINARIA</td>
                        <td width = "10%"  class="rectangulo">@if ($reporte->verificacion->tipo == 1) X @else &nbsp;&nbsp; @endif </td>
                        <td width = "10%"  style="text-align: center; font-size: 7pt;"> (32)</td>
                        <td width = "30%"  style="text-align: center; font-size: 7pt;"> EXTRAORDINARIA</td>
                        <td width = "10%"  class="rectangulo">@if ($reporte->verificacion->tipo == 2) X @else &nbsp;&nbsp; @endif </td>
                        <td width = "30%"  style="text-align: center; font-size: 7pt;"> (33)</td>
                    </tr>
                </table>
            </td>
            <td width = '35%' style="text-align: center; font-size: 7pt;">
                <table width = "100%">
                    <tr>
                        <td width = "10%"  style="text-align: center; font-size: 7pt;">1 </td>
                        <td width = "10%"  class="rectangulo">@if ($reporte->trimestre == 1) X @else &nbsp;&nbsp;@endif</td>
                        <td width = "10%"  style="text-align: center; font-size: 7pt;">2 </td>
                        <td width = "10%"  class="rectangulo">@if ($reporte->trimestre == 2) X @else &nbsp;&nbsp;@endif</td>
                        <td width = "10%"  style="text-align: center; font-size: 7pt;">3</td>
                        <td width = "10%"  class="rectangulo">@if ($reporte->trimestre == 3) X @else &nbsp;&nbsp;@endif</td>
                        <td width = "10%"  style="text-align: center; font-size: 7pt;">4</td>
                        <td width = "10%"  class="rectangulo">@if ($reporte->trimestre == 4) X @else &nbsp;&nbsp;@endif</td>
                        <td width = "20%"  style="text-align: center; font-size: 7pt;"> (34)</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<table width="100%" style="table-layout:fixed">
    <tr>
        <td width="50%">
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="100%" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
                    INCIDENCIAS DETECTADAS (POR PRIMERA VEZ) (35)
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table width="100%">
                        <thead>
                        <tr>
                            <th width="10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">No.</th>
                            <th width="50%" style="text-align: center; font-size: 7pt; border: 1px solid black;">INCIDENCIA</th>
                            <th width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">TRIMESTRE</th>
                            <th width="15%" style="text-align: center; font-size: 7pt; border: 1px solid black;">AÑO</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $contador = 1; ?>
                        @if ($reporte->verificacion->incidencias)
                        @foreach($reporte->verificacion->incidencias as $incidencia)
                            @if ($incidencia == statusIncidencia('Primera vez'))
                                <tr>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">
                                        [{{$incidencia->incidencia->clave }}] {{$incidencia->incidencia->nombre}}
                                    </td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$reporte->trimestre}}</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$reporte->anio}}</td>
                                </tr>
                            <?php $contador += 1; ?>
                            @endif
                        @endforeach
                        @endif
                        @while ($contador < 9)
                            <tr>
                                <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                                <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                            </tr>
                            <?php  $contador += 1;  ?>
                        @endwhile
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        </td>
        <td width="50%">
            <table width="100%">
                <tr>
                    <td width="100%" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">OBSERVACIONES (SÓLO INCIDENCIA 99)
                    </td>
                </tr>
                <tr>
                    <td width="350px" height="80" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->verificacion->observacion) ? $reporte->verificacion->observacion : null}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

    <table width="100%" style="table-layout:fixed">
    <tr>
        <td width="50%">
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td width = "100%" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
                        INCIDENCIAS SUBSISTENTES (PERSISTEN DE VERIFICACIONES ANTERIORES) (36)
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table width="100%">
                            <thead>
                            <tr>
                                <th width="40px" style="text-align: center; font-size: 7pt; border: 1px solid black;">No.</th>
                                <th width="170px" style="text-align: center; font-size: 7pt; border: 1px solid black;">INCIDENCIA</th>
                                <th width="85px" style="text-align: center; font-size: 7pt; border: 1px solid black;">TRIMESTRE</th>
                                <th width="55px" style="text-align: center; font-size: 7pt; border: 1px solid black;">AÑO</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $contador = 1; ?>
                            @if ($reporte->verificacion->incidencias)
                            @foreach($reporte->verificacion->incidencias as $incidencia)
                                @if ($incidencia->status == statusIncidencia('Subsistentes'))
                                    <tr>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">
                                            [{{$incidencia->incidencia->clave }}] {{$incidencia->incidencia->nombre}}
                                        </td>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$reporte->trimestre}}</td>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$reporte->anio}}</td>
                                    </tr>
                                    <?php $contador += 1; ?>
                                @endif
                            @endforeach
                            @endif
                            @while ($contador < 9)
                                <tr>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                </tr>
                                <?php  $contador += 1;  ?>
                            @endwhile
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
                        INCIDENCIAS SUBSANADAS (EN EL TRIMESTRE QUE SE REPORTA HA SIDO CORREGIDA LA INCEDENCIA EN SU TOTALIDAD EN EL CENTRO DE TRABAJO) (37)
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <table width="100%">
                            <thead>
                            <tr>
                                <th width="40px" style="text-align: center; font-size: 7pt; border: 1px solid black;">No.</th>
                                <th width="170px" style="text-align: center; font-size: 7pt; border: 1px solid black;">INCIDENCIA</th>
                                <th width="85px" style="text-align: center; font-size: 7pt; border: 1px solid black;">TRIMESTRE</th>
                                <th width="55px" style="text-align: center; font-size: 7pt; border: 1px solid black;">AÑO</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $contador = 1; ?>
                            @if ($reporte->verificacion->incidencias)
                            @foreach($reporte->verificacion->incidencias as $incidencia)
                                @if ($incidencia == statusIncidencia('Subsanadas'))
                                    <tr>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">
                                            [{{$incidencia->incidencia->clave }}] {{$incidencia->incidencia->nombre}}
                                        </td>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$reporte->trimestre}}</td>
                                        <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$reporte->anio}}</td>
                                    </tr>
                                    <?php $contador += 1; ?>
                                @endif
                            @endforeach
                            @endif
                            @while ($contador < 9)
                                <tr>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                    <td style="text-align: center; font-size: 7pt; border: 1px solid black;">&nbsp;&nbsp;</td>
                                </tr>
                                <?php  $contador += 1;  ?>
                            @endwhile
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td  width = "100%" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">PROPUESTAS REALIZADAS POR LA C.S.S.T., PARA LA CORRECCIÓN DE LOS FACTORES DE RIESGO EN EL CENTRO DE TRABAJO
        </td>
    </tr>
    <tr>
        <td  width = "100%" height="50" style="text-align: center; font-size: 7pt; border: 1px solid black;"></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width = '35%' style="text-align: center; font-size: 5pt;">
            (38)
        </td>
        <td width = '30%' style="text-align: center; font-size: 5pt;">
            (39)
        </td>
        <td width = '35%' style="text-align: center; font-size: 5pt;">
            (39)
        </td>
    </tr>
    <tr>
        <td width = '35%' style="text-align: center; font-size: 7pt;">
            <table width = "100%">
                <tr>
                    <td width = "50%" style="text-align: center; font-size: 7pt; ">No. DE RIESGOS DE TRABAJO:</td>
                    <td width = "50%" style="text-align: center; font-size: 7pt; border: solid 1px black;">{{ isset($reporte->verificacion->numero_de_riesgos_de_trabajo) ? $reporte->verificacion->numero_de_riesgos_de_trabajo : null }}</td>
                </tr>
            </table>
        </td>
        <td width = '30%' style="text-align: center; font-size: 7pt;">
            <table width = "100%">
                <tr>
                    <td width = "50%" style="text-align: center; font-size: 7pt; ">ACCIDENTE DE TRABAJO:</td>
                    <td width = "50%" style="text-align: center; font-size: 7pt; border: solid 1px black;">{{ isset($reporte->verificacion->accidentes_de_trabajo) ? $reporte->verificacion->accidentes_de_trabajo : null }}</td>
                </tr>
            </table>
        </td>
        <td width = '35%' style="text-align: center; font-size: 7pt;">
            <table width = "100%">
                <tr>
                    <td width = "50%" style="text-align: center; font-size: 7pt; " >ENFERMEDAD PROFESIONAL:</td>
                    <td width = "50%" style="text-align: center; font-size: 7pt; border: solid 1px black;">{{ isset($reporte->verificacion->enfermedad_profesional) ? $reporte->verificacion->enfermedad_profesional : null }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan = "3" style="text-align: center; font-size: 5pt; ">
            (LA DESCRIPCIÓN COMPLETA DE CADA UNO DE LOS RIESGOS, DEBERÁN REPORTARSE EN EL FORMATO ENAT-1)
        </td>
    </tr>
</table>