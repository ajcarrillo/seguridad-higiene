<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    7. CALENDARIO DE ACTIVIDADES
</div>
<div class="col col-sm-12">
    <table width="100%">
        <tr>
            <td width="25%" style="text-align: left; font-size: 7pt;" >PERIODO:  (26)</td>
            <td width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->calendario->de) ? $reporte->calendario->de : null}}</td>
            <td width="25%" style="text-align: center; font-size: 7pt; ">A:</td>
            <td width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->calendario->a) ? $reporte->calendario->a : null}}</td>
        </tr>
    </table>
</div>
<div class="col col-sm-12">
    <table>
        <thead>
        <tr>
            <th width = "5%" style="text-align: center; font-size: 7pt; border: 1px solid black;">No. (27)</th>
            <th width = "45%" style="text-align: center; font-size: 7pt; border: 1px solid black;">DESCRIPCIÓN (28)</th>
            <th width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">UNIDAD DE MEDIDA (29)</th>
            <th width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">ENE-MAR (30)</th>
            <th width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">ABR-JUN (30)</th>
            <th width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">JUL-SEP (30)</th>
            <th width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">OCT-DIC (30)</th>
        </tr>
        </thead>
        <tbody>
        <?php $contador = 1; ?>
        @foreach($reporte->calendario->actividades as $actividad)
            <tr>
                <td width = "5%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$contador}}</td>
                <td width = "45%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$actividad->descripcion}}</td>
                <td width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{$actividad->unidad_de_medida}}</td>
                <td width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">@if ($actividad->trimestre == 1) X @else &nbsp;&nbsp;@endif</td>
                <td width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">@if ($actividad->trimestre == 2) X @else &nbsp;&nbsp;@endif</td>
                <td width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">@if ($actividad->trimestre == 3) X @else &nbsp;&nbsp;@endif</td>
                <td width = "10%" style="text-align: center; font-size: 7pt; border: 1px solid black;">@if ($actividad->trimestre == 4) X @else &nbsp;&nbsp;@endif</td>
            </tr>
            <?php $contador += 1; ?>
         @endforeach
        </tbody>
    </table>
</div>
