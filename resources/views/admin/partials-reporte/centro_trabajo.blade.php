    <div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
        1. DATOS DEL CENTRO DE TRABAJO
    </div>
    <div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(6)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">1.2 UBICACIÓN: CALLE Y No. EXT. E INT.</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" style="font-size: 7pt;">
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td width="80%" style="font-size: 7pt; border:1px solid black;">
                    {{ $centros_trabajo['domicilio'] }}
                </td>
                <td width="10%" style="font-size: 7pt; border:1px solid black;">
                    &nbsp;
                </td>
                <td width="10%" style="font-size: 7pt; border:1px solid black;">
                    &nbsp;
                </td>

            </tr>
        </table>
    </div>
</div>
    <div class="section row">
        <div class="col col-sm-3" >
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td width="15%" style="text-align: left; font-size: 5pt;">(7)</td>
                    <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; "> 1.3 CD. Y ENT.FED.</td>
                </tr>
            </table>
        </div>
        <div class="col col-sm-9" >
            <table width = "100%" style="table-layout:fixed">
                <tr>
                    <td style="font-size: 7pt; border:1px solid black;">
                        {!!  $centros_trabajo['ciudad'] !!}
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="section row">
        <div class="col col-sm-3" >
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td width="15%" style="text-align: left; font-size: 5pt;">(8)</td>
                    <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">1.4 COLONIA</td>
                </tr>
            </table>
        </div>
        <div class="col col-sm-9" >
            <table width = "100%" style="table-layout:fixed">
                <tr>
                    <td style="font-size: 7pt; border:1px solid black;">
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="section row">
        <div class="col col-sm-3" >
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td width="15%" style="text-align: left; font-size: 5pt;">(9)</td>
                    <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">1.5 CÓDIGO POSTAL</td>
                </tr>
            </table>
        </div>
        <div class="col col-sm-9" style="font-size: 7pt;">
            <table width = "100%" style="table-layout:fixed">
                <tr>
                    <?php
                    $contador = 0;
                    while ($contador < strlen($centros_trabajo['codigo_postal'])) {?>
                    <td class = "rectangulo">
                        <?php echo substr($centros_trabajo['codigo_postal'],$contador,1); ?>
                    </td>
                    <?php
                    $contador += 1;
                    }
                    if ($contador < 4){
                    while ($contador < 5) {?>
                    <td class = "rectangulo">
                        &nbsp;&nbsp;
                    </td>
                    <?php
                    $contador += 1;
                    }
                    }
                    $contador = 0;
                    while ($contador < 2) {?>
                    <td >
                        &nbsp;&nbsp;
                    </td>
                    <?php
                    $contador += 1;
                    } ?>
                    <td width="20%" style="font-size: 7pt;">
                        No. TELEFÓNICO
                    </td>
                    <?php
                    $contador = 0;
                    while ($contador < 8) {?>
                    <td class = "rectangulo">
                        &nbsp;&nbsp;
                    </td>
                    <?php
                    $contador += 1;
                    }
                    $contador = 0;
                    while ($contador < 2) {?>
                    <td >
                        &nbsp;&nbsp;
                    </td>
                    <?php
                    $contador += 1;
                    }
                    $contador = 0;
                    while ($contador < 8) {?>
                    <td class = "rectangulo">
                        &nbsp;&nbsp;
                    </td>
                    <?php
                    $contador += 1;
                    }
                    ?>
                </tr>
            </table>
        </div>
    </div>
    <div class="section row">
        <div class="col col-sm-3" >
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td width="15%" style="text-align: left; font-size: 5pt;">(10)</td>
                    <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">1.6 No. DE TRABAJADORES:</td>
                </tr>
            </table>
        </div>
        <div class="col col-sm-5" >
            <table width = "100%" style="table-layout:fixed">
                <tr>
                    <td style="font-size: 7pt; border:1px solid black;">
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div class="col col-sm-4" >
            <table width = "100%" style="table-layout:fixed">
                <tr>
                    <td style="font-size: 5pt;">
                        (SÓLO PARA SER REQUISITADO POR LAS COMISIONES AUXILIARES)
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="section row">
        <div class="col col-sm-3" >
            <table width="100%" style="table-layout:fixed">
                <tr>
                    <td width="15%" style="text-align: left; font-size: 5pt;">(11)</td>
                    <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">1.7 TURNOS DE TRABAJO:</td>
                </tr>
            </table>
        </div>
        <div class="col col-sm-9" >
            <table width = "100%" style="table-layout:fixed">
                <tr>
                    <td width = "25%" style="font-size: 7pt;"> DIURNO </td>
                    <td width = "5%" style="border: 1px solid black;"></td>
                    <td width = "25%" style="font-size: 7pt;"> NOCTURNO </td>
                    <td width = "5%" style="border: 1px solid black;"></td>
                    <td width = "25%" style="font-size: 7pt;"> MIXTO </td>
                    <td width = "5%" style="border: 1px solid black;"></td>
                    <td width = "10%" ></td>
                </tr>
            </table>
        </div>
    </div>

