<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    6. DOCUMENTACIÓN ANEXA
</div>
<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    6.1 OFICIO DE DESIGNACIÓN DE REPRESENTACIÓN OFICIAL   (24)
</div>


<div class="section row">
    <table width = "100%">
        <tr>
            <td width="25%" style="text-align: left; font-size: 7pt;">NÚMERO DE OFICIO:</td>
            <td width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->comision->num_oficio_oficial) ? $reporte->comision->num_oficio_oficial : null}}</td>
            <td width="25%" style="text-align: right; font-size: 7pt;">FECHA:</td>
            <td width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->comision->fecha_oficio_oficial) ? $reporte->comision->fecha_oficio_oficial : null}}</td>
        </tr>
    </table>
</div>
<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
6.2 OFICIO DE DESIGNACIÓN DE REPRESENTACIÓN SINDICAL   (25)
</div>
<div class="section row">
    <table width = "100%">
        <tr>
            <td width="25%" style="text-align: left; font-size: 7pt;" >NÚMERO DE OFICIO:</td>
            <td width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->comision->num_oficio_sindical) ? $reporte->comision->num_oficio_sindical : null}}</td>
            <td width="25%" style="text-align: right; font-size: 7pt; ">FECHA:</td>
            <td width="25%" style="text-align: center; font-size: 7pt; border: 1px solid black;">{{isset($reporte->comision->fecha_oficio_sindical) ? $reporte->comision->fecha_oficio_sindical : null}}</td>
        </tr>
    </table>
</div>
