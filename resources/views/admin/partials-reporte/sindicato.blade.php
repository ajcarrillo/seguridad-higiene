<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    2. ORGANIZACIÓN SINDICAL
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(12)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">2.1 NOMBRE DEL SINDICATO O SECCIÓN SINDICAL QUE CORRESPONDE AL CENTRO DE TRABAJO
                </td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;">
                    {{isset($reporte->comision->cat_sindicato->nombre) ? $reporte->comision->cat_sindicato->nombre : null }} <br> SECCIÓN 25
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(13)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">2.2 UBICACIÓN: CALLE Y No. EXT. E INT.</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" style="font-size: 7pt;">
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td width="80%" style="font-size: 7pt; border:1px solid black;">
                    {{ isset($reporte->comision->cat_sindicato->direccion) ? $reporte->comision->cat_sindicato->direccion : null }}
                </td>
                <td width="10%" style="font-size: 7pt; border:1px solid black;">
                    &nbsp;
                </td>
                <td width="10%" style="font-size: 7pt; border:1px solid black;">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(14)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">2.3 CD. Y ENT.FED.</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;">
                    {{ isset($reporte->comision->cat_sindicato->ciudad) ? $reporte->comision->cat_sindicato->ciudad : null }}
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(15)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">2.4 COLONIA</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;">
                    {{ isset($reporte->comision->cat_sindicato->colonia) ? $reporte->comision->cat_sindicato->colonia : null }}
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(16)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">2.5 CÓDIGO POSTAL</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" style="font-size: 7pt;">
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <?php
                $contador = 0;
                    if (isset($reporte->comision->cat_sindicato->codigo_postal))
                        while ($contador < strlen($reporte->comision->cat_sindicato->codigo_postal)) {?>
                            <td class="rectangulo">
                                <?php echo substr($reporte->comision->cat_sindicato->codigo_postal,$contador,1); ?>
                            </td>
                            <?php
                            $contador += 1;
                        }
                if ($contador < 4){
                while ($contador < 5) {?>
                <td class="rectangulo">
                    &nbsp;&nbsp;
                </td>
                <?php
                $contador += 1;
                }
                }
                $contador = 0;
                while ($contador < 2) {?>
                <td >
                    &nbsp;&nbsp;
                </td>
                <?php
                $contador += 1;
                } ?>
                <td style="font-size: 7pt;">&nbsp;&nbsp;No.TELEFÓNICO: </td>
                <?php
                $contador = 0;
                        if (isset($reporte->comision->cat_sindicato->telefono))
                            while ($contador < strlen($reporte->comision->cat_sindicato->telefono)) {?>
                            <td class="rectangulo">
                                <?php echo substr($reporte->comision->cat_sindicato->telefono,$contador,1); ?>
                            </td>
                            <?php
                            $contador += 1;
                            }
                while ($contador < 8) {?>
                <td class="rectangulo">
                    &nbsp;&nbsp;
                </td>
                <?php
                $contador += 1;
                }
                $contador = 0;
                while ($contador < 2) {?>
                <td >
                    &nbsp;&nbsp;
                </td>
                <?php
                $contador += 1;
                }
                $contador = 0;
                while ($contador < 8) {?>
                <td class="rectangulo">
                    &nbsp;&nbsp;
                </td>
                <?php
                $contador += 1;
                }
                ?>
            </tr>
        </table>
    </div>
</div>

