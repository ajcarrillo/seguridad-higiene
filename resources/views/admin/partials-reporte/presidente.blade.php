<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    3. PRESIDENTE DE LA COMISIÓN DE SEGURIDAD Y SALUD EN EL TRABAJO
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(17)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">3.1 NOMBRE
                </td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;" height="15px">
                    {{isset($reporte->comision->presidente->nombre_completo) ? $reporte->comision->presidente->nombre_completo : null }}
                </td>
            </tr>
        </table>
        <span style="text-align: left; font-size: 5pt;">APELLIDO PATERNO     APELLIDO MATERNO     NOMBRE(S)</span>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(18)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">3.2 CARGO</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;" height="15px">
                    {{isset($reporte->comision->presidente->cargo) ? $reporte->comision->presidente->cargo : null }}
                </td>
            </tr>
        </table>
    </div>
</div>
