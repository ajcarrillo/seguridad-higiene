<div class="col-sm-12" style="text-align: right; font-size: 9pt; font-weight:bold;">
    FORMATO ÚNICO CSST
</div>
<div class="section row">
    <div class = "col col-sm-2">
        <img src="assets/img/logo_ISSSTE.jpg" height="30mm" width="30mm"/>
    </div>
    <div class = "col col-sm-10" style="text-align: center; font-size: 12pt; font-weight:bold;">
        <br>
        <br>
        REGISTRO Y FUNCIONAMIENTO DE COMISIONES DE SEGURIDAD Y SALUD EN EL TRABAJO
    </div>
</div>
<div class="section row">
    <div class = "col col-sm-4" style="text-align: right; font-size: 7pt; font-weight:bold;">
        COMISIÓN:
    </div>
    <div class = "col col-sm-8">
        <table width="100%" style="table-layout:fixed" >
            <tr>
                <td width="20%" style="font-size: 7pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td width="5%" style="font-size: 7pt;">&nbsp;&nbsp;</td>
                <td width="20%" style="font-size: 7pt;">CENTRAL</td>
                <td width="5%" class="rectangulo">&nbsp;&nbsp;&nbsp;</td>
                <td width="20%" style="font-size: 7pt;">ESTATAL</td>
                <td width="5%" class="rectangulo">&nbsp;&nbsp;&nbsp;</td>
                <td width="20%" style="font-size: 7pt;">AUXILIAR</td>
                <td width="5%" class="rectangulo">X</td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-4" style="text-align: right; font-size: 7pt; font-weight:bold;">
        ASPECTO(S) QUE SE REPORTA(N):
    </div>
    <div class="col col-sm-8">
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="20%" style="font-size: 7pt;">REGISTRO</td>
                <td width="5%"  class="rectangulo">
                    @if ($a_tipoReporte['registro'])
                        X
                    @else
                        &nbsp;&nbsp;&nbsp;
                    @endif
                </td>
                <td width="20%" style="font-size: 7pt;">ACTUALIZACIÓN</td>
                <td width="5%" class="rectangulo">
                    @if ($a_tipoReporte['actualizacion'])
                        X
                    @else
                        &nbsp;&nbsp;&nbsp;
                    @endif
                </td>
                <td width="20%" style="font-size: 7pt;">CALENDARIO</td>
                <td width="5%" class="rectangulo">
                    @if ($a_tipoReporte['calendario'])
                        X
                    @else
                        &nbsp;&nbsp;&nbsp;
                    @endif
                </td>
                <td width="20%" style="font-size: 7pt;">VERIFICACIÓN</td>
                <td width="5%" class="rectangulo">
                    @if ($a_tipoReporte['verificacion'])
                        X
                    @else
                        &nbsp;&nbsp;&nbsp;
                    @endif
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-4" style="text-align: right; font-size: 7pt; font-weight:bold;">
        <div class="col col-sm-4" style="text-align: left; font-size: 5pt;">
            (1)
        </div>
        <div class="col col-sm-8" style="font-size: 7pt; ">
            CLAVE DE LA COMISIÓN:
        </div>

    </div>
    <div class="col col-sm-6" style="font-size: 7pt;">
        <table width="50%">
            <tr>
                <?php
                $contador = 0;
                while ($contador < strlen($reporte->comision->clave_comision)) {?>
                <td class = "rectangulo">
                    <?php echo substr($reporte->comision->clave_comision,$contador,1); ?>
                </td>
                <?php
                $contador += 1;
                } ?>
            </tr>
        </table>
         <span style="text-align: left; font-size: 5pt;">(EN CASO DE REGISTRO DE COMISIÓN. LA CLAVE SERÁ PROPORCIONADA POR EL ISSSTE)</span>
    </div>
    <div class="col col-sm-2" style="font-size: 7pt;">
        <table style="table-layout:fixed">
            <tr>
                <td class="rectangulo">&nbsp;&nbsp;&nbsp;</td>
                <td class="rectangulo">&nbsp;&nbsp;&nbsp;</td>
                <td style="text-align: left; font-size: 5pt;">(2)</td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-4" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(3)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">DEPENDENCIA O ENTIDAD:</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-8" >
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;">
                    SERVICIOS EDUCATIVOS DE QUINTANA ROO
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-4" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(4)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold;">CENTRO(S) DE TRABAJO:
                </td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-8" style="font-size: 7pt; ">
        <table width = "100%" style="table-layout:fixed">
            <tr>
                <td style="font-size: 7pt; border:1px solid black;">
                    {{ $centros_trabajo['nombre'] }}
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-4" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(5)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold;">RAMO ADMINISTRATIVO:
                </td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-8" style="font-size: 7pt;">
        <table>
            <tr>
                <?php
                $contador = 2;
                while ($contador < 7) {?>
                <td class = "rectangulo">
                    <?php echo substr($reporte->comision->clave_comision,$contador,1); ?>
                </td>
                <?php
                $contador += 1;
                } ?>
            </tr>
        </table>
    </div>
</div>


