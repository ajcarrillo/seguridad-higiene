<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    4. SECRETARIO TÉCNICO DE LA COMISIÓN DE SEGURIDAD Y SALUD EN EL TRABAJO
</div>
<div class="section row">
<div class="col col-sm-3" >
    <table width="100%" style="table-layout:fixed">
        <tr>
            <td width="15%" style="text-align: left; font-size: 5pt;">(19)</td>
            <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">4.1 NOMBRE
            </td>
        </tr>
    </table>
</div>
<div class="col col-sm-9" >
    <table width = "100%" style="table-layout:fixed">
        <tr>
            <td style="font-size: 7pt; border:1px solid black;">
                {{isset($reporte->comision->secretario->nombre_completo) ? $reporte->comision->secretario->nombre_completo : null }}
            </td>
        </tr>
    </table>
    <span style="text-align: left; font-size: 5pt;">APELLIDO PATERNO     APELLIDO MATERNO     NOMBRE(S)</span>
</div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(20)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">4.2 SECTOR:</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table>
            <tr>
                <td width="45%" style="font-size: 7pt; text-align: right">OFICIAL</td>
                <td class="rectangulo">
                    @if ($reporte->comision->secretario->sector == 1)
                        X
                    @else
                        &nbsp;&nbsp;
                    @endif
                </td>
                <td width="45%" style="font-size: 7pt; text-align: right">SINDICAL</td>
                <td class="rectangulo">
                    @if ($reporte->comision->secretario->sector == 2)
                        X
                    @else
                        &nbsp;&nbsp;
                    @endif
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">(21)</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">4.3 REGISTRO FEDERAL DE CONTRIBUYENTES (RFC):</td>
            </tr>
        </table>
    </div>
    <div class="col col-sm-9" >
        <table>
            <tr>
                <?php
                $contador = 0;
                    if (isset( $reporte->comision->secretario->rfc))
                        while ($contador < strlen($reporte->comision->secretario->rfc)) {?>
                            <td class="rectangulo">
                                <?php echo substr($reporte->comision->secretario->rfc,$contador,1); ?>
                            </td>
                        <?php
                        $contador += 1;
                        } ?>
            </tr>
        </table>
    </div>
</div>
<br>
<div class="section row">
    <div class="col col-sm-3" >
        <table width="100%" style="table-layout:fixed">
            <tr>
                <td width="15%" style="text-align: left; font-size: 5pt;">&nbsp;&nbsp;</td>
                <td width="85%" style="text-align: right; font-size: 7pt; font-weight:bold; ">4.4 ACEPTACIÓN DEL CARGO:</td>
            </tr>
        </table>
    </div>

    <div class="col col-sm-9" >
        <table>
            <tr>
                <td width="80%" style="font-size: 7pt;">
                    __________________________________ <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(FIRMA)
                </td>
            </tr>
        </table>
    </div>
</div>


