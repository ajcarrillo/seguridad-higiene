<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    5. INTEGRACIÓN DE LAS REPRESENTACIONES (VOCALES)
</div>
<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    5.1 REPRESENTACIÓN OFICIAL: (22)
</div>
<div class="col col-sm-12">
<table width = '100%' >
    <thead>
    <tr>
        <th width="5%">&nbsp;&nbsp;</th>
        <th width="42.5%" style="font-size: 7pt;">APELLIDO PATERNO   APELLIDO MATERNO   NOMBRE</th>
        <th width="42.5%" style="font-size: 7pt;">PUESTO O CARO QUE DESEMPEÑA EN LA DEPENDENCIA O ENTIDAD</th>
        <th width="10%" style="font-size: 7pt;">FIRMA</th>
    </tr>
    </thead>
    <tbody></tbody>
    <?php  $contador = 0;?>
    @foreach($reporte->comision->vocales as $vocal)
        @if ($vocal->sector == 1 and $vocal->tipo == 1)
            <tr>
                @if ($contador == 0)
                     <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>P<BR>R<BR>O<BR>P<BR>I<BR>E<BR>T<BR>A<BR>R<BR>I<BR>O<BR>S</td>
                @endif
                <td width='42.5%' style='border: 1px solid black;'>
                    {{$vocal->nombre_completo}}
                </td>;
                <td width='42.5%' style='border: 1px solid black;'>
                    {{$vocal->cargo}}
                </td>;
                <td width="10%" style="border: 1px solid black;"></td>
            </tr>
            <?php $contador += 1; ?>
        @endif
    @endforeach
    @while ($contador < 4)
        <tr>
            @if ($contador == 0)
                <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>P<BR>R<BR>O<BR>P<BR>I<BR>E<BR>T<BR>A<BR>R<BR>I<BR>O<BR>S</td>
            @endif
            <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
            <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
            <td width="10%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
        </tr>
        <?php  $contador += 1;  ?>
    @endwhile
    <?php $contador = 0;?>
    @foreach($reporte->comision->vocales as $vocal)
        @if ($vocal->sector == 1 and $vocal->tipo == 2)
            <tr>
                @if ($contador == 0)
                    <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>S<BR>U<BR>P<BR>L<BR>E<BR>N<BR>T<BR>E<BR>S</td>
                @endif

                <td width='42.5%' style='border: 1px solid black;'>
                    {{$vocal->nombre_completo}}
                </td>;
                <td width='42.5%' style='border: 1px solid black;'>
                    {{$vocal->cargo}}
                </td>;
                <td width="10%" style="border: 1px solid black;"></td>
            </tr>
            <?php $contador += 1; ?>
        @endif
    @endforeach
    @while ($contador < 4)
        <tr>
            @if ($contador == 0)
                <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>S<BR>U<BR>P<BR>L<BR>E<BR>N<BR>T<BR>E<BR>S</td>
            @endif
            <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
            <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
            <td width="10%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
        </tr>
        <?php  $contador += 1; ?>
    @endwhile
  </table>
</div>
<div class="col col-sm-12" style="text-align: center; font-size: 8pt; font-weight:bold; border: 1px solid black;">
    5.2 REPRESENTACIÓN SINDICAL: (23)
</div>
<div class="col col-sm-12">
    <table width = '100%' >
        <thead>
            <tr>
                <th width="5%"></th>
                <th width="42.5%" style="font-size: 7pt;">APELLIDO PATERNO   APELLIDO MATERNO   NOMBRE</th>
                <th width="42.5%" style="font-size: 7pt;">CARGO O REPRESENTACIÓN QUE CUBRE DENTRO DEL SINDICATO</th>
                <th width="10%" style="font-size: 7pt;">FIRMA</th>
            </tr>
        </thead>
        <?php $contador = 0; ?>
        @foreach($reporte->comision->vocales as $vocal)
            @if ($vocal->sector == 2 and $vocal->tipo == 1)
                <tr>
                    @if ($contador == 0)
                        <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>P<BR>R<BR>O<BR>P<BR>I<BR>E<BR>T<BR>A<BR>R<BR>I<BR>O<BR>S</td>
                    @endif

                    <td width='42.5%' style='border: 1px solid black;'>
                        {{$vocal->nombre_completo}}
                    </td>;
                    <td width='42.5%' style='border: 1px solid black;'>
                        {{$vocal->cargo}}
                    </td>;
                    <td width="10%" style="border: 1px solid black;"></td>
                </tr>
                <?php $contador += 1;?>
            @endif
        @endforeach
        @while ($contador < 4)
            <tr>
                @if ($contador == 0)
                    <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>P<BR>R<BR>O<BR>P<BR>I<BR>E<BR>T<BR>A<BR>R<BR>I<BR>O<BR>S</td>
                @endif
                <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
                <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
                <td width="10%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
            </tr>
            <?php $contador += 1;?>
        @endwhile
    <?php $contador = 0; ?>
        @foreach($reporte->comision->vocales as $vocal)
            @if ($vocal->sector == 2 and $vocal->tipo == 2)
                <tr>
                    @if ($contador == 0)
                        <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>S<BR>U<BR>P<BR>L<BR>E<BR>N<BR>T<BR>E<BR>S</td>
                    @endif
                    <td width='42.5%' style='border: 1px solid black;'>
                        {{$vocal->nombre_completo}}
                    </td>;
                    <td width='42.5%' style='border: 1px solid black;'>
                        {{$vocal->cargo}}
                    </td>;
                    <td width="10%" style="border: 1px solid black;"></td>
                </tr>
                <?php $contador += 1; ?>
            @endif
        @endforeach
        @while ($contador < 4)
            <tr>
                @if ($contador == 0)
                    <td rowspan='4' width='5%' style='font-size: 7pt; text-align: center; border: 1px solid black;'>S<BR>U<BR>P<BR>L<BR>E<BR>N<BR>T<BR>E<BR>S</td>
                @endif
                <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
                <td width="42.5%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
                <td width="10%" style="border: 1px solid black;">&nbsp;&nbsp;</td>
            </tr>
            <?php $contador += 1;?>
        @endwhile
    </table>
</div>