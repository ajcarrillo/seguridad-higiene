<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title">Centros</h3>
	</div>
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
			<tr>
				<th>CCT</th>
				<th>Nombre</th>
				<th>Municipio</th>
				<th>Localidad</th>
				<th>Nivel</th>
				<th>Turno</th>
			</tr>
			</thead>
			<tbody>
			@forelse($comision->centros as $centro)
				<tr>
					<td>{{ $centro->escuela_lite->cct }}</td>
					<td>{{ $centro->escuela_lite->nombre }}</td>
					<td>{{ $centro->escuela_lite->abrev_municipio }}</td>
					<td>{{ $centro->escuela_lite->d_localidad }}</td>
					<td>{{ $centro->escuela_lite->nivel }}</td>
					<td>{{ $centro->escuela_lite->turno->d_turno }}</td>
				</tr>
			@empty

			@endforelse
			</tbody>
		</table>

	</div>
</div>
