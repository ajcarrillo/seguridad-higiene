@extends('comisiones.apartados')

@section('sectionTitle', 'Centros de trabajo')

@section('sections')
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route('comision.centros.create', ['comisiones'=>$comision->id, 'returnURL'=>Request::url()]) }}" class="btn btn-default pull-right">Agregar centros</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<table id="table-centros-de-trabajo" class="table table-hover" data-url="{!! route('comision.centros.data', $comision->id) !!}">
				<thead>
				<tr>
					<th>id</th>
					<th>cctid</th>
					<th>CCT</th>
					<th>Nombre</th>
					<th>Municipio</th>
					<th>Localidad</th>
					<th>Nivel</th>
					<th>Turno</th>
					<th>Opciones</th>
				</tr>
				</thead>
				{{--<tbody>
				@forelse($comision->centros as $centro)
					<tr>
						<td>{{ $centro->escuela_lite->cct }}</td>
						<td>{{ $centro->escuela_lite->nombre }}</td>
						<td>{{ $centro->escuela_lite->abrev_municipio }}</td>
						<td>{{ $centro->escuela_lite->d_localidad }}</td>
						<td>{{ $centro->escuela_lite->nivel }}</td>
						<td>{{ $centro->escuela_lite->turno->d_turno }}</td>
						<td width="10%">
							<button type="button"
									class="btn btn-danger btn-block"
									data-loading-text="Eliminando..."
									data-id="{{ $centro->id }}"
									data-button="delete-centro">Quitar</button>
						</td>
					</tr>
				@empty
					@include('layout.partials.emptytable', ['colspan' => 7])
				@endforelse
				</tbody>--}}
			</table>
		</div>
	</div>
	{!! Form::open(['route' => ['comision.centros.destroy', $comision->id, ':id:' ], 'id'=>'form-delete-centro-comision', 'method' => 'delete']) !!}
	{!! Form::close() !!}
@stop



@section('scripts')
	@parent
	<script id="template-add-centro" type="text/html">
		<% _.each(json, function(centro) { %>
		<tr>
			<td>
				<button type="button"
						class="btn btn-link"
						data-loading-text="Agregando..."
						data-button="add-centro"
						data-id="<%= centro.id %>"
						data-cct="<%= centro.cct %>"
						data-nombre="<%= centro.nombre %>"
						data-abrev_municipio="<%= centro.abrev_municipio %>"
						data-d_localidad="<%= centro.d_localidad %>"
						data-nivel="<%= centro.nivel %>"
						data-d_turno="<%= centro.turno.d_turno %>">
					<%= centro.cct %>
				</button>
			</td>
			<td><%= centro.nombre %></td>
			<td><%= centro.abrev_municipio %></td>
			<td><%= centro.d_localidad %></td>
			<td><%= centro.turno.d_turno %></td>
			<td><%= centro.nivel %></td>
		</tr>
		<% }); %>
	</script>

	<script type="text/html" id="row-centro-template">
		<% _.each(json, function(centro) { %>
		<tr>
			<td><%= centro.escuela_lite.cct %></td>
			<td><%= centro.escuela_lite.nombre %></td>
			<td><%= centro.escuela_lite.abrev_municipio %></td>
			<td><%= centro.escuela_lite.d_localidad %></td>
			<td><%= centro.escuela_lite.nivel %></td>
			<td><%= centro.escuela_lite.turno.d_turno %></td>
			<td width="10%">
				<button type="button"
						class="btn btn-danger btn-block"
						data-loading-text="Eliminando..."
						data-id="<%= centro.id %>"
						data-button="delete-centro">Quitar</button>
			</td>
		</tr>
		<% }); %>


	</script>
	{!! Html::script('assets/js/comisiones/centros/index.js') !!}
	{{--<script type="text/javascript">
		var comision = JSON.parse('{!! $comision !!}');
		console.log(comision);
	</script>--}}
@stop
