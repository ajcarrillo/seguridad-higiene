@extends('escuela_lite.index')

@section('centrosHeader')
	{!! Form::open(['route' => ['comision.centros.store', $comision->id], 'id'=>'add-centros-form']) !!}
	{!! Form::close() !!}
	<div class="row">
		<div class="col-md-12">
			<a href="{{ $returnURL }}" style="font-size: 18px">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Regresar
			</a>
			<button class="btn btn-success pull-right" data-button="add-centros">Agregar Centros</button>
		</div>
	</div>
@stop

@section('centrosFooter')
	<div class="container">
		<br>
		<div class="row">
			<div class="col-md-12">
				<a href="#!" style="font-size: 18px">
					<span class="glyphicon glyphicon-chevron-left"></span>
					Regresar
				</a>
				<button class="btn btn-success pull-right" data-button="add-centros">Agregar Centros</button>
			</div>
		</div>
		<br>
	</div>
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/comisiones/centros/create.js') !!}
@stop
