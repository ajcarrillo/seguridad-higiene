<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title">Secretario</h3>
	</div>
	<div class="panel-body">
		<h4>Nombre: {{ $comision->secretario->nombreCompleto }}</h4>
		<h4>Cargo: {{ catSectoresById($comision->secretario->sector) }}</h4>
		<h4>RFC: {{ $comision->secretario->rfc }}</h4>
	</div>
</div>
