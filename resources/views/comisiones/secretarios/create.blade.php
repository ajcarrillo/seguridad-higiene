@extends('comisiones.apartados')

@section('sectionTitle', 'Presidente')

@section('sections')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Datos del secretario</h3>
				</div>
				<div class="panel-body">

					{!! Form::open(['route'=>['secretario.store', $comision],'id'=>'create-new-secretario-form']) !!}
					@include('comisiones.secretarios.partials.form')
					<div class="form-group">
						<button class="btn btn-success pull-right">Guardar</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>

		</div>
	</div>
@stop
