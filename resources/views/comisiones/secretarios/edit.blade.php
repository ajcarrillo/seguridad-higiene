@extends('comisiones.apartados')

@section('sectionTitle', 'Presidente')

@section('sections')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Datos del secretario</h3>
				</div>
				<div class="panel-body" id="secretario-data">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">Nombre: </label>
							<div class="col-sm-10">
								<p class="form-control-static">{{ $secretario->nombre_completo }}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">RFC: </label>
							<div class="col-sm-10">
								<p class="form-control-static">{{ $secretario->rfc }}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Sector: </label>
							<div class="col-sm-10">
								<p class="form-control-static">{{ $secretario->sector }}</p>
							</div>
						</div>
					</div>
					<button class="btn btn-link pull-right" data-button="show-secretario-form">Editar</button>
				</div>
				<div class="panel-body hidden" id="secretario-form">
					{!! Form::model($secretario, ['route'=>['secretario.update', 'comision'=>$comision, 'secretario'=>$secretario->id], 'method'=>'patch', 'id'=>'create-new-secretario-form']) !!}
					@include('comisiones.secretarios.partials.form')
					<div class="form-group">
						<button class="btn btn-success pull-right">Guardar</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>

		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/js/comisiones/secretarios/edit.js') !!}
@stop
