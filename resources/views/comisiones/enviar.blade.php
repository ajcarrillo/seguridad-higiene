@extends('comisiones.apartados')

@section('sectionTitle', 'Enviar registro')

@section('sections')

	{!! Form::open(['route' => ['comision.sent', $comision->id], 'id'=>'sent-comision-report']) !!}
	{!! Form::close() !!}

	<div class="row">
		<div class="col-md-12">
			@if (Session::has('alert'))
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('alert') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@include('comisiones.presidentes.details')
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@include('comisiones.secretarios.details')
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@include('comisiones.vocales.details')
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@include('comisiones.centros.details')
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@if($comision->isComplete())
				<button type="submit" form="sent-comision-report" class="btn btn-success pull-right">Enviar</button>
			@endif
		</div>
	</div>

	<br><br>

@stop
