@extends('base')

@section('css')
	{!! Html::style('/assets/bower_components/datetimepicker/jquery.datetimepicker.css') !!}
@endsection

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Solicitar registro de comisión</h2>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@include('layout.partials.errors')
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::open(['route' => ['comision.store'],'class' => 'form-horizontal', 'id'=>'form-comision']) !!}
				<input type="hidden" name="clave_comision" value="0">
				<div class="form-group">
					<label for="sindicato" class="col-sm-4 control-label" style="text-align: left!important">Organización Sindical:</label>
					<div class="col-sm-8">
						{!! Form::select('sindicato_id', $sindicatos, null, ['class'=>'form-control', 'placeholder' => 'Seleccione...', 'required']) !!}
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('num_oficio_oficial', 'Oficio Designación Oficial', ['class'=>'control-label col-md-4', 'style' => 'text-align: left!important']) !!}
					<div class="col-sm-4">
						{!! Form::text('num_oficio_oficial', null, ['class'=>'form-control', 'placeholder' => 'Número de oficio', 'required']) !!}
					</div>
					<div class="col-sm-4">
						{{--{!! Form::text('fecha_oficio_oficial', date('d/m/Y'), ['id'=>'fecha_oficio_oficial','class'=>'form-control text-right', 'placeholder' => 'Fecha de oficio', 'required', 'readonly']) !!}--}}
						{!! Form::date('fecha_oficio_oficial', \Carbon\Carbon::now(), ['id'=>'fecha_oficio_oficial','class'=>'form-control text-right', 'placeholder' => 'Fecha de oficio', 'required']) !!}
						{{--<input type="text" id="txt-fecha-oficio-oficial" name="fecha_oficio_oficial" value="{{ date('d/m/Y') }}" class="form-control text-right" placeholder="Fecha oficio" readonly required/>--}}
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('num_oficio_sindical', 'Oficio Designación Sindical', ['class'=>'control-label col-md-4', 'style' => 'text-align: left!important']) !!}
					{{--<label for="sindicato" class="col-sm-4 control-label" style="text-align: left!important">Oficio Designación Sindical:</label>--}}
					<div class="col-sm-4">
						{!! Form::text('num_oficio_sindical', null, ['class'=>'form-control', 'placeholder' => 'Número de oficio', 'required']) !!}
						{{--<input type="text" id="txt-oficio-sindical" name="num_oficio_sindical" class="form-control" placeholder="Número de oficio" required/>--}}
					</div>
					<div class="col-sm-4">
						{{--{!! Form::text('fecha_oficio_sindical', date('d/m/Y'), ['id'=>'fecha_oficio_sindical','class'=>'form-control text-right', 'placeholder' => 'Fecha de oficio', 'required', 'readonly']) !!}--}}
						{!! Form::date('fecha_oficio_sindical', \Carbon\Carbon::now(), ['id'=>'fecha_oficio_sindical','class'=>'form-control text-right', 'placeholder' => 'Fecha de oficio', 'required']) !!}
						{{--<input type="text" id="txt-fecha-oficio-sindical" name="fecha_oficio_sindical" value="{{ date('d/m/Y') }}" class="form-control text-right" placeholder="Fecha oficio" readonly required/>--}}
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-2 col-md-offset-10">
						<button type="submit" class="btn btn-success btn-block" id="btn-save-comision" data-loading-text="Cargando...">Guardar</button>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/comisiones/create.js') !!}
@stop
