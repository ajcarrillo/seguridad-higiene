<div class="form-group">
	<label class="col-md-6 control-label">En plantilla:</label>

	<div class="col-md-6">
		<p class="form-control-static">{{ $total }}</p>
	</div>
</div>
<div class="form-group">
	<label class="col-md-6 control-label">En comisión:</label>

	<div class="col-md-6">
		<p class="form-control-static">{{ $reporte->comision->total_trabajadores }}</p>
	</div>
</div>
<div class="form-group">
	<label class="col-md-6 control-label">Modificación:</label>

	<div class="col-md-6">
		<p class="form-control-static">{{ $reporte->actualizacion->total_trabajadores }}</p>
	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
		<input type="number" min="0" class="form-control" placeholder="Total nuevo" name="total_trabajadores" required>
	</div>
</div>

<input type="hidden" name="comision_id">
