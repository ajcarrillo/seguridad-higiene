<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>@yield('titleSection')</h2>
			<hr>
		</div>
	</div>
</div>
@yield('contentSection')

