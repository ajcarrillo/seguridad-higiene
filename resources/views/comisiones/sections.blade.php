@extends('base')

@section('content')
	{!! Form::open(['route' => ['comision.sent', $comision->id], 'id'=>'sent-comision-report']) !!}
	{!! Form::close() !!}
	<div class="container top">
		<div class="row">
			<div class="col-md-12">
				<div class="btn-group pull-right" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Presidente
							<span class="caret"></span>
						</button>
						{{--<input id="hf-url-for-create-new-presidente" type="hidden" data-url="{{ route('presidente.create', $comision->id) }}">--}}
						<ul class="dropdown-menu">
							@if($comision->presidente)
								<li><a data-route="{{ route('presidente.edit',["comision"=>$comision->id, "presidente"=>$comision->presidente->id]) }}"
									   data-button="edit-presidente">Editar</a></li>
							@else
								<li><a data-route="{{ route('presidente.create', $comision->id) }}"
									   data-button="create-new-presidente">Agregar</a></li>
								{{--<li><a href="#!" data-button="create-new-presidente-from-plantilla">Buscar en plantilla</a></li>--}}
							@endif
						</ul>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Secretario
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							@if($comision->secretario)
								<li><a data-route="{{ route('secretario.edit',["comision"=>$comision->id, "secretario"=>$comision->secretario->id]) }}"
									   data-button="edit-secretario">Editar</a></li>
							@else
								<li><a data-route="{{ route('secretario.create', $comision->id) }}"
									   data-button="create-new-secretario">Agregar nuevo</a></li>
								{{--<li><a href="#!" data-button="create-new-secretario-from-plantilla">Buscar en plantilla</a></li>--}}
							@endif
						</ul>
					</div>
					@if($comision->isComplete())
						<button type="submit" form="sent-comision-report" class="btn btn-success">Enviar</button>
					@endif

				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="hf-comision-id" value="{{ $comision->id }}">
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
			</div>
		</div>
	</div>
	@if($comision->presidente)
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Presidente</h2>
					<hr>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 form-horizontal">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Nombre</label>
						<div class="col-sm-10">
							<p class="form-control-static text-capitalize">{{ $comision->presidente->nombreCompleto }}</p>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label text">Cargo</label>
						<div class="col-sm-10">
							<p class="form-control-static text-capitalize">{{ $comision->presidente->cargo }}</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	@endif

	@if($comision->secretario)
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Secretario</h2>
					<hr>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 form-horizontal">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Nombre</label>
						<div class="col-sm-10">
							<p class="form-control-static text-capitalize">{{ $comision->secretario->nombreCompleto }}</p>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Sector</label>
						<div class="col-sm-10">
							<p class="form-control-static text-capitalize">{{ catSectoresById($comision->secretario->sector) }}</p>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">RFC</label>
						<div class="col-sm-10">
							<p class="form-control-static text-capitalize">{{ $comision->secretario->rfc }}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

	@include('comisiones.centros.index')

	@include('comisiones.vocales.index')

@stop

@section('scripts')
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/routes/routes.js') !!}
	{!! Html::script('assets/js/Pagination.js') !!}
	{!! Html::script('assets/js/Modal.js') !!}
	{!! Html::script('assets/js/comisiones/sections.js') !!}
@stop
