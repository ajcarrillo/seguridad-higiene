<div class="panel panel-success">
	  <div class="panel-heading">
			<h3 class="panel-title">Presidente</h3>
	  </div>
	  <div class="panel-body">
		  <h4>Nombre: {{ $comision->presidente->nombreCompleto }}</h4>
		  <h4>Cargo: {{ $comision->presidente->cargo }}</h4>
	  </div>
</div>
