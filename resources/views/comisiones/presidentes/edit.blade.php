@extends('comisiones.apartados')

@section('sectionTitle', 'Presidente')

@section('sections')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Datos del presidente</h3>
				</div>
				<div class="panel-body" id="presidente-data">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">Nombre: </label>
							<div class="col-sm-10">
								<p class="form-control-static">{{ $presidente->nombre_completo }}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Cargo: </label>
							<div class="col-sm-10">
								<p class="form-control-static">{{ $presidente->cargo }}</p>
							</div>
						</div>
					</div>
					<button class="btn btn-link pull-right" data-button="show-presidente-form">Editar</button>
				</div>
				<div class="panel-body hidden" id="presidente-form">
					{!! Form::model($presidente, ['route'=>['presidente.update', 'comision'=>$comision, 'presidente'=>$presidente->id], 'method'=>'patch', 'id'=>'create-new-presidente-form']) !!}
					@include('comisiones.presidentes.partials.form')
					<div class="from-group">
						<button class="btn btn-success pull-right">Actualizar</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>

		</div>
	</div>
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/comisiones/presidentes/edit.js') !!}
@stop
