<div class="form-group">
	{!! Form::label('nombre', 'Nombre') !!}
	{!! Form::text('nombre', null, ['class'=>'form-control', 'required', 'autofocus']) !!}
</div>

<div class="form-group">
	{!! Form::label('primer_apellido', 'Primer Apellido') !!}
	{!! Form::text('primer_apellido', null, ['class'=>'form-control', 'required']) !!}
</div>

<div class="form-group">
	{!! Form::label('segundo_apellido', 'Segundo Apellido') !!}
	{!! Form::text('segundo_apellido', null, ['class'=>'form-control', 'required']) !!}
</div>

<div class="form-group">
	{!! Form::label('cargo', 'Cargo') !!}
	{!! Form::text('cargo', null, ['class'=>'form-control', 'required']) !!}
</div>
