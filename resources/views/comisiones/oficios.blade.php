@extends('comisiones.apartados')

@section('sectionTitle', 'Oficios')

@section('sections')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				  <div class="panel-heading">
						<h3 class="panel-title">Generales</h3>
				  </div>
				  <div class="panel-body">
					  {!! Form::model($comision, ['route' => ['comision.update', $comision], 'id'=>'comision-form', 'method'=>'PATCH']) !!}

					  <div class="form-group">
						  {!! Form::label('sindicato_id', 'Sindicato:') !!}
						  {!! Form::select('sindicato_id', $sindicatos, null, ['class'=>'form-control', 'required','placeholder'=>'Seleccione...']) !!}
					  </div>

					  <div class="form-group">
						  <div class="row">
							  <div class="col-md-6">
								  {!! Form::label('num_oficio_sindical', 'Oficio sindical:') !!}
								  {!! Form::text('num_oficio_sindical', null, ['class'=>'form-control text-right', 'required']) !!}</div>
							  <div class="col-md-6">
								  {!! Form::label('fecha_oficio_sindical', 'Fecha oficio sindical:') !!}
								  {!! Form::date('fecha_oficio_sindical', null, ['class'=>'form-control text-right', 'required']) !!}
							  </div>
						  </div>
					  </div>

					  <div class="form-group">
						  <div class="row">
							  <div class="col-md-6">
								  {!! Form::label('num_oficio_oficial', 'Oficio oficial:') !!}
								  {!! Form::text('num_oficio_oficial', null, ['class'=>'form-control text-right', 'required']) !!}
							  </div>
							  <div class="col-md-6">
								  {!! Form::label('fecha_oficio_oficial', 'Fecha oficio oficial:') !!}
								  {!! Form::date('fecha_oficio_oficial', null, ['class'=>'form-control text-right', 'required']) !!}
							  </div>
						  </div>
					  </div>

					  <div class="form-group">
						  <div class="pull-right">
							  <button type="submit" class="btn btn-success">Guardar</button>
						  </div>
					  </div>
					  {!! Form::close() !!}
				  </div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	@parent
@stop
