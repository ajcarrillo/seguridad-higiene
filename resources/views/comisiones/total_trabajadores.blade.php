@extends('layout.baseModal')

@section('modalTitle', 'Total de trabajadores')

@section('modalBody')
	{!! Form::open(['route'=>['comision.update.trabajadores', $comision->id],'id'=>'trabajadores-form', 'class'=>'form-horizontal', 'method'=>'PATCH']) !!}
	<div class="form-group">
		<label class="col-md-6 control-label">En plantilla:</label>

		<div class="col-md-6">
			<p class="form-control-static text-right">{{ $total }}</p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-6 control-label">En comisión:</label>

		<div class="col-md-6">
			<p class="form-control-static pull-right" id="label-total">{{ $comision->total_trabajadores }}</p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-6 control-label">Asignar: </label>
		<div class="col-md-6">
			<input type="number" min="0" class="form-control text-right" placeholder="Total" value=""
				   id="total_trabajadores"
				   name="total_trabajadores" required>
		</div>
	</div>

	<input type="hidden" name="comision_id">
	{!! Form::close() !!}
@stop

@section('modalFooter')
	<button type="submit" data-button="update-trabajadores" form="trabajadores-form" class="btn btn-success pull-right" data-loading-text="Guardando...">Guardar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/js/comisiones/total_trabajadores.js') !!}
@stop
