@extends('base')

@section('css')
	{!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@stop

@section('content')
	<div class="container top">
		<div class="row">
			<div class="col-md-12">
				<h2>Comisión - @yield('sectionTitle')</h2>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div id="menu-lateral-container" class="well">
					<ul class="reset menu-lateral">
						<li class="{{ Request::url() == route('comision.edit', $comision) ? "active":"" }}">
							<a href="{{ route('comision.edit', $comision) }}">Oficios</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('comision.centros.index', $comision->id) ? "active":"" }}">
							<a href="{{ route('comision.centros.index', $comision->id) }}">Centros</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == "#!" ? "active":"" }}">
							<a href="#!" data-url="{{ route('comision.edit.trabajadores', $comision->id) }}" data-button="trabajadores">Núm. Trabajadores</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('presidente.index', $comision->id) ? "active":"" }}">
							<a href="{{ route('presidente.index', $comision->id) }}">Presidente</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('secretario.index', $comision->id) ? "active":"" }}">
							<a href="{{ route('secretario.index', $comision->id) }}">Secretario</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('vocales.index', $comision->id) ? "active":"" }}">
							<a href="{{ route('vocales.index', $comision->id)  }}">Vocales</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="">
							<a href="{{ route('comision.enviar', $comision) }}">Enviar</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-10 pull-right">
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@yield('sections')
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	{!! Html::script('assets/bower_components/bootbox.js/bootbox.js') !!}
	{!! Html::script('assets/js/spanish.bootbox.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/comisiones/apartados.js') !!}
@stop

