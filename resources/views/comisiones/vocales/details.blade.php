<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title">Vocales</h3>
	</div>
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
			<tr>
				<th width="30%">Nombre completo</th>
				<th width="20%">Cargo</th>
				<th width="20%">Tipo</th>
				<th width="20%">Sector</th>
			</tr>
			</thead>
			<tbody>
			@forelse($comision->vocales as $vocal)
				<tr>
					<td>{{ $vocal->nombreCompleto }}</td>
					<td>{{ $vocal->cargo }}</td>
					<td>{{ catVocalesById($vocal->tipo) }}</td>
					<td>{{ catSectoresById($vocal->sector) }}</td>
				</tr>
			@empty

			@endforelse
			</tbody>
		</table>

	</div>
</div>
