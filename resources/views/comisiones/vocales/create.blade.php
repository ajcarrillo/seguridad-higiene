@extends('layout/baseModal')

@section('modalTitle', "Agregar Vocal")

@section('modalBody')

	{!! Form::model($vocal, ['route'=>['vocales.store', $comision],'id'=>'new-vocal-form']) !!}
	@include('comisiones.vocales.partials.form')
	{!! Form::close() !!}

@stop
@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	<button type="submit"
			form="new-vocal-form"
			class="btn btn-success"
			data-button="save-vocal"
			data-loading-text="Guardando...">Guardar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/js/comisiones/vocales/create.js') !!}
@stop
