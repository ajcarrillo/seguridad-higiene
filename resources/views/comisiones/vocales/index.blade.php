@extends('comisiones.apartados')

@section('sectionTitle', 'Vocales')

@section('sections')
	<div class="row">
		<div class="col-md-12">

			<div class="dropdown pull-right">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
					Agregar Vocal
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#!" data-url="{{ route('vocales.create', $comision) }}"
											   data-button="add-vocal">Nuevo</a></li>
					<li role="presentation"><a role="menuitem" tabindex="-1"
											   href="{{ route('vocales.create.plantilla', ['comisiones' => $comision, 'returnURL'=>Request::url()]) }}">Buscar
							en plantilla</a></li>
				</ul>
			</div>
		</div>
		<br><br>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Vocales</h3>
				</div>
				<div class="panel-body">
					<div class="content">
						<table class="table" id="vocales-table"
							   data-url="{{ route('vocales.datatables', $comision->id) }}">
							<thead>
							<tr>
								<th width="0%">id</th>
								<th width="30%">Nombre completo</th>
								<th width="20%">Cargo</th>
								<th width="20%">Tipo</th>
								<th width="20%">Sector</th>
								<th width="10%">Opciones</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	{!! Form::open(['route' => ['vocales.destroy', $comision, ':VOCAL_ID:'], 'id'=>'delete-vocal', 'method'=>'DELETE']) !!}
	{!! Form::close() !!}
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/comisiones/vocales/index.js') !!}
@stop
