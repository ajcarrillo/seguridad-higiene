<div class="form-group">
	{!! Form::label('nombre', 'Nombre:') !!}
	{!! Form::text('nombre', null, ['class'=>'form-control', 'required']) !!}
</div>
<div class="form-group">
	{!! Form::label('primer_apellido', 'Primer apellido:') !!}
	{!! Form::text('primer_apellido', null, ['class'=>'form-control', 'required']) !!}
</div>
<div class="form-group">
	{!! Form::label('segundo_apellido', 'Segundo apellido:') !!}
	{!! Form::text('segundo_apellido', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('cargo', 'Cargo:') !!}
	{!! Form::text('cargo', null, ['class'=>'form-control', 'required']) !!}
</div>
<div class="form-group">
	{!! Form::label('sector', 'Sector:') !!}
	{!! Form::select('sector', $sectores, null, ['class'=>'form-control', 'required','placeholder'=>'Seleccione...']) !!}
</div>
<div class="form-group">
	{!! Form::label('tipo', 'Tipo:') !!}
	{!! Form::select('tipo', $vocalTipos, null, ['class'=>'form-control', 'required','placeholder'=>'Seleccione...']) !!}
</div>
