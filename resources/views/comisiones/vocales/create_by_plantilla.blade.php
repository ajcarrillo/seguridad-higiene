@extends('plantillas.personal')

@section('personalHeader')
	<div class="row">
		<div class="col-md-12">
			<a href="{{ $returnURL }}" style="font-size: 18px">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Regresar
			</a>
		</div>
	</div>
	<input type="hidden" id="form-url" data-url="{{ route('vocales.create', ['comisiones'=>$comision, 'nombre'=>'pNOMBREp', 'primer_apellido'=>'pPRIMER_APELLIDOp', 'segundo_apellido'=>'pSEGUNDO_APELLIDOp']) }}">
@stop

@section('personalFooter')
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/comisiones/vocales/create_by_plantilla.js') !!}
@stop

