<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>@yield('pageTitle')</title>

	<!-- Fonts -->
	{{--<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>--}}

	<!-- Bootstrap -->
	{!! Html::style('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
	{!! Html::style('/assets/css/main.css') !!}
	@yield('css')
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">@yield('appTitle', 'Seguridad y Salud en el Trabajo')</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			 @include('layout.partials.menu', ['items' => config('seguridadhigiene.menu')])
			{{--{!! Html::menu(config('seguridadhigiene.menu')) !!}--}}
		</div>
	</div>
</nav>
	<div class="container hidden" id="container-error-list">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-warning alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<ul id="error-list">
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container" id="container-alert">
		<div class="row">
			<div class="col-md-12">

			</div>
		</div>
	</div>

	<div class="page-wrap">
		@yield('content')
	</div>


	<footer>
		<div class="footer">

		</div>
	</footer>

	<!-- Scripts -->
	{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
	{!! Html::script('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
	{!! Html::script('assets/bower_components/underscore/underscore-min.js') !!}
    {!! Html::script('assets/bower_components/bootbox.js/bootbox.js') !!}
	<div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog">
			<div class="modal-content">

			</div>
		</div>
	</div>
	<script type="text/html" id="error-list-template">
		<% _.each(json, function(error) { %>
			<li><%= error %></li>
		<% }); %>
	</script>

	<script type="text/html" id="bootstrap-alert">
		<div class="alert alert-dismissible <%= type  %>" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<%= body %>
		</div>
	</script>

	<script type="text/html" id="bootstrap-pagination">
		<nav>
			<ul class="pagination">
				<%= pages %>
			</ul>
		</nav>
	</script>
	@yield('scripts')
</body>
</html>
