<div class="container-fluid" id="panel-incidencia-subsistentes">
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table-incidencias-subsanadas">
				@include('incidencias.partials.table_header_incidencias')
				<tbody>
				@foreach($incidencias_subsanadas as $incidencia)
					@include('incidencias.partials.item_subsanadas', compact('incidencia'))
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
