@extends('base')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Incidencias</h2>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<ol class="breadcrumb">
					<li><a href="#!" data-loading-text="Cargando..." data-href="{{ route('get.incidencias.subsistentes', ['verificacion' => $verificacion->id]) }}" data-button="go-to-subsanar">Subsanar Incidencias</a></li>
					<li><a href="#!" data-loading-text="Cargando..." data-href="{{ route('get.incidencias.subsanadas', ['verificacion' => $verificacion->id]) }}" data-button="go-to-subsanadas">Ver Incidencias Subsanadas</a></li>
					<li><a href="{{ route('get.edit.verificacion', $verificacion->id) }}" >Ver verificación</a></li>
				</ol>
			</div>
		</div>
	</div>
	<div class="container" id="panel-incidencia-create" >
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Incidencias detectadas por primera vez</h3>
					</div>
					<div class="panel-body">
						<form action="" id="form-incidencias" class="">
							{!! Form::token() !!}
							<input type="hidden" id="hd-trimestre" name="trimestre" value="{{ trimestreActual() }}">
							<input type="hidden" id="hd-anio" name="anio" value="{{ getYear() }}">
							<input type="hidden" id="hd-status" name="status" value="{{ statusIncidencia("Primera vez") }}">
							<div class="col-md-5">
								<div class="form-group">
									<select name="incidencia_id" id="cbx-incidencia" class="form-control">
										<option value="">Inicidencias...</option>
										@foreach($catIncidencias as $incidencia)
											<option value="{{ $incidencia->id }}">{{ $incidencia->nombre }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<select name="centro_de_trabajo_id" id="cbx-centro-trabajo" class="form-control">
										<option value="">Centro de trabajo...</option>
										@foreach($verificacion->reporte->comision->centros as $centro)
											<option value="{{ $centro->escuela_lite->id }}" data-clavecct="{{ $centro->escuela_lite->cct }}">{{ $centro->escuela_lite->cct }} - {{ $centro->escuela_lite->nombre }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<button type="button" class="btn btn-success btn-block" id="btn-add-incidencia"
											data-url="{{ route('post.create.incidencia', $verificacion->id) }}">Agregar</button>
								</div>
							</div>
						</form>
						{!! Form::open(['route' => ['delete.delete.incidencia', ':INCIDENCIA_ID'], 'id'=>'form-delete-incidencia']) !!}
						{!! Form::close() !!}
					</div>
					<table class="table" id="table-incidencia">
						@include('incidencias.partials.table_header_incidencias')
						<tbody>
						@forelse($verificacion->incidencias as $incidencia)
							<tr>
								<td>{{ $incidencia->centro->escuela_lite->cct }}</td>
								<td>{{ $incidencia->incidencia->nombre  }}</td>
								<td class="text-center">{{ $incidencia->verificacion->reporte->trimestre }}</td>
								<td class="text-center">{{ $incidencia->verificacion->reporte->anio }}</td>
								<td><button type="button" class="btn btn-danger btn-block" data-button="delete-incidencia" data-id="{{ $incidencia->id }}">Quitar</button></td>
							</tr>
						@empty
						@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop


@section('scripts')
	@parent

	<script type="text/html" id="template-row-incidencia">
		<tr>
			<td><%= centro %></td>
			<td><%= incidencia %></td>
			<td class="text-center"><%= trimestre %></td>
			<td class="text-center"><%= anio %></td>
			<td><button type="button" class="btn btn-danger btn-block" data-button="delete-incidencia" data-id="<%= id %>">Quitar</button></td>
		</tr>
	</script>
	<script type="text/html" id="template-option-incidencias">
		<% _.each(json, function(centro){ %>
		<option data-clavecct="<%= centro.clavecct %>" value="<%= centro.id %>"> <%= centro.clavecct %> - <%= centro.NOMBRECT %> </option>
		<% }); %>
	</script>
	{!! Html::script('assets/js/routes/routes.js') !!}
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/incidencias/create.js') !!}
@endsection
