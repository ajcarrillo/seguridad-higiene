<div class="container-fluid" id="panel-incidencia-subsistentes">
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table-incidencias-subsistentes">
				@include('incidencias.partials.table_header_incidencias')
				<tbody>
				@foreach($incidencias_subsistentes as $incidencia)
					@include('incidencias.partials.item_subsistentes', compact('incidencia'))
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	{!! Form::open(['id'=>'form-subsanar-incidencia']) !!}
	{!! Form::close() !!}
</div>
@section('modalScripts')
	@parent
	{!! Html::script('assets/js/incidencias/subsistentes.js') !!}
@stop
