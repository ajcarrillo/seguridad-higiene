@extends('layout/baseModal')

@section('modalTitle', "Incidencias Subsistentes")

@section('modalBody')
	@include('incidencias.subsistentes')
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
@stop
