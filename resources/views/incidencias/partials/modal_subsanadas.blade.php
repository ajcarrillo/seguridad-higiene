@extends('layout/baseModal')

@section('modalTitle', "Incidencias Subsanadas")

@section('modalBody')
	@include('incidencias.subsanadas')
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
@stop
