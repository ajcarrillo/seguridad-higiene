<tr>
	<td>{{ $incidencia->cct }}</td>
	<td>{{ $incidencia->nombre  }}</td>
	<td class="text-center">{{ $incidencia->trimestre  }}</td>
	<td class="text-center">{{ $incidencia->anio  }}</td>
	<td><button type="button" class="btn btn-primary btn-block" data-button="subsanar-incidencia"
				data-action="{{ route('patch.subsanar.incidencia', $incidencia->id) }}"
				data-id="{{ $incidencia->id }}">Subsanar</button></td>
</tr>
