@extends('actualizaciones.sections')

@section('title')
	- Vocales
@stop

@section('actualizacionesContent')
	<div class="row">
		<div class="col-md-12">
			<a href="{{ route('actualizacion.vocales.create', ['reporte'=>$reporte->id, 'comision'=>$reporte->comision, 'returnURL'=>Request::url()]) }}" class="btn btn-default pull-right">Agregar vocal</a>
		</div>
		<br><br>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Vocales</h3>
				</div>
				<div class="panel-body">
					<div class="content">
						<table class="table" id="vocales-table"
							   data-url="{{ route('vocales.datatables', $reporte->comision->id) }}">
							<thead>
							<tr>
								<th width="0%">id</th>
								<th width="30%">Nombre completo</th>
								<th width="20%">Cargo</th>
								<th width="20%">Tipo</th>
								<th width="20%">Sector</th>
								<th width="10%">Opciones</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		{!! Form::open(['route' => ['actualizacion.vocales.store', $reporte->id], 'id'=>'create-transaccion-form']) !!}
		<input type="hidden" name="actions" id="actions">
		{!! Form::close() !!}
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<h3 class="panel-title">Modificaciones</h3>
				</div>
				<div class="panel-body">
					<div class="alert alert-info">
						<strong>Importante</strong> Los cambios que realice sobre los centros de trabajo que
						pertenecen a la comisión se verán reflejados cuando sean aprobados por Recursos Humanos
					</div>
					<div class="content">
						<div class="well well-sm">
							<ul class="reset">
								<li>
									<span class="glyphicon glyphicon-stop text-danger"></span>
									Se eliminará
								</li>
								<li>
									<span class="glyphicon glyphicon-stop text-success"></span>
									Se agregará
								</li>
							</ul>
						</div>
						<table class="table" id="transaccion-table">
							<thead>
							<tr>
								<th width="30%">Nombre completo</th>
								<th width="20%">Cargo</th>
								<th width="20%">Tipo</th>
								<th width="20%">Sector</th>
								<th width="10%">Opciones</th>
							</tr>
							</thead>
							<tbody>
							@forelse($transacciones as $transaccion)
								<tr class="{{ $transaccion->actions == "Delete" ? "danger":"success" }}">
									<td>{{ $transaccion->nombreCompleto }}</td>
									<td>{{ $transaccion->cargo }}</td>
									<td>{{ $transaccion->nombreSector }}</td>
									<td>{{ $transaccion->nombreTipo }}</td>
									<td>
										<button type="button" class="btn btn-default" data-loading-text="Eliminando..."  data-id="{{ $transaccion->id }}"
												data-button="delete-transaccion">
											<i class="glyphicon glyphicon-trash"></i>
											Quitar
										</button>
									</td>
								</tr>
							@empty
								@include('layout.partials.emptytable', ['colspan'=>5])
							@endforelse
							</tbody>
						</table>
						{!! Form::open(['route' => ['actualizacion.vocales.destroy', $reporte->id,':ID:'], 'id'=>'delete-transaccion-form', 'method'=>'DELETE']) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	@parent
	<script type="text/html" id="transaccion-table-template">
		<% _.each(json, function(transaccion) { %>
		<% if (transaccion.actions == "Delete"){ %>
			<tr class="danger">
		<% }else{ %>
			<tr class="success">
		<% } %>
				<td><%= transaccion.nombreCompleto %></td>
				<td><%= transaccion.cargo %></td>
				<td><%= transaccion.nombreSector %></td>
				<td><%= transaccion.nombreTipo %></td>
				<td>
					<button type="button" class="btn btn-default" data-loading-text="Eliminando..." data-id="<%= transaccion.id %>"
							data-button="delete-transaccion">
						<i class="glyphicon glyphicon-trash"></i>
						Quitar
					</button>
				</td>
			</tr>
		<% }); %>
	</script>
	{!! Html::script('assets/js/actualizaciones/vocalesTransacciones/index.js') !!}
@stop


