@extends('layout.baseModal')

@section('modalTitle')
	Agregar vocal
@stop

@section('modalBody')
	{!! Form::model($transaccion, ['route' => ['actualizacion.vocales.store', $reporte->id], 'id'=>'create-vocal-transaccion']) !!}

		@include('actualizaciones.vocalesTransacciones.partials.form')
	{!! Form::close() !!}
@stop

@section('modalFooter')
	<button type="submit" id="btn-create-transaccion" data-loading-text="Guardando..." form="create-vocal-transaccion" class="btn btn-success">Guardar</button>
@stop
@section('modalScripts')
	@parent
	{!! Html::script('assets/js/actualizaciones/VocalesTransacciones/create_transaccion.js') !!}
@stop



