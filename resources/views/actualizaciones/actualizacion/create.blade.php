@extends('base')

@section('content')
	<div class="container top">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Aviso:</strong> Si actualiza los datos del presidente y/o secretario deberá anotar el número de oficio de designación
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				<div class="panel panel-success">
					  <div class="panel-heading">
							<h3 class="panel-title">Actualizar Comisión</h3>
					  </div>
					  <div class="panel-body">
						  {!! Form::open(['route' => ['actualizacion.store', $reporte->id], 'id'=>'form-actualizacion']) !!}
						  @include('actualizaciones.actualizacion.partials.actualizacion_form')
						  <div class="form-group">
							  <button type="submit" class="btn btn-success pull-right">Siguiente</button>
						  </div>
						  {!! Form::close() !!}
					  </div>
				</div>


			</div>
		</div>
	</div>
@stop
