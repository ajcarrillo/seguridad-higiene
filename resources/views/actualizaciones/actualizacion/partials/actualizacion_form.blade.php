<div class="form-group">
	{!! Form::label('num_oficio_sindical', 'Núm. Oficio Sindical:') !!}
	{!! Form::text('num_oficio_sindical', null, ['class'=>'form-control', 'autofocus']) !!}
</div>
<div class="form-group">
	{!! Form::label('fecha_oficio_sindical', 'Fecha oficio sindical:') !!}
	{!! Form::date('fecha_oficio_sindical', isset($actualizacion->fecha_oficio_sindical) ? $actualizacion->fecha_oficio_sindical : \Carbon\Carbon::now(), ['class'=>'form-control text-right']) !!}
</div>
<div class="form-group">
	{!! Form::label('num_oficio_oficial', 'Núm. Oficio oficial:') !!}
	{!! Form::text('num_oficio_oficial', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('fecha_oficio_oficial', 'Fecha oficio oficial:') !!}
	{!! Form::date('fecha_oficio_oficial', isset($actualizacion->fecha_oficio_oficial) ? $actualizacion->fecha_oficio_oficial : \Carbon\Carbon::now(), ['class'=>'form-control text-right']) !!}
</div>

{{--{!! Form::text('num_oficio_sindical', null, ['class'=>'form-control top', 'autofocus', 'placeholder'=>'Núm. Oficion sindical']) !!}
{!! Form::text('fecha_oficio_sindical', null, ['class'=>'form-control no-radius', 'placeholder'=>'Fecha oficio sindical']) !!}
{!! Form::text('num_oficio_oficial', null, ['class'=>'form-control no-radius', 'placeholder'=>'Núm. Oficio oficial']) !!}
{!! Form::text('fecha_oficio_oficial', null, ['class'=>'form-control bottom', 'placeholder'=>'Fecha oficio oficial']) !!}--}}
