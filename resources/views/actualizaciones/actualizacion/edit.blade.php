@extends('actualizaciones.sections')

@section('title')
	- Generales
@stop

@section('actualizacionesContent')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				  <div class="panel-heading">
						<h3 class="panel-title">Generales</h3>
				  </div>
				  <div class="panel-body">
					  {!! Form::model($actualizacion, ['route' => ['actualizacion.update',$reporte->id, $reporte->actualizacion->id], 'method'=>'patch']) !!}
						@include('actualizaciones.actualizacion.partials.actualizacion_form')
					  <div class="form-group">
						  <div class="">
							  <button type="submit" class="btn btn-success">Guardar</button>
						  </div>
					  </div>
					  {!! Form::close() !!}
				  </div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	@parent
@stop


