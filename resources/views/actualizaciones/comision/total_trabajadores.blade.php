@extends('layout.baseModal')

@section('modalTitle', 'Total de trabajadores')

@section('modalBody')
	{!! Form::open(['route' => ['comision.trabajadores.edit', $reporte], 'id'=>'trabajadores-form', 'class'=>'form-horizontal']) !!}
	<div class="form-group">
		<label class="col-md-6 control-label">En plantilla:</label>

		<div class="col-md-6">
			<p class="form-control-static">{{ $total }}</p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-6 control-label">En comisión:</label>

		<div class="col-md-6">
			<p class="form-control-static">{{ $reporte->comision->total_trabajadores }}</p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-md-6 control-label">Modificación:</label>

		<div class="col-md-6">
			<p class="form-control-static">{{ $reporte->actualizacion->total_trabajadores }}</p>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<input type="number" min="0" class="form-control" placeholder="Total nuevo" name="total_trabajadores" required>
		</div>
	</div>

	<input type="hidden" name="comision_id">

	{!! Form::close() !!}
@stop

@section('modalFooter')
	<button type="submit" data-button="update-trabajadores" form="trabajadores-form" class="btn btn-success pull-right" data-loading-text="Guardando...">Guardar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/js/actualizaciones/comisiones/total_trabajadores.js') !!}
@stop
