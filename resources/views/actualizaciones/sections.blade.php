@extends('base')

@section('css')
{{--
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
--}}
	{!! Html::style('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
@stop

@section('content')
	<div class="container top">
		<div class="row">
			<h2>Actualización de Comisión @yield('title')</h2>
			<hr>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div id="menu-lateral-container" class="well">
					<ul class="reset menu-lateral">
						<li class="{{ Request::url() == route('actualizacion.edit', [$reporte->id, $reporte->actualizacion->id])  ? "active":"" }}">
							<a href="{{ route('actualizacion.edit', [$reporte->id, $reporte->actualizacion->id]) }}">Actualización</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('actualizacion.centros', $reporte->id)  ? "active":"" }}">
							<a href="{{ route('actualizacion.centros', $reporte->id) }}">Centros</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li>
							<a href="#!" data-url="{{ route('comision.trabajadores.edit', $reporte->id) }}" data-button="trabajadores">Núm. Trabajadores</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('actualizacion.presidente.index', $reporte->id)  ? "active":"" }}">
							<a href="{{ route('actualizacion.presidente.index', $reporte->id) }}">Presidente</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('actualizacion.secretario.index', $reporte->id)  ? "active":"" }}">
							<a href="{{ route('actualizacion.secretario.index', $reporte->id) }}">Secretario</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						<li class="{{ Request::url() == route('actualizacion.vocales.index', $reporte->id) ? "active":"" }}">
							<a href="{{ route('actualizacion.vocales.index', $reporte->id) }}">Vocal</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>
						{{--<li class="{{ Request::url() == route('actualizacion.show', [$reporte->id, $reporte->actualizacion->id])  ? "active":"" }}">
							<a href="{{ route('actualizacion.show', [$reporte->id, $reporte->actualizacion->id]) }}">Enviar</a>
							<i class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></i>
						</li>--}}
					</ul>
				</div>
			</div>
			<div class="col-md-10 pull-right">
				@if (Session::has('alert'))
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{ Session::get('alert') }}
					</div>
				@endif
				@yield('actualizacionesContent')
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{{--<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>--}}
	{!! Html::script('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') !!}
	{!! Html::script('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/actualizaciones/sections.js') !!}
@stop
