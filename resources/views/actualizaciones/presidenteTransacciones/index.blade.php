@extends('actualizaciones.sections')

@section('title')
	- Presidente
@stop

@section('css')
	@parent
	{!! Html::style('assets/bower_components/animate.css/animate.min.css') !!}
@stop

@section('actualizacionesContent')
	<div id="presidente-container" class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				  <div class="panel-heading">
						<h3 class="panel-title">Presidente Actual</h3>
				  </div>
				  <div class="panel-body">
					  <div class="form-horizontal">
						  <div class="form-group">
							  <label class="col-md-2 control-label">Nombre:</label>

							  <div class="col-md-10">
								  <p class="form-control-static">{{ $presidente->nombre_completo }}</p>
							  </div>
						  </div>
						  <div class="form-group">
							  <label class="col-md-2 control-label">Cargo:</label>

							  <div class="col-md-10">
								  <p class="form-control-static">{{ $presidente->cargo }}</p>
							  </div>
						  </div>
						  @if( ! isset($newPresidente))
							  <a href="#!" data-button="update-presidente-actual" class="pull-right disabled">Cambiar</a>
						  @endif
					  </div>
				  </div>
			</div>
		</div>

		@if(isset($newPresidente))
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Presidente Propuesto</h3>
					</div>
					<div class="panel-body">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-md-2 control-label">Nombre:</label>
								<div class="col-md-10">
									<p class="form-control-static">{{ $newPresidente->nombre_completo }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Cargo:</label>
								<div class="col-md-10">
									<p class="form-control-static">{{ $newPresidente->cargo }}</p>
								</div>
							</div>
							<a href="#!" data-button="update-presidente-propuesto" class="pull-right">Modificar</a>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>

	<div id="update-presidente-container" class="row">

		@if( ! isset($newPresidente))
			<div id="create-transaction" class="col-md-6 hidden">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Actualizar</h3>
					</div>
					<div class="panel-body">
						@include('actualizaciones.presidenteTransacciones.create')
					</div>
				</div>
			</div>
		@endif


		@if(isset($newPresidente))
			<div id="update-transaction" class="col-md-6 pull-right hidden">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Modifcar presidente propuesto</h3>
					</div>
					<div class="panel-body">
						@include('actualizaciones.presidenteTransacciones.edit')
					</div>
				</div>
			</div>
		@endif


	</div>
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/actualizaciones/presidenteTransacciones/index.js') !!}
@stop


