{!! Form::open(['route' => ['actualizacion.presidente.store', $reporte->id], 'id'=>'form-presidente-transaccion']) !!}
@include('actualizaciones.presidenteTransacciones.partials.form')
<div class="form-group">
	<div class="">
		<button type="submit" class="btn btn-success">Guardar</button>
	</div>
</div>
{!! Form::close() !!}
