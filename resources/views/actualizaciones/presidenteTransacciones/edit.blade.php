{!! Form::model($newPresidente, ['route' => ['actualizacion.presidente.update', $reporte->id, $newPresidente->id], 'method'=>'patch', 'id'=>'form-presidente-propuesto-update']) !!}
@include('actualizaciones.presidenteTransacciones.partials.form')
<div class="form-group">
	<div class="">
		<button type="submit" class="btn btn-success">Actualizar</button>
	</div>
</div>
{!! Form::close() !!}
