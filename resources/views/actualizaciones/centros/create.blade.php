@extends('escuela_lite.index')

@section('centrosHeader')
	<div class="row">
		<div class="col-md-12">
			<a href="{{ $returnURL }}" style="font-size: 18px">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Regresar
			</a>
			<button class="btn btn-success pull-right" data-button="add-centros">Agregar Centros</button>
		</div>
	</div>
@stop

@section('centrosFooter')
	<div class="container">
		<hr>
		<div class="row">
			<div class="col-md-12">
				<a href="{{ $returnURL }}" style="font-size: 18px">
					<span class="glyphicon glyphicon-chevron-left"></span>
					Regresar
				</a>
				<button class="btn btn-success pull-right" data-button="add-centros">Agregar Centros</button>
				{!! Form::open(['route' => ['actualizacion.centros.store', $reporte->id], 'id'=>'add-centros-form']) !!}
				<input type="hidden" id="centros" name="centros">
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<br>
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/actualizaciones/centros/create.js') !!}
@stop
