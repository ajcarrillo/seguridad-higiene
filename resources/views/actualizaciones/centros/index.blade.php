@extends('actualizaciones.sections')

@section('title')
	- Centros
@stop

@section('actualizacionesContent')
	<div class="row">
		<div class="col-md-12">
			<div class="btn-group pull-right">
				<a href="{{ route('actualizacion.centros.create', ["reportes"=>$reporte->id, 'returnURL'=>Request::url()]) }}"
				   class="btn btn-default">Agregar Centros</a>
			</div>
			<br><br>
		</div>
	</div>
	<div id="centros-container" class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Centros</h3>
				</div>
				<div class="panel-body">
					<div id="content">
						<table class="table" id="centros-table"
							   data-url="{!! route('comision.centros.data', $reporte->comision->id) !!}">
							<thead>
							<tr>
								<th>id</th>
								<th>cctid</th>
								<th>CCT</th>
								<th>Nombre</th>
								<th>Municipio</th>
								<th>Localidad</th>
								<th>Nivel</th>
								<th>Turno</th>
								<th>Opciones</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>

			</div>
		</div>
		{!! Form::open(['route' => ['actualizacion.centros.store', $reporte->id], 'id'=>'centro-transaccion-form']) !!}
		<input type="hidden" name="row_id" id="row_id">
		<input type="hidden" name="centro_de_trabajo_id" id="centro_de_trabajo_id">
		<input type="hidden" name="comision_id" id="comision_id" value="{{ $reporte->comision->id }}">
		<input type="hidden" name="actions" id="actions">
		{!! Form::close() !!}
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<h3 class="panel-title">Cambios a aplicar</h3>
				</div>
				<div class="panel-body">
					<div class="alert alert-info">
						<strong>Importante</strong> Los cambios que realice sobre los centros de trabajo que
						pertenecen a la comisión se verán reflejados cuando sean aprobados por Recursos Humanos
					</div>
					<div class="content">
						<div class="well well-sm">
							<ul class="reset">
								<li>
									<span class="glyphicon glyphicon-stop text-danger"></span>
									Se eliminará
								</li>
								<li>
									<span class="glyphicon glyphicon-stop text-success"></span>
									Se agregará
								</li>
							</ul>
						</div>
						<table class="table" id="transaccion-table">
							<thead>
							<tr>
								<th>CCT</th>
								<th>Nombre</th>
								<th>Municipio</th>
								<th>Localidad</th>
								<th>Nivel</th>
								<th>Turno</th>
								<th>Opciones</th>
							</tr>
							</thead>
							<tbody>
							@forelse($transacciones as $transaccion)
								<tr class="{{ $transaccion->actions == "Delete" ? "danger":"success" }}">
									<td>{{ $transaccion->escuela_lite->cct }}</td>
									<td>{{ $transaccion->escuela_lite->nombre }}</td>
									<td>{{ $transaccion->escuela_lite->abrev_municipio }}</td>
									<td>{{ $transaccion->escuela_lite->d_localidad }}</td>
									<td>{{ $transaccion->escuela_lite->nivel }}</td>
									<td>{{ $transaccion->escuela_lite->turno->d_turno }}</td>
									<td>
										<button type="button" class="btn btn-default" data-id="{{ $transaccion->id }}"
												data-button="delete-transaccion">
											<i class="glyphicon glyphicon-trash"></i>
											Quitar
										</button>
									</td>
								</tr>
							@empty
								@include('layout.partials.emptytable', ['colspan'=>7])
							@endforelse
							</tbody>
						</table>
						{!! Form::open(['route' => ['actualizacion.centros.destroy',"reportes"=>$reporte->id, "id"=>":ID:"], 'id'=>'delete-transaccion-form', "method"=>"DELETE"]) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	@parent
	<script type="text/html" id="transaccion-table-template">
		<% _.each(json, function(transaccion) { %>
		<% if (transaccion.actions == "Delete"){ %>
			<tr class="danger">
		<% }else{ %>
			<tr class="success">
		<% } %>
			<td><%= transaccion.escuela_lite.cct %></td>
			<td><%= transaccion.escuela_lite.nombre %></td>
			<td><%= transaccion.escuela_lite.abrev_municipio %></td>
			<td><%= transaccion.escuela_lite.d_localidad %></td>
			<td><%= transaccion.escuela_lite.nivel %></td>
			<td><%= transaccion.escuela_lite.turno.d_turno %></td>
			<td>
				<button type="button" class="btn btn-default" data-id="<%= transaccion.id %>"
						data-button="delete-transaccion">
					<i class="glyphicon glyphicon-trash"></i>
					Quitar
				</button>
			</td>
		</tr>
		<% }); %>
	</script>
	{!! Html::script('assets/bower_components/bootbox.js/bootbox.js') !!}
	{!! Html::script('assets/js/spanish.bootbox.js') !!}
	{!! Html::script('assets/js/actualizaciones/centros/index.js') !!}
@stop


