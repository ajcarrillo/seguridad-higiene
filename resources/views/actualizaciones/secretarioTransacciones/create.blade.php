{!! Form::open(['route' => ['actualizacion.secretario.store', $reporte->id], 'id'=>'form-secretario-transaccion']) !!}
@include('actualizaciones.secretarioTransacciones.partials.form')
<div class="form-group">
	<div class="">
		<button type="submit" class="btn btn-success">Guardar</button>
	</div>
</div>
{!! Form::close() !!}
