{!! Form::model($newSecretario, ['route' => ['actualizacion.secretario.update', $reporte->id, $newSecretario->id], 'method'=>'patch', 'id'=>'form-secretario-propuesto-update']) !!}
@include('actualizaciones.secretarioTransacciones.partials.form')
<div class="form-group">
	<div class="">
		<button type="submit" class="btn btn-success">Actualizar</button>
	</div>
</div>
{!! Form::close() !!}
