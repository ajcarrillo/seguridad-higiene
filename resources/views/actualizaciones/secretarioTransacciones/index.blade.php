@extends('actualizaciones.sections')

@section('title')
	- Secretario
@stop

@section('css')
	@parent
	{!! Html::style('assets/bower_components/animate.css/animate.min.css') !!}
@stop

@section('actualizacionesContent')
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title">Secretario Actual</h3>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-md-2 control-label">Nombre:</label>

							<div class="col-md-10">
								<p class="form-control-static">{{ $secretario->nombre_completo }}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">RFC:</label>

							<div class="col-md-10">
								<p class="form-control-static">{{ $secretario->rfc }}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Sector:</label>

							<div class="col-md-10">
								<p class="form-control-static">{{ catSectoresById($secretario->sector) }}</p>
							</div>
						</div>
						@if( ! isset($newSecretario))
							<a href="#!" data-button="update-secretatio-actual" class="pull-right">Cambiar</a>
						@endif
					</div>
				</div>
			</div>
		</div>

		@if(isset($newSecretario))
			<div class="col-md-6">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Secretario Propuesto</h3>
					</div>
					<div class="panel-body">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-md-2 control-label">Nombre:</label>

								<div class="col-md-10">
									<p class="form-control-static">{{ $newSecretario->nombre_completo }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">RFC:</label>

								<div class="col-md-10">
									<p class="form-control-static">{{ $newSecretario->rfc }}</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Sector:</label>

								<div class="col-md-10">
									<p class="form-control-static">{{ catSectoresById($newSecretario->sector) }}</p>
								</div>
							</div>
							<a href="#!" data-button="update-secretatio-propuesto" class="pull-right">Modificar</a>
						</div>
					</div>
				</div>
			</div>
		@endif
	</div>
	<div id="update-secretario-container" class="row">
		@if( ! isset($newSecretario))
			<div id="create-transaction" class="col-md-6 hidden">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Actualizar secretario</h3>
					</div>
					<div class="panel-body">
						@include('actualizaciones.secretarioTransacciones.create')
					</div>
				</div>
			</div>
		@else
			<div id="update-transaction" class="col-md-6 hidden pull-right">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Modificar secretario propuesto </h3>
					</div>
					<div class="panel-body">
						@include('actualizaciones.secretarioTransacciones.edit')
					</div>
				</div>
			</div>
		@endif


	</div>
@stop

@section('scripts')
	@parent
	{!! Html::script('assets/js/actualizaciones/secretarioTransacciones/index.js') !!}
@stop


