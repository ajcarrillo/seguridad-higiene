@extends('layout/baseModal')

@section('modalTitle', "Editar Actividad")

@section('modalBody')
	@include('calendarios.partials.edit_actividad')
@stop

@section('modalFooter')
	<button type="submit" form="form-update-actividad" data-button="actividad-update" class="btn btn-success"
			data-loading-text="Guardando...">Guardar</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/js/calendarios/edit_actividad.js') !!}
@stop
