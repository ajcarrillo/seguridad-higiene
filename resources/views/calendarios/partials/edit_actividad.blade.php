{!! Form::open(['route' => ['patch.actividad', $actividad->id], 'id' => 'form-update-actividad', 'method'=>'PATCH']) !!}
	<div class="form-group">
		<label for="txt-descripcion" class="control-label">Descripción:</label>
		<textarea class="form-control" rows="4" id="txt-descripcion" name="descripcion" required>{{ $actividad->descripcion }}</textarea>
	</div>
	<div class="form-group">
		<label for="txt-unidad_de_medida" class="control-label">Unidad de medida:</label>
		<input type="text" class="form-control" id="txt-unidad_de_medida"
			   name="unidad_de_medida" value="{{ $actividad->unidad_de_medida }}" required>
	</div>
	<div class="form-group">
		<label for="txt-trimestre" class="control-label">Trimestre: </label>

		@foreach($trimestres as $trimestre)
			<div class="checkbox">
				<label>
					<input type="radio" name="trimestre" value="{{ $trimestre }}" {{ $actividad->trimestre == $trimestre ? "checked" : "" }}> {{ catTrimestreById($trimestre) }}
				</label>
			</div>
		@endforeach

	</div>
{!! Form::close() !!}



