<div class="well form-section">Datos generales</div>

<div class="form-group">
	{!! Form::label('descripcion', 'Descripción:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::textarea('descripcion', null, ['class'=>'form-control', 'placeholder'=>'Descripción','required']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('unidad_de_medida', 'Unidad de medida:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('unidad_de_medida', null, ['class'=>'form-control', 'placeholder'=>'Unidad de medida','required']) !!}
	</div>
</div>
<div class="form-group">
	<label for="txt-trimestre" class="col-md-2 control-label">Trimestre: </label>
	<div class="col-md-10">
		<label class="radio-inline">
			{!! Form::radio('trimestre', '1', true) !!} ENE-MAR
		</label>
		<label class="radio-inline">
			{!! Form::radio('trimestre', '2', false) !!} ABRIL-JUN
		</label>
		<label class="radio-inline">
			{!! Form::radio('trimestre', '3', false) !!} JUL-SEP
		</label>
		<label class="radio-inline">
			{!! Form::radio('trimestre', '4', false) !!} OCT-DIC
		</label>
	</div>
</div>

