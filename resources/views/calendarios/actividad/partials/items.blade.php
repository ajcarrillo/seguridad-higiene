@forelse($actividades as $actividad)
	<tr>
		<td>{{ $actividad->descripcion }}</td>
		<td>{{ $actividad->unidad_de_medida }}</td>
		<td>{{ $actividad->trimestre }}</td>
		<td>
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
					Opciones
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li role="presentation"><a role="menuitem" tabindex="-1"
											   href="{{ route('actividad.edit', $actividad->id) }}">Modificar</a>
					</li>
					<li role="presentation"><a role="menuitem" tabindex="-1"
											   href="{{ route('actividad.delete', $actividad->id) }}"
											   data-button="delete-activity" data-token="{{ csrf_token() }}"
											   data-method="delete">Eliminar</a>
					</li>
				</ul>
			</div>
		</td>
	</tr>
@empty
	@include('layout.partials.emptytable', ['colspan'=>4])
@endforelse
