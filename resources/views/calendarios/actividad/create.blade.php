@extends('base')

@section('pageTitle')
	Crear actividad
@stop

@section('content')
	<div class="container">
		@include('layout.partials.flash_messages')
		<div class="row">
			<div class="col-md-12">
				<h2>Crear actividad </h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! Form::open(['route' => ['actividad.store', $calendario->id], 'id'=>'', 'class'=>'form-horizontal']) !!}
				@include('calendarios.actividad.partials.form')
				<div class="form-group">
					<div class="gray-content-block">
						<button type="submit" class="btn btn-success" name="_save">Guardar</button>
						<button type="submit" class="btn btn-success" name="_addanother">Guardar y agregar otro</button>
						@include('calendarios.actividad.partials.cancel_button')
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop
