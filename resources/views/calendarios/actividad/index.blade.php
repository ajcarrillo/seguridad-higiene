@extends('base')

@section('pageTitle', 'Actividades')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12"><h2>Actividades</h2></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="gray-content-block">
					<a href="{{ route('actividad.create', $calendario->id) }}" class="btn btn-success">Nueva
						actividad</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table" id="table-actividades" data-colspan="4">
					<thead>
					<tr>
						<th>Descripción</th>
						<th>Unidad de Medida</th>
						<th class="text-center">Trimestre</th>
						<th class="text-center" width="10%">Opciones</th>
					</tr>
					</thead>
					<tbody>
						@include('calendarios.actividad.partials.items')
					</tbody>
				</table>
				{!! $actividades !!}
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/js/calendarios/actividad/index.js') !!}
@stop
