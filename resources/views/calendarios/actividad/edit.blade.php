@extends('base')

@section('pageTitle')
	Crear actividad
@stop

@section('content')
	<div class="container">
		@include('layout.partials.flash_messages')
		<div class="row">
			<div class="col-md-12">
				<h2>Editar actividad </h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! Form::model($actividad, ['route' => ['actividad.update', $actividad->id], 'id'=>'', 'class'=>'form-horizontal', 'method'=>'PATCH']) !!}
				@include('calendarios.actividad.partials.form')
				<div class="form-group">
					<div class="gray-content-block">
						<button type="submit" class="btn btn-success" name="_save">Guardar</button>
						@include('calendarios.actividad.partials.cancel_button')
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop
