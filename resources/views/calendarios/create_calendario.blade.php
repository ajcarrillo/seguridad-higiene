@extends('base')


@section('css')
	{!! Html::style('/assets/bower_components/datetimepicker/jquery.datetimepicker.css') !!}
@stop

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<br>
				<div class="panel panel-success">
					  <div class="panel-heading">
							<h3 class="panel-title">Establecer periodo</h3>
					  </div>
					  <div class="panel-body">

						  @if (Session::has('alert'))
							  <div class="alert alert-danger">
								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								  {{ Session::get('alert') }}
							  </div>
						  @endif

						  {!! Form::open(['route' => ['post.create.calendario', $reporte->id], 'class' => 'form-horizontal',
                                          'id' => 'form-calendario', 'role' => 'form']) !!}
						  <div class="form-group">
							  <label class="col-md-2 control-label">De: </label>
							  <div class="col-md-10">
								  {!! Form::date('de', \Carbon\Carbon::now(),['class'=>'form-control text-right']) !!}
							  </div>
						  </div>
						  <div class="form-group">
							  <label class="col-md-2 control-label">A: </label>
							  <div class="col-md-10">
								  {!! Form::date('a', \Carbon\Carbon::now(),['class'=>'form-control text-right']) !!}
							  </div>
						  </div>
						  <div class="form-group">
							  <div class="col-md-2 col-md-offset-2">
								  <button type="submit" class="btn btn-success" id="btn-siguiente" data-loading-text="Guardando...">Siguiente</button>
							  </div>
						  </div>
						  {!! Form::close() !!}
					  </div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	{!! Html::script('assets/bower_components/datetimepicker/jquery.datetimepicker.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/calendarios/create_calendario.js') !!}
@stop
