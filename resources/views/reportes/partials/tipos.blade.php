<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
				@foreach($tipos as $tipo)
					<li><a href="#">{{ $tipo->tipo->nombre }}</a></li>
				@endforeach
			</ol>
		</div>
	</div>
</div>
