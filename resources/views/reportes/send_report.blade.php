@extends('layout.baseModal')

@section('modalTitle', "Enviar Acta")

@section('modalBody')
	{!! Form::open(['route' => ['reporte.sendUpdate', $reporteId], 'id'=>'send-report-form']) !!}
	<div class="alert alert-info" role="alert">
		<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
		El reporte se enviará para su evaluación.
	</div>
	{!! Form::close() !!}
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
	<button type="submit"
			class="btn btn-success"
			form="send-report-form"
			data-loading-text="Enviando...">Si</button>
@stop

@section('modalScripts')
@stop()
