@extends('layout.baseModal')

@section('modalTitle', "Tipo de reporte")

@section('modalBody')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				{!! Form::open(['route' => ['post.reporte.create'], 'id' => 'form-reporte','class' => 'form-horizontal']) !!}
				<div class="form-group">
					<label for="" class="col-md-3 control-label">Tipo:</label>
					<div class="col-md-9">
						@foreach($tipos as $tipo)
								<div class="checkbox">
									<label>
										<input type="checkbox" {{ $tipo->id == 3 ? 'checked':'' }} name="tipos[]" data-button="chk-tipo" value="{{ $tipo->id }}"> {{ $tipo->nombre }}
									</label>
								</div>
						@endforeach
						<div class="checkbox hidden">
							<label>
								<input type="checkbox" name="tipos[]" data-button="chk-tipo" value="4" checked>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="alert alert-info" role="alert">
						<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
						El acta de verificación se genera de manera autómatica, usted puede elegir si desea realizar,
						junto con la verificación, una actualización y si el trimestre lo permite una calendarización.
					</div>
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
@stop

@section('modalFooter')
	<button class="btn btn-success" type="submit" data-loading-text="Guardando..." form="form-reporte" id="btn-save-reporte">Siguiente</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
	{!! Html::script('assets/js/reportes/create.js') !!}
@endsection
