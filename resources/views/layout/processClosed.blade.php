@extends('layout.baseModal')

@section('modalTitle', "Proceso Cerrado")

@section('modalBody')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h3>Por el momento el proceso de captura de acta de verificación se encuentra cerrado.</h3>
			</div>
		</div>
	</div>
@stop

@section('modalFooter')
	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
@stop

@section('modalScripts')
@endsection
