<div class="row">
	<div class="col-md-12">
		<br>
		@if (Session::has('alert'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				{{ Session::get('alert') }}
			</div>
		@endif
		@if (Session::has('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				{{ Session::get('success') }}
			</div>
		@endif
	</div>
</div>
