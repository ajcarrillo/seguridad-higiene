<ul class="nav navbar-nav navbar-right">
	@if (Auth::user())

		{!! Menu::make($items[Auth::user()->role], 'nav navbar-nav') !!}
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
			   aria-expanded="false">{{ Auth::user()->data->name }} <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="{{ url('/auth/logout') }}">Cerrar sesión</a></li>
			</ul>
		</li>
	@endif
</ul>
