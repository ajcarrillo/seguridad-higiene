@extends('base')

@section('appTitle', 'Comisión '.$verificacion->reporte->comision->clave_comision)

@section('css')
	{!! Html::style('/assets/bower_components/datetimepicker/jquery.datetimepicker.css') !!}
@endsection

@section('content')
	{{--@include('reportes.partials.tipos', [ 'tipos' => $verificacion->reporte->tipos])--}}

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@include('layout/partials/errors')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h2>Verificación</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{!! Form::model($verificacion,['route' => ['post.edit.verificacion', $verificacion->id],'id' => 'form-verificacion', 'class' => 'form-horizontal', 'method'=>'PATCH']) !!}
				@include('verificaciones.partials.form')

				<div class="form-group">
					<label class="col-sm-2 control-label">Incidencias:</label>
					<div class="col-sm-10">
						<p class="form-control-static">
							<a href="{{ route('verificacion.incidencia.create', $verificacion->id) }}">Agregar incidencias</a>
						</p>
					</div>
				</div>

				<div class="form-group gray-content-block">
					<div class="">
						<button type="submit" id="btn-submit-verificacion" data-loading-text="Guardando..."
								class="btn btn-success">Guardar
						</button>
						<a href="{{ route('actas') }}" class="btn btn-default pull-right">Cancel</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

@stop

@section('scripts')
	@parent
	{!! Html::script('assets/bower_components/datetimepicker/jquery.datetimepicker.js') !!}
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/verificaciones/edit.js') !!}
@endsection
