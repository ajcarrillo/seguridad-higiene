<div class="well form-section">Datos generales</div>

<div class="form-group">
	{!! Form::label('fecha', 'Fecha:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-2">
		{!! Form::date('fecha', isset($verificacion->fecha) ? $verificacion->fecha : \Carbon\Carbon::now(), ['class'=>'form-control text-right', 'placeholder'=>'','required']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('tipo', 'Tipo:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		<label class="radio-inline">
			{!! Form::radio('tipo', 1, true) !!} Ordinaria
		</label>
	</div>
</div>

<div class="form-group">
	{!! Form::label('numero_de_riesgos_de_trabajo', 'Riesgos de trabajo:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-2">
		{!! Form::number('numero_de_riesgos_de_trabajo', null, ['class'=>'form-control', 'placeholder'=>'Riesgos de trabajo','required', 'min'=>'0']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('accidentes_de_trabajo', 'Accidentes de trabajo:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-2">
		{!! Form::number('accidentes_de_trabajo', null, ['class'=>'form-control', 'placeholder'=>'Accidente de trabajo','required', 'min'=>'0']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('enfermedad_profesional', 'Enfermedad prof.:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-2">
		{!! Form::number('enfermedad_profesional', null, ['class'=>'form-control', 'placeholder'=>'Enfermedad profesional','required', 'min'=>'0']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('observacion', 'Obervaciones:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::textarea('observacion', null, ['class'=>'form-control', 'placeholder'=>'Obervaciones']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('propuesta_csst', 'Propuesta csst:', ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::textarea('propuesta_csst', null, ['class'=>'form-control', 'placeholder'=>'Propuesta csst']) !!}
	</div>
</div>
