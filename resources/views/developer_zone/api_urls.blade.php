@extends('base')

@section('content')
	<br><br>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-info">
					  <div class="panel-heading">
							<h3 class="panel-title">API Urls</h3>
					  </div>
					  <div class="panel-body">
						  Crear usuario:
						  <div class="well">
							  <strong>/api/user</strong>
						  </div>

						  Cambiar email:
						  <div class="well">
							  <strong>/api/user/:username:/change-email</strong>
						  </div>

						  Cambiar contraseña:
						  <div class="well">
							  <strong>/api/user/:username:/change-password</strong>
						  </div>

					  </div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	{!! Html::script('assets/js/app/config.js') !!}
	{!! Html::script('assets/js/SEQ.Utilities.js') !!}
	{!! Html::script('assets/js/Modal.js') !!}
	{!! Html::script('assets/bower_components/bootbox.js/bootbox.js') !!}
	{!! Html::script('assets/js/actas/list.js') !!}
@endsection
